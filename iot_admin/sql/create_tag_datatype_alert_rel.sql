
CREATE TABLE `tag_datatype_alert_rel` (
  `dataType` varchar(45) NOT NULL,
  `mac` varchar(45) NOT NULL,
  `alertSettingId` int(11) NOT NULL,
  `updateDate` varchar(45) DEFAULT NULL,
  `updator` varchar(45) DEFAULT NULL,
  `companyId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`dataType`,`mac`,`alertSettingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tag dataType和alert的關聯\n';

INSERT INTO `tag_datatype_alert_rel` VALUES ('0001','0640110208A6',1,NULL,NULL,'itri');
