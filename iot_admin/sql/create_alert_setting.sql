CREATE TABLE `alert_setting` (
  `alertSettingId` int(11) NOT NULL AUTO_INCREMENT,
  `disName` varchar(45) NOT NULL,
  `low` float NOT NULL,
  `high` float NOT NULL,
  `phones` text,
  `emails` text,
  `afterMilliSecondAlert` double NOT NULL DEFAULT '1000',
  `updateDate` varchar(45) DEFAULT NULL,
  `updator` varchar(45) DEFAULT NULL,
  `companyId` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`alertSettingId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `alert_setting` VALUES (1,'一般警示',-10,20,'0926853323','itriA20062@itri.org.tw',60000,NULL,NULL,'itri');
