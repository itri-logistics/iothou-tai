-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 服務器版本:                        5.7.10-log - MySQL Community Server (GPL)
-- 服務器操作系統:                      Win64
-- HeidiSQL 版本:                  9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 導出 itri_iot 的資料庫結構
CREATE DATABASE IF NOT EXISTS `itri_iot` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `itri_iot`;


-- 導出  表 itri_iot.alert_setting 結構
CREATE TABLE IF NOT EXISTS `alert_setting` (
  `alertSettingId` int(11) NOT NULL AUTO_INCREMENT,
  `disName` varchar(45) NOT NULL,
  `low` float NOT NULL,
  `high` float NOT NULL,
  `phones` text,
  `emails` text,
  `criticalEmails` text,
  `afterMilliSecondAlert` double NOT NULL DEFAULT '1000',
  `updateDate` varchar(45) DEFAULT NULL,
  `updator` varchar(45) DEFAULT NULL,
  `companyId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`alertSettingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 資料導出被取消選擇。


-- 導出  表 itri_iot.dashboard 結構
CREATE TABLE IF NOT EXISTS `dashboard` (
  `dashboardId` int(11) NOT NULL AUTO_INCREMENT,
  `disName` varchar(45) DEFAULT NULL COMMENT '看板名稱\n',
  `companyId` varchar(45) DEFAULT NULL,
  `macId` varchar(45) DEFAULT NULL,
  `dataType` varchar(4) DEFAULT NULL COMMENT '資料類型',
  `position` int(11) DEFAULT '-1' COMMENT '看板順序(-1代表不顯示)',
  PRIMARY KEY (`dashboardId`),
  KEY `macId_idx` (`macId`),
  KEY `companyId_idx` (`companyId`),
  CONSTRAINT `macId` FOREIGN KEY (`macId`) REFERENCES `tag` (`mac`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='看板設定\n';

-- 資料導出被取消選擇。


-- 導出  表 itri_iot.place 結構
CREATE TABLE IF NOT EXISTS `place` (
  `placeId` int(11) NOT NULL AUTO_INCREMENT,
  `disName` varchar(45) DEFAULT NULL,
  `companyId` varchar(45) DEFAULT NULL,
  `updateDate` varchar(45) DEFAULT NULL,
  `updator` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`placeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 資料導出被取消選擇。


-- 導出  表 itri_iot.sensor_position 結構
CREATE TABLE IF NOT EXISTS `sensor_position` (
  `id` varchar(45) NOT NULL COMMENT 'companyId-placeId-mac-port',
  `placeId` varchar(45) NOT NULL,
  `companyId` varchar(45) NOT NULL,
  `mac` varchar(45) NOT NULL COMMENT '所屬tag的mac',
  `port` varchar(45) NOT NULL DEFAULT '"00"' COMMENT '"00":本體, "01":port1...',
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `updateDate` varchar(50) DEFAULT NULL,
  `updator` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 資料導出被取消選擇。


-- 導出  表 itri_iot.tag 結構
CREATE TABLE IF NOT EXISTS `tag` (
  `mac` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `port1` varchar(2) NOT NULL DEFAULT '00' COMMENT 'port1的sensor，00：沒有sensor；01：本體溫度；02：本體濕度；03：低溫；04：高溫',
  `port2` varchar(2) NOT NULL DEFAULT '00' COMMENT 'port2的sensor，00：沒有sensor；01：本體溫度；02：本體濕度；03：低溫；04：高溫',
  `port3` varchar(2) NOT NULL DEFAULT '00' COMMENT 'port3的sensor，00：沒有sensor；01：本體溫度；02：本體濕度；03：低溫；04：高溫',
  `port4` varchar(2) NOT NULL DEFAULT '00' COMMENT 'port4的sensor，00：沒有sensor；01：本體溫度；02：本體濕度；03：低溫；04：高溫',
  `rate` int(5) NOT NULL DEFAULT '15' COMMENT '批次記錄取樣率，單位為秒，最低15，最高86400',
  `placeId` int(11) DEFAULT NULL COMMENT '所屬場域',
  `companyId` varchar(45) DEFAULT NULL COMMENT '所屬公司',
  `updateDate` varchar(45) DEFAULT NULL,
  `updator` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`mac`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 資料導出被取消選擇。


-- 導出  表 itri_iot.tag_code 結構
CREATE TABLE IF NOT EXISTS `tag_code` (
  `code` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `mac` varchar(17) COLLATE utf8_unicode_ci NOT NULL,
  `descrpition` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `tagMac` (`mac`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 資料導出被取消選擇。


-- 導出  表 itri_iot.tag_datatype_alert_rel 結構
CREATE TABLE IF NOT EXISTS `tag_datatype_alert_rel` (
  `dataType` varchar(45) NOT NULL,
  `mac` varchar(45) NOT NULL,
  `alertSettingId` int(11) NOT NULL,
  `updateDate` varchar(45) DEFAULT NULL,
  `updator` varchar(45) DEFAULT NULL,
  `companyId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`dataType`,`mac`,`alertSettingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tag dataType和alert的關聯\n';

-- 資料導出被取消選擇。


-- 導出  表 itri_iot.upload_log 結構
CREATE TABLE IF NOT EXISTS `upload_log` (
  `fileKey` varchar(90) NOT NULL,
  `updateDate` varchar(45) DEFAULT NULL,
  `status` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 資料導出被取消選擇。


-- 導出  表 itri_iot.user 結構
CREATE TABLE IF NOT EXISTS `user` (
  `companyId` varchar(45) NOT NULL,
  `password` varchar(90) NOT NULL,
  `companyDisName` varchar(50) NOT NULL,
  `disName` varchar(50) NOT NULL,
  `updateDate` varchar(45) DEFAULT NULL,
  `updator` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `userId` varchar(45) NOT NULL,
  PRIMARY KEY (`userId`,`companyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 資料導出被取消選擇。


-- 導出  表 itri_iot.vehicle 結構
CREATE TABLE IF NOT EXISTS `vehicle` (
  `vehicleId` varchar(50) NOT NULL COMMENT '車牌號碼',
  `disName` varchar(50) DEFAULT NULL,
  `companyId` varchar(50) DEFAULT NULL,
  `updateDate` varchar(50) DEFAULT NULL,
  `updator` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`vehicleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='車輛';

-- 資料導出被取消選擇。
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
