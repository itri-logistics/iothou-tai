<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<!-- Specific Page Vendor CSS -->
<link href="css/custom/fixed-header-table.css" rel="stylesheet" />
<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>
			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>歷史資料檢視(下載)</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>環境動態即時發送</span></li>
							<li><span>歷史資料檢視(下載)</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- start: page -->

				<!-- 查詢 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-search"> 查詢條件</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<form id="queryForm">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">紀錄開始日期</label> <input id="sd"
											name="sd" class="datepicker form-control">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">紀錄結束日期</label> <input id="ed"
											name="ed" class="datepicker form-control">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label">&nbsp;</label>
										<button class="btn btn-primary form-control">
											<i class="el el-search"></i> 查詢
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>

				<script id="place-option-template" type="text/x-handlebars-template">
					{{#each ds}}											
						<option>{{disName}}</option>
					{{/each}}
				</script>

				<section class="panel panel-featured-success panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-list"> 查詢結果</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table id="tag-table"
										class="table fix-table table-bordered table-striped table-condensed mb-none"
										style="display: none">
										<thead class="fix-thead">
											<tr class="fix-tr">
												<th class="fix-th-2 text-center">Tag名稱</th>
												<th class="fix-th-1 text-center">紀錄開始日期</th>
												<th class="fix-th-1 text-center">記錄結束日期</th>
												<th class="fix-th-3 text-center">包含資料類型</th>
												<th class="fix-th-1 text-center">檢視</th>
												<th class="fix-th-1 text-center">刪除</th>
												<th class="fix-th-2"><input name="tag-select-all" id="tag-select-all"
													type="checkbox" />加入待下載列表</th>
											</tr>
										</thead>
										<tbody class="fix-tbody" id="tag-list"></tbody>
									</table>
									<script id="tag-list-template"
										type="text/x-handlebars-template">
										{{#each ds}}
											<tr class="fix-tr">														
												<td class="fix-td-2">{{name}}</td>
												<td class="fix-td-1 text-right">{{sd}}</td>
												<td class="fix-td-1 text-right">{{ed}}</td>
												<td class="fix-td-3">{{signal}}</td>
												<td class="fix-td-1 text-center"><button type="button" class="btn-sm btn btn-info" onclick="module.queryData('{{mac}}','{{name}}','{{sd}}','{{ed}}');"><i class="el el-eye-open"></i></button></td>
												<td class="fix-td-1 text-center"><button type="button" class="btn-sm btn btn-danger" onclick="module.deleteData('{{mac}}','{{sd}}','{{ed}}');"><i class="el el-trash"></i></button></td>												
												<td class="fix-td-2"><input name="tag-select" value="{{name}}" id="{{mac}}" type="checkbox" checked /></td>																	
											</tr>
										{{/each}}
									</script>
								</div>
							</div>
						</div>
						<div class="clearfix">
							<br />
						</div>

						<div class="row">
							<div class="col-md-10">
								<!-- <blockquote class="info" id="tag-select-info"
									style="display: none">
									待下載列表： <span id="tag-select" style="word-break: break-all;"></span>
								</blockquote> -->								
							</div>
							<div class="col-md-2">
								<button id="btn-get-excel-file" style="display: none;"
									type="button" class="btn-primary btn btn-info form-control"
									onclick="module.getExcelFile();">
									<i class="fa fa-file-excel-o"></i> 下載已勾選紀錄
								</button>
							</div>
						</div>
						<div class="clearfix"></div>
						<div id="detail-section" class="row col-md-12"
							style="display: none">
							<blockquote class="success" id="record-info">
								<div>
									tag名稱：<span id="tagName"></span>
								</div>
								<div>
									tag Mac：<span id="tagMac"></span>
								</div>
							</blockquote>
							<div class="col-md-12">

								<div class="table-responsive">
									<table
										class="table fix-table table-bordered table-striped table-condensed mb-none"
										id="tableData">
										<thead class="fix-thead">
											<tr class="fix-tr">
												<th class="fix-th text-center">紀錄時間</th>
												<th class="fix-th text-center">濕度（%rH）</th>
												<th class="fix-th text-center">溫度（℃）</th>
												<th class="fix-th text-center">port1溫度（℃）</th>
												<th class="fix-th text-center">port2溫度（℃）</th>
											</tr>
										</thead>
										<tbody class="fix-tbody">
										</tbody>
									</table>

									<script id="detail-template" type="text/x-handlebars-template">
										{{#each ds}}
											<tr class="fix-tr">
												<td class="fix-td text-right">{{time}}</td>
												<td class="fix-td text-right">{{humidity}}</td>
												<td class="fix-td text-right">{{temperature}}</td>
												<td class="fix-td text-right">{{temperature1}}</td>
												<td class="fix-td text-right">{{temperature2}}</td>																							
											</tr>
										{{/each}}
									</script>
								</div>
								<!-- /.table-responsive -->

							</div>
						</div>
					</div>
				</section>
				<!-- end 查詢 -->
				<!-- end: page -->
			</section>
		</div>

	</section>

	<%@include file="../include/includeFooterJs.jsp"%>
	<script
		src="itri_iot_q/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script
		src="itri_iot_q/vendor/nanoscroller/nanoscroller.js"></script>
	<!--
	<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
	<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
	  -->

	<script
		src="js/common/dateFormat.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/common/array.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/record/broadcastRecordList.js?<c:out value="${applicationScope.js_version}"/>"></script>

</body>
</html>