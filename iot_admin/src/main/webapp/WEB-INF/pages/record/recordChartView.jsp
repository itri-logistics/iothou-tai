<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>
			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>曲線分佈圖</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>環境動態即時記錄</span></li>
							<li><span>曲線分佈圖</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- start: page -->

				<!-- 查詢 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-search"> 查詢條件</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<form id="queryForm">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">紀錄開始日期</label> <input id="sd"
											name="sd" class="datepicker form-control">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">紀錄結束日期</label> <input id="ed"
											name="ed" class="datepicker form-control">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label">&nbsp;</label>
										<button class="btn btn-primary form-control">
											<i class="el el-search"></i> 查詢
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>

				<script id="place-option-template" type="text/x-handlebars-template">
					{{#each ds}}											
						<option>{{disName}}</option>
					{{/each}}
				</script>

				<section class="panel panel-featured-success panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-list"> 查詢結果</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table id="tag-table"
										class="table table-bordered table-striped table-condensed mb-none"
										style="display: none">
										<thead>
											<tr>
												<th>Tag名稱</th>
												<th>紀錄開始日期</th>
												<th>記錄結束日期</th>
												<th>包含資料類型</th>
												<th><input name="tag-select-all" id="tag-select-all"
													type="checkbox" checked /> 檢視</th>
											</tr>
										</thead>
										<tbody id="tag-list"></tbody>
									</table>
								</div>
								<script id="tag-list-template" type="text/x-handlebars-template">
								{{#each ds}}
									<tr>														
										<td>{{name}}</td>
										<td>{{sd}}</td>
										<td>{{ed}}</td>
										<td>{{signal}}</td>
										<td><input name="tag-select" value="{{name}}" id="{{mac}}" type="checkbox" checked /></td>																
									</tr>
								{{/each}}
							</script>
							</div>
						</div>
						<div class="clearfix">
							<br />
						</div>
						<div class="row">
							<div class="col-md-9">
								<!-- 
								<blockquote class="info" id="tag-select-info"
									style="display: none">
									已選取的Tag： <span id="tag-select"  style="word-break: break-all;"></span>
								</blockquote> -->								
							</div>
							<div class="col-md-3">
								<button id="btn-query-chart" style="display: none;"
									type="button" class="btn-primary btn btn-info form-control"
									onclick="module.queryChart();">
									<i class="el el-eye-open"></i> 檢視已勾選
								</button>
							</div>
						</div>

					</div>

				</section>


				<div id="chartHolder">
					<!-- 圖表放置處 -->
				</div>
				<script id="chart-template" type="text/x-handlebars-template">
					{{#each ds}}
						<div class="col-md-6">
							<section class="panel panel-info" role="chart">	
								<header class="panel-heading">																		
									<h5 class="panel-title">
										<i class="fa fa-line-chart"> {{signalName}}圖表</i>
									</h5>
								</header>									
								<div class="panel-body">
									<div id="{{dataType}}-chart" class="chart chart-md"></div>
								</div>
							</section>
						</div>															
					{{/each}}
				</script>
			</section>
		</div>

		<!-- end 查詢 -->
		<!-- end: page -->
	</section>


	<%@include file="../include/includeFooterJs.jsp"%>
	<script
		src="itri_iot_q/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script
		src="js/common/dateFormat.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/common/array.js?<c:out value="${applicationScope.js_version}"/>"></script>
	
	<!-- 圖表  -->	
	<script
		src="itri_iot_q/vendor/flot/jquery.flot.min.js"></script>
	<script
		src="itri_iot_q/vendor/flot.tooltip/flot.tooltip.js"></script>
	<script
		src="itri_iot_q/vendor/flot/jquery.flot.resize.min.js"></script>
	<script
		src="itri_iot_q/vendor/flot/jquery.flot.time.min.js"></script>
	<script
		src="itri_iot_q/vendor/flot/jquery.flot.navigate.min.js"></script>
	<script
		src="itri_iot_q/vendor/flot/excanvas.min.js"></script>
	<!-- for 手機裝置使用  -->
	<script
		src="itri_iot_q/vendor/flot.plugin/jquery.flot.touch.js"></script>
	<script
		src="itri_iot_q/vendor/flot.plugin/jquery.flot.axislabels.js"></script>
	<script
		src="js/record/recordChart.js?<c:out value="${applicationScope.js_version}"/>"></script>

</body>
</html>