<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>

<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>
			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>分析報表</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>環境動態即時發送</span></li>
							<li><span>分析報表</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- start: page -->

				<!-- 查詢 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-search"> 查詢條件</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<form id="queryForm">
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">紀錄開始時間</label> 
										<div class="input-group">
											<span class="input-group-addon"> <i
												class="fa fa-calendar"></i>
											</span> <input id="sd" name="sd" class="datepicker form-control">
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label">&nbsp;</label>
										<div class="input-group">
											<span class="input-group-addon"> <i
												class="fa fa-clock-o"></i>
											</span> <input id="st" name="st" class="timepicker form-control" >
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">紀錄結束時間</label> 
											<div class="input-group">
											<span class="input-group-addon"> <i
												class="fa fa-calendar"></i>
											</span> <input id="ed"
											name="ed" class="datepicker form-control">
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label">&nbsp;</label>
										<div class="input-group">
											<span class="input-group-addon"> <i
												class="fa fa-clock-o"></i>
											</span> <input id="et" name="et" class="timepicker form-control">
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label">&nbsp;</label>
										<button class="btn btn-primary form-control">
											<i class="el el-search"></i> 查詢
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>

				<script id="place-option-template" type="text/x-handlebars-template">
					{{#each ds}}											
						<option>{{disName}}</option>
					{{/each}}
				</script>

				<section class="panel panel-featured-success panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-list"> 查詢結果</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive" style="max-height: 400px; overflow: auto">
									<table id="data-table"
										class="table table-bordered table-striped table-condensed mb-none"
										style="display: none">
										<thead>
											<tr>
												<th>Tag名稱-資料類型</th>
												<th>紀錄開始日期</th>
												<th>記錄結束日期</th>												
												<th><input name="data-select-all" id="data-select-all"
													type="checkbox" />加入待檢視列表</th>
											</tr>
										</thead>
										<tbody id="data-list"></tbody>
									</table>
									<script id="data-list-template"
										type="text/x-handlebars-template">
									{{#each ds}}
										<tr>														
											<td>{{name}}</td>
											<td>{{sd}}</td>
											<td>{{ed}}</td>											
											<td><input name="data-select" value="{{name}}" id="{{key}}" type="checkbox" checked /></td>																	
										</tr>
									{{/each}}
								</script>
								</div>
							</div>
						</div>
						<div class="clearfix">
							<br />
						</div>

						<div class="row">
							<div class="col-md-10">
							<!-- <blockquote class="info" id="data-select-info"
									style="display: none">
									待檢視列表： <span id="data-select"></span>
								</blockquote> -->								
							</div>
							<div class="col-md-2">
								<button id="btn-view-data" style="display: none;"
									type="button" class="btn-primary btn btn-info form-control"
									onclick="module.viewData();">
									<i class="el el-eye-open"></i> 檢視已勾選紀錄
								</button>
							</div>
						</div>
						<div class="clearfix"></div>
						<div id="detail-section" class="row col-md-12"
							style="display: none">
							
							<div class="col-md-12">
								<div class="table-responsive">
									<table
										class="table table-bordered table-striped table-condensed mb-none"
										id="tableData">
										<thead>
											<tr>
												<th class="text-center" style="vertical-align: middle;">資料類型</th>
												<th class="text-center" style="vertical-align: middle;">第一筆時間</th>
												<th class="text-center" style="vertical-align: middle;">最後一筆時間</th>
												<th class="text-center" style="vertical-align: middle;">紀錄筆數（筆）</th>
												<th class="text-center" style="vertical-align: middle;">平均值</th>
												<th class="text-center" style="vertical-align: middle;">最大值</th>
												<th class="text-center" style="vertical-align: middle;">最大值時間</th>
												<th class="text-center" style="vertical-align: middle;">最大值感測器</th>
												<th class="text-center" style="vertical-align: middle;">最小值</th>
												<th class="text-center" style="vertical-align: middle;">最小值時間</th>
												<th class="text-center" style="vertical-align: middle;">最小值感測器</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>									
									<script id="detail-template" type="text/x-handlebars-template">
										{{#each ds}}
											<tr>
												<td class="text-center" style="vertical-align: middle;">{{dataTypeName}}</td>
												<td class="text-right" style="vertical-align: middle;">{{sd}}</td>
												<td class="text-right" style="vertical-align: middle;">{{ed}}</td>
												<td class="text-right" style="vertical-align: middle;">{{count}}</td>
												<td class="text-right" style="vertical-align: middle;">{{avg}}{{unit}}</td>
												<td class="text-right" style="vertical-align: middle;">{{max}}{{unit}}</td>																							
												<td class="text-right" style="vertical-align: middle;">{{maxTime}}</td>																							
												<td class="text-center" style="vertical-align: middle;">{{maxTagName}}-{{maxTypeName}}</td>																							
												<td class="text-right" style="vertical-align: middle;">{{min}}{{unit}}</td>																							
												<td class="text-right" style="vertical-align: middle;">{{minTime}}</td>																							
												<td class="text-center" style="vertical-align: middle;">{{minTagName}}-{{minTypeName}}</td>																							
											</tr>
										{{/each}}
									</script>
								</div>
								<!-- /.table-responsive -->
								<div class="row">
									<div class="text-center col-md-12">
										<img id="reportChart" src="" alt="reportChart" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- end 查詢 -->
				<!-- end: page -->
			</section>
		</div>

	</section>

	<%@include file="../include/includeFooterJs.jsp"%>
	<script
		src="itri_iot_q/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script
		src="itri_iot_q/vendor/bootstrap-timepicker/bootstrap-timepicker.js"></script>
	<script
		src="itri_iot_q/vendor/nanoscroller/nanoscroller.js"></script>
	<script
		src="js/common/dateFormat.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/common/array.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/record/broadcastReport.js?<c:out value="${applicationScope.js_version}"/>"></script>

</body>
</html>