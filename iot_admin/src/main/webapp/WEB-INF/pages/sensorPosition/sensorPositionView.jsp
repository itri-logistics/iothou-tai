<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<!-- Custom CSS -->
<link href="css/custom/sensorPosition.css" rel="stylesheet" />
<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>

			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>圖形視覺化看板設定</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="./index"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>系統設定</span></li>
							<li><span>圖形視覺化看板設定</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- 查詢 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-search"> 場域列表</i>
						</h3>
					</header>
					<div class="panel-body">
						<div id="place-list"></div>
						<script id="place-list-template" type="text/x-handlebars-template">
							{{#each ds}}
								<div id="{{placeId}}" class="panel panel-featured-info panel-thumbnail place-thumbnail" onclick="module.queryTag('{{placeId}}');">									
									<div class="panel-body">
										<img src="./place/icon/{{companyId}}/{{placeId}}">
									</div>
									<div class="panel-footer">	
										<span>{{disName}}</span>
									</div>								
								</div>
							{{/each}}
						</script>
					</div>
				</section>
				<!-- end 查詢 -->

				<!-- 編輯 area -->
				<div class="row">
					<!-- 場域平面圖 -->
					<div class="col-md-9">
						<section class="panel panel-featured-success panel-featured"
							role="update" style="display: none;">
							<header class="panel-heading">
								<h3 class="panel-title">
									<i class="el el-pencil"> 設定感測器位置</i>
								</h3>
							</header>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<button class="btn btn-success form-control"
												onclick="ui.showConfirmDialog('CONFIRM_UPDATE');">
												<i class="el el-ok-sign"></i> 儲存配置
											</button>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="wall">
										<div id="graphic">
											<table id="graphic-containment" style="width: 100%">
												<tbody></tbody>
											</table>
											<script id="sensor-template"
												type="text/x-handlebars-template">											
												<span><i class="{{icon}} fa-stack-2x fa-inverse"></i><i class="fa fa-stack-1x"></i></span>
												<div id="{{id}}" name="{{name}}" value="set" class="draggable col-md-{{size}} style="display:none">
													<div class="well {{style}} well-sm">
														<h4>{{name}}</h4>
														<span>{{port}} {{sensorName}}</span>
													</div>
												</div>											
											</script>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>

					<div class="col-md-3">
						<section class="panel panel-featured-success panel-featured"
							role="update" style="display: none;">
							<header class="panel-heading">
								<h3 class="panel-title">
									<i class="el el-list"> 待定位感測器列表</i>
								</h3>
							</header>
							<div id="tag-list" class="panel-body">
								
								
							</div>
							<script id="tag-list-template" type="text/x-handlebars-template">
									{{#each ds}}
										<div id="{{key}}" name="{{name}}" value="unset" class="draggable col-md-{{size}}">
											<div class="well {{style}} well-sm">
												<h4>{{name}}</h4>
												<span><b>{{port}}</b> {{sensorName}}</span>
											</div>
										</div>
									{{/each}}
								</script>
						</section>
					</div>

				</div>
				<!-- end 編輯 -->

				<!-- end: page -->
			</section>
		</div>

	</section>

	<!-- tooltip -->
	<div id="tooltip" style="display: none;">
		<div class="row">
			<i class="fa fa-tag fa-fw"></i> tagNme <span class="temprature"></span>
			<span class="humidity"></span>
		</div>
	</div>
	<!-- /tooltip -->

	<%@include file="../include/includeFooterJs.jsp"%>
	<script
		src="js/sensorPosition/sensorPosition.js?<c:out value="${applicationScope.js_version}"/>"></script>
</body>
</html>