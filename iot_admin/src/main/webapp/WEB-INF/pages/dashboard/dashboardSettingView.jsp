<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>
			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>數字顯示看板設定</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>系統設定</span></li>
							<li><span>數字顯示看板設定</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- start: page -->

				<!-- 編輯 area -->
				<section class="panel panel-featured-info panel-featured"
					role="update" style="display: none;">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-edit"> 編輯視窗</i>
						</h3>
					</header>
					<div class="panel-body">
						<form class="form-horizontal form-bordered" id="updateForm">
							<div class="form-group">
								<label class="col-md-2 control-label">所屬公司帳號</label>
								<div class="col-md-6">
									<input name="companyId" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">看板名稱</label>
								<div class="col-md-6">
									<input name="disName" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">資料來源Tag</label>
								<div class="col-md-6">
									<select class="form-control mb-md" name="macId">
									</select>
									<p>
										<code><span class="fa fa-exclamation"></span> Tag必須歸屬在場域下，即時看板才會顯示。</code>										
									</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">資料來源類型</label>
								<div class="col-md-6">
									<select class="form-control mb-md" name="dataType">
										<option value="0001">本體溫度</option>
										<option value="0002">本體濕度</option>
										<option value="0101">port1溫度</option>
										<option value="0201">port2溫度</option>
									</select>
								</div>
							</div>
							<input name="position" class="form-control" type="text" value="0"
								style="display: none;">
							<input name="dashboardId" class="form-control" type="text" style="display: none;">
							<div class="text-center">
								<button class="btn btn-default btn-lg"
									onclick="ui.gotoMode('MODE_UPDATE_CANCEL');">
									<i class="el el-return-key"></i> 取消退出
								</button>
								<button class="btn btn-info btn-lg"
									onclick="ui.showConfirmDialog('CONFIRM_UPDATE');">
									<i class="el el-ok"></i> 確定編輯
								</button>
							</div>
						</form>
					</div>
				</section>
				<!-- end 編輯 area -->

				<!-- 新增 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="create" style="display: none;">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-file-new"> 新增視窗</i>
						</h3>
					</header>
					<div class="panel-body">
						<form class="form-horizontal form-bordered" id="createForm">
							<div class="form-group">
								<label class="col-md-2 control-label">所屬公司帳號</label>
								<div class="col-md-6">
									<input name="companyId" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">看板名稱</label>
								<div class="col-md-6">
									<input name="disName" class="form-control" type="text">
								</div>								
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">資料來源Tag</label>
								<div class="col-md-6">
									<select class="form-control mb-md" name="macId">
									</select>
									<p>
										<code><span class="fa fa-exclamation"></span> Tag必須歸屬在場域下，即時看板才會顯示。</code>										
									</p>
									<script id="tag-option-template"
										type="text/x-handlebars-template">
										{{#each ds}}
											<option value="{{mac}}">{{name}}</option>
										{{/each}}
									</script>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">資料來源類型</label>
								<div class="col-md-6">
									<select class="form-control mb-md" name="dataType">
										<option value="0001">本體溫度</option>
										<option value="0002">本體濕度</option>
										<option value="0101">port1溫度</option>
										<option value="0201">port2溫度</option>
									</select>
								</div>
							</div>
							<input name="position" class="form-control" type="text" value="0"
								style="display: none;">
							<div class="text-center">
								<button class="btn btn-default btn-lg"
									onclick="ui.gotoMode('MODE_CREATE_CANCEL');">
									<i class="el el-return-key"></i> 取消退出
								</button>
								<button class="btn btn-primary btn-lg"
									onclick="ui.showConfirmDialog('CONFIRM_CREATE');">
									<i class="el el-ok"></i> 確定新增
								</button>
							</div>
						</form>
					</div>
				</section>
				<!-- end 新增area -->

				<!-- 查詢 -->
				<section class="panel panel-featured-success panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-list"> 數字顯示看板</i>
						</h3>
					</header>
					<div class="panel-body">						
						<div class="col-md-2">
							<div class="form-group">
								<button class="form-control btn btn-primary"
									onclick="ui.gotoMode('MODE_CREATE')">
									<i class="el el-file-new"></i> 新增看板
								</button>
							</div>
						</div>
					</div>					
				</section>	
				<div class="row" id="dashboard" ></div>			
				<script id="place-section-template" type="text/x-handlebars-template">
					{{#each ds}}
						<div class="col-md-12">
							<section role="query">
								<h3>{{disName}}</h3>
								<div id="place-{{placeId}}"></div>
							</section>
						</div>
					{{/each}}
				</script>
				<script id="dashboardSettings-template" type="text/x-handlebars-template">
							{{#each ds}}
							<div class="col-md-3" id="{{dashboardId}}">
								<section class="panel panel-{{style}}"
									role="query">
									<header class="panel-heading">
										<div class="panel-title">{{disName}}</div>	
										<div class="panel-actions">
                                            <button type="button" class="btn-sm btn btn-{{style}}" onclick="ui.gotoMode('MODE_UPDATE','{{dashboardId}}');"><i class="el el-pencil"></i></button>
                                            <button type="button" class="btn-sm btn btn-{{style}}" onclick="ui.showConfirmDialog('CONFIRM_DELETE','{{dashboardId}}');"><i class="el el-trash"></i></button>
                                        </div>																		
									</header>
									<div class="panel-body">
										<div class="fittext text-center" style="line-height: 1.1; margin-top: .67em;">											
											<b id="{{macId}}_{{dataType}}_data">&nbsp;{{lastData}}</b>
										</div>	
										<div class="clearfix"></div>																			
										<div><h3><span id="{{macId}}_{{dataType}}_datetime">&nbsp;</span></h3></div>
									</div>
									<div class="panel-footer">
										<i class="el el-record"></i> {{tagName}} {{dataTypeName}}
									</div>
								</section>
							</div>
						{{/each}}
				</script>
				<!-- end 查詢 -->
				<!-- end: page -->
			</section>
		</div>

	</section>

	<%@include file="../include/includeFooterJs.jsp"%>
	<script src="itri_iot_q/vendor/jquery-fittext/jquery-fittext.js"></script>
	<script
		src="js/common/dateFormat.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/dashboard/dashboardSetting.js?<c:out value="${applicationScope.js_version}"/>"></script>
</body>
</html>