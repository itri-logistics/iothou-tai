<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<link href="css/custom/dashboard.css" rel="stylesheet" />
<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>
			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>數字顯示看板</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>環境動態即時發送</span></li>
							<li><span>數字顯示看板</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- start: page -->
				<!-- 查詢 -->
				<section class="panel panel-featured-success panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-list"> 數字顯示看板</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="col-md-2">
							<div class="form-group">
								<button class="form-control btn btn-primary"
									onclick="module.findNewestData();">
									<i class="el el-refresh"></i> 重新整理
								</button>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<div class="col-md-9">
									<div id="switch-precision" class="switch switch-primary switch-sm">
										<input type="checkbox" name="check-precision"
											data-plugin-ios-switch id="check-precision" />
									</div>
									<label for="check-precision">以0.5為最小單位顯示</label>
								</div>
							</div>
						</div>
					</div>
				</section>
				<div class="row" id="dashboard"></div>
				<script id="place-section-template"
					type="text/x-handlebars-template">
					{{#each ds}}
						<div class="col-md-12">
							<h3>{{disName}}</h3>
							<div id="place-{{placeId}}"></div>
						</div>
					{{/each}}
				</script>

				<script id="dashboardSettings-template"
					type="text/x-handlebars-template">
							{{#each ds}}
							<div class="col-md-3" id="{{dashboardId}}">
								<section class="panel {{style}}"
									role="query">
									<header class="panel-heading">
										<div class="panel-title">{{disName}}</div>																			
									</header>
									<div id="{{macId}}_{{dataType}}" class="panel-body">
										<div class="fittext text-center" style="line-height: 1.1; margin-top: .46em; margin-bottom: .2em">											
											<b id="{{macId}}_{{dataType}}_data">&nbsp;{{lastData}}</b>
										</div>
										<div class="clearfix"></div>																			
										<div><h3><span id="{{macId}}_{{dataType}}_datetime">&nbsp;</span></h3></div>
									</div>
									<div class="panel-footer">
										<i class="el el-record"></i> {{tagName}} {{dataTypeName}}
									</div>
								</section>
							</div>
						{{/each}}
				</script>
				<!-- end 查詢 -->
				<!-- end: page -->
			</section>
		</div>

	</section>

	<%@include file="../include/includeFooterJs.jsp"%>
	<script
		src="itri_iot_q/vendor/jquery-fittext/jquery-fittext.js"></script>
	<script
		src="js/common/dateFormat.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/dashboard/realTimeDashboard.js?<c:out value="${applicationScope.js_version}"/>"></script>
</body>
</html>