<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<body>
	<section class="body">
		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>RFID</h2>
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>歷史紀錄</span></li>
							<li><span>紀錄列表</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- start: page -->
				<!-- 查詢 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-search">目前狀態</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<form id="queryForm">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">倉儲</label> 
										<input id="ed" name="ed" class="datepicker form-control">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">車號</label>
										<input id="sd" name="sd" class="datepicker form-control">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">碼頭</label> 
										<input id="ed" name="ed" class="datepicker form-control">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label">&nbsp;</label>
										<button class="btn btn-primary form-control">
											<i class="el el-search"></i>讀取結束
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>

				<script id="place-option-template" type="text/x-handlebars-template">
					{{#each ds}}											
						<option>{{disName}}</option>
					{{/each}}
				</script>

				<section class="panel panel-featured-success panel-featured" role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-list">讀取結果</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table id="tag-table" class="table table-bordered table-striped table-condensed mb-none" style="display: none">
										<thead>
											<tr>
												<th>Tag名稱</th>
												<th>紀錄開始日期</th>
												<th>記錄結束日期</th>
												<th>包含資料類型</th>
											</tr>
										</thead>
										<tbody id="tag-list"></tbody>
									</table>
									<script id="tag-list-template" type="text/x-handlebars-template">
									{{#each ds}}
										<tr>														
											<td>{{name}}</td>
											<td>{{sd}}</td>
											<td>{{ed}}</td>
											<td>{{signal}}</td>
										</tr>
									{{/each}}
								</script>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- end 查詢 -->

				<!-- end: page -->
			</section>
		</div>
	</section>

	<%@include file="../include/includeFooterJs.jsp"%>
	<script	src="itri_iot_q/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script	src="itri_iot_q/vendor/nanoscroller/nanoscroller.js"></script>
	<script	src="js/rfid/rfid.js?<c:out value="${applicationScope.js_version}"/>"></script>

</body>
</html>