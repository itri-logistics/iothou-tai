<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<!-- Custom CSS -->
<link href="itri_iot_q/vendor/bootstrap-slider/css/bootstrap-slider.min.css" rel="stylesheet" />
<link href="css/custom/graphic.css" rel="stylesheet" />
<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>

			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>圖形視覺化看板</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>環境動態即時發送</span></li>
							<li><span>圖形視覺化看板</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>
				
				<section class="panel panel-featured-success panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-list"> 場域列表</i>
						</h3>
					</header>
					<div class="panel-body">
						<div id="place-list"></div>
						<script id="place-list-template" type="text/x-handlebars-template">
							{{#each ds}}
								<div id="{{placeId}}" class="panel panel-featured-info panel-thumbnail place-thumbnail" onclick="module.queryTag('{{placeId}}');">									
									<div class="panel-body">
										<img src="./place/icon/{{companyId}}/{{placeId}}">
									</div>
									<div class="panel-footer">	
										<span>{{disName}}</span>
									</div>								
								</div>
							{{/each}}
						</script>
					</div>

				</section>
				<!-- end 查詢 -->

				<!-- 場域平面圖 -->
				<div class="row">
					<div class="col-md-9">
						<section class="panel panel-featured-success panel-featured"
							role="result" style="display: none;">
							<header class="panel-heading">
								<h3 class="panel-title">
									<i class="fa fa-eye"> 場域溫度分布</i>
									<div class="col-md-2 pull-right">
										<div class="form-group">
											<button class="form-control btn btn-primary"
												onclick="module.queryData();">
												<i class="el el-refresh"></i> 重新整理
											</button>
										</div>
									</div>
								</h3>
							</header>
							<div class="panel-body">								
								<div class="row">
									<div class="wall">
										<div id="graphic">
											<table id="graphic-containment" style="width: 100%">
												<tbody></tbody>
											</table>
											<script id="sensor-template"
												type="text/x-handlebars-template">											
												<span><i class="{{icon}} fa-stack-2x fa-inverse"></i><i class="fa fa-stack-1x"></i></span>
												<div id="{{id}}" name="{{name}}" value="{{value}}" class="sensor col-md-{{size}}" style="display:none;">
													<div class="well {{style}} well-sm">
														<h4>{{name}}</h4>
														<span>{{port}} {{sensorName}}</span>
													</div>
												</div>											
											</script>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="alert alert-ruler">
										<div class="row">
											顏色與溫度對應
											<table style="width: 100%">
												<tr id="color-ruler"></tr>
											</table>
											<div>
												<span id="minValue" class="pull-left"></span> <span
													id="maxValue" class="pull-right"></span>
											</div>
											
											<br />
											<div id="disconnect-color" class="row"
												style="color: rgba(0, 0, 0, 1);">
												無資料：<span data-toggle="tooltip" data-placement="bottom"
													title="此時段無資料"
													style="color: rgba(140, 140, 140, 1); background: rgba(140, 140, 140, 1);">口</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>

					<div class="col-md-3">
						<section class="panel panel-featured-success panel-featured"
							role="result" style="display: none;">
							<header class="panel-heading">
								<h3 class="panel-title">
									<i class="el el-list"> 感測器列表</i>
								</h3>
							</header>
							<div class="panel-body">
								<div id="tag-list"></div>
								<script id="tag-list-template" type="text/x-handlebars-template">
									{{#each ds}}
										<div id="{{key}}" name="{{name}}" value="{{value}}" class="sensor col-md-{{size}}">
											<div class="well {{style}} well-sm">
												<h4>{{name}}<span class="pull-right">尚未定位</span></h4>
												<div><b>{{port}}</b> {{sensorName}}</div>
												<div><span class="temprature" style="display:none;">{{value}}</span></div>
											</div>
										</div>
									{{/each}}
								</script>
							</div>
						</section>
					</div>

				</div>
				<!-- end 場域平面圖  -->
				<span id="lowerBound" style="display: none;"></span>
        		<span id="upperBound" style="display: none;"></span> 
				<!-- end: page -->
			</section>
		</div>

	</section>

	<!-- tooltip -->
	<div id="tooltip" style="display: none;">
		<div class="row">
			<i class="fa fa-tag fa-fw"></i> tagNme <span class="temprature"></span>
			<span class="humidity"></span>
		</div>
	</div>
	<!-- /tooltip -->
	
	

	<%@include file="../include/includeFooterJs.jsp"%>
	<script
		src="itri_iot_q/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script
		src="itri_iot_q/vendor/bootstrap-slider/js/bootstrap-slider.min.js"></script>
	<script
		src="js/common/dateFormat.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/common/array.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/graphic/realTimeGraphic.js?<c:out value="${applicationScope.js_version}"/>"></script>
</body>
</html>