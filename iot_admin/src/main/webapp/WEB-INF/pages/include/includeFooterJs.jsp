<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

		
		<!-- Vendor -->
		<script src="itri_iot_q/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="itri_iot_q/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="itri_iot_q/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="itri_iot_q/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="itri_iot_q/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="itri_iot_q/vendor/jquery-placeholder/jquery-placeholder.js"></script>
		<script src="itri_iot_q/vendor/sweetalert/sweetalert.min.js"></script>
		

		<script src="itri_iot_q/vendor/jquery-ui/jquery-ui.js"></script>
		<script src="itri_iot_q/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js"></script>
		<script src="itri_iot_q/vendor/select2/js/select2.js"></script>
		<script src="itri_iot_q/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="itri_iot_q/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
		<script src="itri_iot_q/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
		<script src="itri_iot_q/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
		<script src="itri_iot_q/vendor/bootstrap-timepicker/bootstrap-timepicker.js"></script>
		<script src="itri_iot_q/vendor/fuelux/js/spinner.js"></script>
		<script src="itri_iot_q/vendor/dropzone/dropzone.js"></script>
		<script src="itri_iot_q/vendor/bootstrap-markdown/js/markdown.js"></script>
		<script src="itri_iot_q/vendor/bootstrap-markdown/js/to-markdown.js"></script>
		<script src="itri_iot_q/vendor/bootstrap-markdown/js/bootstrap-markdown.js"></script>
		<script src="itri_iot_q/vendor/codemirror/lib/codemirror.js"></script>
		<script src="itri_iot_q/vendor/codemirror/addon/selection/active-line.js"></script>
		<script src="itri_iot_q/vendor/codemirror/addon/edit/matchbrackets.js"></script>
		<script src="itri_iot_q/vendor/codemirror/mode/javascript/javascript.js"></script>
		<script src="itri_iot_q/vendor/codemirror/mode/xml/xml.js"></script>
		<script src="itri_iot_q/vendor/codemirror/mode/htmlmixed/htmlmixed.js"></script>
		<script src="itri_iot_q/vendor/codemirror/mode/css/css.js"></script>
		<script src="itri_iot_q/vendor/summernote/summernote.js"></script>
		<script src="itri_iot_q/vendor/bootstrap-maxlength/bootstrap-maxlength.js"></script>
		<script src="itri_iot_q/vendor/ios7-switch/ios7-switch.js"></script>
		<script src="itri_iot_q/vendor/bootstrap-confirmation/bootstrap-confirmation.js"></script>
				
		<script src="itri_iot_q/javascripts/ajaxOnlyPlugin.js"></script>
		<script src="itri_iot_q/javascripts/handlebars-v4.0.5.js"></script>
		<script src="itri_iot_q/javascripts/jquery.cookie.js"></script>
		<script src="itri_iot_q/javascripts/jquery.form.min.js"></script>
		
				
		<!-- Theme Base, Components and Settings -->
		<script src="itri_iot_q/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="itri_iot_q/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="itri_iot_q/javascripts/theme.init.js"></script>

<%-- This is JSP comment 
<script src="3pty/js/jquery.cookie.js"></script>
<script src="3pty/js/url.min.js"></script>
<script>
var host='<c:out value="${pageContext.request.contextPath}"/>';
$.cookie.json = true;
</script>
--%>
			