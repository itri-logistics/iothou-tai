<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<footer>
			<div class="gototop">
				<span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
			</div>
			<div class="contact-info">
				<div class="container">
					<div class="row">
						<div class="col-md-5 col-sm-4 col-xs-12 item-left">
							<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>地址：412台中市大里區中興路二段671巷110號
						</div>
						<div class="col-md-3 col-sm-4 col-xs-12 item-middle">
							<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>電話：04-2483-3832
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 item-right">
							<span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span>傳真：04-2483-3324
						</div>
					</div>
				</div>
			</div>
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-6 item-left">
							<a href="http://tapinfo.coa.gov.tw/work/tapinfo.php?func=where_eat&amp;main=y" target="_blank">產品履歷食材追溯平台</a>
							<span class="copyright-taft">
							<img src="<c:out value="${applicationScope.lib_link}"/>/images/logo_taft.jpg" alt="">行政院農業委員會  指導
							</span>
						</div>
						<div class="col-md-6 item-right">
							©2016 有心肉舖子 With Heart  All right reserved.
						</div>
					</div>
				</div>
			</div>
		</footer>