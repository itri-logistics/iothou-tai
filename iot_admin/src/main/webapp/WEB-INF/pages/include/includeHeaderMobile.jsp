<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<title>ITRI IOT後端管理平台</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="itri_iot_q/vendor/bootstrap/css/bootstrap.min.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="itri_iot_q/stylesheets/theme.css" />
		
		<!-- Head Libs -->
		<script src="itri_iot_q/vendor/jquery/jquery.js"></script>
	</head>
