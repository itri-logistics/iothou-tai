<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
			<header class="header">
				<div class="logo-container">
					<a href="../" class="logo">
						<h3 style="top: -5px;position: absolute;color: #fff;font-weight: bolder;">環境感知網絡技術平台</h3>
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<div class="profile-info" style="margin-top: 5px;">
								<span class="name"><sec:authentication property="principal.disName"/>(<sec:authentication property="principal.companyDisName"/>)</span>
								<span class="role"><sec:authentication property="principal.type" /></span>
								<span class="companyId" style="display: none;"><sec:authentication property="principal.companyId" /></span>
								<span class="userId" style="display: none;"><sec:authentication property="principal.username" /></span>
								<span class="server" style="display: none;"><sec:authentication property="principal.server" /></span>
							</div>
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fa fa-user"></i>我的帳戶</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="logout"><i class="fa fa-power-off"></i>登出</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
