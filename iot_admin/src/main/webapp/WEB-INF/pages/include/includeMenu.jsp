<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%--  <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %> --%>
			<aside id="sidebar-left" class="sidebar-left">
				<div class="sidebar-header">
					<div class="sidebar-title"> 功能列表 </div>
					<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle"> <i class="fa fa-bars" aria-label="Toggle sidebar"></i> </div>
				</div>
				<style>
					.unauth {
						display: none;
					}
				</style>
				<div class="nano">
					<div class="nano-content">
						<nav id="mainNav" class="nav-main" role="navigation">
							<ul class="nav nav-main">
								<li>
									<a href="./index"> <i class="fa fa-home" aria-hidden="true"></i> <span>平台首頁</span> </a>
								</li>							
								<li class="nav-parent">
									<a> <i class="el el-dashboard" aria-hidden="true"></i> <span>環境動態即時發送</span> </a>
									<ul class="nav nav-children">
										<li>
											<a href="./realTimeDashboard">數字顯示看板</a>
										</li>
										<li>
											<a href="./realTimeGraphic">圖形視覺化看板</a>
										</li>
										<li>
											<a href="./graphic_b">圖形視覺化看板(歷史資料檢視)</a>
										</li>
										<li>
											<a href="./broadcastRecordList">歷史資料檢視(下載)</a>
										</li>
										<li>
											<a href="./broadcastReport">分析報表</a>
										</li>
									</ul>
								</li>					
								<li class="nav-parent">
									<a> <i class="el el-time" aria-hidden="true"></i> <span>環境動態即時記錄</span> </a>
									<ul class="nav nav-children">										
										<li>
											<a href="./recordChart">曲線分佈圖</a>
										</li>
										<li>
											<a href="./graphic">圖形視覺化看板</a>
										</li>
										<li>
											<a href="./recordList">歷史資料檢視(下載)</a>
										</li>						
										<li>
											<a href="./report">分析報表</a>
										</li>								
									</ul>
								</li>								
								<li>
									<a href="./realTimeTag"> <i class="fa fa-tags" aria-hidden="true"></i> <span>感測器狀態檢視</span> </a>
								</li>
								<li class="nav-parent">
									<a> <i class="fa fa-line-chart" aria-hidden="true"></i> <span>品質管理</span> </a>
									<ul class="nav nav-children">
										<li>
											<a href="./proccess">歷程分析</a>
										</li>
										<li>
											<a href="./suggestion">警示設定（依標準建議）</a>
										</li>
										<li>
											<a href="./placeManage">場域管理</a>
										</li>
										<li>
											<a href="./vehicleManage">車廂管理</a>
										</li>
										<li>
											<a href="./cartManage">籠車管理</a>
										</li>
										<li>
											<a href="./wifiSetting">Wi-Fi接收器設定</a>
										</li>
										<li>
											<a href="./classSetting">場域類別設定</a>
										</li>
									</ul>
								</li>
								<sec:authorize access="hasRole('su') and isAuthenticated()">
								<li>
									<a href="./realTimeVehicle"> <i class="fa fa-truck" aria-hidden="true"></i> <span>車輛即時位置</span> </a>
								</li>
								</sec:authorize>								
								<sec:authorize access="hasRole('su') and isAuthenticated()">
								<li class="nav-parent">
								
									<a> <i class="fa fa-cubes" aria-hidden="true"></i> <span>資材管理系統</span> </a>
									<ul class="nav nav-children">
										<li>
											<a href="./palletsSetting">TAG綁定</a>
										</li>
										<li>
											<a href="./palletsTypeSetting">TAG Type設定</a>
										</li>										
										<li>
											<a href="./inventoryDailyReport">日結庫存查詢</a>
										</li>
										<li>
											<a href="./palletsDailyReport">日結進出查詢</a>
										</li>
										<li>
											<a href="./noIdentityPalletsDailyReport">無身分進出查詢</a>
										</li>								
									</ul>
								</li>
								</sec:authorize>						
								<li class="nav-parent">
									<a> <i class="el el-cog" aria-hidden="true"></i> <span>系統設定</span> </a>
									<ul class="nav nav-children">
										<li>
											<a href="./place">場域設定</a>
										</li>
										<li>
											<a href="./tag">感測器設定</a>
										</li>								
										<li>
											<a href="./sensorPosition">圖形視覺化看板設定</a>
										</li>
										<li>
											<a href="./alert">警示設定</a>
										</li>
										<li>
											<a href="./dashboardSetting">數字顯示看板設定</a>
										</li>
										<sec:authorize access="hasRole('su') and isAuthenticated()">
										<li>
											<a href="./vehicle">車輛設定</a>
										</li>
										</sec:authorize>
									</ul>
								</li>
								<sec:authorize access="hasRole('su') and isAuthenticated()">
									<li>
										<a href="./user"> <i class="fa fa-group" aria-hidden="true"></i> <span>公司管理</span> </a>
									</li>
									
									<li class="nav-parent">
									<a> <i class="fa fa-columns" aria-hidden="true"></i> <span>平台管理</span> </a>
									<ul class="nav nav-children">
										<li>
											<a href="./dataServer">Data Server 狀態</a>
										</li>
									<!-- 
										<li>
											<a href="./upload">UPLOAD頁面</a>
										</li>
										<li>
											<a href="./tag/test">Tag測試頁面</a>
										</li>
									 -->																			
									</ul>
									</li>									
										
								</sec:authorize>								
							</ul>
						</nav>
					</div>
					<script>
						$.ajaxSetup({ cache: false });
						// Maintain Scroll Position
						var menuAuths = '<c:out value="${applicationScope.menuAuths}"/>';
						var ms = menuAuths.split(',');
						//$('.nav-children a').hide();
						//for (var i in ms) {
						//	$('.nav-children a[href="./' + ms[i] + '"]').show();
						//}
						if (typeof localStorage !== 'undefined') {
							if (localStorage.getItem('sidebar-left-position') !== null) {
								var initialPosition = localStorage.getItem('sidebar-left-position'),
									sidebarLeft = document.querySelector('#sidebar-left .nano-content');
								sidebarLeft.scrollTop = initialPosition;
							}
						}
						console.debug(url('-1'));
                        var menuli=$('#mainNav a[href="./'+url('-1')+'"]').parent();
                        menuli.addClass('nav-active').parent().parent().addClass('nav-expanded');         						
					</script>
				</div>
			</aside>