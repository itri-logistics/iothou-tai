<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<head>
<!-- Basic -->
<meta charset="UTF-8">
<title>IOT後端管理平台</title>
<meta name="keywords" content="HTML5 Admin Template" />
<meta name="description"
	content="Porto Admin - Responsive HTML5 Template">
<meta name="author" content="okler.net">
<!-- Mobile Metas -->
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<script
	src="itri_iot_q/vendor/jquery/jquery.js"></script>
<script
	src="itri_iot_q/javascripts/url.min.js"></script>
</head>
<body>
	<h1>Tag測試頁面</h1>
	<table border="1">
		<tbody>
			<tr>
				<td>
					<h1>新增Tag資料</h1>
					<h3>Url:http://45.32.45.49:8080/iot_admin/tag/create</h3>
					<h3>Http Method: POST</h3>
					<h4>
						功能和 <a href="http://45.32.45.49:8080/iot_admin/tag"
							target="_blank">http://45.32.45.49:8080/iot_admin/tag</a>中的新增一樣
					</h4>
					<h1>更新Tag資料</h1>
					<h3>Url:http://45.32.45.49:8080/iot_admin/tag/update</h3>
					<h3>Http Method: POST</h3>
					<h4>
						功能和 <a href="http://45.32.45.49:8080/iot_admin/tag"
							target="_blank">http://45.32.45.49:8080/iot_admin/tag</a>中的更新一樣
					</h4>
					<form id="testForm">
						<h3>post參數 ：</h3>
						<textarea rows="5" cols="80" name="datas" id="datas"></textarea>
						<br /> 
						<button onclick="module.postTestData();">新增Tag</button>
						<button onclick="module.updateData();">更新Tag</button>
					</form>					
				</td>
			</tr>
			<tr>
				<td>
					<h1>產生模擬資料</h1>
					<form id="createDataForm">
						<h3>companyId(公司帳號)</h3>
						<input name="companyId" id="companyId" value="itri"><br />
						<h3>placeId(場域Id)</h3>
						<input name="placeId" id="placeId" value="1"><br />
						<h3>mac(Tag MAC)</h3>
						<input name="mac" id="mac" value="MACBLETAGMAC"><br />
						<h3>name(Tag 名稱)</h3>
						<input name="name" id="name" value="TAG（預設）"><br />
						<h3>rate(批次記錄取樣率，單位秒)</h3>
						<input name="rate" id="rate" value="15"><br />
						<h3>port1(port1感測器)</h3>
						<input name="port1" id="port1" value="00"><br />
						<h3>port2(port2感測器)</h3>
						<input name="port2" id="port2" value="00"><br />
						<h3>port3(port3感測器)</h3>
						<input name="port3" id="port3" value="00"><br />
						<h3>port4(port4感測器)</h3>
						<input name="port4" id="port4" value="00"><br />
						<h3>感測器代碼- 00：無感測器，01：溫度，02：濕度，03：低溫，04：高溫，05：光度，06：電流</h3>
						<button onclick="module.getTestData();">取得測試資料</button>
					</form>
				</td>
			</tr>
			<tr>
				<td>
					<h1>查詢上傳後資料</h1>
					<h3>Url:http://45.32.45.49:8080/iot_admin/tag/find?{參數1}={值1}&{參數2}={值2}</h3>
					<form id="queryForm">
						<h3>參數(選填，沒有代參數就是全部列出)<h3>
						<div>公司帳號 companyId = <input name="companyId" value="itri"></div>
						<div>Tag名稱 name = <input name="name"> </div>
						<div>藍芽mac(12碼) mac = <input name="mac"> </div>
						<div>場域Id placeId = <input name="placeId"></div>
					</form>
					<button onclick="module.getQueryData();">查詢</button>
					<h4>結果</h4>
					<div id="qRs"></div>
				</td>
			</tr>
		</tbody>
	</table>

	<script
		src="itri_iot_q/javascripts/ajaxOnlyPlugin.js"></script>
	<script
		src="itri_iot_q/javascripts/handlebars-v4.0.5.js"></script>
	<script
		src="itri_iot_q/javascripts/jquery.cookie.js"></script>
	<script
		src="itri_iot_q/javascripts/jquery.form.min.js"></script>
	<script
		src="../js/tag/tagTest.js?<c:out value="${applicationScope.js_version}"/>"></script>
</body>
</html>