<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeaderMobile.jsp"%>
<link href="css/custom/tagNotExist.css" rel="stylesheet" />
<body class="error-message-wrap">
	
		<div class="row col-md-12">
			<div class="icon-wrap"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
			<h3>您新增的Tag不存在，<br/>請重新確認輸入的資訊</h3>
		</div>

	<%@include file="../include/includeFooterJsMobile.jsp"%>

	<!-- Specific Page Vendor -->
	<script
		src="itri_iot_q/vendor/autosize/autosize.js"></script>


</body>
</html>