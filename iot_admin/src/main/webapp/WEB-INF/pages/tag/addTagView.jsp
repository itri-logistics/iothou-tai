<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeaderMobile.jsp"%>
<!-- Custom CSS -->
<link href="css/custom/addTag.css" rel="stylesheet" />
<body class="add-tag-wrap">
	<div class="sub-title">登錄新的Tag</div>
	<div class="inner-wrapper">
		<section role="main" class="content-body">
			<div class="row col-md-12">
				<div class="row form-list">
					<label class="col-md-1 control-label" for="tagMacInput">Tag
						Mac</label>
					<div class="col-md-11">
						<input type="text" value="${tagMac}" id="tagMacInput"
							class="form-control input-lg" readonly="readonly">
					</div>
				</div>
				<div class="row form-list">
					<label class="col-md-1 control-label" for="tagNameInput">Tag名稱</label>
					<div class="col-md-11">
						<input type="text" value="${tagName}"
							class="form-control input-lg" id="tagNameInput">
					</div>
				</div>
				<div class="row form-list">
					<label class="col-md-1 control-label" for="areaNameInput">所屬場域</label>
					<div class="col-md-11">
						<input id="placeId" value="${placeId}" type="hidden">
						<input type="text" value="${placeName}" id="placeNameInput"
							class="form-control input-lg" readonly="readonly">
					</div>
				</div>
				<div class="row form-list">
					<label class="col-md-1 control-label" for="comNameInput">公司帳號</label>
					<div class="col-md-11">
						<input type="text" value="${comId}" id="comNameInput"
							class="form-control input-lg" readonly="readonly">
					</div>
				</div>
				<div class="row btn-wrap">
					<button id="btn-add-tag" type="button"
						class="btn btn-success btn-lg">確認登錄</button>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="row copyright">Copyright © iMeasure智慧聯網平台</div>
		</section>

	</div>

	<%@include file="../include/includeFooterJsMobile.jsp"%>
	<!-- Specific Page Vendor -->
	<script
		src="itri_iot_q/vendor/autosize/autosize.js"></script>

	<script
		src="js/tag/addTag.js?<c:out value="${applicationScope.js_version}"/>"></script>


</body>
</html>