<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
 <!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
	<body>
		<section class="body">

			<!-- start: header -->
<%@include file="../include/includeTopHeader.jsp"%>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
<%@include file="../include/includeMenu.jsp"%>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>感測器狀態檢視</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.html">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>感測器狀態檢視</span></li>
							</ol>
							&nbsp;&nbsp;&nbsp;
						</div>
					</header>

					<!-- start: page -->
						<!-- 查詢 area -->
						<section class="panel panel-featured-primary panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-search"> 查詢條件</i></h3>
							</header>						
							<div class="panel-body">
								<div class="row">
									<form id="queryForm">
										<input name="companyId" type="hidden">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">tag名稱</label>
												<input name="name" class="form-control">
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">&nbsp;</label>
												<button class="btn btn-primary form-control"><i class="el el-search"></i> 查詢</button>
											</div>
										</div>
									</form>									
								</div>							
							</div>
						</section>


						<section class="panel panel-featured-success panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-list"> 查詢結果</i></h3>
							</header>						
							<div class="panel-body">
								<div class="table-responsive" style="max-height: 450px; overflow: auto">
									<table class="table table-bordered table-striped table-condensed mb-none" id="tableData">
										<thead>
											<tr>
												<th class="text-center">Tag MAC</th>
												<th class="text-center">Tag名稱</th>
												<th class="text-center">Tag類型</th>
												<th class="text-center">所屬場域</th>
												<th class="text-center">電量</th>
												<th class="text-center">更新時間</th>												
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
									<script id="detail-template" type="text/x-handlebars-template">
											{{#each ds}}
											<tr>
												<td>{{mac}}</td>
												<td>{{name}}</td>
												<td>{{type}}</td>
												<td>{{placeId}}</td>
												<td class="text-right" id="{{mac}}_0007_power"></td>
												<td class="text-right" id="{{mac}}_0007_datetime"></td>
											</tr>
											{{/each}}
									</script>
									
									<br/>
									<div class="col-md-offset-5 col-md-7 text-right">
										<form class="form-inline">
											<div class="form-group">												
												<label>共有<span id="totalRecord">0</span>筆資料</label>
											</div>
										</form>
									</div>
								</div>
							</div>
						</section>
						<!-- end 查詢 -->
					<!-- end: page -->
				</section>
			</div>

		</section>

<%@include file="../include/includeFooterJs.jsp"%>
		<script src="js/tag/realTimeTag.js?<c:out value="${applicationScope.js_version}"/>"></script>
	</body>
</html>