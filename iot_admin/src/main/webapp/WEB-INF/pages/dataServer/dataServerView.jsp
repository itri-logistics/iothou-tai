<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
 <!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
	<body>
		<section class="body">

			<!-- start: header -->
<%@include file="../include/includeTopHeader.jsp"%>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
<%@include file="../include/includeMenu.jsp"%>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Data Server列表</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.html">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>平台管理</span></li>
								<li><span>Data Server 狀態</span></li>
							</ol>
							&nbsp;&nbsp;&nbsp;
						</div>
					</header>
					
						<!-- 查詢 area -->					

						<section class="panel panel-featured-success panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-list"> 查詢結果</i></h3>
							</header>						
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-condensed mb-none" id="tableData">
										<thead>
											<tr>												
												<th class="text-center">IP</th>
												<th class="text-center">名稱</th>
												<th class="text-center">Server狀態</th>
												<th class="text-center">CPU使用率(%)</th>
												<th class="text-center">可使用記憶體(Byte)</th>
												<th class="text-center">硬碟剩餘空間(Byte)</th>
												<th class="text-center">最後更新時間</th>												
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
									<script id="detail-template" type="text/x-handlebars-template">
											{{#each ds}}
											<tr>
												<td>{{server}}</td>
												<td>{{name}}</td>
												<td>{{status}}</td>
												<td>{{cpu}}</td>
												<td>{{mem}}</td>
												<td>{{space}}</td>
												<td>{{time}}</td>											
											</tr>
											{{/each}}
									</script>		
								</div>
							</div>
						</section>
						<!-- end 查詢 -->
					<!-- end: page -->
				</section>
			</div>

		</section>

<%@include file="../include/includeFooterJs.jsp"%>
		<script src="js/dataServer/dataServer.js?<c:out value="${applicationScope.js_version}"/>"></script>
	</body>
</html>