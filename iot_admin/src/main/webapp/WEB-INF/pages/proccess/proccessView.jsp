<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<link href="css/custom/proccess.css" rel="stylesheet" />
<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>
			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>歷程分析</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="./index"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>品質管理</span></li>
							<li><span>歷程分析</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- 查詢 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-search"> 查詢條件</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<form id="queryForm">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">歷程開始時間</label> <input id="sd"
											name="sd" class="datepicker form-control">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">歷程結束時間</label> <input id="ed"
											name="ed" class="datepicker form-control">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label">&nbsp;</label>
										<button class="btn btn-primary form-control">
											<i class="el el-search"></i> 查詢
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>
				<!-- end 查詢 -->
				
				<section class="panel panel-featured-success panel-featured"
					role="query" style="display: none">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-list"> 查詢結果</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table id="data-table"
										class="table table-bordered table-striped table-condensed mb-none">
										<thead>
											<tr>
												<th class="text-center">Tag名稱</th>
												<th class="text-center">歷程開始時間</th>
												<th class="text-center">歷程結束時間</th>												
												<th class="text-center">檢視</th>												
											</tr>
										</thead>
										<tbody id="data-list">											
										</tbody>										
									</table>
									<script id="data-list-template"
										type="text/x-handlebars-template">
										{{#each ds}}
											<tr class="fix-tr">														
												<td class="fix-td-2">{{name}}</td>
												<td class="fix-td-1 text-right">{{sd}}</td>
												<td class="fix-td-1 text-right">{{ed}}</td>
												<td class="fix-td-1 text-center"><button type="button" class="btn-sm btn btn-info" onclick="module.getItemData('{{mac}}', '{{sd}}', '{{ed}}');"><i class="el el-eye-open"></i></button></td>																											
											</tr>
										{{/each}}
									</script>									
								</div>
							</div>
						</div>						
					</div>
				</section>
				
				<section class="panel panel-featured-success panel-featured"
					role="chart" style="display: none">					
					<div class="panel-body">
						<div class="row">
							<div id="chartContainer" class="col-md-12">
								<!-- 圖表區  -->
								<div class="svg-container">
						            <svg></svg>
						        </div>
							</div>
						</div>						
					</div>
				</section>
				
				<!-- end: page -->
			</section>
		</div>

	</section>

	<%@include file="../include/includeFooterJs.jsp"%>
	<script
		src="js/common/dateFormat.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/common/d3.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/proccess/proccess.js?<c:out value="${applicationScope.js_version}"/>"></script>
</body>
</html>