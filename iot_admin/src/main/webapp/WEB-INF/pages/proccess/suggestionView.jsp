<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<!-- Specific Page Vendor CSS -->
<link href="css/custom/fixed-header-table.css" rel="stylesheet" />
<link rel="stylesheet"
	href="itri_iot_q/vendor/pnotify/pnotify.custom.css">
<link rel="stylesheet"
	href="itri_iot_q/vendor/jquery-datatables-bs3/assets/css/datatables.css">
<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>
			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>警示設定（依標準建議）</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>品質管理</span></li>
							<li><span>警示設定（依標準建議）</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- 編輯 area -->
				<section class="panel panel-featured-info panel-featured"
					role="update" style="display: none;">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-edit"> 編輯視窗</i>
						</h3>
					</header>
					<div class="panel-body">
						<form class="form-horizontal form-bordered" id="updateForm">
							<div class="form-group" style="display: none">
								<label class="col-md-2 control-label">alertSettingId</label>
								<div class="col-md-6">
									<input name="alertSettingId" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">警示名稱</label>
								<div class="col-md-6">
									<input name="disName" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">警示上限值</label>
								<div class="col-md-6">
									<input name="high" class="form-control" type="number"
										step="0.01">
								</div>
								<div class="col-md-4">
									<button class="btn btn-info btn-lg"
										onclick="ui.showSuggestionDialog();">
										<i class="el el-search"></i> 標準建議查詢
									</button>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">警示下限值</label>
								<div class="col-md-6">
									<input name="low" class="form-control" type="number"
										step="0.01">
								</div>
								<div class="col-md-4">
									<button class="btn btn-info btn-lg"
										onclick="ui.showSuggestionDialog();">
										<i class="el el-search"></i> 標準建議查詢
									</button>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">異常持續多久發送通知（秒）</label>
								<div class="col-md-6">
									<div data-plugin-spinner
										data-plugin-options='{ "value":60, "min": 0 }'>
										<div class="input-group" style="width: 150px;">
											<input name="afterMilliSecondAlert" type="text"
												class="spinner-input form-control" maxlength="3">
											<div class="spinner-buttons input-group-btn">
												<button type="button" class="btn btn-default spinner-up">
													<i class="fa fa-angle-up"></i>
												</button>
												<button type="button" class="btn btn-default spinner-down">
													<i class="fa fa-angle-down"></i>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">輕度警示通知對象（Email）</label>
								<div class="col-md-6">
									<textarea name="emails" class="form-control" rows="3"></textarea>
									<p>
										請以
										<code>;</code>
										分隔，範例：abc@example.com;def@example.com;...
									</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">緊急警示通知對象（Email）</label>
								<div class="col-md-6">
									<textarea name="criticalEmails" class="form-control" rows="3"></textarea>
									<p>
										當數值超出警示邊界值3單位時，需要通知的對象。<br /> 請以
										<code>;</code>
										分隔，範例：abc@example.com;def@example.com;...
									</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">公司帳號</label>
								<div class="col-md-6">
									<input name="companyId" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="text-center">
								<button class="btn btn-default btn-lg"
									onclick="ui.gotoMode('MODE_UPDATE_CANCEL');">
									<i class="el el-return-key"></i> 取消退出
								</button>
								<button class="btn btn-info btn-lg"
									onclick="ui.showConfirmDialog('CONFIRM_UPDATE');">
									<i class="el el-ok"></i> 確定編輯
								</button>
							</div>
						</form>
					</div>
				</section>
				<!-- end 編輯 area -->

				<!-- 新增 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="create" style="display: none;">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-file-new"> 新增視窗</i>
						</h3>
					</header>
					<div class="panel-body">
						<form class="form-horizontal form-bordered" id="createForm">
							<div class="form-group">
								<label class="col-md-2 control-label">警示名稱</label>
								<div class="col-md-6">
									<input name="disName" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">警示上限值</label>
								<div class="col-md-6">
									<input name="high" class="form-control" type="number"
										step="0.01">
								</div>
								<div class="col-md-4">
									<button class="btn btn-info btn-lg"
										onclick="ui.showSuggestionDialog();">
										<i class="el el-search"></i> 標準建議查詢
									</button>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">警示下限值</label>
								<div class="col-md-6">
									<input name="low" class="form-control" type="number"
										step="0.01">
								</div>
								<div class="col-md-4">
									<button class="btn btn-info btn-lg"
										onclick="ui.showSuggestionDialog();">
										<i class="el el-search"></i> 標準建議查詢
									</button>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">異常持續多久發送通知（秒）</label>
								<div class="col-md-6">
									<div data-plugin-spinner
										data-plugin-options='{ "value":60, "min": 0 }'>
										<div class="input-group" style="width: 150px;">
											<input name="afterMilliSecondAlert" type="text"
												class="spinner-input form-control" maxlength="3">
											<div class="spinner-buttons input-group-btn">
												<button type="button" class="btn btn-default spinner-up">
													<i class="fa fa-angle-up"></i>
												</button>
												<button type="button" class="btn btn-default spinner-down">
													<i class="fa fa-angle-down"></i>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">輕度警示通知對象（Email）</label>
								<div class="col-md-6">
									<textarea name="emails" class="form-control" rows="3"></textarea>
									<p>
										請以
										<code>;</code>
										分隔，範例：abc@example.com;def@example.com;...
									</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">緊急警示通知對象（Email）</label>
								<div class="col-md-6">
									<textarea name="criticalEmails" class="form-control" rows="3"></textarea>
									<p>
										當數值超出警示邊界值3單位時，需要通知的對象。<br /> 請以
										<code>;</code>
										分隔，範例：abc@example.com;def@example.com;...
									</p>
								</div>
							</div>
							<!-- 
							<div class="form-group">
								<label class="col-md-2 control-label">警示通知對象（手機）</label>
								<div class="col-md-6">
									<textarea name="phones" class="form-control" rows="3"></textarea>
									<p>
										請以
										<code>;</code>
										分隔，範例：0900000000;0912345678;...
									</p>
								</div>
							</div>
							 -->
							<div class="form-group">
								<label class="col-md-2 control-label">公司帳號</label>
								<div class="col-md-6">
									<input name="companyId" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="text-center">
								<button class="btn btn-default btn-lg"
									onclick="ui.gotoMode('MODE_CREATE_CANCEL');">
									<i class="el el-return-key"></i> 取消退出
								</button>
								<button class="btn btn-primary btn-lg"
									onclick="ui.showConfirmDialog('CONFIRM_CREATE');">
									<i class="el el-ok"></i> 確定新增
								</button>
							</div>
						</form>
					</div>
				</section>
				<!-- end 新增area -->

				<!-- 查詢 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-search"> 警示設定</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<button class="btn btn-primary form-control"
										onclick="ui.gotoMode('MODE_CREATE')">
										<i class="el el-file-new"></i> 新增
									</button>
								</div>
							</div>
						</div>
						<div class="clearfix">
							<br />
						</div>
						<div class="row">
							<div class="col-md-12">
								<div>
									<table id="setting-table"
										class="fix-table table-bordered table-striped table-condensed mb-none">
										<thead class="fix-thead">
											<tr class="fix-tr">
												<th class="fix-th">警示名稱</th>
												<th class="fix-th">警示上限值</th>
												<th class="fix-th">警示下限值</th>
												<th class="fix-th">持續幾秒後通知</th>
												<th class="fix-th">輕度警示通知對象</th>
												<th class="fix-th">緊急警示通知對象</th>
												<th class="fix-th"></th>
											</tr>
										</thead>
										<tbody id="setting-list" class="fix-tbody"></tbody>
									</table>
									<script id="setting-template" type="text/x-handlebars-template">
										{{#each ds}}
											<tr class="fix-tr">														
												<td class="fix-td">{{disName}}</td>
												<td class="fix-td">{{high}}</td>
												<td class="fix-td">{{low}}</td>
												<td class="fix-td">{{afterSecondAlert}}</td>
												<td class="fix-td">{{emails}}</td>
												<td class="fix-td">{{criticalEmails}}</td>		
												<td class="fix-td text-center">
													<button type="button" class="btn-sm btn btn-info" onclick="ui.gotoMode('MODE_UPDATE','{{alertSettingId}}');"><i class="el el-pencil"></i></button>
													<button type="button" class="btn-sm btn btn-danger" onclick="ui.showConfirmDialog('CONFIRM_DELETE','{{alertSettingId}}');"><i class="el el-trash"></i></button>
												</td>															
											</tr>
										{{/each}}
									</script>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="panel panel-featured-primary panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-search"> 警示關聯</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<form id="queryForm">
								<input name="companyId" type="hidden">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">tag名稱</label> <input name="name"
											class="form-control">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label">&nbsp;</label>
										<button class="btn btn-primary form-control">
											<i class="el el-search"></i> 查詢
										</button>
									</div>
								</div>
							</form>
						</div>
						<div class="clearfix">
							<br />
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table id="tag-table"
										class="table fix-table table-bordered table-striped table-condensed mb-none">
										<thead class="fix-thead">
											<tr class="fix-tr">
												<th class="fix-th">tag名稱</th>
												<th class="fix-th">本體電量警示</th>
												<th class="fix-th">本體溫度警示</th>
												<th class="fix-th">本體濕度警示</th>
												<th class="fix-th">port1警示</th>
												<th class="fix-th">port2警示</th>
												<th class="fix-th"></th>
											</tr>
										</thead>
										<tbody id="tag-list" class="fix-tbody"></tbody>
									</table>
									<script id="rel-template" type="text/x-handlebars-template">
										{{#each ds}}
											<tr class="fix-tr">														
												<td class="fix-td">{{name}}</td>
												<td class="fix-td">
													<select id="{{mac}}-0007" class="form-control mb-md" name="alertSettingId">																										
													</select>
												</td>
												<td class="fix-td">
													<select id="{{mac}}-0001" class="form-control mb-md" name="alertSettingId">																										
													</select>
												</td>
												<td class="fix-td">
													<select id="{{mac}}-0002" class="form-control mb-md" name="alertSettingId">																										
													</select>
												</td>
												<td class="fix-td">
													<select id="{{mac}}-0101" class="form-control mb-md" name="alertSettingId" {{#if invaild1}}readonly{{/if}}>																										
													</select>
												</td>
												<td class="fix-td">
													<select id="{{mac}}-0201" class="form-control mb-md" name="alertSettingId" {{#if invaild2}}readonly{{/if}}>																										
													</select>
												</td>													
												<td class="text-center fix-td">
													<button type="button" class="btn-sm btn btn-info" onclick="ui.showConfirmDialog('CONFIRM_UPDATE_REL','{{mac}}');"><i class="el el-pencil"></i></button>
												</td>															
											</tr>
										{{/each}}
									</script>
									<script id="alert-option-template"
										type="text/x-handlebars-template">
										{{#each ds}}
												<option value="{{alertSettingId}}">{{disName}}</option>
										{{/each}}
									</script>
								</div>
							</div>
						</div>
					</div>
				</section>

				<!-- end 查詢 -->

				<!-- 標準建議 dialog -->
				<div id="suggestionDialog" class="modal fade " role="dialog">
					<div class="modal-dialog modal-body" style="width: 100%;">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">查詢標準建議</h4>
							</div>
							<div>
								<div class="col-md-12">

									<table id="suggestion-table"
										class="table table-bordered table-striped mb-none" style="table-layout: fixed;">
										<thead>
											<tr>
												<th class="text-center">產品類別</th>
												<th class="text-center">類型</th>
												<th class="text-center">名稱</th>
												<th class="text-center">儲存溫度（℃）</th>
												<th class="text-center">儲存濕度（%）</th>
												<th class="text-center">長途運輸（直接運輸）（℃）</th>
												<th class="text-center">短途配送（不超過5小時）（℃）</th>
												<th class="text-center">展售櫃溫度（℃）</th>
											</tr>
										</thead>
										<tbody id="suggestion-list">
										</tbody>
									</table>
									<script id="suggestion-list-template"
										type="text/x-handlebars-template">
										{{#each ds}}
											<tr class="fix-tr">														
												<td class="fix-td-2">{{category}}</td>
												<td class="fix-td-1 text-right">{{type}}</td>
												<td class="fix-td-1 text-right">{{name}}</td>																																							
												<td class="fix-td-1 text-right"><a onclick='module.setAlert({{#if temperatureLow}}{{temperatureLow}}{{else}}"null"{{/if}},{{#if temperatureUp}}{{temperatureUp}}{{else}}"null"{{/if}})'>{{temperature}}</a></td>																																							
												<td class="fix-td-1 text-right"><a onclick='module.setAlert({{#if humidityLow}}{{humidityLow}}{{else}}"null"{{/if}},{{#if humidityUp}}{{humidityUp}}{{else}}"null"{{/if}})'>{{humidity}}</a></td>
												<td class="fix-td-1 text-right"><a onclick='module.setAlert({{#if longTermTempLow}}{{longTermTempLow}}{{else}}"null"{{/if}},{{#if longTermTempUp}}{{longTermTempUp}}{{else}}"null"{{/if}})'>{{longTermTemp}}</a></td>																																							
												<td class="fix-td-1 text-right"><a onclick='module.setAlert({{#if shortTermTempLow}}{{shortTermTempLow}}{{else}}"null"{{/if}},{{#if shortTermTempUp}}{{shortTermTempUp}}{{else}}"null"{{/if}})'>{{shortTermTemp}}</a></td>
												<td class="fix-td-1 text-right"><a onclick='module.setAlert({{#if exhibitionTempLow}}{{exhibitionTempLow}}{{else}}"null"{{/if}},{{#if exhibitionTempUp}}{{exhibitionTempUp}}{{else}}"null"{{/if}})'>{{exhibitionTemp}}</a></td>																																						
											</tr>
										{{/each}}
									</script>

								</div>
							</div>
							<div class="modal-footer center">
								<button class="btn btn-default btn-lg modal-dismiss"
									data-dismiss="modal">取消</button>
							</div>
						</div>
					</div>
				</div>

				<!-- end 標準建議 dialog -->

				<!-- end: page -->
			</section>
		</div>

	</section>
	<%@include file="../include/includeFooterJs.jsp"%>
	<script
		src="itri_iot_q/vendor/pnotify/pnotify.custom.js"></script>
	<script
		src="itri_iot_q/vendor/nanoscroller/nanoscroller.js"></script>
	<script
		src="itri_iot_q/vendor/fuelux/js/spinner.js"></script>


	<script
		src="itri_iot_q/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
	<script
		src="itri_iot_q/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>


	<script
		src="js/common/array.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/proccess/suggestion.js?<c:out value="${applicationScope.js_version}"/>"></script>
</body>
</html>