<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>
			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>場域類別設定</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>品質管理</span></li>
							<li><span>場域類別設定</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- start: page -->
				<!-- 編輯 area -->
				<section class="panel panel-featured-info panel-featured"
					role="update" style="display: none;">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-edit"> 編輯視窗</i>
						</h3>
					</header>
					<div class="panel-body">
						<form class="form-horizontal form-bordered" id="updateForm"
							method="post">
							<div class="form-group" style="display: none;">
								<label class="col-md-2 control-label">系統編碼</label>
								<div class="col-md-6">
									<input name="placeId" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">公司帳號</label>
								<div class="col-md-6">
									<input name="companyId" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">場域類別名稱</label>
								<div class="col-md-6">
									<input name="disName" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">場域類別說明</label>
								<div class="col-md-6">
									<input name="description" class="form-control" type="text">
								</div>
							</div>							
							<div class="text-center">
								<button class="btn btn-default btn-lg"
									onclick="ui.gotoMode('MODE_UPDATE_CANCEL');">
									<i class="el el-return-key"></i> 取消退出
								</button>
								<button class="btn btn-info btn-lg"
									onclick="ui.showConfirmDialog('CONFIRM_UPDATE');">
									<i class="el el-ok"></i> 確定編輯
								</button>
							</div>
						</form>
					</div>
				</section>
				<!-- end 編輯 area -->

				<!-- 新增 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="create" style="display: none;">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-file-new"> 新增視窗</i>
						</h3>
					</header>
					<div class="panel-body">
						<form class="form-horizontal form-bordered" id="createForm">
							<div class="form-group" style="display: none;">
								<label class="col-md-2 control-label">系統編碼</label>
								<div class="col-md-6">
									<input name="placeId" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">公司帳號</label>
								<div class="col-md-6">
									<input name="companyId" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">場域類別名稱</label>
								<div class="col-md-6">
									<input name="disName" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">場域類別說明</label>
								<div class="col-md-6">
									<input name="description" class="form-control" type="text">
								</div>
							</div>	
							<div class="text-center">
								<button class="btn btn-default btn-lg"
									onclick="ui.gotoMode('MODE_CREATE_CANCEL');">
									<i class="el el-return-key"></i> 取消退出
								</button>
								<button class="btn btn-primary btn-lg"
									onclick="ui.showConfirmDialog('CONFIRM_CREATE');">
									<i class="el el-ok"></i> 確定新增
								</button>
							</div>
						</form>
					</div>
				</section>
				<!-- end 新增area -->

				<!-- 查詢 area -->			
				<section class="panel panel-featured-success panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-list"> 場域類別列表</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="table-responsive">
							<table
								class="table table-bordered table-striped table-condensed mb-none"
								id="tableData">
								<thead>
									<tr>
										<th class="text-center">場域類別</th>
										<th class="text-center">說明</th>
										<th class="text-center"></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>代收點</td>
										<td>超商</td>
										<td class="text-center">
											<button type="button" class="btn-sm btn btn-info"
												onclick="ui.gotoMode('MODE_UPDATE','{{placeId}}');">
												<i class="el el-pencil"></i>
											</button>
											<button type="button" class="btn-sm btn btn-danger"
												onclick="ui.showConfirmDialog('CONFIRM_DELETE','{{placeId}}');">
												<i class="el el-trash"></i>
											</button>
										</td>
									</tr>
									<tr>
										<td>轉運站</td>
										<td></td>
										<td class="text-center">
											<button type="button" class="btn-sm btn btn-info"
												onclick="ui.gotoMode('MODE_UPDATE','{{placeId}}');">
												<i class="el el-pencil"></i>
											</button>
											<button type="button" class="btn-sm btn btn-danger"
												onclick="ui.showConfirmDialog('CONFIRM_DELETE','{{placeId}}');">
												<i class="el el-trash"></i>
											</button>
										</td>
									</tr>
									<tr>
										<td>營業所</td>
										<td></td>
										<td class="text-center">
											<button type="button" class="btn-sm btn btn-info"
												onclick="ui.gotoMode('MODE_UPDATE','{{placeId}}');">
												<i class="el el-pencil"></i>
											</button>
											<button type="button" class="btn-sm btn btn-danger"
												onclick="ui.showConfirmDialog('CONFIRM_DELETE','{{placeId}}');">
												<i class="el el-trash"></i>
											</button>
										</td>
									</tr>									
								</tbody>
							</table>
							<script id="detail-template" type="text/x-handlebars-template">
								{{#each ds}}
									<tr>
										<td>{{disName}}</td>
										<td>{{companyId}}</td>
										<!--<td class="text-center" style="vertical-align: middle;"><img src="./place/img/{{companyId}}/{{placeId}}" width="50px"/></td>
										<td class="text-center" style="vertical-align: middle;"><img src="./place/icon/{{companyId}}/{{placeId}}" width="50px"/></td>-->
										<td class="text-center">
										<button type="button" class="btn-sm btn btn-info" onclick="ui.gotoMode('MODE_UPDATE','{{placeId}}');"><i class="el el-pencil"></i></button>
										<button type="button" class="btn-sm btn btn-danger" onclick="ui.showConfirmDialog('CONFIRM_DELETE','{{placeId}}');"><i class="el el-trash"></i></button>
										</td>
									</tr>
								{{/each}}
							</script>
							<br />
							<div class="col-md-offset-5 col-md-7 text-right">
								<form class="form-inline">
									<div class="form-group">
										<label>跳至</label> <select class="form-control" id="qpage">
										</select> <label>，共有<span id="totalRecord">4</span>筆資料
										</label>
									</div>
								</form>
							</div>
							<!-- </div> -->
						</div>
					</div>
				</section>
				<!-- end 查詢 -->
				<!-- end: page -->
			</section>
		</div>

	</section>

	<%@include file="../include/includeFooterJs.jsp"%>
</body>
</html>