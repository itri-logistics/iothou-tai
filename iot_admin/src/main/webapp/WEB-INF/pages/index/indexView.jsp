<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
 <!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
	<body>
		<section class="body">

			<!-- start: header -->
<%@include file="../include/includeTopHeader.jsp"%>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
<%@include file="../include/includeMenu.jsp"%>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>首頁</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="./index">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>首頁</span></li>
							</ol>
							&nbsp;&nbsp;&nbsp;
						</div>
					</header>
						
						<!-- 查詢 area -->
						<section class="panel panel-featured-primary panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-search"> 首頁</i></h3>
							</header>						
							<div class="panel-body">
								從左列選單選取功能
							</div>
						</section>

						<!-- end 查詢 -->
					<!-- end: page -->
				</section>
			</div>

		</section>

<%@include file="../include/includeFooterJs.jsp"%>		
	</body>
</html>