<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
 <!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
	<body class="login-wrap">
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">			
				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-center">
						<img src="img/logo.png" alt="品質管理平台">
						<!-- <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i>後端管理平台登入</h2> -->
					</div>
					<div class="panel-body">
						<form action="j_spring_security_check" method="post" id="loginForm">
							<div class="form-group mb-lg">
								<label>公司帳號</label>
								<div class="input-group input-group-icon">
									<input id="companyId" type="text" class="form-control input-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>
							<div class="form-group mb-lg">
								<label>會員帳號</label>
								<div class="input-group input-group-icon">
									<input id="j_username" name="j_username" type="hidden" />
									<input id="loginName" type="text" class="form-control input-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">密碼</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="j_password" type="password" class="form-control input-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>
						</form>
							<div class="row">
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
									</div>
								</div>
								<div class="col-sm-12">
									<button onclick="login();"  class="btn btn-primary hidden-xs btn-lg btn-block">登入</button>
									<button onclick="login();" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">登入</button>
								</div>
							</div>
						
					</div>
				</div>
				<p class="text-center text-muted mt-md mb-md">&copy; Copyright 2016. All Rights Reserved.</p>
			</div>
		</section>
<%@include file="../include/includeFooterJs.jsp"%>
		<script src="js/index/login.js?<c:out value="${applicationScope.js_version}"/>"></script>
	</body>
</html>