<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>
			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>日結庫存查詢</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>資材管理系統</span></li>
							<li><span>日結庫存查詢</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- start: page -->

				<!-- 查詢 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-search"> 查詢條件</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<form id="queryForm">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">紀錄日期</label> <input id="ed"
											name="ed" class="datepicker form-control">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="control-label">&nbsp;</label>
										<button class="btn btn-primary form-control">
											<i class="el el-search"></i> 查詢
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</section>

				<script id="place-option-template" type="text/x-handlebars-template">
					{{#each ds}}											
						<option>{{disName}}</option>
					{{/each}}
				</script>

				<section class="panel panel-featured-success panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-list"> 查詢結果</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive" style="max-height: 300px; overflow: auto">
									<table id="inventory-table"
										class="table table-bordered table-striped table-condensed mb-none"
										style="display: none">
										<thead>
											<tr>
												<th>地點</th>
												<th>類型</th>
												<th>數量</th>
												<th>時間</th>
												<th>公司</th>
											</tr>
										</thead>
										<tbody id="inventory-list"></tbody>
									</table>
									<script id="inventory-template"
										type="text/x-handlebars-template">
										{{#each ds}}
											<tr>														
												<td>{{placeId}}</td>
												<td>{{palletsType}}</td>
												<td>{{QTY}}</td>
												<td>{{updateDate}}</td>																
												<td>{{companyId}}</td>	
											</tr>
										{{/each}}
									</script>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- end 查詢 -->
				<!-- end: page -->
			</section>
		</div>

	</section>

	<%@include file="../include/includeFooterJs.jsp"%>
	<script
		src="itri_iot_q/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script
		src="itri_iot_q/vendor/nanoscroller/nanoscroller.js"></script>
	<!--
	<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
	<script src="<c:out value="${applicationScope.lib_link}"/>/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
	  -->

	<script
		src="js/common/dateFormat.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/common/array.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/pallets/inventoryDailyReport.js?<c:out value="${applicationScope.js_version}"/>"></script>

</body>
</html>