<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
 <!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
	<body>
		<section class="body">

			<!-- start: header -->
<%@include file="../include/includeTopHeader.jsp"%>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
<%@include file="../include/includeMenu.jsp"%>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>無身分進出查詢</h2>
					
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>資材管理系統</span></li>
							<li><span>無身分進出查詢</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
					</header>

					<!-- start: page -->
						<!-- 查詢 area -->
						<section class="panel panel-featured-primary panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-search"> 查詢條件</i></h3>
							</header>						
							<div class="panel-body">
								<div class="row">
									<form id="queryForm">
										<div class="col-md-3">
											<label class="control-label">所屬場域</label>
											<select class="form-control mb-md" name="placeId">																										
											</select>
											<script id="place-option-template" type="text/x-handlebars-template">
											{{#each ds}}
												<option value="{{placeId}}">{{disName}}</option>
											{{/each}}
											</script>	
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">紀錄日期</label> <input id="ed"
												name="ed" class="datepicker form-control">
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">&nbsp;</label>
												<button class="btn btn-primary form-control"><i class="el el-search"></i> 查詢</button>
											</div>
										</div>
									</form>									
								</div>							
							</div>
						</section>


						<section class="panel panel-featured-success panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-list"> 查詢結果</i></h3>
							</header>						
							<div class="panel-body">
								<div class="table-responsive" style="max-height: 450px; overflow: auto">
									<table class="table table-bordered table-striped table-condensed mb-none" id="dailyReport-table">
										<thead>
											<tr>
												<th class="text-center">場域名稱</th>
												<th class="text-center">進/出</th>
												<th class="text-center">時間</th>											
											</tr>
										</thead>
										<tbody id="dailyReport-list"></tbody>
									</table>
									<script id="dailyReport-template" type="text/x-handlebars-template">
											{{#each ds}}
											<tr>
												<td>{{place}}</td>
												<td>{{in_out}}</td>
												<td>{{updateDate}}</td>
											</tr>
											{{/each}}
									</script>
									
									<br/>
									<div class="col-md-offset-5 col-md-7 text-right">
										<form class="form-inline">
											<div class="form-group">												
												<label>共有<span id="totalRecord">0</span>筆資料</label>
											</div>
										</form>
									</div>
								</div>
							</div>
						</section>
						<!-- end 查詢 -->
					<!-- end: page -->
				</section>
			</div>

		</section>

<%@include file="../include/includeFooterJs.jsp"%>
		<script src="js/pallets/noIdentityPalletsDailyReport.js?<c:out value="${applicationScope.js_version}"/>"></script>
	</body>
</html>