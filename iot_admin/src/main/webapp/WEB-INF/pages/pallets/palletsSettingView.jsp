<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
 <!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
	<body>
		<section class="body">

			<!-- start: header -->
<%@include file="../include/includeTopHeader.jsp"%>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
<%@include file="../include/includeMenu.jsp"%>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>無身分進出查詢</h2>
					
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>資材管理系統</span></li>
							<li><span>無身分進出查詢</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
					</header>

					<!-- start: page -->
						<!-- 查詢 area -->
						<section class="panel panel-featured-primary panel-featured" role="query">
							<header class="panel-heading">
							<h3 class="panel-title"><i class="el el-search"> 設定TAG</i></h3>
							</header>						
							<div class="panel-body">
								<div class="row">
									<form id="queryForm">
										<div class="form-group">
											<label class="col-md-1 control-label">Tag Id</label>
											<div class="col-md-5">
											<textarea name="palletsTags" class="form-control" rows="5"></textarea>
											<p>
											請以
												<code>;</code>
												分隔，範例：xxxx;xxxx;xxxx...
											</p>
										</div>
										</div>
										<div class="col-xs-4">
											<div class="form-group">
												<label class="col-md-2 control-label">棧板類型</label>
													<div class="col-md-6">
														<select class="form-control mb-md" name="palletsType">																										
														</select>
													<script id="type-option-template" type="text/x-handlebars-template">
													{{#each ds}}
														<option value="{{id}}">{{name}}</option>
													{{/each}}
												</script>	
											</div>
										</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">&nbsp;</label>
												<button class="btn btn-primary form-control"><i class="el el-search"></i> 設定</button>
											</div>
										</div>
									</form>									
								</div>							
							</div>
						</section>
					<!-- end: page -->
				</section>
			</div>

		</section>

<%@include file="../include/includeFooterJs.jsp"%>
		<script src="js/pallets/palletsSetting.js?<c:out value="${applicationScope.js_version}"/>"></script>
	</body>
</html>