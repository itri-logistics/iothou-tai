<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>
<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>
			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>TAG Type設定</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>資材管理系統</span></li>
							<li><span>TAG Type設定</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- 編輯 area -->
				<section class="panel panel-featured-info panel-featured"
					role="update" style="display: none;">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-edit"> 編輯視窗</i>
						</h3>
					</header>
					<div class="panel-body">
						<form class="form-horizontal form-bordered" id="updateForm">
							<div class="form-group" style="display: none">
								<label class="col-md-2 control-label">id</label>
								<div class="col-md-6">
									<input name="id" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">名稱</label>
								<div class="col-md-6">
									<input name="name" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">顏色</label>
								<div class="col-md-6">
									<input name="color" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">客戶</label>
								<div class="col-md-6">
									<input name="customer" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">材質</label>
								<div class="col-md-6">
									<input name="meterial" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">屬性</label>
								<div class="col-md-6">
									<select class="form-control mb-md" name="property">																										
										</select>
										<script id="property-option-template" type="text/x-handlebars-template">
										{{#each ds}}
											<option value="{{pid}}">{{name}}</option>
										{{/each}}
										</script>	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">規格</label>
								<div class="col-md-6">
									<select class="form-control mb-md" name="format">																										
										</select>
										<script id="format-option-template" type="text/x-handlebars-template">
										{{#each ds}}
											<option value="{{pid}}">{{name}}</option>
										{{/each}}
										</script>	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">類型</label>
								<div class="col-md-6">
									<select class="form-control mb-md" name="type">																										
										</select>
										<script id="type-option-template" type="text/x-handlebars-template">
										{{#each ds}}
											<option value="{{pid}}">{{name}}</option>
										{{/each}}
										</script>	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">公司帳號</label>
								<div class="col-md-6">
									<input name="companyId" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="text-center">
								<button class="btn btn-default btn-lg"
									onclick="ui.gotoMode('MODE_UPDATE_CANCEL');">
									<i class="el el-return-key"></i> 取消退出
								</button>
								<button class="btn btn-info btn-lg"
									onclick="ui.showConfirmDialog('CONFIRM_UPDATE');">
									<i class="el el-ok"></i> 確定編輯
								</button>
							</div>
						</form>
					</div>
				</section>
				<!-- end 編輯 area -->

				<!-- 新增 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="create" style="display: none;">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-file-new"> 新增視窗</i>
						</h3>
					</header>
					<div class="panel-body">
						<form class="form-horizontal form-bordered" id="createForm">
							<div class="form-group">
								<label class="col-md-2 control-label">名稱</label>
								<div class="col-md-6">
									<input name="name" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">顏色</label>
								<div class="col-md-6">
									<input name="color" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">客戶</label>
								<div class="col-md-6">
									<input name="customer" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">材質</label>
								<div class="col-md-6">
									<input name="meterial" class="form-control" type="text">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">屬性</label>
								<div class="col-md-6">
									<select class="form-control mb-md" name="property">																										
										</select>
										<script id="property-option-template" type="text/x-handlebars-template">
										{{#each ds}}
											<option value="{{pid}}">{{name}}</option>
										{{/each}}
										</script>	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">規格</label>
								<div class="col-md-6">
									<select class="form-control mb-md" name="format">																										
										</select>
										<script id="format-option-template" type="text/x-handlebars-template">
										{{#each ds}}
											<option value="{{pid}}">{{name}}</option>
										{{/each}}
										</script>	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">類型</label>
								<div class="col-md-6">
									<select class="form-control mb-md" name="type">																										
										</select>
										<script id="type-option-template" type="text/x-handlebars-template">
										{{#each ds}}
											<option value="{{pid}}">{{name}}</option>
										{{/each}}
										</script>	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">公司帳號</label>
								<div class="col-md-6">
									<input name="companyId" class="form-control" type="text"
										readonly="readonly">
								</div>
							</div>
							<div class="text-center">
								<button class="btn btn-default btn-lg"
									onclick="ui.gotoMode('MODE_CREATE_CANCEL');">
									<i class="el el-return-key"></i> 取消退出
								</button>
								<button class="btn btn-primary btn-lg"
									onclick="ui.showConfirmDialog('CONFIRM_CREATE');">
									<i class="el el-ok"></i> 確定新增
								</button>
							</div>
						</form>
					</div>
				</section>
				<!-- end 新增area -->

				<!-- 查詢 area -->
				<section class="panel panel-featured-primary panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-search"> TAG Type設定</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<button class="btn btn-primary form-control"
										onclick="ui.gotoMode('MODE_CREATE')">
										<i class="el el-file-new"></i> 新增
									</button>
								</div>
							</div>
						</div>
						<div class="clearfix">
							<br />
						</div>
						<div class="row">
							<div class="col-md-12">
								<div style="max-height: 300px; overflow: auto">
									<table id="setting-table"
										class="table table-bordered table-striped table-condensed mb-none">
										<thead>
											<tr>
												<th>名稱</th>
												<th>顏色</th>
												<th>客戶</th>
												<th>材質</th>
												<th>屬性</th>
												<th>規格</th>
												<th>類型</th>
												<th>編輯/刪除</th>
											</tr>
										</thead>
										<tbody id="setting-list"></tbody>
									</table>
									<script id="setting-template" type="text/x-handlebars-template">
										{{#each ds}}
											<tr>												
												<td>{{name}}</td>
												<td>{{color}}</td>
												<td>{{customer}}</td>
												<td>{{meterial}}</td>
												<td>{{property}}</td>
												<td>{{format}}</td>
												<td>{{type}}</td>
												<td class="text-center">
													<button type="button" class="btn-sm btn btn-info" onclick="ui.gotoMode('MODE_UPDATE','{{id}}');"><i class="el el-pencil"></i></button>
													<button type="button" class="btn-sm btn btn-danger" onclick="ui.showConfirmDialog('CONFIRM_DELETE','{{id}}');"><i class="el el-trash"></i></button>
												</td>																		
											</tr>
										{{/each}}
									</script>
								</div>
							</div>
						</div>
					</div>
				</section>

			</section>
		</div>

	</section>
	<%@include file="../include/includeFooterJs.jsp"%>
	<script
		src="itri_iot_q/vendor/nanoscroller/nanoscroller.js"></script>
	<script
		src="itri_iot_q/vendor/fuelux/js/spinner.js"></script>
	<script
		src="js/common/array.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/pallets/palletsTypeSetting.js?<c:out value="${applicationScope.js_version}"/>"></script>
</body>
</html>