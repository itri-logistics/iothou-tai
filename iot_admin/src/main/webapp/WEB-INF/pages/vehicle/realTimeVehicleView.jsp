<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<!-- sidebar-light -->
<html class="fixed sidebar-light" lang="zh-Hant-TW">
<%@include file="../include/includeHeader.jsp"%>

<body>
	<section class="body">

		<!-- start: header -->
		<%@include file="../include/includeTopHeader.jsp"%>
		<!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<%@include file="../include/includeMenu.jsp"%>
			<!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2>車輛即時位置</h2>

					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="index.html"> <i class="fa fa-home"></i>
							</a></li>
							<li><span>車輛即時位置</span></li>
						</ol>
						&nbsp;&nbsp;&nbsp;
					</div>
				</header>

				<!-- start: page -->
				<!-- 查詢 -->
				<section class="panel panel-featured-success panel-featured"
					role="query">
					<header class="panel-heading">
						<h3 class="panel-title">
							<i class="el el-list"> 車輛即時位置地圖</i>
						</h3>
					</header>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-2">
								<div class="form-group">
									<button class="form-control btn btn-primary"
										onclick="module.findNewestData();">
										<i class="el el-refresh"></i> 重新整理
									</button>
								</div>
							</div>
						</div>
						<div class="clearfix">
							<br />
						</div>
						<div class="row">
							<div class="col-md-12" style="min-height: 600px;">
								<div id="map"
									style="min-height: 600px; height: 100%; padding: 10px;"></div>
							</div>
						</div>
					</div>
				</section>


				<!-- end 查詢 -->
				<!-- end: page -->
			</section>
		</div>

	</section>

	<%@include file="../include/includeFooterJs.jsp"%>	
	<script
		src="js/common/dateFormat.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-iVZ0oBSHSEEGw5oXzaoMnyYlN3MIAbE">
		
	</script>
	<script
		src="js/vehicle/markerWithLabel.js?<c:out value="${applicationScope.js_version}"/>"></script>
	<script
		src="js/vehicle/realTimeVehicle.js?<c:out value="${applicationScope.js_version}"/>"></script>
</body>
</html>