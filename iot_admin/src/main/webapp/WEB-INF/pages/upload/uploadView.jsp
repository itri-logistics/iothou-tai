<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<title>IOT後端管理平台</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<script src="itri_iot_q/vendor/jquery/jquery.js"></script>
		<script src="itri_iot_q/javascripts/url.min.js"></script>
		</head>
	<body>
		<h1>測試頁面資料</h1>
		<table border="1">
			<tbody>
				<tr>
					<td>
					<h1>上傳資料和解析驗證資料</h1>
							<h3>Url:http://45.32.45.49:8080/iot_admin/upload/uploadBleData</h3>
							<h3>Http Method:POST</h3>
						<form id="testForm">
							<h3>Datas=</h3><textarea rows="5" cols="80" name="datas" id="datas" ></textarea><br/>
							<input type="submit" value="POST"></input>
						</form>
						<button onclick="module.parse();">解析資料</button>
						<br/>
						解析結果<div id="rs"></div>				
					</td>
				</tr>
				<tr>
					<td>
					<h1>模擬資料資料</h1>
						<h3>companyId</h3>
						<input name="companyId" id="companyId" value="itri"><br/>
						<button onclick="module.getTestData();">取得測試資料</button>	
					</td>
				</tr>
				<tr>
					<td>
						<h1>查詢上傳後資料</h1>
						<form id="queryForm">
							公司帳號<input name="companyId" value="itri">
							起始日期(8碼)<input name="sd" value="20161202">
							結束日期(8碼)<input name="ed"  value="20161203">
							藍芽mac(12碼)<input name="macsStr" >
						</form>
						<button onclick="module.getQueryData();">查詢</button>
						<h4>結果</h4>
						<div id="qRs"></div>
					</td>
				</tr>
			</tbody>
		</table>
		
		<script src="itri_iot_q/javascripts/ajaxOnlyPlugin.js"></script>
		<script src="itri_iot_q/javascripts/handlebars-v4.0.5.js"></script>
		<script src="itri_iot_q/javascripts/jquery.cookie.js"></script>
		<script src="itri_iot_q/javascripts/jquery.form.min.js"></script>	
		<script src="js/upload/upload.js?<c:out value="${applicationScope.js_version}"/>"></script>
	</body>
</html>