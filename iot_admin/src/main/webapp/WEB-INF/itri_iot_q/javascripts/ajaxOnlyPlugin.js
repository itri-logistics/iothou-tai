//-----------------------------jquery plugin
$.extend({
	removeUndefined:function(keys,models){
		for(var i in models){
			for(var j in keys){
				if(models[i][keys[j]]==undefined){
					models[i][keys[j]]='';
				}
			}
		}
	},
	getDisName:function(key,models,list){
		for(var i in models){
			for(var j in list){
				if( models[i][key]!=undefined && models[i][key]===list[j].keyVal){
					models[i][key+'_disName']=list[j].disName;
					break;
				}
			}
		}
	},
	//取得資料庫的key disName list
	initList:function(listJson){
		for(var i in listJson){
			var model=listJson[i];
			ajx.getData({url:listJson[i].url,callback:function(datas){
				for(var j in datas){
					model.list.push(datas[j]);
				}
				if(typeof listJson[i].callback == 'function'){
					model.callback();
				}
			}});
		}
	},
	formatYYYYMMDDStr:function(date){
		var week=['日','一','二','三','四','五','六'];
		var str='';
		if(date==undefined||date.length!=8)
			return;
		var d=new Date();
		d.setFullYear(date.substr(0,4));
		d.setMonth(parseInt(date.substr(4,2))-1);
		d.setDate(date.substr(6,2));
//		console.debug(date);
//		console.debug(d);
		str+=d.getYear()+1900+'/';
		str+=(d.getMonth()+1)<10?'0'+(d.getMonth()+1):''+(d.getMonth()+1);
		str+='/';
		str+=(d.getDate())<10?'0'+(d.getDate()):''+(d.getDate());
		str+='(星期'+week[d.getDay()]+')';
		return str;
	},
	getYYYYMMDDStr:function(addDay){
		var week=['日','一','二','三','四','五','六'];
		var str='';
		var d=new Date();
		d.setDate(d.getDate()+addDay);
//		console.debug(d.getDate());
//		console.debug('星期'+week[d.getDay()]);
//		console.debug(d.getMonth()+1);
//		console.debug(d.getYear()+1900);
		str+=d.getYear()+1900+'/';
		str+=(d.getMonth()+1)<10?'0'+(d.getMonth()+1):''+(d.getMonth()+1);
		str+='/';
		str+=(d.getDate())<10?'0'+(d.getDate()):''+(d.getDate());
		str+='(星期'+week[d.getDay()]+')';
		return str;
	},
	getYYYYMMDD:function(addDay){
		var str='';
		var d=new Date();
		d.setDate(d.getDate()+addDay);
//		console.debug(d.getDate());
//		console.debug('星期'+week[d.getDay()]);
//		console.debug(d.getMonth()+1);
//		console.debug(d.getYear()+1900);
		str+=d.getYear()+1900;
		str+=(d.getMonth()+1)<10?'0'+(d.getMonth()+1):''+(d.getMonth()+1);
		str+=(d.getDate())<10?'0'+(d.getDate()):''+(d.getDate());
		return str;
	},
	formatFloat:function(num, pos){
		var size = Math.pow(10, pos);
		return Math.round(num * size) / size;
	},
	fomatDate8:function (d){
		if(d==undefined||d.length!=8)
			return "";
		return d.substr(0,4)+'/'+d.substr(4,2)+'/'+d.substr(6,2)+' '+d.substr(8,2);
	},
	fomatDate14:function (d){
		if(d==undefined||d.length!=14)
			return "";
		return d.substr(0,4)+'/'+d.substr(4,2)+'/'+d.substr(6,2)+' '+d.substr(8,2)+':'+d.substr(10,2)+':'+d.substr(12,2);
	},
	formval:{
		isEmail:function(val){//電子郵件
			var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
			return re.test(val);
		},
		isPhone:function(val){//手機
			var re=/\d{10}/igm;
			return re.test(val);
		}
	}
});

//----------------------------- com.js
var ajx = {
	getData : function(opt) {
		var _defaultSettings = {
			dataType : 'json',
			global:true
		};

		var _settings = $.extend(_defaultSettings, opt);
		// console.debug(_settings);
		// return;
		$.ajax({
			url : _settings.url,
			global: _settings.global,
			dataType : _settings.dataType,
		}).done(function(datas) {
			_settings.callback(datas.content);
		});
	},
	postData : function(opt) {
		var _defaultSettings = {
			dataType : 'json',
			orginalResp:false,
			global: true
		};

		var _settings = $.extend(_defaultSettings, opt);
		$.ajax({
			  type: "POST",
			  url: _settings.url,
			  data: _settings.data,
			  global: _defaultSettings.global,
			  dataType:_settings.dataType,
			  success: function(datas){
				  var resp=_settings.orginalResp?datas:datas.rs;
				  _settings.callback(resp);	  
			  },
			});
	}
};
//------------------------------end 