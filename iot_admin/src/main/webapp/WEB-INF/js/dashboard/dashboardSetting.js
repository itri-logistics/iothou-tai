var panelStyle = [ 'primary', 'success', 'warning', 'danger', 'info', 'dark' ];
var module = {
	macIdDataTypes : '',// 即時看板傳入的參數
	datas : [],
	places : [], // 暫存場域
	placeIds : [], // 場域Id列表
	tags : [], // 暫存tag
	queryPlace : function() {
		var param = 'companyId=' + $('.companyId').text();
		ajx.getData({
			url : './place/find?' + param,
			callback : function(models) {
				models.sort(function(a, b) {
					return a.disName.localeCompare(b.disName);
				});
				models.push({
					placeId : "-1",
					disName : "其他",
					companyId : $('.companyId').text()
				});
				module.places = models;
				module.queryTag();
			}
		});
	},
	queryTag : function() {
		var param = 'companyId=' + $('.companyId').text();
		ajx.getData({
			url : './tag/find?' + param,
			callback : function(models) {
				module.tags = models;

				var source = $('#tag-option-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('select[name="macId"]').each(function() {
					$(this).html('').html(html);
				});
				module.findDashboardSetting();
			}
		});
	},
	findDashboardSetting : function() {
		ajx.getData({
			url : './realTimeDashboard/findDashboardSetting',
			callback : function(models) {
				models.sort(function(a, b) {
					return a.disName.localeCompare(b.disName);
				});

				module.datas = [];
				module.placeIds = [];
				for (var i = 0; i < module.places.length; i++) {
					module.datas[module.places[i].placeId] = [];
					module.placeIds.push(module.places[i].placeId);
				}

				var placeIdHaveData = [];
				for ( var i in models) {
					placeId = "-1";
					module.macIdDataTypes += ',' + models[i].macId
							+ '_' + models[i].dataType;
					if (models[i].dataType.substr(0, 2) == "00") {
						models[i].dataTypeName = "本體";
					} else {
						models[i].dataTypeName = "port"
								+ models[i].dataType.substr(1, 1);
					}

					if (models[i].dataType.substr(2, 2) == "01") {
						models[i].dataTypeName += "溫度";
						models[i].unit = "℃";
					} else if (models[i].dataType.substr(2, 2) == "02") {
						models[i].dataTypeName += "濕度";
						models[i].unit = "%rH";
					}
					models[i].tagName = models[i].macId;
					for ( var j in module.tags) {
						if (models[i].macId == module.tags[j].mac) {
							models[i].tagName = module.tags[j].name;
							placeId = module.tags[j].placeId;
							break;
						}
					}

					if (placeId == null) {
						placeId = "-1";
					}
					module.datas[placeId].push(models[i]);

					placeIdHaveData.push(placeId);
					var k = $.inArray(placeId, module.placeIds);
					var k = k % panelStyle.length;
					models[i].style = panelStyle[k];

				}

				var placesHaveData = [];
				for (var i = 0; i < module.places.length; i++) {
					if ($.inArray(module.places[i].placeId,
							placeIdHaveData) >= 0) {
						placesHaveData.push(module.places[i]);
					}
				}
				var source = $('#place-section-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : placesHaveData
				});
				$('#dashboard').html(html);

				for ( var i in module.datas) {
					var source = $('#dashboardSettings-template')
							.html();
					var template = Handlebars.compile(source);
					var html = template({
						ds : module.datas[i]
					});
					$('#place-' + i).html(html);
				}

				module.macIdDataTypes = module.macIdDataTypes.substr(1);

				$(".fittext").fitText(0.4);

				module.findNewestData();
			}
		});
	},
	findNewestData : function() {
		ajx.getData({
			url : '../iot_realtime/' + $('.server').text()
					+ '/query/getNewestTagsData?companyId='
					+ $('.companyId').text() + '&macIdDataTypes='
					+ module.macIdDataTypes,
			callback : function(models) {
				console.debug(models);
				var now = new Date().getTime();
				for ( var dataType in models) {
					var d = models[dataType];
					var t = d.lastDataDatetimeStr.substr(8, 2) + ":"
							+ d.lastDataDatetimeStr.substr(10, 2) + ":"
							+ d.lastDataDatetimeStr.substr(12, 2);
					var recordTime = parseInt(d.lastDataDatetime);
					if (now - recordTime <= 300000) {// 5分鐘內
						var unit = "";
						if (d.dataType.substr(2, 2) == "01") {
							unit = "℃";
						} else if (d.dataType.substr(2, 2) == "02") {
							unit = "%";
						}

						// console.debug('#' + dataType + '_data' + d.lastData);
						$('#' + dataType + '_data').html(d.lastData + unit);
						$('#' + dataType + '_datetime').html(
								'<i class="el el-time" aria-hidden="true"></i> '
										+ t);
					} else {
						$('#' + dataType + '_data').html('&nbsp');
						$('#' + dataType + '_datetime').html('&nbsp');
					}
				}
			}
		});
	},
	createData : function() {
		var pa = $('#createForm').serialize();
		console.debug(pa);
		ajx.postData({
			url : './dashboardSetting/createDashboardSetting',
			data : pa,
			callback : function(datas) {
				// console.debug(datas);
				if (datas == 'error') {
					swal('新增失敗');
				} else {
					module.findDashboardSetting();
					ui.gotoMode('MODE_CREATE_CANCEL');
				}
			}
		});
	},
	updateData : function() {
		var pa = $('#updateForm').serialize();
		console.debug(pa);
		ajx.postData({
			url : './dashboardSetting/updateDashboardSetting',
			data : pa,
			callback : function(datas) {
				// console.debug(datas);
				if (datas == 'error') {
					swal('編輯失敗');
				} else {
					module.findDashboardSetting();
					ui.gotoMode('MODE_UPDATE_CANCEL');
				}
			}
		});
	},
	del : function() {
		ajx.postData({
			url : './dashboardSetting/delDashboardSetting',
			data : module.assign,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					swal('刪除失敗');
				} else {
					module.findDashboardSetting();
				}
			}
		});
	}
};

var ui = {
	gotoMode : function(action, p1, p2) {
		if (action == 'MODE_CREATE') {
			$('section[role="create"] input').val('');
			$('section[role="create"] input[name="companyId"]').val(
					$('.companyId').text());
			$('section[role="create"] input[name="position"]').val('0');
			$('section[role="query"]').hide();
			$('section[role="create"]').show(200);
		} else if (action == 'MODE_UPDATE') {
			for (var j = 0; j < module.datas.length; j++) {
				for ( var i in module.datas[j]) {
					if (p1 == module.datas[j][i].dashboardId) {
						module.assign = module.datas[j][i];
						module.assign = module.datas[j][i];
						break;
					}
				}
			}

			console.debug(module.assign);
			var textInputs = [ 'dashboardId', 'disName', 'companyId' ];
			for ( var i in textInputs) {
				$('#updateForm [name="' + textInputs[i] + '"]').val(
						module.assign[textInputs[i]]);
			}
			var selectInputs = [ 'dataType', 'macId' ];
			for ( var i in selectInputs) {
				console.debug(selectInputs[i]);
				console.debug(module.assign[selectInputs[i]]);

				$('#updateForm select[name="' + selectInputs[i] + '"]').val(
						module.assign[selectInputs[i]]);
			}
			$('#updateForm [name="position"]').val('0');
			$('section[role="query"]').hide();
			$('section[role="update"]').show(200);
		} else if (action == 'MODE_CREATE_CANCEL') {
			$('section[role="create"]').hide();
			$('section[role="query"]').show(200);
		} else if (action == 'MODE_UPDATE_CANCEL') {
			$('section[role="update"]').hide();
			$('section[role="query"]').show(200);
		}
	},
	showConfirmDialog : function(action, p1, p2) {
		var confirmText, backFn;
		if (action == 'CONFIRM_UPDATE') {
			confirmText = "確定要修改嗎?";
			backFn = module.updateData;
		} else if (action == 'CONFIRM_DELETE') {

			module.assign = {
				dashboardId : p1
			};
			// for(var j = 0;j<module.datas.length;j++){
			// for ( var i in module.datas[j]) {
			// if (p1 == module.datas[j][i].dashboardId) {
			// module.assign = module.datas[j][i];
			// break;
			// }
			// }
			// }

			confirmText = "確定要刪除嗎?";
			backFn = module.del;
		} else if (action == 'CONFIRM_CREATE') {
			confirmText = "確定要新增嗎?";
			backFn = module.createData;
		}

		swal({
			title : confirmText,
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "red",
			confirmButtonText : "確定",
			cancelButtonText : "取消",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {
			if (isConfirm && backFn != undefined) {
				backFn();
			}
		});
	}

};

$(document).ready(function() {
	module.queryPlace();
	$("#updateForm,#createForm").on("submit", function(event) {
		event.preventDefault();
	});
});
