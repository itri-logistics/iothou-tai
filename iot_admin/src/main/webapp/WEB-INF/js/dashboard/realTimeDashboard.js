var refreshThread;
var panelStyle = [ 'panel-primary', 'panel-success', 'panel-warning',
		'panel-danger', 'panel-info', 'panel-dark' ];
var module = {
	macIdDataTypes : '',// 即時看板傳入的參數
	datas : [],
	places : [], // 暫存場域
	placeIds : [], // 場域Id列表
	tags : [], // 暫存tag
	alerts : [], // 暫存alert
	alertKeys : [], // 暫存alert
	precisionFlag : false,
	queryPlace : function() {
		var param = 'companyId=' + $('.companyId').text();
		ajx.getData({
			url : './place/find?' + param,
			callback : function(models) {
				models.sort(function(a, b) {
					return a.disName.localeCompare(b.disName);
				});
				models.push({
					placeId : "-1",
					disName : "其他",
					companyId : $('.companyId').text()
				});
				module.places = models;
				module.queryTag();
			}
		});
	},
	queryTag : function() {
		var param = 'companyId=' + $('.companyId').text();
		ajx.getData({
			url : './tag/find?' + param,
			callback : function(models) {
				module.tags = models;
				module.findDashboardSetting();
			}
		});
	},
	findDashboardSetting : function() {
		ajx.getData({
			url : './realTimeDashboard/findDashboardSetting',
			callback : function(models) {
				models.sort(function(a, b) {
					return a.disName.localeCompare(b.disName);
				});

				module.datas = [];
				module.placeIds = [];
				for (var i = 0; i < module.places.length; i++) {
					module.datas[module.places[i].placeId] = [];
					module.placeIds.push(module.places[i].placeId);
				}

				var placeIdHaveData = [];
				for ( var i in models) {
					placeId = "-1";
					module.macIdDataTypes += ',' + models[i].macId
							+ '_' + models[i].dataType;
					if (models[i].dataType.substr(0, 2) == "00") {
						models[i].dataTypeName = "本體";
					} else {
						models[i].dataTypeName = "port"
								+ models[i].dataType.substr(1, 1);
					}
					if (models[i].dataType.substr(2, 2) == "01") {
						models[i].dataTypeName += "溫度";
						models[i].unit = "℃";
					} else if (models[i].dataType.substr(2, 2) == "02") {
						models[i].dataTypeName += "濕度";
						models[i].unit = "%rH";
					}
					models[i].tagName = models[i].macId;
					for ( var j in module.tags) {
						if (models[i].macId == module.tags[j].mac) {
							models[i].tagName = module.tags[j].name;
							placeId = module.tags[j].placeId;
							break;
						}
					}

					if (placeId == null) {
						placeId = "-1";
					}
					module.datas[placeId].push(models[i]);

					placeIdHaveData.push(placeId);
					var k = $.inArray(placeId, module.placeIds);
					var k = k % panelStyle.length;
					models[i].style = panelStyle[k];

				}

				var placesHaveData = [];
				for (var i = 0; i < module.places.length; i++) {
					if ($.inArray(module.places[i].placeId,
							placeIdHaveData) >= 0) {
						placesHaveData.push(module.places[i]);
					}
				}
				var source = $('#place-section-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : placesHaveData
				});
				$('#dashboard').html(html);

				for ( var i in module.datas) {
					var source = $('#dashboardSettings-template')
							.html();
					var template = Handlebars.compile(source);
					var html = template({
						ds : module.datas[i]
					});
					$('#place-' + i).html(html);
				}

				module.macIdDataTypes = module.macIdDataTypes.substr(1);

				$(".fittext").fitText(0.4);

				module.queryAlertSettingDetail();
			}
		});
	},
	queryAlertSettingDetail : function(){
		ajx.getData({
            url : './alert/queryAlertSettingDetail?companyId=' + $('.companyId').text(),
            callback : function(models) {
            	module.alerts = [];
                module.alertKeys = [];
                for(var i = 0;i<models.length;i++){
                	module.alertKeys.push(models[i].mac+'_'+models[i].dataType);
                	module.alerts[models[i].mac+'_'+models[i].dataType] = {low : models[i].low, high : models[i].high};
                }
                module.findNewestData();
            }
        });
	},
	findNewestData : function() {
		ajx.getData({
			url : '../iot_realtime/' + $('.server').text()
					+ '/query/getNewestTagsData?companyId='
					+ $('.companyId').text() + '&macIdDataTypes='
					+ module.macIdDataTypes,
			callback : function(models) {
				// console.debug(models);
				var now = new Date().getTime();
				for ( var dataType in models) {
					var d = models[dataType];
					var t = d.lastDataDatetimeStr.substr(8, 2) + ":"
							+ d.lastDataDatetimeStr.substr(10, 2) + ":"
							+ d.lastDataDatetimeStr.substr(12, 2);
					var recordTime = parseInt(d.lastDataDatetime);
					if (now - recordTime <= 600000) {// 10分鐘內

						var unit = "";
						if (d.dataType.substr(2, 2) == "01") {
							unit = "℃";
						} else if (d.dataType.substr(2, 2) == "02") {
							unit = "%";
						}
						//console.debug('#' + dataType + '_data' + d.lastData);
						if(module.precisionFlag){
							if(d.lastData.indexOf('.')>=0){
								var floatData = d.lastData.split('.');
								var decimal = parseFloat(d.lastData) - parseInt(floatData[0]);	//減掉整數的部份
								if(decimal >= 0.75){
									decimal = 1;
								}else if(decimal >= 0.25){
									decimal = 0.5;
								}else{
									decimal = 0;
								}
								decimal += parseFloat(floatData[0]);
								d.lastData = decimal.toString();								
							}
						}
						
						$('#' + dataType + '_data').html(d.lastData + unit);
						$('#' + dataType + '_datetime').html(
								'<i class="el el-time" aria-hidden="true"></i> '
										+ t);
						
						
						//檢查是否超過警示
						if($.inArray(dataType, module.alertKeys)>=0){
							var alert = module.alerts[dataType];
							var value = parseFloat(d.lastData);
							if(value > parseFloat(alert.high) || value < parseFloat(alert.low)){
								$('#' + dataType).addClass('alertData');						
							}else{
								$('#' + dataType).removeClass('alertData');
							}
						}else{
							$('#' + dataType).removeClass('alertData');
						}
						
					} else {
						$('#' + dataType + '_data').html('&nbsp');
						$('#' + dataType + '_datetime').html('&nbsp');
					}
				}
				clearInterval(refreshThread);
				refreshThread = setInterval(module.findNewestData, 30000);
			}
		});
	}
};

var ui = {

};

$(document).ready(function() {
	module.queryPlace();
});

//checkbox事件
$(document).on('click', '#switch-precision', function() {
	module.precisionFlag = $(this).children('div:first').hasClass('on');
	module.findNewestData();
});