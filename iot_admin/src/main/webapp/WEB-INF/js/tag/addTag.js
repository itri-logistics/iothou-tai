$(document).on('click', '#btn-add-tag', function() {
	var placeId = $('#placeId').val();
	var tagMac = $('#tagMacInput').val();
	var tagName = $('#tagNameInput').val();
	var comId = $('#comNameInput').val();

	var tagData = {
		mac : tagMac,
		name : tagName,
		placeId : placeId,
		rate : "15",
		companyId : comId,
		type : "BLE"
	};

	postTagData(tagData);

})

function postTagData(tagData) {
	var pa = "";
	$.each(tagData, function(k, v) {
		pa += "&" + k + "=" + v;
	})

	console.debug(pa);

	ajx.postData({
		url : './tag/create',
		data : pa,
		callback : function(datas) {
			if (datas == 'error') {
				alert('新增失敗');
			} else {
				alert("更新成功");
			}
		}
	});

}