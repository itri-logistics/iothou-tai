var refreshThread;
var tagType ={
		type : ["BLE", "NFC", "WIFI"],
		name : ["藍牙", "NFC", "WiFi"]
	}
var module = {
	companyId : "",
	places : [],
	tags : '',
	queryPlace : function() {
		var param = 'companyId=' + module.companyId;
		ajx.getData({
			url : './place/find?' + param,
			callback : function(models) {
				module.places = models;				
				module.queryTag();
			}
		});
	},
	queryTag : function() {
		var param = $('#queryForm').serialize();
		ajx.getData({
			url : './tag/find?' + param,
			callback : function(models) {
				for(var i in models){
					//console.debug(models[i].mac);
					for (var j = 0; j < module.places.length; j++) {
						if (models[i].placeId == module.places[j].placeId){
							models[i].placeId = module.places[j].disName;
							break;
						}
					}
					var pos = $.inArray(models[i].type, tagType.type);
					if (pos >= 0) {						
						models[i].type = tagType.name[pos];
					}
					module.tags += ',' + models[i].mac + '_0007';
				}
				module.tags = module.tags.substr(1);
				var source = $('#detail-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('#tableData > tbody').html('').html(html);
				$('#totalRecord').html(models.length);
				module.queryData();
			}
		});
	},
	queryData : function() {		
		ajx.getData({
			url : '../iot_realtime/' + $('.server').text()
			+ '/query/getNewestTagsData?companyId='
			+ $('.companyId').text() + '&macIdDataTypes='+ module.tags,
			callback : function(models) {
				var now = new Date().getTime();
				for ( var dataType in models) {
					//console.debug(dataType);
					var d = models[dataType];
					var t = d.lastDataDatetimeStr.substr(0, 4) + "/" //年 (4位)
					+ d.lastDataDatetimeStr.substr(4, 2) + "/" //月 (2位)
					+ d.lastDataDatetimeStr.substr(6, 2) + " " //日 (2位)
					+ d.lastDataDatetimeStr.substr(8, 2) + ":" //時 (2位)
					+ d.lastDataDatetimeStr.substr(10, 2) + ":" //分 (2位)
					+ d.lastDataDatetimeStr.substr(12, 2); //秒 (2位)
					var recordTime = parseInt(d.lastDataDatetime);	
					//console.debug(d);
					//console.debug("recordTime:" + recordTime);
					//console.debug("now:" + now);
					if(now - recordTime <= 300000){// 5分鐘內
						if(parseFloat(d.lastData) <= 25){
							$('#' + dataType + '_power').css("color", "red");
						}else{
							$('#' + dataType + '_power').css("color", "");
						}
						$('#' + dataType + '_power').html(d.lastData+' %');
						$('#' + dataType + '_datetime').html(t);
					}else{
						$('#' + dataType + '_power').html('&nbsp');
						$('#' + dataType + '_datetime').html('&nbsp');
					}
				}
				
				clearInterval(refreshThread);
				refreshThread = setInterval(module.queryData, 30000);
				// console.debug(module.datas);
			}
		});
	}
};

var ui = {
	
};

$(document).ready(function() {
	module.companyId = $('.companyId').text();

	$('#queryForm input[name="companyId"]').val(module.companyId);

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		module.queryTag();
	});
	
	module.queryPlace();
});
