var module = {
	getTestData : function() {
		if($('#companyId').val().length==0){
			alert('請輸入公司名稱');
			return;
		}
		if($('#mac').val().length==0){
			alert('請輸入Tag MAC');
			return;
		}
		if($('#name').val().length==0){
			alert('請輸入Tag名稱');
			return;
		}
		var rs =$('#createDataForm').serialize();
//			console.debug(rs);
		$('#datas').html(rs);
		
		
	},
	getQueryData : function() {
		var pa=$('#queryForm').serialize();
		ajx.getData({
			url : './find?'+pa,
			callback : function(rs) {
				$('#qRs').html(JSON.stringify(rs));
			}
		});
	},
	postTestData :function() {
		var pa=$('#datas').val();
//		console.debug(pa);
//		return;
		ajx.postData({
			url : './create',
			data : pa,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					alert('新增失敗');
				} else {
					alert('成功');
				}
			}
		});
	},
	updateData :function() {		
		var pa=$('#datas').val();
//		console.debug(pa);
//		return;
		ajx.postData({
			url : './update',
			data : pa,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					alert('更新失敗');
				} else {
					alert('成功');
				}
			}
		});
	}
};


$(document).ready(function() {
	$( "#testForm" ).on( "submit", function( event ) {
		  event.preventDefault();
	});
	
	$( "#createDataForm" ).on( "submit", function( event ) {
		event.preventDefault();
	});
	
	$( "#queryForm" ).on( "submit", function( event ) {
		  event.preventDefault();
	});
	
});
