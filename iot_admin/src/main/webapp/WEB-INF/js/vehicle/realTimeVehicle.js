var refreshThread;
var module = {
	markers : [],
	mapLabels : [],
	infoWins : [],
	datas :[],
	findNewestData : function() {
		return $.ajax({
			url : './realTimeVehicle/getNewestLocationData',			
			success: function(returnData) {
				var models = returnData.content;
				// console.debug(models);
				var now = new Date().getTime();
				module.datas = models;
				if (ui.map == null) {
					initMap();
				}
				var lat = 0,lng = 0,count = 0;
				for ( var dataType in models) {
					var data = models[dataType];
					var t = data.lastDataDatetimeStr.substr(8, 2) + ":"
							+ data.lastDataDatetimeStr.substr(10, 2) + ":"
							+ data.lastDataDatetimeStr.substr(12, 2);
					var recordTime = parseInt(data.lastDataDatetime);
					if (now - recordTime <= 300000) {// 5分鐘內
						lat += parseFloat(data.lat);
						lng += parseFloat(data.lng);
						count++;
						// 畫地圖
						var marker = null;
						var infowindow = null;
						var mapLabel = null;
						for (var i = 0; i < module.markers.length; i++) {
							if (module.markers[i].getTitle() == data.vehicleId) {
								marker = module.markers[i];
								info = module.infoWins[i];
								mapLabel = module.mapLabels[i];
								break;
							}
						}
						
						if (marker == null) {
							marker = new google.maps.Marker({
								position : {
									lat : data.lat,
									lng : data.lng
								},
								title : data.vehicleId,
								//label : data.disName,								
								map : ui.map,
								icon : 'http://maps.google.com/mapfiles/ms/micons/truck.png'
							});
							info = new google.maps.InfoWindow({								
							    content: "車牌號碼："+data.vehicleId+"<br />"+"更新時間："+t+"<br />位置：<br />讀取中..."
							  });
							mapLabel = new MapLabel({
						          text: data.disName,
						          position: new google.maps.LatLng(data.lat, data.lng),
						          map: ui.map
						        });
							
							marker.bindTo('map', mapLabel);
							module.codeAddress(data.lat, data.lng, info,t,data.vehicleId);
							module.markers.push(marker);
							module.mapLabels.push(mapLabel);
							module.infoWins.push(info);
							marker.addListener('click', function() {
								info.open(map, marker);
							});
							//info.open(map, marker);
						} else {
							marker.setPosition({
								lat : data.lat,
								lng : data.lng
							});
							mapLabel.set('position', new google.maps.LatLng(data.lat, data.lng));
							module.codeAddress(data.lat, data.lng, info,t);							
							marker.setMap(ui.map);
						}
						
					}else{
						for (var i = 0; i < module.markers.length; i++) {
							if (module.markers[i].getTitle() == data.vehicleId) {
								marker = module.markers[i];
								marker.setMap(null);
								break;
							}
						}
					}
					
				}
				
				if(count>0){
					ui.map.setCenter({lat : lat/count, lng : lng/count});
				}				
				
				clearInterval(refreshThread);
				refreshThread = setInterval(module.findNewestData, 30000);
			}
		});
	},
	codeAddress : function(lat, lng, info, t,vid) {
		var geocoder = new google.maps.Geocoder();
		var place;
		geocoder.geocode({'location': {lat : lat, lng : lng}}, 
			function(results, status){
					if (status == google.maps.GeocoderStatus.OK)//若地址解析請求成功
					{
						console.debug(results[0].formatted_address+"");
						place = results[0].formatted_address+"";	
						var p = place.indexOf("台灣");
						if(p>=0){
							place = place.substring(p+2);
						}
					}
					else//若地址解析請求失敗
					{
						place = lat+","+lng;
					}
					
					info.setContent("車牌號碼："+vid+"<br />"+"更新時間："+t+"<br />位置：<br />"+place);
					
				});
		
	}

};

var ui = {
	mapOpt : {
		center : {
			lat : 23.769498,
			lng : 120.3741491
		},
		zoom : 8
	},
	map : null
};

function initMap(){
	ui.map = new google.maps.Map(document.getElementById('map'), ui.mapOpt);
}

$(document).ready(function() {
	google.maps.event.addDomListener(window, 'load', module.findNewestData);
	//module.findNewestData();
});
