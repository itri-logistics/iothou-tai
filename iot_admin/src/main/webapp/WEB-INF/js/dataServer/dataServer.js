var module = {

	queryData : function() {
		ajx.getData({
			url : './dataServer/getNewestServerData',
			callback : function(models) {
				module.datas = models;
				var source = $('#detail-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('#tableData > tbody').html('').html(html);
			}
		});
	}
	
};


$(document).ready(function() {
	module.queryData();
});
