var module = {
	companyId: "",
	lists : {}, // 暫存各種參數
	assignModel : {},
	datas : [],	
	queryData : function() {
		var param = $('#queryForm').serialize();
		ajx.getData({
			url : './place/find?' + param,
			callback : function(models) {
				module.datas = models;
				var source = $('#detail-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('#tableData > tbody').html('').html(html);
			}
		});
	},
	queryPage : function() {
		var param = $('#queryForm').serialize();
		ajx.getData({
			url : './place/findPage?' + param,
			callback : function(d) {
				console.debug(d);
				var html = '';
				var page = d.total / d.page_num;
				if (d.total % d.page_num > 0) {
					page += 1;
				}
				for (var i = 1; i <= page; i++) {
					html += '<option value="' + i + '">第' + i + '頁</option>';
				}
				$('#qpage').html('').html(html);
				$('#totalRecord').html(d.total);
			}
		});
	},
	createData : function() {
		if (!module.formvalCreate())
			return;
		var as = $('#createForm').serializeArray();
		var pa = {};
		for ( var i in as) {
			pa[as[i].name] = as[i].value;
		}
		// console.debug(pa);
		// return;
		ajx.postData({
			url : './place/create',
			data : pa,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					swal('新增失敗');
				} else {
					module.queryData();
					ui.gotoMode('MODE_CREATE_CANCEL');
				}
			}
		});
	},
	updateData : function() {
		if (!module.formvalEdit())
			return;
		var pa = $('#updateForm').serialize();

		// console.debug(pa);
		// return;
		ajx.postData({
			url : './place/update',
			data : pa,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					swal('更新失敗');
				} else {
					module.queryData();
					ui.gotoMode('MODE_UPDATE_CANCEL');
				}
			}
		});

	},
	updateImg : function() {
		if (!module.formvalEdit())
			return;
		var pa = $('#updateImgForm').serializeArray();
		var formData = new FormData(document.getElementById("updateImgForm"));
//		for ( var i in pa) {
//			formData.append(pa[i].name, pa[i].value);
//		}
//		formData.append("file", $('#btnUpdatePlaceImage').get()[0].files[0]);

		console.debug(pa);
		console.debug(formData);

		$.ajax({
			url : './place/updateImage',
			data : formData,
			dataType : 'json',
			processData : false,
			contentType : false,
			type : 'POST',
			success : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					swal('更新失敗');
				} else {
					module.queryData();
					ui.gotoMode('MODE_UPDATE_CANCEL');
				}
			}
		});

	},
	updateIcon : function() {
		if (!module.formvalEdit())
			return;
		var pa = $('#updateIconForm').serializeArray();
		
		uploadBtnImg = $('input[name="iconOption"]:checked').val();
		if (uploadBtnImg == 'upload') {
			var formData = new FormData(document.getElementById("updateIconForm"));
			/*for ( var i in pa) {
				formData.append(pa[i].name, pa[i].value);
			}*/
			//formData.append("iconFile", $('#btnUpdatePlaceIcon').get()[0].files[0]);

			//console.debug(formData);
			$.ajax({
				url : './place/updateIcon',
				data : formData,
				dataType : 'json',
				processData : false,
				contentType : false,
				type : 'POST',
				success : function(datas) {
					console.debug(datas);
					if (datas.rs == 'error') {
						if(datas.content == 'file size too large.'){
							alert('圖檔過大，請上傳50kb以內的圖檔。');
						}else{
							swal('更新失敗');
						}
						
					} else {
						module.queryData();
						ui.gotoMode('MODE_UPDATE_CANCEL');
					}
				}
			});
		} else {
			// console.debug(pa);
			ajx.postData({
				url : './place/updateIconByOption',
				data : pa,
				callback : function(datas) {
					console.debug(datas);
					if (datas == 'error') {
						swal('更新失敗');
					} else {
						module.queryData();
						ui.gotoMode('MODE_UPDATE_CANCEL');
					}
				}
			});
		}
		

	},
	del : function() {
		ajx.postData({
			url : './place/del',
			data : module.assign,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					swal('刪除失敗');
				} else {
					module.queryData();
				}
			}
		});
	},
	formvalEdit : function() {
		return true;
	},
	formvalCreate : function() {
		return true;
	}
};

var ui = {
	gotoMode : function(action, p1, p2) {
		if (action == 'MODE_CREATE') {
			$('section[role="create"] input').val('');
			$('section[role="create"] input[name="companyId"]').val(module.companyId);
			$('section[role="query"]').hide();
			$('section[role="create"]').show(200);
		} else if (action == 'MODE_UPDATE') {
			for ( var i in module.datas) {
				if (p1 == module.datas[i].placeId) {
					module.assign = module.datas[i];
					break;
				}
			}
			var textInputs = [ 'companyId', 'disName', 'placeId' ];
			for ( var i in textInputs) {
				$('#updateForm [name="' + textInputs[i] + '"]').val(
						module.assign[textInputs[i]]);
				$('#updateImgForm [name="' + textInputs[i] + '"]').val(
						module.assign[textInputs[i]]);
				$('#updateIconForm [name="' + textInputs[i] + '"]').val(
						module.assign[textInputs[i]]);
			}

			$('input[type="file"]').val('');
			$("#imgPreview").attr("src","./place/img/"+module.assign['companyId']+"/"+module.assign['placeId']);
			$("#iconPreview").attr("src","./place/icon/"+module.assign['companyId']+"/"+module.assign['placeId']);

			$('section[role="query"]').hide();
			$('section[role="update"]').show(200);
		} else if (action == 'MODE_CREATE_CANCEL') {
			$('section[role="create"]').hide();
			$('section[role="query"]').show(200);
		} else if (action == 'MODE_UPDATE_CANCEL') {
			$('section[role="update"]').hide();
			$('section[role="query"]').show(200);

		}
	},
	showConfirmDialog : function(action, p1, p2) {
		var confirmText, backFn;
		if (action == 'CONFIRM_UPDATE') {
			if (!module.formvalEdit()) {
				return;
			}
			confirmText = "確定要修改嗎?";
			backFn = module.updateData;
		} else if (action == 'CONFIRM_DELETE') {
			if (!module.formvalEdit()) {
				return;
			}
			for ( var i in module.datas) {
				if (p1 == module.datas[i].placeId) {
					module.assign = module.datas[i];
					break;
				}
			}
			confirmText = "確定要刪除嗎?";
			backFn = module.del;
		} else if (action == 'CONFIRM_CREATE') {
			if (!module.formvalCreate()) {
				return;
			}
			confirmText = "確定要新增嗎?";
			backFn = module.createData;
		} else if (action == 'CONFIRM_UPDATE_IMG') {
			if (!module.formvalCreate()) {
				return;
			}
			confirmText = "確定要修改嗎?";
			backFn = module.updateImg;
		}else if (action == 'CONFIRM_UPDATE_ICON') {
			if (!module.formvalCreate()) {
				return;
			}
			confirmText = "確定要修改嗎?";
			backFn = module.updateIcon;
		}

		swal({
			title : confirmText,
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "red",
			confirmButtonText : "確定",
			cancelButtonText : "取消",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {
			if (isConfirm && backFn != undefined) {
				backFn();
			}
		});
	}
};

$(document).ready(function() {
	
	module.companyId = $('.companyId').text();
	$('#queryForm input[name="companyId"]').val(module.companyId);

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		$('#queryForm input[name="page"]').val('1');
		module.queryData();
		module.queryPage();
	});
	$("#updateForm,#updateImgForm,#updateIconForm,#createForm").on("submit", function(event) {
		event.preventDefault();
	});

	$("#qpage").change(function(event) {
		$('#queryForm input[name="page"]').val($(this).val());
		module.queryData();
	});

	module.queryData();
	module.queryPage();

});
