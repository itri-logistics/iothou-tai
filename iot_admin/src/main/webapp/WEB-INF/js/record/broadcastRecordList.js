var module = {
	companyId : "",
	tagQueryModel : {
		companyId : "",
		sd : "",
		ed : ""
	},
	dataQueryModel : {
		companyId : "",
		sd : "",
		ed : "",
		macsStr : "", // 被選取的tag
		page : ""
	},
	tagSelect : [], // 被選取的tag
	tagSelectName : [], // 被選取的tag的名稱（確認選取用）
	tagInfos : [],
	tags : [],
	datas : [],
	queryTag : function() {
		var param = 'companyId=' + $('.companyId').text();
		ajx.getData({
			url : './tag/find?' + param,
			callback : function(models) {
				module.tagInfos = models;
				module.queryTagData();
			}
		});
	},
	queryTagData : function() {
		module.tagQueryModel.companyId = module.companyId;
		module.tagQueryModel.sd = $('#sd').val().replace(new RegExp('/', 'g'),
				'').substr(2);
		module.tagQueryModel.ed = $('#ed').val().replace(new RegExp('/', 'g'),
				'').substr(2);
		var pa = "";
		for ( var i in module.tagQueryModel) {
			if (module.tagQueryModel[i].length > 0) {
				pa += "&" + i + "=" + module.tagQueryModel[i];
			}
		}
		ajx.getData({
			url : '../iot_realtime/' + $('.server').text()
					+ '/query/getTagsInQueryInterval?' + pa,
			callback : function(models) {
				module.tagSelect = [];
				module.tagSelectName = [];
				for (var i = 0; i < models.length; i++) {
					models[i].name = models[i].mac;
					for (var j = 0; j < module.tagInfos.length; j++) {
						if (module.tagInfos[j].mac == models[i].mac) {
							models[i].name = module.tagInfos[j].name;
							break;
						}
					}
					module.tagSelect.push(models[i].mac);
					module.tagSelectName.push(models[i].name);
					var sigArray = models[i].signal.split(',');
					sigArray.sort(function(a, b) {
						return parseInt(a) - parseInt(b);
					});
					console.debug("queryTagData: " + sigArray);
					var sigArrayToShow = [];
					for (var j = 0; j < sigArray.length; j++) {
						var port = sigArray[j].substr(0, 2);
						var type = sigArray[j].substr(2, 2);
						if (type == "01" || type == "02") {
							if (port == "00") {
								port = "本體";
							} else {
								port = "port" + sigArray[j].substr(1, 1);
							}

							if (type == "01") {
								type = "溫度"
							} else if (type == "02") {
								type = "濕度"
							} else if (type == "05") {
								type = "光度"
							}
							sigArray[j] = port + type;
							sigArrayToShow.push(sigArray[j]);
						}
					}
					models[i].signal = sigArrayToShow.join(', ');
				}
				module.tags = models;
				var source = $('#tag-list-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('#tag-list').html('').html(html);
				ui.gotoMode('MODE_AFTER_TAG_QUERY');
			}
		});
	},
	getExcelFile : function() {
		module.dataQueryModel.companyId = module.companyId;
		module.dataQueryModel.sd = $('#sd').val().replace(new RegExp('/', 'g'),
				'');
		module.dataQueryModel.ed = $('#ed').val().replace(new RegExp('/', 'g'),
				'');
		module.dataQueryModel.macsStr = module.tagSelect.join(',');
		var pa = "";
		for ( var i in module.dataQueryModel) {
			if (module.dataQueryModel[i].length > 0) {
				pa += "&" + i + "=" + module.dataQueryModel[i];
			}
		}
		window.open('../iot_realtime/' + $('.server').text() + '/getExcelFile?'
				+ pa);
	},
	deleteData : function(mac, sd, ed) {
		
		swal({
			title : '這會刪除'+mac+'從'+sd+'到'+ed+'的所有紀錄，確定嗎？',
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "red",
			confirmButtonText : "確定",
			cancelButtonText : "取消",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {
			ajx.getData({
				url : '../iot_realtime/' + $('.server').text()
						+ '/delete/'+module.companyId+'/'+mac+'/'+sd.replace(new RegExp('/', 'g'), '')+'/'+ed.replace(new RegExp('/', 'g'), ''),
				callback : function(rs) {
					
					module.queryTag();
					
				}
			});
		});
	
		
	},
	queryData : function(mac, name, sd, ed) {
		module.dataQueryModel.companyId = module.companyId;
		module.dataQueryModel.sd = sd.replace(new RegExp('/', 'g'), '');
		module.dataQueryModel.ed = ed.replace(new RegExp('/', 'g'), '');
		module.dataQueryModel.macsStr = mac;
		var pa = "";
		for ( var i in module.dataQueryModel) {
			if (module.dataQueryModel[i].length > 0) {
				pa += "&" + i + "=" + module.dataQueryModel[i];
			}
		}
		// console.debug(module.dataQueryModel);
		ajx.getData({
			url : '../iot_realtime/' + $('.server').text()
					+ '/query/getQueryData?' + pa,
			callback : function(rs) {
				if (rs == "data size out of limit") {
					swal('資料量過大，無法檢視', '最多僅能檢視15天的紀錄，請縮短查詢的紀錄區間，或改為下載Excexl檔。',
							'warning');
					return;
				}
				module.datas = rs.split('#');
				console.debug("queryData: " + module.datas);
				
				$('#tagName').html(name);
				$('#tagMac').html(mac);

				var models = [];
				var time = [];

				for (var i = 0; i < module.datas.length; i++) {
					var d = module.datas[i].split(',');
					if (d[0] == "0001") {// 本體溫度
						for (var j = 1; j < d.length; j++) {
							var val = parseFloat(d[j]
									.substring(12, d[j].length));
							if (val >= -35) {
								var t = "20" + d[j].substr(0, 2) + "/"
										+ d[j].substr(2, 2) + "/"
										+ d[j].substr(4, 2) + " "
										+ d[j].substr(6, 2) + ":"
										+ d[j].substr(8, 2) + ":"
										+ d[j].substr(10, 2); // 紀錄時間

								var pos = $.inArray(t, time); // 資料存放的位置
								if (pos == -1) {
									var model = {
										time : t,
										humidity : "",
										temperature : "",
										temperature1 : "",
										temperature2 : ""
									};
									models.push(model);
									time.push(t);
									pos = models.length - 1;
								}
								models[pos].temperature = d[j].substring(12,
										d[j].length);
							}
						}

						// console.debug(models);

					} else if (d[0] == "0002") {// 本體濕度
						for (var j = 1; j < d.length; j++) {
							var val = parseFloat(d[j]
									.substring(12, d[j].length));
							if (val >= -35) {
								var t = "20" + d[j].substr(0, 2) + "/"
										+ d[j].substr(2, 2) + "/"
										+ d[j].substr(4, 2) + " "
										+ d[j].substr(6, 2) + ":"
										+ d[j].substr(8, 2) + ":"
										+ d[j].substr(10, 2); // 紀錄時間

								var pos = $.inArray(t, time); // 資料存放的位置
								if (pos == -1) {
									var model = {
										time : t,
										humidity : "",
										temperature : "",
										temperature1 : "",
										temperature2 : ""
									};
									models.push(model);
									time.push(t);
									pos = models.length - 1;
								}
								models[pos].humidity = (d[j].substring(12,
										d[j].length));
							}
						}
					} else if (d[0] == "0101") { // port1溫度
						for (var j = 1; j < d.length; j++) {
							var val = parseFloat(d[j]
									.substring(12, d[j].length));
							if (val >= -35) {
								var t = "20" + d[j].substr(0, 2) + "/"
										+ d[j].substr(2, 2) + "/"
										+ d[j].substr(4, 2) + " "
										+ d[j].substr(6, 2) + ":"
										+ d[j].substr(8, 2) + ":"
										+ d[j].substr(10, 2); // 紀錄時間

								var pos = $.inArray(t, time); // 資料存放的位置
								if (pos == -1) {
									var model = {
										time : t,
										humidity : "",
										temperature : "",
										temperature1 : "",
										temperature2 : ""
									};
									models.push(model);
									time.push(t);
									pos = models.length - 1;
								}
								models[pos].temperature1 = d[j].substring(12,
										d[j].length);
							}

						}
					} else if (d[0] == "0201") {// port2溫度
						for (var j = 1; j < d.length; j++) {
							var val = parseFloat(d[j]
									.substring(12, d[j].length));
							if (val >= -35) {
								var t = "20" + d[j].substr(0, 2) + "/"
										+ d[j].substr(2, 2) + "/"
										+ d[j].substr(4, 2) + " "
										+ d[j].substr(6, 2) + ":"
										+ d[j].substr(8, 2) + ":"
										+ d[j].substr(10, 2); // 紀錄時間

								var pos = $.inArray(t, time);
								if (pos == -1) {
									var model = {
										time : t,
										humidity : "",
										temperature : "",
										temperature1 : "",
										temperature2 : ""
									};
									models.push(model);
									time.push(t);
									pos = models.length - 1;
								}
								models[pos].temperature2 = d[j].substring(12,
										d[j].length);
							}
						}
					}
				}

				models.sort(function(a, b) {
					return a.time.localeCompare(b.time);
				});

				var source = $('#detail-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				// if(module.dataTable!=null){
				// module.dataTable.destroy();
				// }
				$('#tableData > tbody').html('').html(html);
				// module.dataTable = $('#tableData').DataTable({
				// responsive: true,
				// });

				ui.gotoMode('MODE_SHOW_DETAIL');

			}
		});
	}
};

var ui = {
	gotoMode : function(action) {
		if (action == 'MODE_SHOW_DETAIL') {
			$('#detail-section').show(200);
		} else if (action == 'MODE_AFTER_TAG_QUERY') {
			$('#detail-section').hide();
			$('#tag-select').html(module.tagSelectName.join());
			$('#chartHolder').html('');
			$('#tag-select-info').show(200);
			$('#tag-table').show(200);
			$('#btn-get-excel-file').show(200);
			$('input[name="tag-select-all"]').prop("checked", true);

		}
	}
};

$(document).ready(function() {

	module.companyId = $('.companyId').text();

	$('.datepicker').datepicker({
		format : 'yyyy/mm/dd'
	});

	$('#sd').datepicker('setDate', getYesterday());
	$('#ed').datepicker('setDate', new Date());

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		module.queryTag();
	});

});

// checkbox事件
$(document).on(
		'change',
		'input[type="checkbox"]',
		function() {
			// 單一tag選取事件
			if ($(this).prop("name") == 'tag-select') {
				if ($(this).prop("checked")) {
					var mac = $(this).prop("id");
					var name = $(this).prop("value");
					if (jQuery.inArray(mac, module.tagSelect) < 0) {
						module.tagSelect.push(mac);
						module.tagSelectName.push(name);
					}
					if (module.tagSelect.length > 0) {
						$('#tag-select').html(module.tagSelectName.join(', '));
						if (module.tagSelect.length == module.tags.length) {
							$('input[name="tag-select-all"]').prop("checked",
									true);
						}
					}
				} else {
					var mac = $(this).prop("id");
					var name = $(this).prop("value");
					module.tagSelect.remove(mac);
					module.tagSelectName.remove(name);
					if (module.tagSelect.length > 0) {
						$('#tag-select').html(module.tagSelectName.join(', '));
					} else {
						$('#tag-select').empty();
					}
					$('input[name="tag-select-all"]').prop("checked", false);
				}
			}

			// Tag全選事件
			if ($(this).prop("name") == 'tag-select-all') {
				$('input[name="tag-select"]').prop("checked",
						$("#tag-select-all").prop("checked"));

				$('input[name="tag-select"]').each(function() {
					var mac = $(this).prop("id");
					var name = $(this).prop("value");
					if ($(this).prop("checked")) {
						if (jQuery.inArray(mac, module.tagSelect) < 0) {
							module.tagSelect.push(mac);
							module.tagSelectName.push(name);
						}
					} else {
						module.tagSelect.remove(mac);
						module.tagSelectName.remove(name);
					}
				});

				if (module.tagSelect.length > 0) {
					$('#tag-select').html(module.tagSelectName.join(', '));
				} else {
					$('#tag-select').empty();
				}
			}

		});

function getYesterday() {
	var date = new Date();
	date.setDate(date.getDate() - 1);
	return date;
}