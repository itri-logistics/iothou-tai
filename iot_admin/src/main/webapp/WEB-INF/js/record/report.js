var module = {
	companyId : "",
	dataQueryModel : {
		companyId : "",
		sd : "",
		ed : ""
	},
	dataSelect : [], // 被選取的data
	dataSelectName : [], // 被選取的data名稱（確認選取用）
	datas : [],
	raws : [],
	deferredObjectItems : [],
	queryData : function() {
		var st = $('#st').val(), et = $('#et').val();
		if(st.length==4){
			st = '0'+st;
		}
		if(et.length==4){
			et = '0'+et;
		}
		module.dataQueryModel.companyId = module.companyId;
		module.dataQueryModel.sd = $('#sd').val().replace(new RegExp('/', 'g'),
				'')
				+ st.substr(0, 2);
		module.dataQueryModel.ed = $('#ed').val().replace(new RegExp('/', 'g'),
				'')
				+ et.substr(0, 2);
		var pa = "";
		for ( var i in module.dataQueryModel) {
			if (module.dataQueryModel[i].length > 0) {
				pa += "&" + i + "=" + module.dataQueryModel[i];
			}
		}
		// console.debug(module.dataQueryModel);
		ajx.getData({
			url : './report/getAvailableDataInQueryInterval?' + pa,
			callback : function(models) {
				module.datas = models;
				module.dataSelect = [];
				module.dataSelectName = [];
				models.sort(function(a, b) {
					return a.key.localeCompare(b.key);
				});
				
				for(var i=0;i<models.length;i++){
					module.dataSelect.push(models[i].key);
					module.dataSelectName.push(models[i].name);
				}
				
				var source = $('#data-list-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('#data-list').html('').html(html);
				ui.gotoMode('MODE_AFTER_TAG_QUERY');
			}
		});
	},
	viewData : function() {	
		module.queryChart();
		
		module.deferredObjectItems = [];
		module.raws = [];
		
		var queryData = {};
		
		for(var i=0;i<module.dataSelect.length;i++){
			var dkey = module.dataSelect[i].split('-');
			if(!queryData[dkey[1].substr(2,2)]){
				queryData[dkey[1].substr(2,2)] = [];
			}
			queryData[dkey[1].substr(2,2)].push(module.dataSelect[i]);
		}				
		
		for ( var k in queryData) {
			if (queryData[k].length > 0) {
				module.deferredObjectItems.push(module.queryDetail(queryData[k].join(','),k));				
			}
		}
		
		$.when.apply($, module.deferredObjectItems).done(function(){
			module.raws.sort(function(a,b){
				return parseInt(a.dataType) - parseInt(b.dataType);
			});
			var source = $('#detail-template').html();
			var template = Handlebars.compile(source);
			var html = template({
				ds : module.raws
			});

			$('#tableData > tbody').html('').html(html);

			ui.gotoMode('MODE_SHOW_DETAIL');
		});		
		
	},
	queryChart : function(){	
		sessionStorage.removeItem("chart");
		$('#reportChart').hide();
		var st = $('#st').val(), et = $('#et').val();
		if(st.length==4){
			st = '0'+st;
		}
		if(et.length==4){
			et = '0'+et;
		}
		var dataQueryModel = {
				companyId : module.companyId,
				sd : $('#sd').val().replace(new RegExp('/', 'g'),'')+ st.substr(0, 2),
				ed : $('#ed').val().replace(new RegExp('/', 'g'),'')+ et.substr(0, 2),
				datasStr : module.dataSelect.join(',')
			}
		var pa = "";
		for ( var i in dataQueryModel) {
			if (dataQueryModel[i].length > 0) {
				pa += "&" + i + "=" + dataQueryModel[i];
			}
		}
		
		var img = new Image();
		img.onload = function(){
			var canvas = document.createElement("canvas");
			canvas.width = this.width;
			canvas.height = this.height;
			var ctx = canvas.getContext("2d");
			ctx.drawImage(this, 0, 0);
			var dataURL = canvas.toDataURL("image/jpeg");
			
			sessionStorage.setItem("chart",dataURL);
			$('#reportChart').attr("src", sessionStorage.getItem("chart"));
			//console.debug(sessionStorage.getItem("chart"));
			$('#reportChart').show();
		}
		img.src = './report/getChart?' + pa;
		
	},
	queryDetail : function(mac, type) {
		var st = $('#st').val(), et = $('#et').val();
		if(st.length==4){
			st = '0'+st;
		}
		if(et.length==4){
			et = '0'+et;
		}
		var dataQueryModel = {
				companyId : module.companyId,
				sd : $('#sd').val().replace(new RegExp('/', 'g'),'')+ st.substr(0, 2),
				ed : $('#ed').val().replace(new RegExp('/', 'g'),'')+ et.substr(0, 2),
				datasStr : mac,
				dataType : type
			}
		var pa = "";
		for ( var i in dataQueryModel) {
			if (dataQueryModel[i].length > 0) {
				pa += "&" + i + "=" + dataQueryModel[i];
			}
		}
		return $.ajax({
			type : 'GET',
			url : './report/getDataInQueryIntervalByDataType?' + pa,
			success : function(rs) {
				module.raws.push(rs.content);
			}
		})
	}
};

var ui = {
	gotoMode : function(action) {
		if (action == 'MODE_SHOW_DETAIL') {
			$('#detail-section').show(200);
		} else if (action == 'MODE_AFTER_TAG_QUERY') {
			$('#detail-section').hide();
			$('#data-select').html(module.dataSelectName.join(', '));
			$('#chartHolder').html('');
			$('#data-select-info').show(200);
			$('#data-table').show(200);
			$('#btn-view-data').show(200);
			$('input[name="data-select-all"]').prop("checked", true);

		}
	}
};

$(document).ready(function() {

	module.companyId = $('.companyId').text();

	$('.datepicker').datepicker({
		format : 'yyyy/mm/dd'
	});

	$('.timepicker').timepicker({
		showMeridian : false,
		minuteStep : 60,
		defaultTime : 'current',
		icons : {
			up : 'el el-chevron-up',
			down : 'el el-chevron-down'
		}
	});

	$('#sd').datepicker('setDate', new Date());
	$('#ed').datepicker('setDate', new Date());

	$('#st').timepicker('setTime', getLastHour(1));
	$('#et').timepicker('setTime', getLastHour(0));

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		module.queryData();
	});

});

// checkbox事件
$(document).on(
		'change',
		'input[type="checkbox"]',
		function() {
			// 單一tag選取事件
			if ($(this).prop("name") == 'data-select') {
				if ($(this).prop("checked")) {
					var mac = $(this).prop("id");
					var name = $(this).prop("value");
					if (jQuery.inArray(mac, module.dataSelect) < 0) {
						module.dataSelect.push(mac);
						module.dataSelectName.push(name);
					}
					if (module.dataSelect.length > 0) {
						$('#data-select').html(module.dataSelectName.join(', '));
						if (module.dataSelect.length == module.datas.length) {
							$('input[name="data-select-all"]').prop("checked",
									true);
						}
					}
				} else {
					var mac = $(this).prop("id");
					var name = $(this).prop("value");
					module.dataSelect.remove(mac);
					module.dataSelectName.remove(name);
					if (module.dataSelect.length > 0) {
						$('#data-select').html(module.dataSelectName.join(', '));
					} else {
						$('#data-select').empty();
					}
					$('input[name="data-select-all"]').prop("checked", false);
				}
			}

			// Tag全選事件
			if ($(this).prop("name") == 'data-select-all') {
				$('input[name="data-select"]').prop("checked",
						$("#data-select-all").prop("checked"));

				$('input[name="data-select"]').each(function() {
					var mac = $(this).prop("id");
					var name = $(this).prop("value");
					if ($(this).prop("checked")) {
						if (jQuery.inArray(mac, module.dataSelect) < 0) {
							module.dataSelect.push(mac);
							module.dataSelectName.push(name);
						}
					} else {
						module.dataSelect.remove(mac);
						module.dataSelectName.remove(name);
					}
				});

				if (module.dataSelect.length > 0) {
					$('#data-select').html(module.dataSelectName.join(', '));
				} else {
					$('#data-select').empty();
				}
			}

		});

function getLastHour(i) {
	var date = new Date();
	date.setHours(date.getHours() - i);
	date.setMinutes(0);
	return date;
}