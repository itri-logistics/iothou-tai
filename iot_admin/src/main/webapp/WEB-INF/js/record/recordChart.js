var module = {
	companyId : "",
	tagQueryModel : {
		companyId : "",
		sd : "",
		ed : ""
	},
	tagSelect : [], // 被選取的tag
	tagSelectName : [], // 被選取的tag的名稱（確認選取用）
	chartModels : [],
	chartType : [],
	deferredObjectItems : [],
	datas : {
		temperature : [],
		humidity : [],
		temperature1 : [],
		temperature2 : []
	},
	queryTagData : function() {
		module.tagQueryModel.companyId = module.companyId;
		module.tagQueryModel.sd = $('#sd').val().replace(new RegExp('/', 'g'),
				'');
		module.tagQueryModel.ed = $('#ed').val().replace(new RegExp('/', 'g'),
				'');
		var pa = "";
		for ( var i in module.tagQueryModel) {
			if (module.tagQueryModel[i].length > 0) {
				pa += "&" + i + "=" + module.tagQueryModel[i];
			}
		}
		ajx.getData({
			url : './query/getTagsInQueryInterval?' + pa,
			callback : function(models) {
				module.tagSelect = [];
				module.tagSelectName = [];
				for (var i = 0; i < models.length; i++) {
					module.tagSelect.push(models[i].mac);
					module.tagSelectName.push(models[i].name);					
					var sigArray = models[i].signal.split(',');
					// console.debug(sigArray);
					for (var j = 0; j < sigArray.length; j++) {
						var port = sigArray[j].substr(0, 2);
						var type = sigArray[j].substr(2, 2);
						if (port == "00") {
							port = "本體";
						} else {
							port = "port" + sigArray[j].substr(1, 1);
						}

						if (type == "01") {
							type = "溫度"
						} else if (type == "02") {
							type = "濕度"
						}
						sigArray[j] = port + type;
					}
					models[i].signal = sigArray.join(', ');
				}
				module.tags = models;
				var source = $('#tag-list-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('#tag-list').html('').html(html);
				ui.gotoMode('MODE_AFTER_TAG_QUERY');
			}
		});
	},
	queryData : function(mac, name) {
		var dataQueryModel = {
			companyId : module.companyId,
			sd : $('#sd').val().replace(new RegExp('/', 'g'), ''),
			ed : $('#ed').val().replace(new RegExp('/', 'g'), ''),
			macsStr : mac // 被選取的tag
		}
		var pa = "";
		for ( var i in dataQueryModel) {
			if (dataQueryModel[i].length > 0) {
				pa += "&" + i + "=" + dataQueryModel[i];
			}
		}
		// console.debug(dataQueryModel);
		return $.ajax({
			type : 'GET',
			url : './query/getQueryData?' + pa,
			success : function(rs) {
				if(rs == "data size out of limit"){
					swal(name+'資料量過大，請縮短查詢的紀錄區間。');
					return;
				}
				var temp = rs.content.split('#');

				$('#tagName').html(name);
				$('#tagMac').html(mac);

				for (var i = 0; i < temp.length; i++) {
					var d = temp[i].split(',');
					if (d[0] == "0001") {// 本體溫度
						var chartModel = {
							signalName : "溫度（℃）",
							dataType : "temperature",
							chartType : "0001"
						};
						if ($.inArray("0001", module.chartType) < 0) {
							module.chartModels.push(chartModel);
							module.chartType.push("0001");
						}
						for (var j = 1; j < d.length; j++) {
							var t = "20" + d[j].substr(0, 2) + "/"
									+ d[j].substr(2, 2) + "/"
									+ d[j].substr(4, 2) + " "
									+ d[j].substr(6, 2) + ":"
									+ d[j].substr(8, 2) + ":"
									+ d[j].substr(10, 2); // 紀錄時間
							var model = {
								mac : mac,
								name : name,
								time : t,
								data : d[j].substring(12, d[j].length),
							};
							module.datas.temperature.push(model);
						}

					} else if (d[0] == "0002") {// 本體濕度
						var chartModel = {
							signalName : "濕度（%rH）",
							dataType : "humidity",
							chartType : "0002"
						};
						if ($.inArray("0002", module.chartType) < 0) {
							module.chartModels.push(chartModel);
							module.chartType.push("0002");
						}
						for (var j = 1; j < d.length; j++) {
							var t = "20" + d[j].substr(0, 2) + "/"
									+ d[j].substr(2, 2) + "/"
									+ d[j].substr(4, 2) + " "
									+ d[j].substr(6, 2) + ":"
									+ d[j].substr(8, 2) + ":"
									+ d[j].substr(10, 2); // 紀錄時間

							var model = {
								mac : mac,
								name : name,
								time : t,
								data : d[j].substring(12, d[j].length),
							};
							module.datas.humidity.push(model);
						}
					} else if (d[0] == "0101") {// port1溫度

						for (var j = 1; j < d.length; j++) {

							var val = parseFloat(d[j]
									.substring(12, d[j].length));
							if (val > -35) {
								var t = "20" + d[j].substr(0, 2) + "/"
										+ d[j].substr(2, 2) + "/"
										+ d[j].substr(4, 2) + " "
										+ d[j].substr(6, 2) + ":"
										+ d[j].substr(8, 2) + ":"
										+ d[j].substr(10, 2); // 紀錄時間
								var model = {
									mac : mac,
									name : name,
									time : t,
									data : d[j].substring(12, d[j].length),
								};
								module.datas.temperature1.push(model);
							}
						}

						if (module.datas.temperature1.length > 0) {
							if ($.inArray("0101", module.chartType) < 0) {
								var chartModel = {
									signalName : "port1溫度（℃）",
									dataType : "temperature1",
									chartType : "0101"
								};
								module.chartModels.push(chartModel);
								module.chartType.push("0101");
							}
						}

					} else if (d[0] == "0201") {// port2溫度

						for (var j = 1; j < d.length; j++) {
							var val = parseFloat(d[j]
									.substring(12, d[j].length));

							if (val > -35) {
								var t = "20" + d[j].substr(0, 2) + "/"
										+ d[j].substr(2, 2) + "/"
										+ d[j].substr(4, 2) + " "
										+ d[j].substr(6, 2) + ":"
										+ d[j].substr(8, 2) + ":"
										+ d[j].substr(10, 2); // 紀錄時間
								var model = {
									mac : mac,
									name : name,
									time : t,
									data : d[j].substring(12, d[j].length),
								};
								module.datas.temperature2.push(model);
							}
						}

						if (module.datas.temperature2.length > 0) {
							if ($.inArray("0201", module.chartType) < 0) {
								var chartModel = {
									signalName : "port2溫度（℃）",
									dataType : "temperature2",
									chartType : "0201"
								};
								module.chartModels.push(chartModel);
								module.chartType.push("0201");
							}
						}

					}
				}

				// console.debug(module.chartModels);
				// console.debug(module.datas);
				// console.debug(module.deferredObjectItems);

			}
		});
	},
	queryChart : function() {
		module.datas = {
			temperature : [],
			humidity : [],
			temperature1 : [],
			temperature2 : []
		};
		module.chartModels = [];
		module.chartType = [];
		module.deferredObjectItems = [];

		for (var i = 0; i < module.tagSelect.length; i++) {
			var q = module.queryData(module.tagSelect[i],
					module.tagSelectName[i]);
			module.deferredObjectItems.push(q);
		}

		$.when.apply($, module.deferredObjectItems).done(module.plot);

	},
	plot : function() {

		// 初始化圖表
		module.chartModels.sort(function(a, b) {
			return parseInt(a.chartType) - parseInt(b.chartType);
		});
		var source = $('#chart-template').html();
		var template = Handlebars.compile(source);
		var html = template({
			ds : module.chartModels
		});
		$('#chartHolder').html('').html(html);

		// 畫圖
		if (module.datas.temperature.length > 0) {
			var dArray = module.datas.temperature;
			dArray.sort(function(a, b) {
				return new Date(Date.parse(a.time)).valueOf()
						- new Date(Date.parse(b.time)).valueOf();
			});
			var chartLines = [];
			for (var i = 0; i < module.tagSelectName.length; i++) {
				chartLines.push({
					label : module.tagSelectName[i],
					data : []
				});
			}
			for (var i = 0; i < dArray.length; i++) {
				var pos = -1;
				for ( var j in chartLines) {
					if (chartLines[j].label == dArray[i].name) {
						pos = j;
						break;
					}
				}

				if (pos < 0) {
					chartLines.push({
						label : dArray[i].name,
						data : []
					});
					pos = chartLines.length - 1;
				}

				chartLines[pos].data.push([
						new Date(Date.parse(dArray[i].time)).valueOf(),
						parseFloat(dArray[i].data) ]);
			}
			var chart = $.plot('#temperature-chart', chartLines,
					chartOpt.temperatureOptions);
		}

		if (module.datas.humidity.length > 0) {
			var dArray = module.datas.humidity;
			dArray.sort(function(a, b) {
				return new Date(Date.parse(a.time)).valueOf()
						- new Date(Date.parse(b.time)).valueOf();
			});
			var chartLines = [];
			for (var i = 0; i < module.tagSelectName.length; i++) {
				chartLines.push({
					label : module.tagSelectName[i],
					data : []
				});
			}
			for (var i = 0; i < dArray.length; i++) {
				pos = -1;
				for ( var j in chartLines) {
					if (chartLines[j].label == dArray[i].name) {
						pos = j;
						break;
					}
				}

				if (pos < 0) {
					chartLines.push({
						label : dArray[i].name,
						data : []
					});
					pos = chartLines.length - 1;
				}

				chartLines[pos].data.push([
						new Date(Date.parse(dArray[i].time)).valueOf(),
						parseFloat(dArray[i].data) ]);
			}
			var chart = $.plot('#humidity-chart', chartLines,
					chartOpt.humidityOptions);
		}

		if (module.datas.temperature1.length > 0) {
			var dArray = module.datas.temperature1;
			dArray.sort(function(a, b) {
				return new Date(Date.parse(a.time)).valueOf()
						- new Date(Date.parse(b.time)).valueOf();
			});
			var chartLines = [];
			for (var i = 0; i < module.tagSelectName.length; i++) {
				chartLines.push({
					label : module.tagSelectName[i],
					data : []
				});
			}
			for (var i = 0; i < dArray.length; i++) {
				pos = -1;
				for ( var j in chartLines) {
					if (chartLines[j].label == dArray[i].name) {
						pos = j;
						break;
					}
				}

				if (pos < 0) {
					chartLines.push({
						label : dArray[i].name,
						data : []
					});
					pos = chartLines.length - 1;
				}

				chartLines[pos].data.push([
						new Date(Date.parse(dArray[i].time)).valueOf(),
						parseFloat(dArray[i].data) ]);
			}
			var chart = $.plot('#temperature1-chart', chartLines,
					chartOpt.temperatureOptions);
		}

		if (module.datas.temperature2.length > 0) {
			var dArray = module.datas.temperature2;
			dArray.sort(function(a, b) {
				return new Date(Date.parse(a.time)).valueOf()
						- new Date(Date.parse(b.time)).valueOf();
			});
			var chartLines = [];
			for (var i = 0; i < module.tagSelectName.length; i++) {
				chartLines.push({
					label : module.tagSelectName[i],
					data : []
				});
			}
			for (var i = 0; i < dArray.length; i++) {
				pos = -1;
				for ( var j in chartLines) {
					if (chartLines[j].label == dArray[i].name) {
						pos = j;
						break;
					}
				}

				if (pos < 0) {
					chartLines.push({
						label : dArray[i].name,
						data : []
					});
					pos = chartLines.length - 1;
				}

				chartLines[pos].data.push([
						new Date(Date.parse(dArray[i].time)).valueOf(),
						parseFloat(dArray[i].data) ]);
			}
			var chart = $.plot('#temperature2-chart', chartLines,
					chartOpt.temperatureOptions);
		}

	}
};

var ui = {
	gotoMode : function(action) {
		if (action == 'MODE_AFTER_TAG_QUERY') {
			$('#tag-select').html(module.tagSelectName.join());
			$('#chartHolder').html('');
			$('#tag-select-info').show(200);
			$('#tag-table').show(200);
			$('#btn-query-chart').show(200);
			$('input[name="tag-select-all"]').prop("checked", true);
		}
	},
	showTemperatureTooltip : function(label, x, y) {// 圖表tootip
		var rt = new Date(x).customFormat('#YYYY#/#MM#/#DD# #hhhh#:#mm#:#ss#');
		return label + '<br />' + y + '℃<br />' + rt;
	},
	showHumidityTooltip : function(label, x, y) {
		var rt = new Date(x).customFormat('#YYYY#/#MM#/#DD# #hhhh#:#mm#:#ss#');
		return label + '<br />' + y + '%rH<br />' + rt;
	},
	showLightTooltip : function(label, x, y) {
		var rt = new Date(x).customFormat('#hhhh#:#mm#:#ss#');
		return label + '<br />' + y + 'L<br />' + rt;
	}
};

var chartOpt = {
	// 圖表設定
	temperatureOptions : {// 溫度
		series : {
			lines : {// 線的樣式
				show : true,
				lineWidth : 3
			},
			points : {// 點的樣式
				show : false
			}
		},
		grid : {// 背景格線樣式
			hoverable : true, // IMPORTANT! this is needed for tooltip to work
			backgroundColor : "#f5fcfe", // 背景顏色
			color : "#000000" // 格線顏色
		},
		yaxis : {// y軸
			axisLabel : "溫度（℃）",
			min : -30,
			max : 200

		// 平移範圍
		},
		xaxis : {// x軸
			axisLabel : "時間",
			mode : "time", // 宣告x軸類型為時間
			timeformat : "%m/%d %H:%M", // 顯示時間
			ticks : 3,
			timezone : "browser" // 時區以瀏覽器為準
		},
		tooltip : true, // 開啟tooltip功能
		tooltipOpts : { // tooltip設定
			content : ui.showTemperatureTooltip, // tooltip顯示內容(函數)
			shifts : { // tooltip位置
				x : -60,
				y : 25
			}
		},
		legend : { // 資料標示
			show : true
		},
		zoom : { // 縮放
			interactive : true
		},
		pan : { // 平移
			interactive : true
		}
	},
	humidityOptions : {// 濕度
		series : {
			lines : {
				show : true,
				lineWidth : 3
			},
			points : {
				show : false
			}
		},
		grid : {
			hoverable : true, // IMPORTANT! this is needed for tooltip to work
			backgroundColor : "#f5fefa",
			color : "#000000"

		},
		yaxis : {
			axisLabel : "濕度（%rH）",
			min : -20,
			max : 120
		},
		xaxis : {// x軸
			axisLabel : "時間",
			mode : "time", // 宣告x軸類型為時間
			timeformat : "%m/%d %H:%M", // 顯示時間
			ticks : 3,
			timezone : "browser" // 時區以瀏覽器為準
		},
		tooltip : true,
		tooltipOpts : {
			content : ui.showHumidityTooltip,
			shifts : {
				x : -60,
				y : 25
			}
		},
		legend : {
			show : true
		},
		zoom : {
			interactive : true
		},
		pan : {
			interactive : true
		}
	},
	lightOptions : { // 光度
		series : {
			lines : {
				show : true,
				lineWidth : 3
			},
			points : {
				show : false
			}
		},
		grid : {
			hoverable : true, // IMPORTANT! this is needed for tooltip to work
			backgroundColor : "#fcf8e3",
			color : "#000000"

		},
		yaxis : {
			axisLabel : "光度（kL）",
			min : 0,
			max : 120
		},
		xaxis : {// x軸
			axisLabel : "時間",
			mode : "time", // 宣告x軸類型為時間
			timeformat : "%m/%d %H:%M", // 顯示時間
			ticks : 3,
			timezone : "browser" // 時區以瀏覽器為準
		},
		tooltip : true,
		tooltipOpts : {
			content : ui.showLightTooltip,
			shifts : {
				x : -60,
				y : 25
			}
		},
		legend : {
			show : true
		},
		zoom : {
			interactive : true
		},
		pan : {
			interactive : true
		}
	}
};

$(document).ready(function() {

	module.companyId = $('.companyId').text();
	
	$('#tag-table').hide();
	$('#tableData').hide();

	$('.datepicker').datepicker({
		format : 'yyyy/mm/dd'
	});

	$('#sd').datepicker('setDate', getYesterday());
	$('#ed').datepicker('setDate', new Date());

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		module.queryTagData();
	});

});

// checkbox事件
$(document).on(
		'change',
		'input[type="checkbox"]',
		function() {
			// 單一tag選取事件
			if ($(this).prop("name") == 'tag-select') {
				if ($(this).prop("checked")) {
					var mac = $(this).prop("id");
					var name = $(this).prop("value");
					if (jQuery.inArray(mac, module.tagSelect) < 0) {
						module.tagSelect.push(mac);
						module.tagSelectName.push(name);
					}
					if (module.tagSelect.length > 0) {
						$('#tag-select').html(module.tagSelectName.join(', '));
						if (module.tagSelect.length == module.tags.length) {
							$('input[name="tag-select-all"]').prop("checked",
									true);
						}
					}
				} else {
					var mac = $(this).prop("id");
					var name = $(this).prop("value");
					module.tagSelect.remove(mac);
					module.tagSelectName.remove(name);
					if (module.tagSelect.length > 0) {
						$('#tag-select').html(module.tagSelectName.join(', '));
					} else {
						$('#tag-select').empty();
					}
					$('input[name="tag-select-all"]').prop("checked", false);
				}
			}

			// Tag全選事件
			if ($(this).prop("name") == 'tag-select-all') {
				$('input[name="tag-select"]').prop("checked",
						$("#tag-select-all").prop("checked"));

				$('input[name="tag-select"]').each(function() {
					var mac = $(this).prop("id");
					var name = $(this).prop("value");
					if ($(this).prop("checked")) {
						if (jQuery.inArray(mac, module.tagSelect) < 0) {
							module.tagSelect.push(mac);
							module.tagSelectName.push(name);
						}
					} else {
						module.tagSelect.remove(mac);
						module.tagSelectName.remove(name);
					}
				});

				if (module.tagSelect.length > 0) {
					$('#tag-select').html(module.tagSelectName.join(', '));
				} else {
					$('#tag-select').empty();
				}
			}

		});

function getYesterday() {
	var date = new Date();
	date.setDate(date.getDate() - 1);
	return date;
}
