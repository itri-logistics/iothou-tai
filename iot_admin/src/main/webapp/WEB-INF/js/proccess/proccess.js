var itemMacs1 = [ 'WIFIT00047', 'WIFIT00048'];
var itemMacs2 = [ 'WIFIT00046' ];
var itemMacs;
var placeMacs;
var itemMac = "";
var itemDisName = "貨品溫度";
var module = {
	sd : '',
	ed : '',
	itemSsid : [], // [{time, data}, {time, data},...]
	placeDataList : {}, // {placeIds[0]:[{time, data}, {time, data},...],
	// placeId[1]:[...],...
	itemData : [],
	places : {},
	tagInfos : [],
	deferredObjectItems : [],
	getPlaces : function() {
		module.places = {};
		ajx.getData({
			url : './place/find?companyId=' + $('.companyId').text(),
			callback : function(models) {
				for (var i = 0; i < models.length; i++) {
					module.places[models[i].placeId] = {
						placeId : models[i].placeId,
						disName : models[i].disName
					};
				}
			}
		});
		module.queryTag();
	},
	queryTag : function() {
		var param = 'companyId=' + $('.companyId').text();
		ajx.getData({
			url : './tag/find?' + param,
			callback : function(models) {
				module.tagInfos = models;
			}
		});
	},
	getData : function(mac, placeId, sd, ed) {
		var dataQueryModel = {};
		dataQueryModel.companyId = module.companyId;
		dataQueryModel.sd = sd;
		dataQueryModel.ed = ed;
		dataQueryModel.macsStr = mac;
		var pa = "";
		for ( var i in dataQueryModel) {
			if (dataQueryModel[i].length > 0) {
				pa += "&" + i + "=" + dataQueryModel[i];
			}
		}
		return $.ajax({
			type : 'GET',
			url : '../iot_realtime/' + $('.server').text()
					+ '/query/getQueryData?' + pa,
			success : function(rs) {
				var temp = rs.content.split('#');

				module.placeDataList[placeId] = [];
				for (var i = 0; i < temp.length; i++) {
					var d = temp[i].split(',');
					if (d[0] == "0001") {// 本體溫度
						for (var j = 1; j < d.length; j++) {
							if (d[j].length > 12) {
								var t = "20" + d[j].substr(0, 12); // 紀錄時間
								var model = {
									time : t,
									data : d[j].substring(12)
								};
								module.placeDataList[placeId].push(model);
							}
						}
					}
				}
				module.placeDataList[placeId].sort(function(a, b) {
					return fDate(a.time).valueOf() - fDate(b.time).valueOf();
				});
				console.log(placeId + ' done.');
			}
		})
	},
	queryTagData : function() {
		var tagQueryModel = {};
		tagQueryModel.companyId = module.companyId;
		tagQueryModel.sd = $('#sd').val().replace(new RegExp('/', 'g'), '')
				.substr(2);
		tagQueryModel.ed = $('#ed').val().replace(new RegExp('/', 'g'), '')
				.substr(2);
		var pa = "";
		for ( var i in tagQueryModel) {
			if (tagQueryModel[i].length > 0) {
				pa += "&" + i + "=" + tagQueryModel[i];
			}
		}
		ajx.getData({
			url : '../iot_realtime/' + $('.server').text()
					+ '/query/getTagsInQueryInterval?' + pa,
			callback : function(models) {
				var tags = [];
				for (var i = 0; i < models.length; i++) {
					if ($.inArray(models[i].mac, itemMacs) >= 0) {
						var sigArray = models[i].signal.split(',');
						if ($.inArray('0001', sigArray) >= 0) {
							models[i].name = models[i].mac;
							for (var j = 0; j < module.tagInfos.length; j++) {
								if (module.tagInfos[j].mac == models[i].mac) {
									models[i].name = module.tagInfos[j].name;
									break;
								}
							}
							tags.push(models[i]);							
						}

					}

				}

				var source = $('#data-list-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : tags
				});
				$('#data-list').html('').html(html);
				ui.gotoMode('MODE_AFTER_TAG_QUERY');
			}
		});
	},
	getItemData : function(mac, sd, ed) {
		sd = sd.replace(new RegExp('/', 'g'), '');
		ed = ed.replace(new RegExp('/', 'g'), '');
		module.sd = sd;
		module.ed = ed;
		module.placeDataList = {};
		module.itemData = [];
		module.itemSsid = [];
		itemMac = mac;
		placeIds = [];
		placeMacs = [];

		var dataQueryModel = {};
		dataQueryModel.companyId = module.companyId;
		dataQueryModel.sd = sd;
		dataQueryModel.ed = ed;
		dataQueryModel.macsStr = mac;
		var pa = "";
		for ( var i in dataQueryModel) {
			if (dataQueryModel[i].length > 0) {
				pa += "&" + i + "=" + dataQueryModel[i];
			}
		}
		$
				.ajax({
					type : 'GET',
					url : '../iot_realtime/' + $('.server').text()
							+ '/query/getQueryData?' + pa,
					success : function(rs) {
						var temp = rs.content.split('#');
						var t;
						var tt;
						var maxT;
						var max = 0;
						for (var i = 0; i < temp.length; i++) {
							var d = temp[i].split(',');
							if (d[0] == "0001") {// 本體溫度
								for (var j = 1; j < d.length; j++) {
									if (d[j].length > 12) {
										var t = "20" + d[j].substr(0, 12); // 紀錄時間
										tt = fDate(t).valueOf();
										if (tt - max > 0) {
											max = tt;
											maxT = t;
										}
										var model = {
											time : t,
											data : d[j].substring(12)
										};
										module.itemData.push(model);
									}
								}
							} else if (d[0] == "0010") { // ssid
								for (var j = 1; j < d.length; j++) {
									var data = d[j].substring(12);
									if (d[j].length > 12) {
										t = "20" + d[j].substr(0, 12); // 紀錄時間
										tt = fDate(t).valueOf();
										if (tt - max > 0) {
											max = tt;
											maxT = t;
										}
										var old = $.inArray(data, placeIds);
										if (old < 0) {
											var disName = '';
											try {
												disName = (module.places[data]).disName;
											} catch (e) {
												console.error('場域' + data
														+ '不存在於此公司');
												console.error(e);
												continue;
											}

											var model = {
												time : t,
												pid : data,
												disName : disName
											};

											module.itemSsid.push(model);
											placeMacs.push('WIFI0000' + data);
											placeIds.push(data);
										} else {
											console.debug(old);
											var oldT = module.itemSsid[old].time;
											if (tt - fDate(oldT).valueOf() < 0) {
												module.itemSsid[old].time = t;
											}
										}
									}
								}
							}
							var model = {
								time : maxT,
								pid : 'last',
								disName : ''
							};
							module.itemSsid.push(model);
						}
						module.itemData.sort(function(a, b) {
							return fDate(a.time).valueOf()
									- fDate(b.time).valueOf();
						});
						module.getAllData(mac, sd, ed);
					}
				})
	},
	getAllData : function(mac, sd, ed) {

		module.deferredObjectItems = [];

		for (var i = 0; i < placeIds.length; i++) {
			var q = module.getData(placeMacs[i], placeIds[i], sd, ed);
			module.deferredObjectItems.push(q);
		}
		$.when.apply($, module.deferredObjectItems).done(function(args) {
			console.log(args);
			ui.gotoMode('SHOW_RESULT');
		});
	}

}

var ui = {
	gotoMode : function(action) {
		if (action == 'SHOW_RESULT') {
			console.log('show result.');
			$('section[role="chart"]').show();
			plot.createDataSet();
			plot.draw();
		} else if (action == 'MODE_AFTER_TAG_QUERY') {
			$('section[role="query"]').show();
		}
	}
}

$(document).ready(function() {

	module.companyId = $('.companyId').text();

	if (module.companyId == 'hct') {
		itemMacs = itemMacs1;
	} else if (module.companyId == 'lgsc') {
		itemMacs = itemMacs2;
	}

	$('.datepicker').datepicker({
		format : 'yyyy/mm/dd'
	});

	$('#sd').datepicker('setDate', new Date());
	$('#ed').datepicker('setDate', new Date());

	module.getPlaces();

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		module.queryTagData();
	});

});

var plot = {
	dataset : [],
	main : null,
	cycleR : 10,
	limitLine : null,
	width : 1000,
	height : 500,
	padding : {
		top : 50,
		right : 50,
		bottom : 50,
		left : 50
	},
	draw : function() {
		$('.svg-container svg').html('');

		plot.main = d3.select('.svg-container svg').attr("preserveAspectRatio",
				"xMinYMin meet").attr("viewBox", "0 0 1000 500").classed(
				'svg-content-responsive', true).append('g')
				.attr(
						'transform',
						"translate(" + plot.padding.top + ','
								+ plot.padding.left + ')');

		// x軸比例尺
		var xScale = d3.time.scale().range(
				[ 0, plot.width - plot.padding.left - plot.padding.right ]);

		xScale.domain(d3.extent(module.itemData, function(d) {
			return fDate(d.time);
		}));

		// y軸比例尺
		var yScale = d3.scale.linear().domain(
				[ d3.min(module.itemData, function(d) {
					return parseFloat(d.data) - 40;
				}), d3.max(module.itemData, function(d) {
					return parseFloat(d.data) + 50;
				}) ]).range(
				[ plot.height - plot.padding.top - plot.padding.bottom, 0 ]);

		// 畫折線
		var line = d3.svg.line().x(function(d, i) {
			return xScale(fDate(d.time));
		}).y(function(d, i) {
			return yScale(parseFloat(d.data));
		}).interpolate('linear');

		plot.drawChart(xScale, yScale, line);
		plot.drawProccess(xScale, yScale);
		plot.addStationBackgound(xScale);
		plot.addText(xScale);
		plot.addRect(xScale);
		plot.drawBounds(line);
		plot.drawLabels(xScale, yScale)
	},
	drawChart : function(xScale, yScale, line) { // 畫折線圖

		// 產生x軸
		var xAxis = d3.svg.axis().scale(xScale).tickFormat(
				d3.time.format('%H:%M:%S')).orient('bottom').ticks(5);
		// 產生y軸
		var yAxis = d3.svg.axis().scale(yScale).orient('left')

		// 增加x軸的SVG元素
		plot.main
				.append('g')
				.attr('class', 'axis')
				.attr(
						'transform',
						'translate(0,'
								+ (plot.height - plot.padding.top - plot.padding.bottom)
								+ ')').call(xAxis);

		// 增加y軸的SVG元素
		plot.main.append('g').attr('class', 'axis').call(yAxis);

		plot.main.append('path').attr('class', 'itemLine').attr('d',
				line(module.itemData));

		plot.main.append('path').attr('class', 'line').attr('d',
				line(plot.dataset));

	},
	drawProccess : function(xScale, yScale) { // 畫歷程
		var minStationX = d3.min(module.itemSsid, function(d) {
			return fDate(d.time);
		});
		var maxStationX = d3.max(module.itemSsid, function(d) {
			return fDate(d.time);
		});

		var maxStationY = d3.max(module.itemData, function(d) {
			return parseFloat(d.data);
		}) + 50;

		var sline = [ {
			x : minStationX
		}, {
			x : maxStationX
		} ];

		var stationLine = d3.svg.line().x(function(d) {
			return xScale(d.x)
		}).y(function(d) {
			return yScale(maxStationY);
		}).interpolate('linear');

		plot.main.append('path').attr('class', 'stationLine').attr('d',
				stationLine(sline));
	},
	drawLabels : function(xScale, yScale) {
		plot.main.append('text').attr('x',
				plot.width - plot.padding.left - plot.padding.right - 70).attr(
				'y', plot.height - plot.padding.top - plot.padding.bottom - 50)
				.text('■').attr('class', 'itemText');

		plot.main.append('text').attr('x',
				plot.width - plot.padding.left - plot.padding.right - 60).attr(
				'y', plot.height - plot.padding.top - plot.padding.bottom - 50)
				.text('貨品溫度');

		plot.main.append('text').attr('x',
				plot.width - plot.padding.left - plot.padding.right - 70).attr(
				'y', plot.height - plot.padding.top - plot.padding.bottom - 30)
				.text('■').attr('class', 'text');

		plot.main.append('text').attr('x',
				plot.width - plot.padding.left - plot.padding.right - 60).attr(
				'y', plot.height - plot.padding.top - plot.padding.bottom - 30)
				.text('歷程溫度');
	},
	drawBounds : function(line) {
		var lowOrUpBoundValue = 8;
		var lowOrUpBound = 'UPPER';
		plot.addBoundLine(lowOrUpBoundValue, line, lowOrUpBound);

		lowOrUpBoundValue = 0;
		lowOrUpBound = 'LOW';
		plot.addBoundLine(lowOrUpBoundValue, line, lowOrUpBound);
	},
	addBoundLine : function(lowBoundValue, lineStyle, lowOrUpBound) {
		// limit Line
		var data = [ {
			time : module.itemData[0].time,
			data : lowBoundValue
		}, {
			time : module.itemData[module.itemData.length - 1].time,
			data : lowBoundValue
		} ];
		var boundClass;
		if (lowOrUpBound == 'LOW') {
			boundClass = 'lineLowLimit';
		} else if (lowOrUpBound == 'UPPER') {
			boundClass = 'lineUpperLimit';
		}
		limitLine = plot.main.append('path').attr('class', boundClass).attr(
				'd', lineStyle(data));
	},
	addText : function(xScale) {
		plot.main.selectAll('div').data(module.itemSsid).enter().append('text')
				.attr('x', function(d, i) {
					return xScale(fDate(d.time));
					/*var pos = xScale(fDate(d.time)) - 25;
					if (i == module.itemSsid.length - 1) {
						pos = xScale(fDate(d.time)) - 20;
					} else if (i == 0) {
						pos = xScale(fDate(d.time)) + 10;
					}
					return pos;*/
				}).attr('y', function(d, i) {
					if (i % 4 == 0) {
						return plot.cycleR - 45;
					} else if(i % 4 == 1){
						return plot.cycleR - 25;
					}else if(i % 4 == 2){
						return plot.cycleR + 15;
					}else if(i % 4 == 3){
						return plot.cycleR + 35;
					}
				}).text(function(d) {
					return d.disName;
				});

		/*plot.main.selectAll('div').data(module.itemSsid).enter().append('text')
				.attr('x', function(d, i) {
					var pos = xScale(fDate(d.time)) - 25;
					if (i == module.itemSsid.length - 1) {
						pos = xScale(fDate(d.time)) - 40;
					} else if (i == 0) {
						pos = xScale(fDate(d.time)) + 10;
					}
					return pos;
				}).attr('y', function(d, i) {
					if (i % 2 == 0) {
						return plot.cycleR + 40;
					} else {
						return plot.cycleR - 20;
					}
				}).text(function(d, i) {
					return fDate(d.time).customFormat("#hhhh#:#mm#:#ss#");
				});*/

	},
	addRect : function(xScale) {
		plot.main.selectAll('div').data(module.itemSsid).enter().append('rect')
				.attr('x', function(d, i) {
					return xScale(fDate(d.time));
				}).attr('y', plot.cycleR).attr('id', function(d, i) {
					return 'station_' + i
				}).attr(
						'width',
						function(d, i) {
							var rs = 0;
							if (i < module.itemSsid.length - 1) {
								rs = xScale(fDate(module.itemSsid[i + 1].time))
										- xScale(fDate(d.time));
							}
							// console.debug(rs);
							return rs;
						}).attr('height', plot.cycleR + 400).style("opacity",
						0.0);
	},
	addStationBackgound : function(xScale) {
		var r = plot.cycleR;
		plot.main.selectAll('circle').data(module.itemSsid).enter().append(
				'circle').attr('cx', function(d) {
			return xScale(fDate(d.time))
		}).attr('cy', 0).attr('r', r).attr('fill', '#00C4EA').on(
				'click',
				function(e, i) {
					// console.debug('test');
					var newwin = window.open();
					newwin.location = './proccessGraphic?placeId='
							+ module.itemSsid[i].pid + '&sd=' + module.sd
							+ '&ed=' + module.ed;
				}).on(
				'mouseover',
				function(e, i) {
					// limitLine.style("opacity", 1);
					d3.select(this).style("stroke", "#0077B3").style(
							"stroke-width", "5");
					d3.select(plot.main.selectAll('rect')[0][i]).style(
							"opacity", 0.2);

				}).on('mouseout', function(e, i) {
			d3.select(this).style("stroke-width", "0");
			d3.select(plot.main.selectAll('rect')[0][i]).style("opacity", 0);
			// limitLine.style("opacity", 0.0);
		}).append("svg:title").text(function(d) {
			return d.disName+'\n'+fDate(d.time).customFormat("#YYYY#/#MM#/#DD# #hhhh#:#mm#:#ss#");
		});
	},
	createDataSet : function() {
		plot.dataset = [];
		var dataList = module.placeDataList;
		module.itemSsid.sort(function(a, b) {
			return fDate(a.time).valueOf() - fDate(b.time).valueOf();
		});

		var maxTime;
		var minTime;
		var pid;
		var pNum = module.itemSsid.length;
		for (var i = 1; i < pNum; i++) {
			maxTime = module.itemSsid[i].time;
			minTime = module.itemSsid[i - 1].time;
			pid = module.itemSsid[i - 1].pid;
			if ($.inArray(pid, placeIds) >= 0) {
				for (var j = 0; j < dataList[pid].length; j++) {
					if (i == pNum - 1) {
						if (dataList[pid][j].time >= minTime) {
							plot.dataset.push(dataList[pid][j]);
						}
					} else {
						if (dataList[pid][j].time < maxTime
								&& dataList[pid][j].time >= minTime) {
							plot.dataset.push(dataList[pid][j]);
						}
					}
				}
			}
		}

	}
}

function fDate(_14str) {
	return new Date(parseInt(_14str.substr(0, 4)),
			parseInt(_14str.substr(4, 2)) - 1, parseInt(_14str.substr(6, 2)),
			parseInt(_14str.substr(8, 2)), parseInt(_14str.substr(10, 2)),
			parseInt(_14str.substr(12, 2)));
}

function getLastHour(i) {
	var date = new Date();
	date.setHours(date.getHours() - i);
	date.setMinutes(0);
	return date;
}

var testItemData = "0010,1709261800000,1709261810008,1709261820008,1709261830008,1709261840008,1709261850008,1709261900008,1709261910008,1709261920008,1709261930008,1709261940009,1709261950009,1709262000009,1709262010009,1709262020009,1709262030009,1709262040009,1709262050009,1709262100009,1709262110009,1709262120009,1709262130009,1709262140009,1709262150009,1709262200009,1709262210009,1709262220009,1709262230009,1709262240009,1709262250009,1709262300009,17092623100010,17092623200010,17092623300010,17092623400011,17092623500011,#0001,1709261800001.86,1709261810002.16,1709261820001.25,1709261830004.89,1709261840004.05,1709261850004.92,1709261900000.54,1709261910000.99,1709261920001.69,1709261930002.11,1709261940002.28,1709261950004.77,170926200000-3.88,1709262010001.57,170926202000-2.17,1709262030005.28,1709262040003.84,1709262050009.57,170926210000-1.86,1709262110003.86,170926212000-2.19,1709262130005.23,170926214000-1.67,1709262150004.79,1709262200003.27,1709262210005.11,1709262220001.90,1709262230001.93,1709262240002.73,1709262250007.06,1709262300002.87,1709262310008.69,1709262320005.27,1709262330005.94,170926234000-1.52,1709262350002.36,";
var data1 = "0001,1709261800001.43,1709261810002.22,1709261820005.51,1709261830000.91,1709261840003.76,1709261850003.91,1709261900002.92,1709261910001.30,1709261920003.50,1709261930006.45,1709261940006.09,1709261950004.20,1709262000002.09,1709262010000.40,170926202000-1.78,1709262030003.32,1709262040004.87,1709262050003.15,1709262100002.46,170926211000-0.30,1709262120004.28,1709262130003.57,1709262140004.90,1709262150002.48,1709262200007.03,1709262210003.94,1709262220003.91,1709262230004.51,1709262240000.01,1709262250002.77,1709262300002.00,1709262310003.52,1709262320004.00,1709262330004.25,1709262340004.54,1709262350000.62,";
var data2 = "0001,1709261800001.43,1709261810002.22,1709261820005.51,1709261830000.91,1709261840003.76,1709261850003.91,1709261900002.92,1709261910001.30,1709261920003.50,1709261930006.45,1709261940006.09,1709261950004.20,1709262000002.09,1709262010000.40,170926202000-1.78,1709262030003.32,1709262040004.87,1709262050003.15,1709262100002.46,170926211000-0.30,1709262120004.28,1709262130003.57,1709262140004.90,1709262150002.48,1709262200007.03,1709262210003.94,1709262220003.91,1709262230004.51,1709262240000.01,1709262250002.77,1709262300002.00,1709262310003.52,1709262320004.00,1709262330004.25,1709262340004.54,1709262350000.62,";
var data3 = "0001,1709261800001.43,1709261810002.22,1709261820005.51,1709261830000.91,1709261840003.76,1709261850003.91,1709261900002.92,1709261910001.30,1709261920003.50,1709261930006.45,1709261940006.09,1709261950004.20,1709262000002.09,1709262010000.40,170926202000-1.78,1709262030003.32,1709262040004.87,1709262050003.15,1709262100002.46,170926211000-0.30,1709262120004.28,1709262130003.57,1709262140004.90,1709262150002.48,1709262200007.03,1709262210003.94,1709262220003.91,1709262230004.51,1709262240000.01,1709262250002.77,1709262300002.00,1709262310003.52,1709262320004.00,1709262330004.25,1709262340004.54,1709262350000.62,";
var data4 = "0001,1709261800001.43,1709261810002.22,1709261820005.51,1709261830000.91,1709261840003.76,1709261850003.91,1709261900002.92,1709261910001.30,1709261920003.50,1709261930006.45,1709261940006.09,1709261950004.20,1709262000002.09,1709262010000.40,170926202000-1.78,1709262030003.32,1709262040004.87,1709262050003.15,1709262100002.46,170926211000-0.30,1709262120004.28,1709262130003.57,1709262140004.90,1709262150002.48,1709262200007.03,1709262210003.94,1709262220003.91,1709262230004.51,1709262240000.01,1709262250002.77,1709262300002.00,1709262310003.52,1709262320004.00,1709262330004.25,1709262340004.54,1709262350000.62,1709270000000.62";