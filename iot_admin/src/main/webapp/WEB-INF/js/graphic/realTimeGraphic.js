var refreshThread;
var LOWER_BOUND = 2, UPPER_BOUND = 20; // 溫度尺顯示範圍，單位為度C
var sensor = {
	dataType : [ "00", "01", "02", "03", "04", "05", "06" ],
	sensorName : [ "無", "溫度", "濕度", "低溫探針", "高溫探針", "光度", "電流" ]
};
var intervalSlider = $('#slider-play-interval').bootstrapSlider();
var module = {
	companyId : "",
	placeSelect : "",
	alertLowerBound : LOWER_BOUND,
	alertUpperBound : UPPER_BOUND,
	places : [],
	rawData : [],
	sensors : [],
	sensorPositions : [],
	tags : [],
	deferredObjectItems : [],
	macIdDataTypes : "",
	queryPlace : function() {
		ajx.getData({
			url : './place/find?companyId=' + module.companyId,
			callback : function(models) {
				var placeIds = [];
				for (var i = 0; i < models.length; i++) {
					module.places.push(models[i]);
					placeIds.push(models[i].placeId+'');
				}
				if (module.places.length > 0) {
					var source = $('#place-list-template').html();
					var template = Handlebars.compile(source);
					var html = template({
						ds : module.places
					});

					$('#place-list').html('').html(html);					
					
					var reg = new RegExp("(^|&)placeId=([^&]*)(&|$)");
					var pid = window.location.search.substr(1).match(reg);
					console.debug(pid);
					if (pid != null){
						if(pid.length>2){
							pid = pid[2];
						}
						if($.inArray(pid, placeIds)>=0){
							module.queryTag(pid);
						}
					}
					
				} else {
					$('#place-list').html('無場域資料');
				}
			}
		});
	},
	queryTag : function(placeId) {
		clearInterval(refreshThread);
		module.placeSelect = placeId;
		var param = 'companyId=' + module.companyId + '&placeId=' + module.placeSelect;
		console.debug(param);
		ajx.getData({
			url : './tag/find?' + param,
			callback : function(models) {
				module.tags = models; // deep clone
				module.sensors = [];
				module.macIdDataTypes = "";

				for (var i = 0; i < models.length; i++) {
					module.macIdDataTypes += "," + models[i].mac + "_0001";
					var key = models[i].companyId + '-' + models[i].placeId
							+ '-' + models[i].mac + '-';
					module.sensors.push({
						key : key + '00',
						name : models[i].name,
						mac : models[i].mac,
						port : '本體',
						sensorName : '溫度',
						size : '12',
						style : 'primary',
						value : ''
					});

					if (models[i].port1 != '00') {
						pos = $.inArray(models[i].port1, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '01',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port1',
								sensorName : sensorName,
								size : '12',
								style : 'success',
								value : ''
							});
						}
						module.macIdDataTypes += "," + models[i].mac + "_0101";
					}

					if (models[i].port2 != '00') {
						pos = $.inArray(models[i].port2, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '02',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port2',
								sensorName : sensorName,
								size : '12',
								style : 'warning',
								value : ''
							});
						}
						module.macIdDataTypes += "," + models[i].mac + "_0201";
					}

					if (models[i].port3 != '00') {
						pos = $.inArray(models[i].port3, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '03',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port3',
								sensorName : sensorName,
								size : '12',
								style : 'info',
								value : ''
							});
						}
					}

					if (models[i].port4 != '00') {
						pos = $.inArray(models[i].port4, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '04',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port4',
								sensorName : sensorName,
								size : '12',
								style : 'danger',
								value : ''
							});
						}
					}

				}
				// console.debug(module.datas);

				var source = $('#tag-list-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : module.sensors
				});
				$('#tag-list').html('').html(html);
				if(module.macIdDataTypes.length > 0 ){
					module.macIdDataTypes = module.macIdDataTypes.substr(1);					
				}
				// 下載raw data
				module.queryData();
			}
		});
	},
	queryData : function() {	
		module.rawData = [];
		console.debug(module.macIdDataTypes);
		ajx.getData({
			url : '../iot_realtime/' + $('.server').text()
			+ '/query/getNewestTagsData?companyId='
				+ $('.companyId').text() + '&macIdDataTypes='+ module.macIdDataTypes,
			callback : function(models) {				
				var now = new Date().getTime();
				for ( var dataType in models) {
					var d = models[dataType];
					var t = d.lastDataDatetimeStr.substr(8, 2) + ":"
					+ d.lastDataDatetimeStr.substr(10, 2) + ":"
					+ d.lastDataDatetimeStr.substr(12, 2);
					var recordTime = parseInt(d.lastDataDatetime);					
					if(now - recordTime <= 300000){// 5分鐘內
						var key = module.companyId + '-' + module.placeSelect + '-' + d.macId + '-' + d.dataType.substr(0,2);	
						var model = {
								id : key,
								time : recordTime,
								value : d.lastData,
						};
						module.rawData.push(model);						
					}			
					
				}
				clearInterval(refreshThread);
				refreshThread = setInterval(module.queryData, 30000);		

				module.querySensorPositon();
			}
		});
	},	
	querySensorPositon : function() {

		// 刷新table
		$('#graphic-containment').html('<tbody></tbody>');
		// 產生20*20的方格
		for (var i = 0; i < 20; i++) {
			html = '';
			for (var j = 0; j < 20; j++) {
				html += '<td><span class="fa-stack"></span></td>';
			}
			$('<tr></tr>').html(html).appendTo('#graphic-containment > tbody');
		}

		// 更改背景圖
		$('#graphic').css(
				"background-image",
				"url(./place/img/" + module.companyId + "/"
						+ module.placeSelect + ")");

		// 取得sensor位置
		var param = 'companyId=' + module.companyId + '&placeId='
				+ module.placeSelect;
		ajx.getData({
			url : './sensorPosition/find?' + param,
			callback : function(models) {
				module.sensorPositions = [];
				for (var i = 0; i < models.length; i++) {
					module.sensorPositions.push(models[i].id);
					var size = '12';
					var port = 'port' + models[i].port.substr(1, 1);
					var sensorName = '溫度';
					var style = 'primay';
					var icon = 'wi-thermometer';

					if (models[i].port == '00') {
						port = '本體';
						icon = 'fa fa-tag'
					}

					var name = models[i].mac;
					for (var j = 0; j < module.tags.length; j++) {
						if (models[i].mac == module.tags[j].mac) {
							name = module.tags[j].name;
							if (models[i].port == '01') {
								pos = $.inArray(module.tags[j].port1,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'success';
							} else if (models[i].port == '02') {
								pos = $.inArray(module.tags[j].port2,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'warning';
							} else if (models[i].port == '03') {
								pos = $.inArray(module.tags[j].port3,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'info';
							} else if (models[i].port == '04') {
								pos = $.inArray(module.tags[j].port4,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'danger';
							}
							var sensorInfo = {
									id : models[i].id,
									mac : models[i].mac,
									name : name,
									port : port,
									sensorName : sensorName,
									size : size,
									style : style,
									icon : icon,
									value : ''
								};

								$('#' + models[i].id + ' .pull-right').html(
										'<input type="checkbox" name="checkbox-tag-enable" value="'
												+ i + '" checked /> 顯示');

								var source = $('#sensor-template').html();
								var template = Handlebars.compile(source);
								var html = template(sensorInfo);
								var pos = $('#graphic-containment > tbody').find(
										'tr:eq(' + models[i].x + ')').find(
										'td:eq(' + models[i].y + ')');
								$(pos).html('').html(html);
							break;
						}
					}
					
				}

				ui.gotoMode('MODE_PLAY_ANIMATION');

			}
		});
	}
};

var ui = {
	playing : false,
	gotoMode : function(action, p1, p2) {
		if (action == 'MODE_PLAY_ANIMATION') {
			// 顯示圖控畫面
			$('section[role="result"]').show();
			// 產生溫度範圍
			ui.drawColorRuler();
			// 溫度顏色
			ui.drawTempColor();
		}
	},	
	drawTempColor : function() {

		// 溫度背景
		var css = '';
		// console.log('rawData[' + playProgress + '].length = ' +
		// rawData[playProgress].length);
		$('#graphic-containment>tbody>tr>td')
				.each(
						function() {
							if ($(this).children('div').length > 0
									&& $(this).children('span').is(':visible')) {
								var id = $(this).children('div').attr('id');
								var v;
								for (var j = 0; j < module.rawData.length; j++) {
									if (module.rawData[j].id == id) {
										v = parseFloat(module.rawData[j].value);
										break;
									}
								}

								$(this).children('div').attr('value', v);
								$('#tag-list div[id="' + id + '"]').attr(
										'value', v);

								// 產生css
								// var left = $(this).offset().left -
								// $('#graphic-containment >
								// tbody').offset().left + ($(this).width() /
								// 2);
								console.debug($(this).offset().left);
								console.debug($('#graphic-containment > tbody')
										.offset().left);
								var left = $(this).offset().left
										- $('#graphic-containment > tbody')
												.offset().left
										+ ($(this).width() / 2);
								var top = $(this).offset().top
										- $('#graphic-containment > tbody')
												.offset().top
										+ ($(this).height() / 2);

								if (typeof v === 'undefined' || v === null) {
									css += 'radial-gradient( 100px at '
											+ left
											+ 'px '
											+ top
											+ 'px , rgba(140,140,140,1), rgba(255,255,255,0)),';
									$(
											'#tag-list div[id="' + id
													+ '"] .temprature').hide();
								} else if (v <= -35) {
									css += 'radial-gradient( 100px at '
											+ left
											+ 'px '
											+ top
											+ 'px , rgba(140,140,140,1), rgba(255,255,255,0)),';
									$(
											'#tag-list div[id="' + id
													+ '"] .temprature').hide();
								} else {
									var tColor = temperatureColor(v);
									console.debug(v);
									console.debug(tColor);
									css += 'radial-gradient( 100px at ' + left
											+ 'px ' + top + 'px ,' + tColor
											+ ', rgba(255,255,255,0)),';
									$(
											'#tag-list div[id="' + id
													+ '"] .temprature').html(v)
											.show();
								}
							}
						});

		$('#graphic-containment').css('background-image',
				css.substring(0, css.length - 1));
	},
	drawColorRuler : function() {
		$('#color-ruler').empty();

		var css = '';
		var html = '';

		$('#minValue').html(module.alertLowerBound - 5);
		$('#maxValue').html(module.alertUpperBound + 5);
		$('#lowerBound').html(module.alertLowerBound);
		$('#upperBound').html(module.alertUpperBound);
		$('#lowerBound').show();
		$('#upperBound').show();
		for (var i = module.alertLowerBound - 5; i <= module.alertUpperBound + 5; i++) {
			css = temperatureColor(i);
			html += '<td id="d' + i
					+ '" data-toggle="tooltip" data-placement="bottom" title="'
					+ i + '℃" style="background: ' + css + ';"></td>';
		}
		$('#color-ruler').html(html);
		
		var left = $('#d' + module.alertLowerBound).position().left;
		var top = $('#minValue').offset().top - parseInt($('.inner-wrapper').css('padding-top').slice(0,-2));
		// var top = $('#color-ruler').offset().top +
		// $('#color-ruler').height();
		$('#lowerBound').css('left', left + 'px').css('top', top + 'px');

		left = $('#d' + module.alertUpperBound).position().left;
		// top = $('#color-ruler').offset().top + $('#color-ruler').height();
		$('#upperBound').css('left', left + 'px').css('top', top + 'px');
	},
	showTooltip : function(id, name) {
		var t, h, time, port;
		port = id.split('-')[3];
		if (port == '00') {
			port = '本體';
		} else {
			port = 'port' + port.substr(1, 1);
		}
		for (var j = 0; j < module.rawData.length; j++) {
			if (module.rawData[j].id == id) {
				t = module.rawData[j].value;
				time = new Date(module.rawData[j].time)
						.customFormat("#YYYY#/#MM#/#DD# #hhhh#:#mm#");
				break;
			}
		}
		var html = '<div class="row">';
		html += '<i class="fa fa-tag fa-fw fa-lg"></i>' + name + ' ' + port;
		html += '<div class="temprature"> <i class="wi wi-thermometer "></i> '
				+ (typeof t === 'undefined' || t === null ? '無資料' : t)
				+ '</div> ';
		html += '<div class="time"> <i class="fa fa-clock-o"></i> '
				+ (typeof time === 'undefined' || time === null ? '無資料' : time)
				+ '</div>';
		html += '</div>';

		// tooltip顯示位置
		$('#tooltip').html(html);
		var left = $('#graphic-containment>tbody div[id="' + id + '"]')
				.parent().offset().left;
		var top = $('#graphic-containment>tbody div[id="' + id + '"]').parent()
				.offset().top
				+ $('#graphic-containment>tbody div[id="' + id + '"]').parent()
						.height();
		$('#tooltip').css('left', left + 'px').css('top', top + 'px');
		$('#tooltip').fadeIn('slow');
		$('#tooltip').show();
		$(pos).children('span:first').addClass('animated bounce');
	}
};

$(document).ready(function() {

	module.companyId = $('.companyId').text();
	
	module.queryPlace();

});

// 視窗大小改變時，重畫背景
$(window).resize(function() {
	ui.drawTempColor();
	ui.drawColorRuler();
});

$('.body').bind("resize", function() {
	ui.drawTempColor();
	ui.drawColorRuler();
});

$(document).on('click', '.sidebar-toggle', function() {
	ui.drawTempColor();
	ui.drawColorRuler();
});

// 平面圖上的tag hover出現最新溫、濕度
$(document).on(
		'mouseenter',
		'#graphic-containment>tbody>tr>td',
		function(event) {

			if ($(this).children('div').length > 0
					&& $(this).children('span').is(':visible')) {
				var i = $(this).children('div').attr('id');
				var name = $(this).children('div').attr('name');
				ui.showTooltip(i, name);
			}

		});

// tag列表上hover，平面圖上相對應的tag出現最新溫、濕度
$(document).on(
		'mouseenter',
		'.sensor',
		function(event) {
			var id = $(this).attr('id');
			var name = $(this).attr('name');
			if ($(this).children('div').find(
					'input[name="checkbox-tag-enable"]').prop('checked')) {
				if ($.inArray(id, module.sensorPositions) >= 0) {
					ui.showTooltip(id, name);
				}
			}

		});

// 滑鼠離開平面圖事件
$(document).on('mouseleave', '#graphic-containment>tbody>tr>td',
		function(event) {

			$('#tooltip').hide();
			$(this).children('span:first').removeClass('ui-state-hover');

		});

// 滑鼠離開tag列表事件
$(document).on('mouseleave', '.sensor', function(event) {

	var id = $(this).parent().attr('id');

	var pos = $('#graphic-containment>tbody div[id="' + id + '"]').parent();
	$(pos).children('span:first').removeClass('animated bounce');
	$('#tooltip').hide();

});

// TAG list 中顯示/隱藏物件 觸發事件
$(document).on('change', 'input[name="checkbox-tag-enable"]', function(event) {

	var id = $(this).parent().parent().parent().parent().attr('id');
	var pos = $('#graphic-containment>tbody div[id="' + id + '"]').parent();
	if ($(this).prop('checked')) {
		$(pos).children('span:first').show();
	} else {
		$(pos).children('span:first').hide();
	}

	ui.drawTempColor();

});

function getYesterday() {
	var date = new Date();
	date.setDate(date.getDate() - 1);
	return date;
}

// 計算溫度顏色
function temperatureColor(degree) {

	var l = module.alertLowerBound;
	var h = module.alertUpperBound;

	var range;
	var d = 0;
	if (l < 0) {
		d = 0 - l + 1;
		degree += d;
		l += d;
		h += d;
	}

	if (l > 0) {
		d = 0 + l - 1;
		degree -= d;
		l -= d;
		h -= d;
	}

	var r, g, b;

	range = h - l;

	if (degree < l) {// 低於下限
		b = 255;
		g = 0;
		r = 0;
	} else if (degree > h) {// 高於上限
		r = 255;
		b = 0;
		g = 0;
	} else {// 中間，從綠到紅
		if (degree > (l + range / 2)) {
			g = 255 - (degree - l - 1 - range / 2) * 255 / (range / 2);
			b = 0;
			r = (degree - l - 1) * 255 / range;
		} else if (degree < (l + range / 2)) {
			g = (degree - l + 1) * 255 / (range / 2);
			r = 0;
			b = 255 - (degree - l - 1) * 255 / range;
		} else {
			g = 255;
			r = 0;
			b = 0;
		}
	}

	if (r < 0)
		r = 0;
	if (r > 255)
		r = 255;

	if (g < 0)
		g = 0;
	if (g > 255)
		g = 255;

	if (b < 0)
		b = 0;
	if (b > 255)
		b = 255;

	// console.log("r = " + r + ", g = " + g + ", b = " + b);

	return ' rgba(' + Math.floor(r) + ',' + Math.floor(g) + ',' + Math.floor(b)
			+ ',1)';
}