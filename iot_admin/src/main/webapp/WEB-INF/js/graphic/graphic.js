var LOWER_BOUND = -10, UPPER_BOUND = 50; // 溫度尺顯示範圍，單位為度C
var player = {
	playProgress : 0,
	playing : false,
	timer : null
}
var sensor = {
	dataType : [ "00", "01", "02", "03", "04", "05", "06" ],
	sensorName : [ "無", "溫度", "濕度", "低溫探針", "高溫探針", "光度", "電流" ]
};
var intervalSlider = $('#slider-play-interval').bootstrapSlider();
var module = {
	companyId : "",
	placeSelect : "",
	alertLowerBound : LOWER_BOUND,
	alertUpperBound : UPPER_BOUND,
	places : [],
	rawData : [],
	sensors : [],
	sensorPositions : [],
	tags : [],
	deferredObjectItems : [],
	queryDataInInterval : function() {
		var queryModel = {
			companyId : module.companyId,
			sd : $('#sd').val().replace(new RegExp('/', 'g'), ''),
			ed : $('#ed').val().replace(new RegExp('/', 'g'), '')
		};
		var pa = "";
		for ( var i in queryModel) {
			if (queryModel[i].length > 0) {
				pa += "&" + i + "=" + queryModel[i];
			}
		}
		ajx.getData({
			url : './query/getTagsInQueryInterval?' + pa,
			callback : function(models) {
				module.deferredObjectItems = [];
				module.places = [];
				var places = [];
				// console.debug(models);
				for (var i = 0; i < models.length; i++) {
					if ($.inArray(models[i].placeId, places) < 0) {
						places.push(models[i].placeId);
					}
				}

				for (var i = 0; i < places.length; i++) {
					module.deferredObjectItems.push(module
							.queryPlace(places[i]))
				}

				$.when.apply($, module.deferredObjectItems).done(function() {
					if (module.places.length > 0) {
						var source = $('#place-list-template').html();
						var template = Handlebars.compile(source);
						var html = template({
							ds : module.places
						});

						$('#place-list').html('').html(html);
					} else {
						$('#place-list').html('此時間區間內無資料');
					}
				});
			}
		});
	},
	queryPlace : function(placeId) {
		if(placeId.length>0){
			var param = 'placeId=' + placeId;
			return $.ajax({
				type : 'GET',
				url : './place/find?' + param,
				success : function(data) {
					data = JSON.parse(data);				
					for (var i = 0; i < data.content.length; i++) {
						module.places.push(data.content[i]);
					}

				}
			});
		}
		
	},
	queryTag : function(placeId) {
		module.placeSelect = placeId;
		var param = 'companyId=' + module.companyId + '&placeId='
				+ module.placeSelect;
		ajx.getData({
			url : './tag/find?' + param,
			callback : function(models) {
				module.tags = models; // deep clone
				module.sensors = [];
				var tags = [];

				for (var i = 0; i < models.length; i++) {
					tags.push(models[i].mac);
					var key = models[i].companyId + '-' + models[i].placeId
							+ '-' + models[i].mac + '-';
					module.sensors.push({
						key : key + '00',
						name : models[i].name,
						mac : models[i].mac,
						port : '本體',
						sensorName : '溫度',
						size : '12',
						style : 'primary',
						value : ''
					});

					if (models[i].port1 != '00') {
						pos = $.inArray(models[i].port1, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '01',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port1',
								sensorName : sensorName,
								size : '12',
								style : 'success',
								value : ''
							});
						}
					}

					if (models[i].port2 != '00') {
						pos = $.inArray(models[i].port2, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '02',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port2',
								sensorName : sensorName,
								size : '12',
								style : 'warning',
								value : ''
							});
						}
					}

					if (models[i].port3 != '00') {
						pos = $.inArray(models[i].port3, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '03',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port3',
								sensorName : sensorName,
								size : '12',
								style : 'info',
								value : ''
							});
						}
					}

					if (models[i].port4 != '00') {
						pos = $.inArray(models[i].port4, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '04',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port4',
								sensorName : sensorName,
								size : '12',
								style : 'danger',
								value : ''
							});
						}
					}

				}
				// console.debug(module.datas);

				var source = $('#tag-list-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : module.sensors
				});
				$('#tag-list').html('').html(html);

				// 下載raw data
				module.rawData = [];
				module.deferredObjectItems = [];
				for (var i = 0; i < tags.length; i++) {
					module.deferredObjectItems.push(module.queryData(tags[i]));
				}

				$.when.apply($, module.deferredObjectItems).done(
						module.processRawData);

			}
		});
	},
	queryData : function(mac) {
		var dataQueryModel = {
			companyId : module.companyId,
			sd : $('#sd').val().replace(new RegExp('/', 'g'), ''),
			ed : $('#ed').val().replace(new RegExp('/', 'g'), ''),
			macsStr : mac
		}
		var pa = "";
		for ( var i in dataQueryModel) {
			if (dataQueryModel[i].length > 0) {
				pa += "&" + i + "=" + dataQueryModel[i];
			}
		}
		return $.ajax({
			type : 'GET',
			url : './query/getQueryData?' + pa,
			success : function(rs) {
				if(rs.content=='data size out of limit'){
					swal('資料量過大，無法檢視','最多僅能檢視15天的紀錄，請縮短查詢的紀錄區間。','warning');
					return;
				}
				var temp = rs.content.split('#');
				var key = module.companyId + '-' + module.placeSelect + '-'
						+ mac + '-';
				for (var i = 0; i < temp.length; i++) {
					var d = temp[i].split(',');
					if (d[0] == "0001") {// 本體溫度
						for (var j = 1; j < d.length; j++) {
							var t = "20" + d[j].substr(0, 2) + "/"
									+ d[j].substr(2, 2) + "/"
									+ d[j].substr(4, 2) + " "
									+ d[j].substr(6, 2) + ":"
									+ d[j].substr(8, 2) + ":"
									+ d[j].substr(10, 2); // 紀錄時間
							var model = {
								id : key + '00',
								time : new Date(Date.parse(t)).valueOf(),
								value : d[j].substring(12, d[j].length),
							};
							module.rawData.push(model);
						}

					} else if (d[0] == "0101") {// port1溫度
						for (var j = 1; j < d.length; j++) {
							var t = "20" + d[j].substr(0, 2) + "/"
									+ d[j].substr(2, 2) + "/"
									+ d[j].substr(4, 2) + " "
									+ d[j].substr(6, 2) + ":"
									+ d[j].substr(8, 2) + ":"
									+ d[j].substr(10, 2); // 紀錄時間
							var model = {
								id : key + '01',
								time : new Date(Date.parse(t)).valueOf(),
								value : d[j].substring(12, d[j].length),
							};
							module.rawData.push(model);
						}

					} else if (d[0] == "0201") {// port2溫度
						for (var j = 1; j < d.length; j++) {
							var t = "20" + d[j].substr(0, 2) + "/"
									+ d[j].substr(2, 2) + "/"
									+ d[j].substr(4, 2) + " "
									+ d[j].substr(6, 2) + ":"
									+ d[j].substr(8, 2) + ":"
									+ d[j].substr(10, 2); // 紀錄時間
							var model = {
								id : key + '02',
								time : new Date(Date.parse(t)).valueOf(),
								value : d[j].substring(12, d[j].length),
							};
							module.rawData.push(model);
						}

					}
				}
			}
		});
	},
	processRawData : function() {

		module.rawData.sort(function(a, b) {
			return a.time - b.time;
		});

		// 時間最小值、最大值
		$('#minTime').html(
				new Date(module.rawData[0].time)
						.customFormat("#YYYY#/#MM#/#DD# #hhhh#:#mm#"));
		$('#maxTime').html(
				new Date(module.rawData[module.rawData.length - 1].time)
						.customFormat("#YYYY#/#MM#/#DD# #hhhh#:#mm#"));

		var rawDataGroup = [];
		var rawData = [];
		var add = true;

		for (var i = 0; i < module.rawData.length; i++) {
			add = true;
			for (var j = 0; j < rawDataGroup.length; j++) {
				if (module.rawData[i].id == rawDataGroup[j].id) {
					if (module.rawData[i].time - rawDataGroup[j].time >= 30000) {
						rawData.push(rawDataGroup.slice());
						rawDataGroup.remove(rawDataGroup[j]);
					} else {
						add = false;
					}
					break;
				}
			}
			if (add) {
				rawDataGroup.push(module.rawData[i]);
				// console.log('add: ' + rawDataTemp[i].rawTime);
			}

		}

		// console.log('rawData = \n' + JSON.stringify(rawData));

		if (add) {
			rawData.push(rawDataGroup.slice());
		}

		module.rawData = rawData;
		module.querySensorPositon();
	},
	querySensorPositon : function() {

		// 刷新table
		$('#graphic-containment').html('<tbody></tbody>');
		// 產生20*20的方格
		for (var i = 0; i < 20; i++) {
			html = '';
			for (var j = 0; j < 20; j++) {
				html += '<td><span class="fa-stack"></span></td>';
			}
			$('<tr></tr>').html(html).appendTo('#graphic-containment > tbody');
		}

		// 更改背景圖
		$('#graphic').css(
				"background-image",
				"url(./place/img/" + module.companyId + "/"
						+ module.placeSelect + ")");

		// 取得sensor位置
		var param = 'companyId=' + module.companyId + '&placeId='
				+ module.placeSelect;
		ajx.getData({
			url : './sensorPosition/find?' + param,
			callback : function(models) {
				module.sensorPositions = [];
				for (var i = 0; i < models.length; i++) {
					module.sensorPositions.push(models[i].id);
					var size = '12';
					var port = 'port' + models[i].port.substr(1, 1);
					var sensorName = '溫度';
					var style = 'primay';
					var icon = 'wi-thermometer';

					if (models[i].port == '00') {
						port = '本體';
						icon = 'fa fa-tag'
					}

					var name = models[i].mac;
					for (var j = 0; j < module.tags.length; j++) {
						if (models[i].mac == module.tags[j].mac) {
							name = module.tags[j].name;
							if (models[i].port == '01') {
								pos = $.inArray(module.tags[j].port1,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'success';
							} else if (models[i].port == '02') {
								pos = $.inArray(module.tags[j].port2,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'warning';
							} else if (models[i].port == '03') {
								pos = $.inArray(module.tags[j].port3,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'info';
							} else if (models[i].port == '04') {
								pos = $.inArray(module.tags[j].port4,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'danger';
							}
							var sensorInfo = {
									id : models[i].id,
									mac : models[i].mac,
									name : name,
									port : port,
									sensorName : sensorName,
									size : size,
									style : style,
									icon : icon,
									value : ''
								};

								$('#' + models[i].id + ' .pull-right').html('<input type="checkbox" name="checkbox-tag-enable" value="' + i + '" checked /> 顯示');

								var source = $('#sensor-template').html();
								var template = Handlebars.compile(source);
								var html = template(sensorInfo);
								var pos = $('#graphic-containment > tbody').find(
										'tr:eq(' + models[i].x + ')').find(
										'td:eq(' + models[i].y + ')');
								$(pos).html('').html(html);
							break;
						}
					}

					
				}

				ui.gotoMode('MODE_PLAY_ANIMATION');

			}
		});
	}
};

var ui = {
	playing : false,
	gotoMode : function(action, p1, p2) {
		if (action == 'MODE_PLAY_ANIMATION') {
			player.playProgress = 0;
			// 產生滑動捲軸
			if (module.rawData.length > 1) {
				intervalSlider = $('#slider-play-interval').bootstrapSlider({
					tooltip : 'always',
					orientation : 'horizontal',
					tooltip_position : 'up',
					step : 1,
					min : 0,
					max : module.rawData.length - 1,
					value : player.playProgress,
					formatter : sliderTooltipFormatter
				});
			} else {
				intervalSlider = $('#slider-play-interval').bootstrapSlider({
					tooltip : 'always',
					orientation : 'horizontal',
					tooltip_position : 'up',
					step : 1,
					min : 0,
					max : 1,
					value : 1,
					formatter : sliderTooltipFormatter
				});
			}

			// 顯示圖控畫面
			$('section[role="result"]').show();
			// 產生溫度範圍
			ui.drawColorRuler();
			// 溫度變化動畫
			ui.tempColorAnimate();
		}
	},
	tempColorAnimate : function() {
		if (module.rawData.length > 1) {
			if (player.playProgress < module.rawData.length) {
				player.playing = true;
				player.timer = setTimeout(ui.tempColorAnimate, 500);
				ui.drawTempColor();
				player.playProgress++;
				intervalSlider.bootstrapSlider('setValue', player.playProgress);
			} else {
				$('#btn-play').html('<i class="fa fa-rotate-left"></i> 重播');
				player.playing = false;
				player.playProgress = module.rawData.length - 1;
				intervalSlider.bootstrapSlider('setValue', player.playProgress);
			}
		} else {
			$('#btn-play').html('<i class="fa fa-rotate-left"></i> 重播');
			player.playing = false;
			player.playProgress = 0;
			ui.drawTempColor();
			intervalSlider.bootstrapSlider('setValue', 1);
		}
	},
	drawTempColor : function() {

		// 溫度背景
		var css = '';
		// console.log('rawData[' + playProgress + '].length = ' +
		// rawData[playProgress].length);
		$('#graphic-containment>tbody>tr>td')
				.each(
						function() {
							if ($(this).children('div').length > 0
									&& $(this).children('span').is(':visible')) {
								var id = $(this).children('div').attr('id');
								var v;
								for (var j = 0; j < module.rawData[player.playProgress].length; j++) {
									if (module.rawData[player.playProgress][j].id == id) {
										v = parseFloat(module.rawData[player.playProgress][j].value);
										break;
									}
								}

								$(this).children('div').attr('value', v);
								$('#tag-list div[id="' + id + '"]').attr(
										'value', v);

								// 產生css
								// var left = $(this).offset().left -
								// $('#graphic-containment >
								// tbody').offset().left + ($(this).width() /
								// 2);
								console.debug($(this).offset().left);
								console.debug($('#graphic-containment > tbody')
										.offset().left);
								var left = $(this).offset().left
										- $('#graphic-containment > tbody')
												.offset().left
										+ ($(this).width() / 2);
								var top = $(this).offset().top
										- $('#graphic-containment > tbody')
												.offset().top
										+ ($(this).height() / 2);

								if (typeof v === 'undefined' || v === null) {
									css += 'radial-gradient( 100px at '
											+ left + 'px '
											+ top + 'px , rgba(140,140,140,1), rgba(255,255,255,0)),';
									$('#tag-list div[id="' + id + '"] .temprature').hide();
								} else if (v <= -35){
									css += 'radial-gradient( 100px at '
										+ left + 'px '
										+ top + 'px , rgba(140,140,140,1), rgba(255,255,255,0)),';
									$('#tag-list div[id="' + id + '"] .temprature').hide();
								} else {
									var tColor = temperatureColor(v);
									console.debug(v);
									console.debug(tColor);
									css += 'radial-gradient( 100px at ' + left
											+ 'px ' + top + 'px ,' + tColor
											+ ', rgba(255,255,255,0)),';
									$('#tag-list div[id="' + id+ '"] .temprature').html(v).show();
								}
							}
						});

		$('#graphic-containment').css('background-image',
				css.substring(0, css.length - 1));
	},
	drawColorRuler : function() {
		$('#color-ruler').empty();

		var css = '';
		var html = '';

		$('#minValue').html(module.alertLowerBound - 5);
		$('#maxValue').html(module.alertUpperBound + 5);
		$('#lowerBound').html(module.alertLowerBound);
		$('#upperBound').html(module.alertUpperBound);
		$('#lowerBound').show();
		$('#upperBound').show();
		for (var i = module.alertLowerBound - 5; i <= module.alertUpperBound + 5; i++) {
			css = temperatureColor(i);
			html += '<td id="d' + i
					+ '" data-toggle="tooltip" data-placement="bottom" title="'
					+ i + '℃" style="background: ' + css + ';"></td>';
		}
		$('#color-ruler').html(html);
		
		var left = $('#d' + module.alertLowerBound).position().left;
		var top = $('#minValue').offset().top - parseInt($('.inner-wrapper').css('padding-top').slice(0,-2));
		//var top = $('#color-ruler').offset().top + $('#color-ruler').height();
		$('#lowerBound').css('left', left + 'px').css('top', top + 'px');

		left = $('#d' + module.alertUpperBound).position().left;
		//top = $('#color-ruler').offset().top + $('#color-ruler').height();
		$('#upperBound').css('left', left + 'px').css('top', top + 'px');
	},
	pause : function() {
		player.playing = false;
		clearTimeout(player.timer);
		$('#btn-play').html('<i class="fa fa-play"></i> 播放');
	},
	playHandler : function() {
		if (player.playing) {
			ui.pause();
		} else {
			$('#btn-play').html('<i class="fa fa-pause"></i> 暫停');
			if (player.playProgress >= module.rawData.length - 1) {
				player.playProgress = 0;
			}
			ui.tempColorAnimate();
		}
	},
	showTooltip : function(id, name) {
		var t, h, time, port;
		port = id.split('-')[3];
		if(port == '00'){
			port = '本體';
		}else{
			port = 'port'+port.substr(1,1);
		}
		for (var j = 0; j < module.rawData[player.playProgress].length; j++) {
			if (module.rawData[player.playProgress][j].id == id) {
				t = module.rawData[player.playProgress][j].value;				
				time = new Date(module.rawData[player.playProgress][j].time)
						.customFormat("#YYYY#/#MM#/#DD# #hhhh#:#mm#");
				break;
			}
		}
		var html = '<div class="row">';
		html += '<i class="fa fa-tag fa-fw fa-lg"></i>' + name + ' ' + port;
		html += '<div class="temprature"> <i class="wi wi-thermometer "></i> '
				+ (typeof t === 'undefined' || t === null ? '無資料' : t)
				+ '</div> ';
		html += '<div class="time"> <i class="fa fa-clock-o"></i> '
				+ (typeof time === 'undefined' || time === null ? '無資料' : time)
				+ '</div>';
		html += '</div>';

		// tooltip顯示位置
		$('#tooltip').html(html);
		var left = $('#graphic-containment>tbody div[id="'+id+'"]').parent().offset().left;
		var top = $('#graphic-containment>tbody div[id="'+id+'"]').parent().offset().top + $('#graphic-containment>tbody div[id="'+id+'"]').parent().height();
		$('#tooltip').css('left', left + 'px').css('top', top + 'px');
		$('#tooltip').fadeIn('slow');
		$('#tooltip').show();
		$(pos).children('span:first').addClass('animated bounce');
	}
};

// 播放進度條tooltip
function sliderTooltipFormatter() {
	var i = intervalSlider.bootstrapSlider('getValue');
	var lastTime = 0;
	if (i >= module.rawData.length) {
		i = module.rawData.length - 1;
	}
	for (var j = 0; j < module.rawData[i].length; j++) {
		if (module.rawData[i][j].time > lastTime) {
			lastTime = module.rawData[i][j].time;
		}
	}

	return new Date(lastTime).customFormat("#YYYY#/#MM#/#DD# #hhhh#:#mm#");
}

$(document).ready(function() {

	module.companyId = $('.companyId').text();

	$('.datepicker').datepicker({
		format : 'yyyy/mm/dd'
	});

	$('#sd').datepicker('setDate', getYesterday());
	$('#ed').datepicker('setDate', new Date());

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		module.queryDataInInterval();
	});

});

// 視窗大小改變時，重畫背景
$(window).resize(function() {
	ui.drawTempColor();
	ui.drawColorRuler();
});

$('.body').bind("resize", function() {
	ui.drawTempColor();
	ui.drawColorRuler();
});

$(document).on('click', '.sidebar-toggle', function() {
	ui.drawTempColor();
	ui.drawColorRuler();
});

// 平面圖上的tag hover出現最新溫、濕度
$(document).on(
		'mouseenter',
		'#graphic-containment>tbody>tr>td',
		function(event) {

			if ($(this).children('div').length > 0
					&& $(this).children('span').is(':visible')) {
				var i = $(this).children('div').attr('id');
				var name = $(this).children('div').attr('name');
				ui.showTooltip(i , name);
			}

		});

// tag列表上hover，平面圖上相對應的tag出現最新溫、濕度
$(document).on(
		'mouseenter',
		'.sensor',
		function(event) {
			var id = $(this).attr('id');
			var name = $(this).attr('name');
			if ($(this).children('div').find('input[name="checkbox-tag-enable"]').prop(
					'checked')) {
				if ($.inArray(id, module.sensorPositions) >= 0) {
					ui.showTooltip(id, name);
				}
			}

		});

// 滑鼠離開平面圖事件
$(document).on('mouseleave', '#graphic-containment>tbody>tr>td',
		function(event) {

			$('#tooltip').hide();
			$(this).children('span:first').removeClass('ui-state-hover');

		});

// 滑鼠離開tag列表事件
$(document).on(
		'mouseleave',
		'.sensor',
		function(event) {

			var id = $(this).parent().attr('id');
			
			var pos = $('#graphic-containment>tbody div[id="'+id+'"]').parent();
			$(pos).children('span:first').removeClass('animated bounce');
			$('#tooltip').hide();

		});

//TAG list 中顯示/隱藏物件 觸發事件
$(document).on('change', 'input[name="checkbox-tag-enable"]', function (event) {

	var id = $(this).parent().parent().parent().parent().attr('id');
	var pos = $('#graphic-containment>tbody div[id="'+id+'"]').parent();
	if ($(this).prop('checked')) {
		$(pos).children('span:first').show();
	} else {
		$(pos).children('span:first').hide();
	}

	ui.drawTempColor();

});

// 點播放進度條時，先暫停播放
$("#slider-play-interval").on("slideStart", function(slideEvt) {
	ui.pause();
});
// 拖拉播放進度條時，同時更改畫面
$("#slider-play-interval").on("slide", function(slideEvt) {
	player.playProgress = slideEvt.value;
	ui.drawTempColor();
});

function getYesterday() {
	var date = new Date();
	date.setDate(date.getDate() - 1);
	return date;
}

// 計算溫度顏色
function temperatureColor(degree) {

	var l = module.alertLowerBound;
	var h = module.alertUpperBound;

	var range;
	var d = 0;
	if (l < 0) {
		d = 0 - l + 1;
		degree += d;
		l += d;
		h += d;
	}

	if (l > 0) {
		d = 0 + l - 1;
		degree -= d;
		l -= d;
		h -= d;
	}

	var r, g, b;

	range = h - l;

	if (degree < l) {// 低於下限
		b = 255;
		g = 0;
		r = 0;
	} else if (degree > h) {// 高於上限
		r = 255;
		b = 0;
		g = 0;
	} else {// 中間，從綠到紅
		if (degree > (l + range / 2)) {
			g = 255 - (degree - l - 1 - range / 2) * 255 / (range / 2);
			b = 0;
			r = (degree - l - 1) * 255 / range;
		} else if (degree < (l + range / 2)) {
			g = (degree - l + 1) * 255 / (range / 2);
			r = 0;
			b = 255 - (degree - l - 1) * 255 / range;
		} else {
			g = 255;
			r = 0;
			b = 0;
		}
	}

	if (r < 0)
		r = 0;
	if (r > 255)
		r = 255;

	if (g < 0)
		g = 0;
	if (g > 255)
		g = 255;

	if (b < 0)
		b = 0;
	if (b > 255)
		b = 255;

	// console.log("r = " + r + ", g = " + g + ", b = " + b);

	return ' rgba(' + Math.floor(r) + ',' + Math.floor(g) + ',' + Math.floor(b)
			+ ',1)';
}