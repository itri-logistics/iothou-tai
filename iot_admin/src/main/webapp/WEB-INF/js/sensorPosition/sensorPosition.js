var sensor = {
	dataType : [ "00", "01", "02", "03", "04", "05", "06" ],
	sensorName : [ "無", "溫度", "濕度", "低溫探針", "高溫探針", "光度", "電流" ]
};
var module = {
	companyId : "",
	placeSelect : "",
	places : [],
	tags : [],
	sensors : [],
	sensorPosition : [],
	deferredObjectItems : [],
	queryPlace : function() {
		var param = 'companyId=' + module.companyId;
		ajx.getData({
			url : './place/find?' + param,
			callback : function(models) {
				module.places = models;
				var source = $('#place-list-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});

				$('#place-list').html('').html(html);

			}
		});
	},
	queryTag : function(placeId) {
		module.placeSelect = placeId;
		var param = 'companyId=' + module.companyId + '&placeId=' + module.placeSelect;
		ajx.getData({
			url : './tag/find?' + param,
			callback : function(models) {
				module.tags = models; // deep clone
				module.sensors = [];

				for (var i = 0; i < models.length; i++) {
					var key = models[i].companyId + '-' + models[i].placeId
							+ '-' + models[i].mac+'-';
					
					module.sensors.push({
						key : key + '00',
						name : models[i].name,
						mac : models[i].mac,
						port : '本體',
						sensorName : '溫度',
						size : '12',
						style : 'primary'
					});

					if (models[i].port1 != '00') {
						var pos = $.inArray(models[i].port1, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '01',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port1',
								sensorName : sensorName,
								size : '12',
								style : 'success'
							});
						}
					}

					if (models[i].port2 != '00') {
						pos = $.inArray(models[i].port2, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '02',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port2',
								sensorName : sensorName,
								size : '12',
								style : 'warning'
							});
						}
					}

					if (models[i].port3 != '00') {
						pos = $.inArray(models[i].port3, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '03',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port3',
								sensorName : sensorName,
								size : '12',
								style : 'info'
							});
						}
					}

					if (models[i].port4 != '00') {
						pos = $.inArray(models[i].port4, sensor.dataType);
						if (pos >= 0) {
							var sensorName = sensor.sensorName[pos];
							module.sensors.push({
								key : key + '04',
								name : models[i].name,
								mac : models[i].mac,
								port : 'port4',
								sensorName : sensorName,
								size : '12',
								style : 'danger'
							});
						}
					}

				}
				// console.debug(module.datas);

				var source = $('#tag-list-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : module.sensors
				});
				$('#tag-list').html('').html(html);
				module.querySensorPositon();
			}
		});
	},
	querySensorPositon : function() {

		// 刷新table
		$('#graphic-containment').html('<tbody></tbody>');
		// 產生20*20的方格
		for (var i = 0; i < 20; i++) {
			html = '';
			for (var j = 0; j < 20; j++) {
				html += '<td><span class="fa-stack"></span></td>';
			}
			$('<tr></tr>').html(html).appendTo('#graphic-containment > tbody');
		}

		// 更改背景圖
		$('#graphic').css("background-image","url(./place/img/" + module.companyId + "/"+ module.placeSelect + ")");

		// 取得sensor位置
		var param = 'companyId=' + module.companyId + '&placeId=' + module.placeSelect;
		ajx.getData({
			url : './sensorPosition/find?' + param,
			callback : function(models) {
				module.sensorPosition = models;
				for (var i = 0; i < models.length; i++) {

					var size = '12';
					var port = 'port' + models[i].port.substr(1, 1);
					var sensorName = '溫度';
					var style = 'primary';
					var icon = 'wi-thermometer';

					if (models[i].port == '00') {
						port = '本體';
						icon = 'fa fa-tag'
					}

					var name = models[i].mac;
					for (var j = 0; j < module.tags.length; j++) {
						if (models[i].mac == module.tags[j].mac) {
							name = module.tags[j].name;
							if (models[i].port == '01') {
								var pos = $.inArray(module.tags[j].port1,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'success';
							} else if (models[i].port == '02') {
								pos = $.inArray(module.tags[j].port2,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'warning';
							} else if (models[i].port == '03') {
								pos = $.inArray(module.tags[j].port3,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'info';
							} else if (models[i].port == '04') {
								pos = $.inArray(module.tags[j].port4,
										sensor.dataType);
								sensorName = sensor.sensorName[pos];
								style = 'danger';
							}
							var sensorInfo = {
									id : models[i].id,
									mac : models[i].mac,
									name : name,
									port : port,
									sensorName : sensorName,
									size : size,
									style : style,
									icon : icon
								};

							$('#'+models[i].id).remove();
							
							var source = $('#sensor-template').html();
							var template = Handlebars.compile(source);
							var html = template(sensorInfo);
							var pos = $('#graphic-containment > tbody').find('tr:eq(' + models[i].x + ')').find('td:eq(' + models[i].y + ')');
							$(pos).html('').html(html);	
							break;
						}
					}
									
				}
				
				ui.gotoMode('MODE_EDIT');
				
			}
		});
	},
	savePosition: function(){//上傳場域配置位置
		module.deferredObjectItems = [];
		
		//新增or修改
		$('#graphic-containment>tbody>tr>td').each(function () {
			if ($(this).children('div').length > 0) {
				var sensorInfo;
				var x = $(this).parent().index();
				var y = $(this).index();
				$(this).children('div').each(function () {

					var id = $(this).attr('id');
					var info = id.split('-');
					var state = $(this).attr('value');
					
					var sensorInfo = {	
							id: id,
							companyId: info[0],
							placeId: info[1],
							mac:info[2],
							port:info[3],
							x: x,
							y: y							
					}
					
					console.debug(sensorInfo);

					if (state == 'unset') {//新增						
						module.deferredObjectItems.push(module.createData(sensorInfo));
					} else {//修改						
						module.deferredObjectItems.push(module.updateData(sensorInfo));
					}

				});
			}
		});
		
		//刪除
		$('#tag-list').children('div').each(function () {
			if ($(this).attr('value') != "unset")//從圖形介面被拖曳到list的
			{
				module.deferredObjectItems.push(module.deleteData($(this).attr('id')));
			}
		});
		
		$.when.apply($, module.deferredObjectItems).done(function(){swal('更新成功');});
	},
	createData:function(sensorInfo){
		var pa = "";
		$.each(sensorInfo, function(k, v) {
			pa += "&"+k + "=" + v ;
		  })
		// console.debug(pa);
		
		return $.ajax({			
			type: 'POST',
			url: './sensorPosition/create',
			data: pa,			
			success: function(data, textStatus, jqXHR){
				console.debug(data);
			}
		});
	},
	updateData:function(sensorInfo){
		var pa = "";
		$.each(sensorInfo, function(k, v) {
			pa += "&"+k + "=" + v ;
		  })
		return $.ajax({			
			type: 'POST',
			url: './sensorPosition/update',
			data: pa,			
			success: function(data, textStatus, jqXHR){
				console.debug(data);
			}
		});
	},
	deleteData:function(id){
		var pa = "id="+id;
		
		return $.ajax({			
			type: 'POST',
			url: './sensorPosition/del',
			data: pa,			
			success: function(data, textStatus, jqXHR){
				console.debug(data);
			}
		});
	}
};
var ui = {
	gotoMode : function(action, p1, p2) {
		if (action == 'MODE_EDIT') {			
			ui.setGraphic();
			ui.setupDragAndDrop();
			$('section[role="update"]').show(200);
			$('.panel-thumbnail').each(function(){
				id = $(this).attr('id');
				if(id==module.placeSelect){
					$(this).addClass('active');
				}else{
					$(this).removeClass('active');
				}
			});
		}
	},
	showConfirmDialog : function(action, p1, p2) {
		var confirmText, backFn;
		if (action == 'CONFIRM_UPDATE') {			
			confirmText = "確定要修改嗎?";
			backFn = module.savePosition;
		} else if (action == 'CONFIRM_DELETE') {
			if (!module.formvalEdit()) {
				return;
			}
			for ( var i in module.datas) {
				if (p1 == module.datas[i].mac) {
					module.assign = module.datas[i];
					break;
				}
			}
			confirmText = "確定要刪除嗎?";
			backFn = module.del;
		} else if (action == 'CONFIRM_CREATE') {
			if (!module.formvalCreate()) {
				return;
			}
			confirmText = "確定要新增嗎?";
			backFn = module.createData;
		}

		swal({
			title : confirmText,
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "red",
			confirmButtonText : "確定",
			cancelButtonText : "取消",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {
			if (isConfirm && backFn != undefined) {
				backFn();
			}
		});
	},
	setGraphic: function(){
		//整理圖形畫面			
		$('#graphic-containment>tbody>tr>td').each(function () {

			if ($(this).children('div').length <= 0) {
				$(this).children("span:first").hide();
			} else {
				var port = $(this).children('div:first').attr('id').split('-')[3];
				var name = $(this).children('div:first').attr('name');
				var portName = "本體";
				var icon;
				if(port == '00'){
					icon = 'fa fa-tag';
				}else{
					icon = 'wi-thermometer';
					portName = "port" + port.substr(1,1);
				}
				$(this).children("span:first").html('<i class="'+icon+' fa-stack-2x fa-inverse"></i><i class="fa fa-stack-1x"></i>');
				$(this).children("span:first").show();
			}

			$(this).children("span:first").draggable({
				//containment : ".ui-droppable",
				scroll: false,
				snap: true,
				snapMode: "inner",
				revert: "invalid",
				cursorAt: {
					top: 0,
					left: 0
				},				
				stop: function (event, ui) {					
					$(this).removeAttr("style");
					if (($(this).parent().children().length - 1) < 1) {
						$(this).hide();
					} else {
						var port = $(this).parent().children('div:first').attr('id').split('-')[3];
						var name = $(this).parent().children('div:first').attr('name');
						var portName = "本體";
						var icon;
						if(port == '00'){
							icon = 'fa fa-tag';
						}else{
							icon = 'wi-thermometer';
							portName = "port" + port.substr(1,1);
						}
						//$(this).html('<i class="fa fa-tags fa-stack-2x fa-inverse"></i><i class="fa fa-stack-1x">' + ($(this).parent().children('div').length) + '</i>');
						$(this).html('<i class="'+icon+' fa-stack-2x fa-inverse"></i><i class="fa fa-stack-1x"></i>');
					}
				}
			});

			$(this).children('div').each(function () {
				$(this).hide();
			});

		});
	},
	setupDragAndDrop: function(){
		//拖曳
		$(".draggable").draggable({
			scroll: false,
			revert: "invalid",
			cursorAt: {
				top: 0,
				left: 0
			},
			start: function (event, ui) {
				var port = $(this).attr('id').split('-')[3];
				var icon;
				if(port == '00'){
					icon = 'fa fa-tag';
				}else{
					icon = 'wi-thermometer';
				}
				var name = $(this).attr('name');				
			}
			
		});
		
		//放置
		$("#graphic-containment>tbody>tr>td").droppable({
			accept: ".ui-draggable",
			hoverClass: "ui-state-hover",
			tolerance: "pointer",
			drop: function (event, ui) { //觸發放置時的事件，即在目標Tag上放置事件

				//1格只能放一個
				if ($(this).children('div').length > 0) {
					ui.draggable.removeAttr("style");
					//$(ui.draggable).children('i').remove();
					return;
				}

				if ($(ui.draggable).is('span')) {
					$(ui.draggable).removeAttr("style");
					var content = $(this);
					$(ui.draggable).parent().children('div').each(function () {
						$(this).html($(this).contents()).appendTo(content);
					});

				} else {
					$(ui.draggable).removeAttr("style");
					//$(ui.draggable).children('i').remove();
					$(ui.draggable).hide();
					$(ui.draggable).appendTo(this);
				}

				if (($(this).children().length - 1) < 1) {
					$(this).children("span:first").hide();
				} else {
					var port = $(this).children('div:first').attr('id').split('-')[3];					
					var name = $(this).children('div:first').attr('name');		
					var portName = "本體";
					var icon;
					if(port == '00'){
						icon = 'fa fa-tag';
					}else{
						icon = 'wi-thermometer';
						portName = "port" + port.substr(1,1);
					}
					$(this).children("span:first").html('<i class="'+icon+' fa-stack-2x fa-inverse"></i><i class="fa fa-stack-1x"></i>');
					$(this).children("span:first").show();
				}

				$(this).children("span:first").draggable({
					//containment : ".ui-droppable",
					scroll: false,
					//snap: true,
					//snapMode: "inner",
					revert: "invalid",
					cursorAt: {
						top: 0,
						left: 0
					},					
					stop: function (event, ui) {						
						$(this).removeAttr("style");
						if (($(this).parent().children().length - 1) < 1) {
							$(this).hide();
						} 
					}
				});

			}

		});

		$('#tag-list').droppable({
			accept: ".ui-draggable",
			hoverClass: "ui-state-hover",
			tolerance: "pointer",
			drop: function (event, ui) { //觸發放置時的事件，即在目標Tag上放置事件
				$(ui.draggable).removeAttr("style");
				$(ui.draggable).show();

				//還原回去
				var html;
				$(ui.draggable).parent().children("span:first").hide();
				$(ui.draggable).parent().children('div').each(function () {
					$(this).removeAttr("style");
					$(this).show();
					$(this).children('div:first').show();
					$(this).appendTo('#tag-list');

				});

			}

		});

		
	}
};

$(document).ready(function() {

	module.companyId = $('.companyId').text();

	module.queryPlace();

});

//hover出現最新溫、濕度
$(document).on('mouseenter', '#graphic-containment>tbody>tr>td', function (event) {

	if ($(this).children('div').length > 0) {

		var html = '';
		$(this).children('div').each(function () {
			var name = $(this).attr('name');
			var port = $(this).attr('id').split('-')[3];
			if(port == '00'){
				port = '本體';
			}else{
				port = 'port'+port.substr(1,1);
			}
			html += '<div class="row">';
			html += '<i class="fa fa-tag fa-fw fa-lg"></i> ' + name+' '+port;
			html += '</div>';
		});
		$('#tooltip').html(html);
		var left = $(this).offset().left;
		var top = $(this).offset().top + $(this).height();
		$('#tooltip').css('left', left + 'px').css('top', top + 'px');
		$('#tooltip').fadeIn('slow');
		$('#tooltip').show();

	}

});


$(document).on('mouseleave', '#graphic-containment>tbody>tr>td', function (event) {

	$('#tooltip').hide();

});