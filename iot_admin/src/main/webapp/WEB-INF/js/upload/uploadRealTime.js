var module = {
	getTestData : function() {
		if($('#companyId').val().length==0){
			alert('請輸入公司名稱');
			return;
		}
		ajx.getData({
			url : './upload/getTestData?companyId='+$('#companyId').val(),
			callback : function(rs) {
//				console.debug(rs);
				$('#datas').val(rs);
			}
		});
	},
	getQueryData : function() {
		var pa=$('#queryForm').serialize();
		ajx.getData({
			url : './query/getQueryData?'+pa,
			callback : function(rs) {
				$('#qRs').html(rs);
			}
		});
	},
	parse : function() {
		console.debug(encodeURI($('#datas').val()));
		ajx.getData({
			url : './upload/parse?datas='+ encodeURIComponent($('#datas').val()),
			callback : function(data) {
				$('#rs').html(data);
//				console.debug(data);
			}
		});
	},
	postTestData :function() {
		var pa={datas:$('#datas').val()};
//		console.debug(pa);
//		return;
		ajx.postData({
			url : './uploadRealTime/uploadBleData',
			data : pa,
			callback : function(datas) {
//				console.debug(datas);
				alert(datas);
			}
		});
	}
};


$(document).ready(function() {
	$( "#testForm" ).on( "submit", function( event ) {
		  event.preventDefault();
		  module.postTestData();
	});
	
});
