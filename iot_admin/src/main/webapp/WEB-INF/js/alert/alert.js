var module = {
	companyId : "",
	alertSetting : [],
	alertRel : [],
	tags : [],
	assign : {},
	deferredObjectItems : [],
	querySetting : function() {
		ajx
				.getData({
					url : './alert/queryAlertSetting',
					callback : function(models) {
						module.alertSetting = models;
						// 產生table
						var source = $('#setting-template').html();
						var template = Handlebars.compile(source);
						for (var i = 0; i < models.length; i++) {
							models[i].afterSecondAlert = parseInt(models[i].afterMilliSecondAlert) / 1000;
						}
						var html = template({
							ds : models
						});
						$('#setting-list').html('').html(html);

						// 產生下拉選單
						source = $('#alert-option-template').html();
						template = Handlebars.compile(source);
						html = '<option value="-1">無</option>';
						html += template({
							ds : models
						});
						$('select[name="alertSettingId"]').each(function() {
							$(this).html('').html(html);
							$(this).val('-1');
						});
						// 讀取關聯
						module.queryRel();
					}
				});
		
	},
	queryRel : function() {
		ajx.getData({
			url : './alert/queryAlertRel',
			callback : function(models) {
				module.alertRel = models;
				// tag各port和警示關聯配對，並修正選單值
				for (var i = 0; i < models.length; i++) {
					$('#' + models[i].mac + "-" + models[i].dataType).val(
							models[i].alertSettingId);
				}
			}
		});
	},
	refreshSetting : function() {
		ajx.getData({
			url : '../iot_alert/upload/updateTagDataTypeAlertRel',
			callback : function(models) {
				console.debug(models);
			}
		});
		
		ajx.getData({
			url : '../iot_alert/upload/updateAlertSettingPool',
			callback : function(models) {
				console.debug(models);
			}
		});
	},
	queryTag : function() {
		var param = $('#queryForm').serialize();
		ajx.getData({
			url : './tag/find?' + param,
			callback : function(models) {
				module.tags = models;
				for (var i = 0; i < models.length; i++) {
					if (models[i].port1 == '00') {
						models[i].invaild1 = true;
					}

					if (models[i].port2 == '00') {
						models[i].invaild2 = true;
					}

					if (models[i].port3 == '00') {
						models[i].invaild3 = true;
					}

					if (models[i].port4 == '00') {
						models[i].invaild4 = true;
					}
				}
				// 產生tag列表
				var source = $('#rel-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('#tag-list').html('').html(html);

				// 讀取設定
				module.querySetting();
			}
		});
	},
	createSetting : function() {
		var as = $('#createForm').serializeArray();
		var pa = "";
		for ( var i in as) {
			if (as[i].name == 'afterMilliSecondAlert') {
				as[i].value = parseInt(as[i].value) * 1000;
			}
			pa += "&" + as[i].name + "=" + as[i].value;
		}
		ajx.postData({
			url : './alert/createAlertSetting',
			data : pa,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					swal('新增失敗', "", "error");
				} else {
					module.querySetting();
					module.refreshSetting();
					ui.gotoMode('MODE_CREATE_CANCEL');
				}
			}
		});
	},
	updateSetting : function() {
		var as = $('#updateForm').serializeArray();
		var pa = "";
		for ( var i in as) {
			if (as[i].name == 'afterMilliSecondAlert') {
				as[i].value = parseInt(as[i].value) * 1000;
			}
			pa += "&" + as[i].name + "=" + as[i].value;
		}
		ajx.postData({
			url : './alert/updateAlertSetting',
			data : pa,
			callback : function(datas) {
				// console.debug(datas);
				if (datas == 'error') {
					swal('更新失敗', "", "error");
				} else {
					module.refreshSetting();
					module.querySetting();
					ui.gotoMode('MODE_UPDATE_CANCEL');
				}
			}
		});
	},
	delSetting : function() {
		ajx.postData({
			url : './alert/delSetting',
			data : module.assign,
			callback : function(datas) {
				console.debug(datas);
				if (datas == 'error') {
					swal('刪除失敗', "", "error");
				} else {
					module.refreshSetting();
					module.querySetting();
				}
			}
		});
	},
	createRel : function(mac, alertSettingId, dataType) {
		var pa = "";
		var relInfo = {
			mac : mac,
			alertSettingId : alertSettingId,
			dataType : dataType
		};
		$.each(relInfo, function(k, v) {
			pa += "&" + k + "=" + v;
		})
		return $.ajax({
			type : 'POST',
			url : './alert/createTagDataTypeAndAlertRel',
			data : pa,
			success : function(data, textStatus, jqXHR) {
				console.debug(data);
				if (data.content == 'error') {
					swal('修改失敗', "", "error");
				}
			}
		});
	},
	delRel : function(mac, alertSettingId, dataType) {
		var pa = "";
		var relInfo = {
			mac : mac,
			alertSettingId : alertSettingId,
			dataType : dataType
		};
		$.each(relInfo, function(k, v) {
			pa += "&" + k + "=" + v;
		})
		return $.ajax({
			type : 'POST',
			url : './alert/delRel',
			data : pa,
			success : function(data, textStatus, jqXHR) {
				console.debug(data);
				if (data.content == 'error') {
					swal('刪除失敗', "", "error");
				}
			}
		});
	},
	updateRel : function(mac, oldAlertSettingId, newAlertSettingId, dataType) {
		var pa = "";
		var relInfo = {
			mac : mac,
			oldAlertSettingId : oldAlertSettingId,
			newAlertSettingId : newAlertSettingId,
			dataType : dataType
		};
		$.each(relInfo, function(k, v) {
			pa += "&" + k + "=" + v;
		})
		return $.ajax({
			type : 'POST',
			url : './alert/updateRel',
			data : pa,
			success : function(data, textStatus, jqXHR) {
				console.debug(data);
				if (data.content == 'error') {
					swal('修改失敗', "", "error");
				}
			}
		});
	},
	saveRel : function() {
		// 根據內容決定新增或刪除警示關聯
		module.deferredObjectItems = [];
		var mac = module.assign.mac;
		var dataType = [ '0001', '0002', '0007', '0101', '0201' ];
		for (var i = 0; i < module.alertRel.length; i++) {
			if (module.alertRel[i].mac == mac) {
				var newSetting = $(
						'#' + mac + '-' + module.alertRel[i].dataType).val();
				if (newSetting != module.alertRel[i].alertSettingId) {// 刪除
					if (newSetting == "-1") {
						module.deferredObjectItems.push(module.delRel(mac,
								module.alertRel[i].alertSettingId,
								module.alertRel[i].dataType));
					} else {
						module.deferredObjectItems.push(module.updateRel(mac,
								module.alertRel[i].alertSettingId, newSetting,
								module.alertRel[i].dataType));
					}
				}
				dataType.remove(module.alertRel[i].dataType);
			}
		}

		for (var i = 0; i < dataType.length; i++) {
			var newSetting = $('#' + mac + '-' + dataType[i]).val();
			console.debug(newSetting);
			if (newSetting != '-1') {
				module.deferredObjectItems.push(module.createRel(mac,
						newSetting, dataType[i]));
			}
		}

		$.when.apply($, module.deferredObjectItems).done(function() {
			module.refreshSetting();
			module.queryRel();
			//swal('修改完成', "", "success");
			var notice = new PNotify({
				title : '修改完成',
				text : '成功',
				styling : 'fontawesome',
				type : 'success',
				addclass : 'stack-bottomright',
				stack : ui.notifyStack
			});
		});
	}
}

var ui = {
	notifyStack : {
		"dir1" : "up",
		"dir2" : "left",
		"firstpos1" : 15,
		"firstpos2" : 15
	},
	gotoMode : function(action, p1, p2) {
		if (action == 'MODE_CREATE') {
			$('section[role="create"] input').val('');
			$('section[role="create"] textarea').val('');
			$('section[role="create"] input[name="companyId"]').val(
					$('.companyId').text());
			$('section[role="create"] input[name="afterMilliSecondAlert"]')
					.val('60');
			$('section[role="query"]').hide();
			$('section[role="create"]').show(200);
		} else if (action == 'MODE_UPDATE') {
			for ( var i in module.alertSetting) {
				if (p1 == module.alertSetting[i].alertSettingId) {
					module.assign = module.alertSetting[i];
					break;
				}
			}
			console.debug(module.assign);
			var textInputs = [ 'alertSettingId', 'disName', 'companyId',
					'high', 'low', 'criticalEmails', 'emails' ];
			for ( var i in textInputs) {
				$('#updateForm [name="' + textInputs[i] + '"]').val(
						module.assign[textInputs[i]]);
			}

			$('#updateForm [name="afterMilliSecondAlert"]').val(
					parseInt(module.assign['afterMilliSecondAlert']) / 1000);

			var selectInputs = [ 'dataType' ];
			for ( var i in selectInputs) {
				console.debug(selectInputs[i]);
				console.debug(module.assign[selectInputs[i]]);

				$('#updateForm select[name="' + selectInputs[i] + '"]').val(
						module.assign[selectInputs[i]]);
			}
			$('section[role="query"]').hide();
			$('section[role="update"]').show(200);
		} else if (action == 'MODE_CREATE_CANCEL') {
			$('section[role="create"]').hide();
			$('section[role="query"]').show(200);
		} else if (action == 'MODE_UPDATE_CANCEL') {
			$('section[role="update"]').hide();
			$('section[role="query"]').show(200);
		}
	},
	showConfirmDialog : function(action, p1, p2) {
		var confirmText, backFn;
		if (action == 'CONFIRM_UPDATE') {
			if ($('#updateForm [name="emails"]').val().length
					+ $('#updateForm [name="criticalEmails"]').val().length == 0) {
				swal("請至少填入一名警示通知對象的E-mail", "", "error");
			} else if (parseFloat($('#updateForm [name="high"]').val()) <= parseFloat($(
					'#updateForm [name="low"]').val())) {
				swal("警示上限值需大於警示下限值", "", "error");
			} else {
				confirmText = "確定要修改嗎?";
				backFn = module.updateSetting;
			}

		} else if (action == 'CONFIRM_UPDATE_REL') {
			confirmText = "確定要修改嗎?";
			for ( var i in module.tags) {
				if (p1 == module.tags[i].mac) {
					module.assign = module.tags[i];
					break;
				}
			}

			backFn = module.saveRel;
		} else if (action == 'CONFIRM_DELETE') {
			for ( var i in module.alertSetting) {
				if (p1 == module.alertSetting[i].alertSettingId) {
					module.assign = module.alertSetting[i];
					break;
				}
			}
			confirmText = "確定要刪除嗎?";
			backFn = module.delSetting;
		} else if (action == 'CONFIRM_CREATE') {
			if ($('#createForm [name="emails"]').val().length
					+ $('#updateForm [name="criticalEmails"]').val().length == 0) {
				swal("請至少填入一名警示通知對象的E-mail", "", "error");
			} else if (parseFloat($('#createForm [name="high"]').val()) <= parseFloat($(
					'#createForm [name="low"]').val())) {
				swal("警示上限值需大於警示下限值", "", "error");
			} else {
				confirmText = "確定要新增嗎?";
				backFn = module.createSetting;
			}
		}

		swal({
			title : confirmText,
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "red",
			confirmButtonText : "確定",
			cancelButtonText : "取消",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {
			if (isConfirm && backFn != undefined) {
				backFn();
			}
		});
	}

};

$(document).ready(function() {

	module.companyId = $('.companyId').text();

	$('#queryForm input[name="companyId"]').val(module.companyId);

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		module.queryTag();
	});

	$("#updateForm,#createForm").on("submit", function(event) {
		event.preventDefault();
	});

	module.queryTag();

});