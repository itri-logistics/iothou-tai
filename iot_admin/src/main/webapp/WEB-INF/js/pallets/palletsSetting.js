var module = {
	companyId : "",
	tagQueryModel : {
		companyId : "",
		tagId:"",
		typeId:"",
	},
	palletsType : [],
	queryPalletsType : function() {
		var param = 'companyId=' + module.companyId;
		ajx.getData({
			url : './palletsSetting/findPalletsType?' + param,
			callback : function(models) {
				console.debug(models);
				module.palletsType = models;
				var source = $('#type-option-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('select[name="palletsType"]').each(function() {
					$(this).html('').html(html);
				});
			}
		});
	},
	setTagData : function() {
		module.tagQueryModel.companyId = module.companyId;
		//module.tagQueryModel.tagId = $('#queryForm').val();
		//module.tagQueryModel.typeId = "1";
		var pa = 'companyId=' + module.tagQueryModel.companyId;
		var as = $('#queryForm').serializeArray();
		for ( var i in as) {
			pa += "&" + as[i].name + "=" + as[i].value;
		}
		console.debug(pa);
		ajx.postData({
			url : './palletsSetting/setPalletsTagTypeSetting?' + pa,
			callback : function(models) {
				console.debug(models);
				//module.inventory = models;
				// 產生table
				//var source = $('#dailyReport-template').html();
				//var template = Handlebars.compile(source);
				//var html = template({
					//ds : models
				//});
				//$('#dailyReport-list').html('').html(html);
				ui.gotoMode('MODE_AFTER_TAG_QUERY');
			}
		});
	},
};

var ui = {
	gotoMode : function(action) {
		if (action == 'MODE_AFTER_TAG_QUERY') {
			$('#dailyReport-table').show(200);

		}
	}
};

$(document).ready(function() {

	module.companyId = $('.companyId').text();

	//$('.datepicker').datepicker({
	//	format : 'yyyy/mm/dd'
	//});
	//$('#ed').datepicker('setDate', new Date());

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		module.setTagData();
	});
	module.queryPalletsType();
});