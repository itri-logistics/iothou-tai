var module = {
	companyId : "",
	palletsTypeSetting : [],
	tags : [],
	assign : {},
	deferredObjectItems : [],
	params : [],
	queryParams : function() {
		var param = 'companyId=' + module.companyId;
		ajx.getData({
			url : './palletsTypeSetting/findParam?' + param,
			callback : function(models) {
				module.querySetting();
				module.params = JSON.parse(JSON.stringify(models));
			}
		});
	},
	querySetting : function() {
		ajx.getData({
					url : './palletsTypeSetting/queryPalletsTypeSetting',
					callback : function(models) {
						module.palletsTypeSetting = JSON.parse(JSON.stringify(models));
						for (var i = 0; i < models.length; i++) {
							for (var k = 0; k < module.params.length; k++) {
								if (models[i].type == module.params[k].pid && module.params[k].formName == "type"){
									models[i].type = module.params[k].name;
									break;
								}
							}
							for (var k = 0; k < module.params.length; k++) {
								if (models[i].property == module.params[k].pid  && module.params[k].formName == "property"){
									models[i].property = module.params[k].name;
									break;
								}
							}
							for (var k = 0; k < module.params.length; k++) {
								if (models[i].format == module.params[k].pid && module.params[k].formName == "format"){
									models[i].format = module.params[k].name;
									break;
								}	
							}
						}
						//module.palletsTypeSetting = models;
						console.debug(module.palletsTypeSetting);
						// 產生table
						var source = $('#setting-template').html();
						var template = Handlebars.compile(source);
						var html = template({
							ds : models
						});
						$('#setting-list').html('').html(html);
					}
				});
	},
	createSetting : function() {
		var as = $('#createForm').serializeArray();
		var pa = "";
		for ( var i in as) {
			pa += "&" + as[i].name + "=" + as[i].value;
		}
		ajx.postData({
			url : './palletsTypeSetting/createPalletsTypeSetting',
			data : pa,
			callback : function(datas) {
				if (datas == 'error') {
					swal('新增失敗',"","error");
				} else {
					module.querySetting();
					ui.gotoMode('MODE_CREATE_CANCEL');
				}
			}
		});
	},
	updateSetting : function() {
		var as = $('#updateForm').serializeArray();
		var pa = "";
		for ( var i in as) {
			pa += "&" + as[i].name + "=" + as[i].value;
		}
		ajx.postData({
			url : './palletsTypeSetting/updatePalletsTypeSetting',
			data : pa,
			callback : function(datas) {
				if (datas == 'error') {
					swal('更新失敗',"","error");
				} else {
					module.querySetting();
					ui.gotoMode('MODE_UPDATE_CANCEL');
				}
			}
		});
	},
	typeParams : [],
	queryTypeParams : function() {
		var param = 'companyId=' + module.companyId + '&formName='+"type";
		ajx.getData({
			url : './palletsTypeSetting/findType?' + param,
			callback : function(models) {
				console.debug(models);
				module.typeParams = JSON.parse(JSON.stringify(models));
				var source = $('#type-option-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('select[name="type"]').each(function() {
					$(this).html('').html(html);
				});
			}
		});
	},
	formatParams : [],
	queryFormatParams : function() {
		var param = 'companyId=' + module.companyId + '&formName='+"format";
		ajx.getData({
			url : './palletsTypeSetting/findFormat?' + param,
			callback : function(models) {
				console.debug(models);
				module.formatParams = JSON.parse(JSON.stringify(models));
				var source = $('#format-option-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('select[name="format"]').each(function() {
					$(this).html('').html(html);
				});
			}
		});
	},
	propertyParams : [],
	queryPropertyParams : function() {
		var param = 'companyId=' + module.companyId + '&formName='+"property";
		ajx.getData({
			url : './palletsTypeSetting/findProperty?' + param,
			callback : function(models) {
				console.debug(models);
				module.propertyParams = JSON.parse(JSON.stringify(models));
				var source = $('#property-option-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('select[name="property"]').each(function() {
					$(this).html('').html(html);
				});
			}
		});
	},
	delSetting : function() {
		ajx.postData({
			url : './palletsTypeSetting/delPalletsTypeSetting',
			data : module.assign,
			callback : function(datas) {
				if (datas == 'error') {
					swal('刪除失敗',"","error");
				} else {
					module.querySetting();
				}
			}
		});
	},
}

var ui = {
	gotoMode : function(action, p1, p2) {
		if (action == 'MODE_CREATE') {
			$('section[role="create"] input').val('');
			$('section[role="create"] textarea').val('');
			$('section[role="create"] input[name="companyId"]').val(
					$('.companyId').text());
			$('section[role="query"]').hide();
			$('section[role="create"]').show(200);
		} else if (action == 'MODE_UPDATE') {
			for ( var i in module.palletsTypeSetting) {
				if (p1 == module.palletsTypeSetting[i].id) {
					module.assign = module.palletsTypeSetting[i];
					break;
				}
			}
			console.debug(module.assign);
			var textInputs = ['id','name', 'color',
					'customer', 'meterial', 'companyId' ];
			for ( var i in textInputs) {
				$('#updateForm [name="' + textInputs[i] + '"]').val(
						module.assign[textInputs[i]]);
			}

			var selectInputs = ['property', 'format', 'type' ];
			for ( var i in selectInputs) {
				console.debug(selectInputs[i]);
				console.debug(module.assign[selectInputs[i]]);

				$('#updateForm select[name="' + selectInputs[i] + '"]').val(
						module.assign[selectInputs[i]]);
			}
			$('section[role="query"]').hide();
			$('section[role="update"]').show(200);
		} else if (action == 'MODE_CREATE_CANCEL') {
			$('section[role="create"]').hide();
			$('section[role="query"]').show(200);
		} else if (action == 'MODE_UPDATE_CANCEL') {
			$('section[role="update"]').hide();
			$('section[role="query"]').show(200);
		}
	},
	showConfirmDialog : function(action, p1, p2) {
		var confirmText, backFn;
		if (action == 'CONFIRM_UPDATE') {
				confirmText = "確定要修改嗎?";
				backFn = module.updateSetting;
		} else if (action == 'CONFIRM_UPDATE_REL') {
			confirmText = "確定要修改嗎?";
			for ( var i in module.tags) {
				if (p1 == module.tags[i].mac) {
					module.assign = module.tags[i];
					break;
				}
			}

			backFn = module.saveRel;
		} else if (action == 'CONFIRM_DELETE') {
			for ( var i in module.palletsTypeSetting) {
				if (p1 == module.palletsTypeSetting[i].id) {
					module.assign = module.palletsTypeSetting[i];
					break;
				}
			}
			confirmText = "確定要刪除嗎?";
			backFn = module.delSetting;
		} else if (action == 'CONFIRM_CREATE') {
			confirmText = "確定要新增嗎?";
			backFn = module.createSetting;
		}

		swal({
			title : confirmText,
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "red",
			confirmButtonText : "確定",
			cancelButtonText : "取消",
			closeOnConfirm : true,
			closeOnCancel : true
		}, function(isConfirm) {
			if (isConfirm && backFn != undefined) {
				backFn();
			}
		});
	}

};

$(document).ready(function() {
	module.queryParams();
	module.companyId = $('.companyId').text();
	module.queryTypeParams();
	module.queryFormatParams();
	module.queryPropertyParams();
	$('#queryForm input[name="companyId"]').val(module.companyId);

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		module.queryTag();
	});

	$("#updateForm,#createForm").on("submit", function(event) {
		event.preventDefault();
	});
	//module.querySetting();
	
});