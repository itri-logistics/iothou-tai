var module = {
	companyId : "",
	placeId:"",
	tagQueryModel : {
		companyId : "",
		ed : "",
		placeId:"",
	},
	places : [],
	queryPlace : function() {
		var param = 'companyId=' + module.companyId;
		ajx.getData({
			url : './noIdentityPalletsDailyReport/find?' + param,
			callback : function(models) {
				module.places = JSON.parse(JSON.stringify(models));
				var source = $('#place-option-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('select[name="placeId"]').each(function() {
					$(this).html('').html(html);
				});
			}
		});
	},
	inventory : [],
	queryTagData : function() {
		module.tagQueryModel.companyId = module.companyId;
		module.tagQueryModel.ed = $('#ed').val().replace(new RegExp('/', 'g'),'');
		module.tagQueryModel.placeId = $('select[name="placeId"]').val();
		var pa = "";
		for ( var i in module.tagQueryModel) {
			if (module.tagQueryModel[i].length > 0) {
				pa += "&" + i + "=" + module.tagQueryModel[i];
			}
		}
		ajx.getData({
			url : './noIdentityPalletsDailyReport/getTradingInQueryDateAndPlaceId?' + pa,
			callback : function(models) {
				console.debug(module.types);
				module.inventory = JSON.parse(JSON.stringify(models));
				console.debug(module.inventory);
				for (var i = 0; i < models.length; i++) {
					var updateDate = models[i].updateDate;
					updateDate = updateDate.substring(0,4)+'-'+updateDate.substring(4,6)+'-'+updateDate.substring(6,8)+' '+
					updateDate.substring(8,10)+':'+updateDate.substring(10,12)+':'+updateDate.substring(12,14);
					models[i].updateDate = updateDate; 
					if(models[i].in_out == 1){
						models[i].in_out ="進";
					}else{
						models[i].in_out ="出";
					}
					for (var j = 0; j < module.places.length; j++) {
						if (models[i].place == module.places[j].placeId){
							models[i].place = module.places[j].disName;
							break;
						}							
					}
				}
				$('#totalRecord').html(models.length);
				// 產生table
				var source = $('#dailyReport-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('#dailyReport-list').html('').html(html);
				ui.gotoMode('MODE_AFTER_TAG_QUERY');
			}
		});
	},
};

var ui = {
	gotoMode : function(action) {
		if (action == 'MODE_AFTER_TAG_QUERY') {
			$('#dailyReport-table').show(200);

		}
	}
};

$(document).ready(function() {

	module.companyId = $('.companyId').text();
	$('.datepicker').datepicker({
		format : 'yyyy/mm/dd'
	});
	$('#ed').datepicker('setDate', new Date());

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		module.queryTagData();
	});
	module.queryPlace();
});