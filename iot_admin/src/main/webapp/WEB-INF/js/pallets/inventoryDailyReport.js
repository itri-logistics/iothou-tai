var module = {
	companyId : "",
	tagQueryModel : {
		companyId : "",
		ed : ""
	},
	inventory : [],
	places:[],
	types:[],
	queryPlace:function(){
		var param = 'companyId=' + module.companyId;
		ajx.getData({
			url : './inventoryDailyReport/findPlace?' + param,
			callback : function(models) {
				console.debug(models);
				module.places = JSON.parse(JSON.stringify(models));
			}
		});
	},
	queryType:function(){
		var param = 'companyId=' + module.companyId;
		ajx.getData({
			url : './inventoryDailyReport/findType?' + param,
			callback : function(models) {
				console.debug(models);
				module.types = JSON.parse(JSON.stringify(models));
			}
		});
	},
	queryTagData : function() {
		module.tagQueryModel.companyId = module.companyId;
		module.tagQueryModel.ed = $('#ed').val().replace(new RegExp('/', 'g'),
				'');
		var pa = "";
		for ( var i in module.tagQueryModel) {
			if (module.tagQueryModel[i].length > 0) {
				pa += "&" + i + "=" + module.tagQueryModel[i];
			}
		}
		ajx.getData({
			url : './inventoryDailyReport/getTagsInQueryInterval?' + pa,
			callback : function(models) {
				console.debug(module.types);
				module.inventory = JSON.parse(JSON.stringify(models));
				console.debug(module.inventory);
				for (var i = 0; i < models.length; i++) {
					var updateDate = models[i].updateDate;
					updateDate = updateDate.substring(0,4)+'-'+updateDate.substring(4,6)+'-'+updateDate.substring(6,8)+' '+
					updateDate.substring(8,10)+':'+updateDate.substring(10,12)+':'+updateDate.substring(12,14);
					models[i].updateDate = updateDate; 
					for (var j = 0; j < module.places.length; j++) {
						if (models[i].placeId == module.places[j].placeId){
							models[i].placeId = module.places[j].disName;
							break;
						}							
					}
					for (var k = 0; k < module.types.length; k++) {
						if (models[i].palletsType == module.types[k].id){
							models[i].palletsType = module.types[k].name;
							break;
						}							
					}
				}
				// 產生table
				var source = $('#inventory-template').html();
				var template = Handlebars.compile(source);
				var html = template({
					ds : models
				});
				$('#inventory-list').html('').html(html);
				ui.gotoMode('MODE_AFTER_TAG_QUERY');
			}
		});
	},
};

var ui = {
	gotoMode : function(action) {
		if (action == 'MODE_AFTER_TAG_QUERY') {
			$('#inventory-table').show(200);

		}
	}
};

$(document).ready(function() {

	module.companyId = $('.companyId').text();
	module.queryPlace();
	module.queryType();
	$('.datepicker').datepicker({
		format : 'yyyy/mm/dd'
	});
	$('#ed').datepicker('setDate', new Date());

	$("#queryForm").on("submit", function(event) {
		event.preventDefault();
		module.queryTagData();
	});

});