package com.utility;

import java.util.ArrayDeque;
import java.util.HashMap;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.security.SecurityController;

/******************************************
 * 警示Email的Pool
 *
 * @author ATone create by 2017/05/08
 *******************************************/
public class SendEmailJob {
	
	private static ArrayDeque<String> queue = new ArrayDeque<>();
	private static HashMap<String, Mail> mails = new HashMap<>();

	@Autowired
	EmailAlertUtility emailAlertUtility;
	
	//static EmailAlertUtility staticEmailAlertUtility;
	
	public static void init() {
		SecurityController.nLog.info("send mail job initial.");
	}
	
	public void send() {
		while(!queue.isEmpty()){			
			try {
				synchronized (queue) {
					String email = queue.poll();
					synchronized (mails) {
						Mail mail = mails.get(email);
						if(mail!=null){
							mails.remove(email);
							emailAlertUtility.sendMail(mail.emails, mail.title, mail.content.toString());
							//System.err.println("send mail to "+email+".");
						}
					}
				}
				Thread.sleep(2000);	//間隔兩秒再發下一封
			} catch (InterruptedException e) {
				e.printStackTrace();
				SecurityController.eLog.error("send mail failed. " + e.getMessage());
			}
		}		
	}

	public static void sendMail(String emails, String title, String content){					
		String[] tos=emails.split(";");
		for(String s:tos){
			synchronized (queue) {
				synchronized (mails) {
					if(!mails.containsKey(s)){
						mails.put(s, new Mail(s, title, content));
						queue.add(s);
					}else{
						Mail mail = mails.get(s);
						mail.num++;
						mail.title = "即時資料有"+mail.num+"則異常狀況發生";
						mail.content.append("<br><br>"+content);
					}
				}
			}					
		}
	}
	
	static class Mail{
		String emails;
		String title;
		StringBuilder content;
		int num = 1;
		public Mail(String emails, String title, String content) {
			super();
			this.emails = emails;
			this.title = title;
			this.content = new StringBuilder(content);
		}		
		
	}
	
}
