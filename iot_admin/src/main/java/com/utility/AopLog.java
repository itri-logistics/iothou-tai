package com.utility;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
public class AopLog {
	@Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
	public void anyRequestMappingMethod() {

	}

    @Pointcut("anyRequestMappingMethod()")
//    @Pointcut("anyControllerMethod() && anyPublicMethod() && anyRequestMappingMethod()")
    public void anyInteractionMethod() {
    }
	
	@Before("anyRequestMappingMethod()")
//	@Before("anyInteractionMethod()")
	public void recordIP(JoinPoint pjp) {
		String remoteAddress = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
//		System.out.println(pjp.toString() + " ip:" + remoteAddress);
//		if(pjp.getSignature().getName().indexOf("find")!=-1){
//	        final MethodSignature signature = (MethodSignature) pjp.getSignature();
//	        final String[] parameterNames = signature.getParameterNames();
//	        if(parameterNames.length>0){
//	        	final Object[] args= pjp.getArgs();
//	        	StringBuilder sb=new StringBuilder();
//	        	sb.append(pjp.getSignature().getName()+":");
//	        	for(int i=0;i<parameterNames.length;i++){
//	        		if(args[i] instanceof String){
//	        			sb.append(","+parameterNames[i]+":"+args[i].toString());
//	        		}
//	        	}
//	        	System.out.println(sb.toString());
//	        }
//		}
	}
}