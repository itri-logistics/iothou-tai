package com.utility;

import org.springframework.beans.factory.annotation.Autowired;

import com.dao.job.RoundJobDao;

public class RoundJob {

	@Autowired
	RoundJobDao roundJobDao;	

	@Autowired 
	EmailAlertUtility emailAlertUtility;
	
	public void start() {		
		roundJobDao.proc();
	}
	
	public void checkDataAndAlert() {
//		roundJobDao.proc();
	}
	
	
}
 