package com.utility;

import java.util.ArrayList;
import java.util.HashMap;

import jodd.datetime.JDateTime;

/******************************************
 * Data Server 的 Pool
 *
 * @author ATone create by 2017/08/07
 *******************************************/
public class DataServerPool {

	private static HashMap<String, HashMap<String, String>> serverList = new HashMap<>();
	
	public static void updateServerTime(String serverIp, String name, String cpu, String mem, String space, String status) {
		HashMap<String, String> serverData = new HashMap<>();
		serverData.put("time", new JDateTime().toString("YYYYMMDDhhmmss"));
		serverData.put("cpu", cpu);
		serverData.put("mem", mem);
		serverData.put("space", space);	
		serverData.put("status", status);	
		serverData.put("server", serverIp);	
		serverData.put("name", name);	
		serverList.put(serverIp, serverData);
	}

	public static HashMap<String, HashMap<String, String>> getServerList() {
		
		
		return serverList;
	}

	
}
