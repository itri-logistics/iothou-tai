package com.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.security.SecurityController;

import jodd.datetime.JDateTime;

/******************************************
 * 即時資料的Pool
 *
 * @author Tim create by 2016/12/25
 *******************************************/
public class RealTimeDataPool {

	private static Map<String, TagDataType> tagDataPool = new HashMap();// <macId_dataType,Tag.class>
																		// key=comapnyId_macId_dataType
	private static Map<Integer, Alert> tagAlertPool = new HashMap();// <alertId,Alert.class>

	public static Map getNewTagsData(String companyId, String macIdDataTypes) { // macIdDataTypes
																				// 中間要用底線隔開
		Map rs = new HashMap();
		String[] tagDatatypes = macIdDataTypes.split(",");
		for (String tagDatatype : tagDatatypes) {
			TagDataType tag = tagDataPool.get(companyId + "_" + tagDatatype);
			if (tag != null) {
				rs.put(tagDatatype, tag);
			}
		}
		return rs;
	}

	// 更新tag的最新Data
	public static void updateTagData(String companyId, String macId, String name, String[] dataTypes,
			String[][] datas) {


		for (int i = 0; i < dataTypes.length; i++) {
			String dataType = dataTypes[i];
			String[] data = datas[i];
			String d = data[data.length - 1];
			float tempData = Float.parseFloat(d.substring(12));
			long tempDatetime = new JDateTime("20" + d.substring(0, 12), "YYYYMMDDhhmmss").getTimeInMillis();

			String key = String.format("%s_%s_%s", companyId, macId, dataType);// companyId_macId_dataType
			TagDataType tag = tagDataPool.get(key);
			if (tag == null) {
				tag = new TagDataType(macId, dataType, name);
				tagDataPool.put(key, tag);
			}

			SecurityController.dLog.debug("updateTagData: tempDatetime ="+tempDatetime+",lastDataDatetime = "+tag.lastDataDatetime);
			
			if (tempDatetime > tag.lastDataDatetime) {// 資料時間較新的才更新
				checkalert(tag, tempData, tempDatetime);

				tag.lastData = tempData;
				tag.lastDataDatetime = tempDatetime;
			}
		}
	}

	// 檢查是否有alert有問題
	private static void checkalert(TagDataType dataType, float tempData, long tempDatetime) {
		if (dataType.lastDataDatetime > 0 && dataType.lastDataDatetime < tempDatetime) {
			for (Alert alert : dataType.alerts) {
				int alertId = alert.alertSettingId;
				float diff = Math.max(tempData - alert.high, alert.low - tempData); // 溫度和警示邊界的差值，若無異常則差值應小於等於0
				if (diff >= 3) {
					if (dataType.criticalErrorStartMilliSecondTimes.get(alertId) == null
							|| dataType.criticalErrorStartMilliSecondTimes.get(alertId) <= 0) {// 第一次出現錯誤
						dataType.criticalErrorStartMilliSecondTimes.put(alertId, tempDatetime);
						dataType.criticalErrorMillSecondTimes.put(alertId, 0l);
						dataType.criticalErrorAlerted.put(alertId, false);

					} else {
						Long startErrorMilltime = dataType.criticalErrorStartMilliSecondTimes.get(alertId);
						dataType.criticalErrorMillSecondTimes.put(alertId, tempDatetime - startErrorMilltime);// 已經持續多少毫秒了

						if (dataType.criticalErrorMillSecondTimes.get(alertId) > alert.afterMilliSecondAlert) {// 超過時間和資料上下限
							if (!dataType.criticalErrorAlerted.get(alertId)) {// 還沒警示過
								dataType.criticalErrorAlerted.put(alertId, true);// 警示完成要改flag
								String dataTypeName;
								if (dataType.dataType.startsWith("00")) {
									dataTypeName = "本體";
								} else {
									dataTypeName = "port" + dataType.dataType.substring(1, 2);
								}

								if (StringUtils.isNotBlank(alert.criticalEmails)) {
									SendEmailJob.sendMail(alert.criticalEmails,
											String.format("%s即時資料有%s狀況發生", dataType.disName + " " + dataTypeName,
													alert.disName),
											String.format("%s即時資料有%s狀況發生，上限:%s，下限:%s，異常數值:%s，發生時間點:%s",
													dataType.disName + " " + dataTypeName, alert.disName,
													String.valueOf(alert.high), String.valueOf(alert.low),
													String.valueOf(tempData),
													new JDateTime(tempDatetime).toString("YYYY/MM/DD hh:mm:ss")));

									SecurityController.nLog.info("critical alert emails:" + alert.criticalEmails);
								}								
							}
						}
					}
				} else {
					dataType.criticalErrorStartMilliSecondTimes.put(alertId, -1l);
					dataType.criticalErrorMillSecondTimes.put(alertId, -1l);
					dataType.criticalErrorAlerted.put(alertId, false);
				}
				
				if (diff > 0) {
					if (dataType.errorStartMilliSecondTimes.get(alertId) == null
							|| dataType.errorStartMilliSecondTimes.get(alertId) <= 0) {// 第一次出現錯誤
						dataType.errorStartMilliSecondTimes.put(alertId, tempDatetime);
						dataType.errorMillSecondTimes.put(alertId, 0l);
						dataType.errorAlerted.put(alertId, false);

					} else {
						Long startErrorMilltime = dataType.errorStartMilliSecondTimes.get(alertId);
						dataType.errorMillSecondTimes.put(alertId, tempDatetime - startErrorMilltime);// 已經持續多少毫秒了

						if (dataType.errorMillSecondTimes.get(alertId) > alert.afterMilliSecondAlert) {// 超過時間和資料上下限
							if (!dataType.errorAlerted.get(alertId)) {// 還沒警示過
								dataType.errorAlerted.put(alertId, true);// 警示完成要改flag
								String dataTypeName;
								if (dataType.dataType.startsWith("00")) {
									dataTypeName = "本體";
								} else {
									dataTypeName = "port" + dataType.dataType.substring(1, 2);
								}

								if (StringUtils.isNotBlank(alert.emails)) {
									SendEmailJob.sendMail(alert.emails,
											String.format("%s即時資料有%s狀況發生", dataType.disName + " " + dataTypeName,
													alert.disName),
											String.format("%s即時資料有%s狀況發生，上限:%s，下限:%s，異常數值:%s，發生時間點:%s",
													dataType.disName + " " + dataTypeName, alert.disName,
													String.valueOf(alert.high), String.valueOf(alert.low),
													String.valueOf(tempData),
													new JDateTime(tempDatetime).toString("YYYY/MM/DD hh:mm:ss")));

									SecurityController.nLog.info("alert emails:" + alert.emails);
								}

								/*if (StringUtils.isNotBlank(alert.phones)) {
									System.out.println("alert phones:" + alert.phones);
									if (StringUtils.isNotBlank(alert.phones)) {
										PhoneAlertUtility.sendSms(alert.phones, String.format(
												"%s即時資料有%s狀況發生，"
														+ String.format("異常數值:%s，發生時間點:%s", String.valueOf(tempData),
																new JDateTime(tempDatetime)
																		.toString("YYYY/MM/DD hh:mm:ss")),
												dataType.disName + " " + dataTypeName, alert.disName));
									}
								}*/
							}
						}
					}
				} else {
					dataType.errorStartMilliSecondTimes.put(alertId, -1l);
					dataType.errorMillSecondTimes.put(alertId, -1l);
					dataType.errorAlerted.put(alertId, false);
				}
			}
		}
	}

	// 修改tag裡面的alert關聯
	public static void initRealTimePool(List<Map<String, Object>> tags, List<Map<String, Object>> alerts,
			List<Map<String, Object>> rels) {

		// 先設定Tag
		for (Map<String, Object> t : tags) {
			// Tag 電量
			tagDataPool.put(String.format("%s_%s_%s", t.get("companyId").toString(), t.get("mac").toString(), "0007"),
					new TagDataType(t.get("mac").toString(), "0007", t.get("name").toString()));
			//本體溫度
			tagDataPool.put(String.format("%s_%s_%s", t.get("companyId").toString(), t.get("mac").toString(), "0001"),
					new TagDataType(t.get("mac").toString(), "0001", t.get("name").toString()));
			//本體濕度
			tagDataPool.put(String.format("%s_%s_%s", t.get("companyId").toString(), t.get("mac").toString(), "0002"),
					new TagDataType(t.get("mac").toString(), "0002", t.get("name").toString()));
			//port1溫度
			tagDataPool.put(String.format("%s_%s_%s", t.get("companyId").toString(), t.get("mac").toString(), "0101"),
					new TagDataType(t.get("mac").toString(), "0101", t.get("name").toString()));
			//port2溫度
			tagDataPool.put(String.format("%s_%s_%s", t.get("companyId").toString(), t.get("mac").toString(), "0201"),
					new TagDataType(t.get("mac").toString(), "0201", t.get("name").toString()));
			
			for (int i = 3; i <= 4; i++) {	//光度、電流或震動
				String val = t.get("port" + i).toString();
				tagDataPool.put(String.format("%s_%s_0%d%s", t.get("companyId").toString(), t.get("mac").toString(), i,val),
						new TagDataType(t.get("mac").toString(), "0" + i + val, t.get("name").toString()));
			}
		}

		// 新增警示
		for (Map<String, Object> a : alerts) {
			int alertSettingId = (int) a.get("alertSettingId");
			//System.out.println(a.toString());
			Alert alert = new Alert(alertSettingId, a.get("disName").toString(), (float) a.get("low"),
					(float) a.get("high"), (double) a.get("afterMilliSecondAlert"), a.get("emails").toString(),
					a.get("criticalEmails").toString(), a.get("phones").toString());
			tagAlertPool.put(alertSettingId, alert);
		}

		// 建立關聯
		for (Map<String, Object> r : rels) {
			String key = String.format("%s_%s_%s", r.get("companyId").toString(), r.get("mac").toString(),
					r.get("dataType").toString());
			TagDataType t = tagDataPool.get(key);
			try{
				t.alerts.add(tagAlertPool.get((int) r.get("alertSettingId")));
			}catch(Exception e){
				System.err.println("error alert tag key = " + key == null ? "null" : key);
				System.err.println("error alert setting id = " + r.get("alertSettingId")==null ? "null" : r.get("alertSettingId"));
				String key1 = String.format("%s_%s_0001", r.get("companyId").toString(), r.get("mac").toString());
				String disName = tagDataPool.get(key1).disName;
				tagDataPool.put(key,
						new TagDataType(r.get("mac").toString(), r.get("dataType").toString(), disName));
				t = tagDataPool.get(key);
				t.alerts.add(tagAlertPool.get((int) r.get("alertSettingId")));
				e.printStackTrace();
			}

		}

	}

	// 新增alert的基本資訊
	public static void createAlertSetting(Map<String, Object> alertSetting) {
		int alertSettingId = (int) alertSetting.get("alertSettingId");
		Alert alert = new Alert(alertSettingId, alertSetting.get("disName").toString(), (float) alertSetting.get("low"),
				(float) alertSetting.get("high"), (double) alertSetting.get("afterMilliSecondAlert"),
				alertSetting.get("emails").toString(), alertSetting.get("criticalEmails").toString(),
				alertSetting.get("phones").toString());
		tagAlertPool.put(alertSettingId, alert);
	}

	// 新增關聯alert的基本資訊
	public static void createTagDataTypeAndAlertRel(int alertSettingId, String companyId, String mac, String dataType) {
		String key = String.format("%s_%s_%s", companyId, mac, dataType);
		TagDataType t = tagDataPool.get(key);
		t.alerts.add(tagAlertPool.get(alertSettingId));
	}

	// 更新alert的基本資訊 emails or phones 上下界
	public static void updateAlertSetting(Map<String, Object> alertSetting) {
		Alert alert = tagAlertPool.get((int) alertSetting.get("alertSettingId"));
		alert.disName = alertSetting.get("disName").toString();
		alert.phones = alertSetting.get("phones").toString();
		alert.emails = alertSetting.get("emails").toString();
		alert.criticalEmails = alertSetting.get("criticalEmails").toString();
		alert.high = (float) alertSetting.get("high");
		alert.low = (float) alertSetting.get("low");
		alert.afterMilliSecondAlert = (double) alertSetting.get("afterMilliSecondAlert");

		for (TagDataType d : tagDataPool.values()) {
			if (d.errorMillSecondTimes.get(alert.alertSettingId) != null) {
				d.errorStartMilliSecondTimes.put(alert.alertSettingId, -1l);
				d.errorMillSecondTimes.put(alert.alertSettingId, -1l);
				d.errorAlerted.put(alert.alertSettingId, false);
			}
		}
	}

	// 刪除alert何某一個Tag dataType的關聯
	public static void delTagDataTypeAndAlertRel(int alertSettingId, String companyId, String mac, String dataType) {
		String key = String.format("%s_%s_%s", companyId, mac, dataType);
		TagDataType t = tagDataPool.get(key);
		Iterator<Alert> it = t.alerts.iterator();
		while (it.hasNext()) {
			Alert al = it.next();
			if (al.alertSettingId == alertSettingId) {
				it.remove();
			}
		}
		t.errorStartMilliSecondTimes.remove(alertSettingId);
		t.errorMillSecondTimes.remove(alertSettingId);
		t.errorAlerted.remove(alertSettingId);
	}

	// 刪除alert的基本資訊
	public static void delAlert(int alertSettingId) {
		for (TagDataType d : tagDataPool.values()) {
			if (d.errorMillSecondTimes.get(alertSettingId) != null) {
				d.errorStartMilliSecondTimes.remove(alertSettingId);
				d.errorMillSecondTimes.remove(alertSettingId);
				d.errorAlerted.remove(alertSettingId);
				// 刪除關聯
				Iterator<Alert> it = d.alerts.iterator();
				while (it.hasNext()) {
					Alert al = it.next();
					if (al.alertSettingId == alertSettingId) {
						it.remove();
					}
				}
			}
		}
		tagAlertPool.remove(alertSettingId);
	}

}

class TagDataType {
	public String macId;
	public String disName;
	public String dataType;
	public float lastData;// 最新資料
	public long lastDataDatetime = 0;// 用數字毫秒去表示日期會比較快，最新一筆上傳時間
	private String lastDataDatetimeStr;
	public List<Alert> alerts = new ArrayList();
	public Map<Integer, Long> errorStartMilliSecondTimes = new HashMap();// <alertId,second>
																			// 第一筆發生的時間
	public Map<Integer, Long> errorMillSecondTimes = new HashMap<Integer, Long>();// <alertId,second>
																					// 每個alert發生了多少秒
	public Map<Integer, Long> criticalErrorStartMilliSecondTimes = new HashMap();// <alertId,second>
																			// 第一筆critical alert發生的時間
	public Map<Integer, Long> criticalErrorMillSecondTimes = new HashMap<Integer, Long>();// <alertId,second>
																					// 每個critical alert發生了多少秒
	public Map<Integer, Boolean> errorAlerted = new HashMap();// <alertId,boolean>
																// 這個alert是否已經警示過了
	public Map<Integer, Boolean> criticalErrorAlerted = new HashMap();// <alertId,boolean>
																// 這個alert是否已經警示過了(危急警示)

	public TagDataType(String macId, String dataType, String disName) {
		this.macId = macId;
		this.dataType = dataType;
		this.disName = disName;
	}

	public String getLastDataDatetimeStr() {
		return new JDateTime(lastDataDatetime).toString("YYYYMMDDhhmmss");
	}
}

class Alert {
	public int alertSettingId;
	public String disName;// alert的名稱 之後會通報xxx異常
	public float low, high;
	public double afterMilliSecondAlert; // 持續發生多少秒後alert
	public String emails;
	public String criticalEmails;
	public String phones;

	public Alert(int alertId, String disName, float low, float high, double afterMilliSecondAlert, String emails,
			String criticalEmails, String phones) {
		super();
		this.alertSettingId = alertId;
		this.disName = disName;
		this.low = low;
		this.high = high;
		this.afterMilliSecondAlert = afterMilliSecondAlert;
		this.emails = emails;
		this.criticalEmails = criticalEmails;
		this.phones = phones;
	}

}
