package com.utility;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/******************************************
 * SMS Api傳送api
 *
 * @author Tim create by 2017/01/09
 *******************************************/
public class PhoneAlertUtility {
    public static void sendSms(String toss,String content) {
    	try {
    		String[] tos=toss.split(";");
    		String apiKey = "0cf7c896d81cb5e77853eb42815c6f64";
    		String username = "itrilogistics";
    		String password = "itri#000";
    		UrlSmsSender smsSender = new UrlSmsSender(apiKey, username, password);
    		HashMap<String, String> response1 = smsSender.sendNow(tos, content);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    
    /**
     * 負責發送智邦 PP 一元簡訊的類別。
     */
    public static class UrlSmsSender {
        private static final String API_URL ="http://pp.url.com.tw/api/msg";

        private String apiKey;
        private String username;
        private String password;
        
        private String parameterFirstHalf;

        /**
         * 建立一個發送智邦 PP 一元簡訊的物件。
         * @param apiKey 簡訊發送 API 金鑰。
         * @param username 智邦會員中心帳號。
         * @param password 智邦會員中心密碼。
         */
        public UrlSmsSender(String apiKey, String username, String password) {
            if(apiKey == null || apiKey.length() < 1)
                throw new IllegalArgumentException("apiKey is empty");
            if(username == null || username.length() < 1)
                throw new IllegalArgumentException("username is empty");
            if(password == null || password.length() < 1)
                throw new IllegalArgumentException("password is empty");
            
            this.apiKey = apiKey;
            this.username = username;
            this.password = password;
            
            generateParameterFirstHalf();
        }
        
        /**
         * 設定簡訊發送 API 金鑰。
         */
        public void setApiKey(String apiKey) {
            if(apiKey == null || apiKey.length() < 1)
                throw new IllegalArgumentException("apiKey is empty");
            
            this.apiKey = apiKey;
            generateParameterFirstHalf();
        }

        /**
         * 設定智邦會員中心帳號。
         */
        public void setUsername(String username) {
            if(username == null || username.length() < 1)
                throw new IllegalArgumentException("username is empty");
            
            this.username = username;
            generateParameterFirstHalf();
        }
        
        /**
         * 設定智邦會員中心密碼。
         */
        public void setPassword(String password) {
            if(password == null || password.length() < 1)
                throw new IllegalArgumentException("password is empty");
            
            this.password = password;
            generateParameterFirstHalf();
        }
        
        /**
         * 立即傳送一則簡訊。
         * @param smsList 簡訊的接收門號。最多可達 250 個。
         * @param smsBody 簡訊的內容，一則簡訊最多可傳送 140 個字元。
         *                其中一個中文字佔 2 個字元，一個英文字佔 1 個字元，一個換行符號佔 1 個字元。
         * @return 主機的回傳訊息。
         * @throws IOException
         */
        public HashMap<String, String> sendNow(String[] smsList, String smsBody) throws IOException {
            String s = null;
            return sendScheduled(smsList, smsBody, s);
        }
        
        /**
         * 在特定的時間傳送一則簡訊。
         * @param smsList 簡訊的接收門號。最多可達 250 個。
         * @param smsBody 簡訊的內容，一則簡訊最多可傳送 140 個字元。
         *                其中一個中文字佔 2 個字元，一個英文字佔 1 個字元，一個換行符號佔 1 個字元。
         * @param smsTime 預約傳送的時間；若為 null 則表示立即發送。
         * @return 主機的回傳訊息。
         * @throws IOException
         */
        public HashMap<String, String> sendScheduled(String[] smsList, String smsBody, Calendar smsTime) throws IOException {
            String translatedTime = null;
            
            if(smsTime != null) {
                SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                translatedTime = dateformat.format(smsTime.getTime());
            }
            
            return sendScheduled(smsList, smsBody, translatedTime);
        }

        /**
         * 在特定的時間傳送一則簡訊。
         * @param smsList 簡訊的接收門號。最多可達 250 個。
         * @param smsBody 簡訊的內容，一則簡訊最多可傳送 140 個字元。
         *                其中一個中文字佔 2 個字元，一個英文字佔 1 個字元，一個換行符號佔 1 個字元。
         * @param smsTime 預約傳送的時間，格式為 yyyy-MM-dd HH:mm:ss；若為 null 或空字串則表示立即發送。
         * @return 主機的回傳訊息。
         * @throws IOException
         */
        public HashMap<String, String> sendScheduled(String[] smsList, String smsBody, String smsTime) throws IOException {
            String parameters = createParametersString(smsList, smsBody, smsTime);
            
            URL url = null;
            try {
                url = new URL(API_URL);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false);
            
            sendParameters(connection, parameters);
            String response = getResponse(connection);
            
            connection.disconnect();

            return parseResponse(response);
        }
        
        /**
         * 產生前半段的 POST 參數
         */
        private void generateParameterFirstHalf() {
            parameterFirstHalf = "api_key=" + apiKey + "&user_name=" + username 
                    + "&password=" + password;
        }
        
        /**
         * 將參數串成一個字串。
         */
        private String createParametersString(String[] addresses, String message, String time) {
            if(addresses == null || addresses.length < 1)
                throw new IllegalArgumentException("No addresses.");
            if(message == null || message.length() < 1)
                throw new IllegalArgumentException("No message content.");
            
            StringBuilder params = new StringBuilder(parameterFirstHalf);

            params.append("&sms_list=");
            for(String number : addresses) {
                if(number != null && number.length() > 0)
                    params.append(number).append("%2c");
            }

            try {
                params.append("&sms_body=").append(URLEncoder.encode(message, "UTF-8"));
                
                if(time != null && time.length() > 0) {
                    params.append("&sms_time=").append(URLEncoder.encode(time, "UTF-8"));
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            
            return params.toString();
        }

        /**
         * 傳送參數至伺服器。
         */
        private void sendParameters(HttpURLConnection connection, String parameters) throws IOException {
            try {
                connection.setRequestMethod("POST");
            } catch (ProtocolException e) {
                e.printStackTrace();
            }

            byte[] parametersBytes = parameters.getBytes();
            
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length", Integer.toString(parametersBytes.length));
            connection.setUseCaches(false);

            BufferedOutputStream bos = new BufferedOutputStream(connection.getOutputStream());
            bos.write(parametersBytes);
            bos.flush();
            bos.close();
        }

        /**
         * 從伺服器接收回傳訊息。
         */
        private String getResponse(HttpURLConnection connection) throws IOException {
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(connection.getInputStream(), "UTF-8"));

            String inputLine;
            while((inputLine = br.readLine()) != null) {
                stringBuilder.append(inputLine);
            }

            br.close();
            return stringBuilder.toString();
        }

        /**
         * 解析回應訊息。
         */
        private static HashMap<String, String> parseResponse(String response) {
            HashMap<String, String> result = new HashMap<String, String>();
            
            String pattern = "('[^']*')";
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(response);

            String key = null;
            String value = null;

            while(m.find()) {
                key = m.group();
                key = key.substring(1, key.length() - 1);
                m.find();
                
                value = m.group();
                value = value.substring(1, value.length() - 1);
                result.put(key, value);
            }

            return result;
        }
    }

}
