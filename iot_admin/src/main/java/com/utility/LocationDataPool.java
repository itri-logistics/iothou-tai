package com.utility;

import java.util.HashMap;
import java.util.Map;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import jodd.datetime.JDateTime;

public class LocationDataPool {

	private static Map<String, VehicleData> vehicleDataPool = new HashMap();// <vehiclevKey,VehicleData.class>
																			//key = comapnyId_vehicleId
	
	//取得最新資料
	public static Map getNewLocationData(String companyId, String vehicleIds) { // macIdDataTypes
		// 中間要用底線隔開
		Map rs = new HashMap();
		String[] vehicleIdArray = vehicleIds.split(",");
		for (String vehicleId : vehicleIdArray) {
			VehicleData vehicleData = vehicleDataPool.get(companyId + "_" + vehicleId);
			if (vehicleData != null) {
				rs.put(vehicleId, vehicleData);
			}
		}
		return rs;
	}

	//更新行動裝置的最新Data
	public static void updateVehicleData(String companyId, String vehicleId, String disName, String[] datas){
		String data = datas[datas.length - 1];
		String[] d = data.substring(12).split("\\.");
		long tempDatetime = new JDateTime("20" + data.substring(0, 12), "YYYYMMDDhhmmss").getTimeInMillis();
		
		String key = String.format("%s_%s", companyId, vehicleId);
		VehicleData vehicleData = vehicleDataPool.get(key);
		if(vehicleData == null){
			vehicleData = new VehicleData(vehicleId, disName);
			vehicleDataPool.put(key, vehicleData);
		}
		
		if (tempDatetime > vehicleData.lastDataDatetime) {// 資料時間較新的才更新
			vehicleData.lat = Double.parseDouble(d[0])/(Math.pow(10, 7));
			vehicleData.lng = Double.parseDouble(d[1])/(Math.pow(10, 7));
			vehicleData.lastDataDatetime = tempDatetime;
			if(!vehicleData.disName.equals(disName)){
				vehicleData.disName = disName;
			}
		}
		
	}
	
}

class VehicleData {
	public String vehicleId;
	public String disName;
	public double lat, lng;// 最新資料
	public long lastDataDatetime = 0;

	public VehicleData(String vehicleId, String disName) {
		this.vehicleId = vehicleId;
		this.disName = disName;
	}

	public String getLastDataDatetimeStr() {
		return new JDateTime(lastDataDatetime).toString("YYYYMMDDhhmmss");
	}
}
