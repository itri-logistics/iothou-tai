package com.utility;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.security.SecurityController;

/******************************************
 * Email Api傳送api
 *
 *
 * @author Tim create by 2017/01/09
 *******************************************/
@Service
public class EmailAlertUtility {

	@Autowired
	JavaMailSenderImpl mailSender;

	public void sendMail(String emails, String title, String content) {
		try {
			String[] tos = emails.split(";");
			for (String s : tos) {
				if (!isValidEmailAddress(s)) {
					throw new Exception("email格式 有誤!!");
				}
			}
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");// 處理中文編碼
			helper.setSubject(title); // 主題

			helper.setFrom(mailSender.getUsername()); // 寄件者
			helper.setTo(tos); // 收件人
			helper.setText(content, true); // 內容(HTML)
			mailSender.send(message);
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("send mail failed. " + e.getMessage());
		}
		SecurityController.nLog.info("sent mail to " + emails + ", title: " + title + ", content: " + content);

	}

	public static boolean isValidEmailAddress(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}

	class Mail {
		String emails;
		String title;
		String content;
		int num = 0;

		public Mail(String emails, String title, String content) {
			super();
			this.emails = emails;
			this.title = title;
			this.content = content;
		}
	}

}
