package com.utility;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Map;
import java.util.Random;

public class Captcha {
	final private static  int width = 80;
	final private static int height = 30;
	final private static int codeCount = 4;
	final static char[] codeSequence = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

	public static void getCaptcha(Map map){

		// 定義圖像buffer
		BufferedImage buffImg = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D gd = buffImg.createGraphics();

		// 創建一個亂數產生器類
		Random random = new Random();

		// 將圖像填充為白色
		gd.setColor(Color.WHITE);
		gd.fillRect(0, 0, width, height);

        // 創建字體，字體的大小應該根據圖片的高度來定。
        Font font = new Font("Times New Roman", Font.HANGING_BASELINE, 28);
         // 設置字體。
        gd.setFont(font);

		// 畫邊框。
//		gd.setColor(Color.BLACK);
//		gd.drawRect(0, 0, width - 1, height - 1);

		// 隨機產生3條干擾線，使圖像中的認證碼不易被其它程式探測到,干擾線太明顯則會導致用戶無法看清驗證碼
		gd.setColor(Color.GRAY);
		for (int i = 0; i < 3; i++) {
			int x = random.nextInt(width);
			int y = random.nextInt(height);
			int xl = random.nextInt(12);
			int yl = random.nextInt(12);
			gd.drawLine(x, y, x + xl, y + yl);
		}

		// randomCode用於保存隨機產生的驗證碼。
		StringBuffer randomCode = new StringBuffer();
		int red = 0, green = 0, blue = 0;

		// 隨機產生codeCount數位的驗證碼。
		for (int i = 0; i < codeCount; i++) {
			// 得到隨機產生的驗證碼數位。
			String strRand = String.valueOf(codeSequence[random.nextInt(codeSequence.length)]);
			// 產生隨機的顏色分量來構造顏色值，這樣輸出的每位元數位的顏色值都將不同。
			red = random.nextInt(255);
			green = random.nextInt(255);
			blue = random.nextInt(255);
			// 用隨機產生的顏色將驗證碼繪製到圖像中。
			gd.setColor(new Color(red, green, blue));
//			gd.drawString(strRand, (i * xx) + xx / 2, codeY);
//			gd.drawString(strRand, 0, 20);
			gd.drawString(strRand, 20 * i + 6, 24);

			// 將產生的四個亂數組合在一起。
			randomCode.append(strRand);
		}
		map.put("buffImg", buffImg);
		map.put("str", randomCode);
//		ImageIO.write(buffImg, "jpeg", new File(Pa.PDF_PATH+"test.jpg"));
	}

}
