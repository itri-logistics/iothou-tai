package com.controller.place;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.ServletContextResource;
import org.springframework.web.multipart.MultipartFile;

import com.dao.place.PlaceDao;
import com.dao.tag.TagDao;
import com.google.gson.Gson;
import com.security.SecurityController;

/******************************************
 * 場域設定
 *
 * 套件:place 
 * PlaceController.java 
 * PlaceDao.java 
 * placeAssign.js 
 * placeView.jsp
 *
 * @author tim create by 2016/12/02
 *******************************************/
@Controller
@RequestMapping(value = "place")
public class PlaceController extends SecurityController {

	@Autowired
	Gson gson;

	@Autowired
	PlaceDao placeDao;
	
	@Autowired
	TagDao tagDao;

	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "place/placeView";
	}

	@RequestMapping(value = "/find", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String find(
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "placeId", defaultValue = "") String placeId,
			@RequestParam(value = "disName", defaultValue = "") String disName,
			@RequestParam(value = "page", defaultValue = "") String page, ModelMap model) {
		return gson.toJson(new RsMsg("success", placeDao.find(disName, companyId, placeId, page)));
	}
	
	@RequestMapping(value = "/findWithTagNum", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findWithTagNum(
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "placeId", defaultValue = "") String placeId,
			@RequestParam(value = "disName", defaultValue = "") String disName,
			@RequestParam(value = "page", defaultValue = "") String page, ModelMap model) {
		
		try{
			List<Map<String, Object>> list = (List<Map<String, Object>>)placeDao.find(disName, companyId, placeId, page);
			for(Map<String, Object> map: list){
				map.put("tagNum", tagDao.getTagNumByPlace(map.get("placeId").toString()));
			}
			return gson.toJson(new RsMsg("success", list));
		}catch(Exception e){
			e.printStackTrace();
			return gson.toJson(new RsMsg("error", e));
		}
		
	}

	@RequestMapping(value = "/findPage", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findPage(@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "disName", defaultValue = "") String disName, ModelMap model) {
		return gson.toJson(new RsMsg("success", placeDao.findPage(disName, companyId)));
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody Object update(@RequestParam(value = "placeId", defaultValue = "") String placeId,
			@RequestParam(value = "disName", defaultValue = "") String disName,
			@RequestParam(value = "companyId", defaultValue = "") String companyId, ModelMap model) {
		try {
			// 更新資訊
			placeDao.update(placeId, companyId, disName, getUpdateDate(),getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
	@RequestMapping(value="/del",method = RequestMethod.POST)
	public @ResponseBody Object del(
			@RequestParam(value="placeId",defaultValue="") String placeId,			
			ModelMap model) {
		try {
			placeDao.del(placeId);		
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 更新場域平面圖
	@RequestMapping(value = "/updateImage", method = RequestMethod.POST)
	public @ResponseBody Object updateImage(
			@RequestParam(value = "placeId", defaultValue = "") String placeId,
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam("file") MultipartFile file, ModelMap model) {
		try {

			// 更新圖
			String rootPath = System.getProperty("user.home");
			// img dir
			String imgPath = rootPath + File.separator + "iot_img";
			File imgDirFile = new File(imgPath);
			if (!imgDirFile.exists()) {
				imgDirFile.mkdir();
			}

			if (!file.isEmpty()) {
				try {

					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(
							new File(imgPath + File.separator + companyId + "-" + placeId + ".jpg")));
					FileCopyUtils.copy(file.getInputStream(), stream);
					stream.flush();
					stream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			return Pa.success;

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 更換場域圖示
	@RequestMapping(value = "/updateIcon", method = RequestMethod.POST)
	public @ResponseBody Object updateIcon(@RequestParam(value = "placeId", defaultValue = "") String placeId,
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam("iconFile") MultipartFile iconFile, // 圖示
			ModelMap model) {
		try {

			// 更新圖
			String rootPath = System.getProperty("user.home");
			// img dir
			String imgPath = rootPath + File.separator + "iot_img";
			File imgDirFile = new File(imgPath);
			if (!imgDirFile.exists()) {
				imgDirFile.mkdir();
			}

			if (!iconFile.isEmpty()) {
				
				if(iconFile.getSize() > 1024 * 50){	//超過50kb
					return new RsMsg("error", "file size too large.");
				}
				
				try {
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(
							new File(imgPath + File.separator + companyId + "-" + placeId + "_icon.png")));
					FileCopyUtils.copy(iconFile.getInputStream(), stream);
					stream.flush();
					stream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			return Pa.success;

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}

	}

	// 使用預設圖更換場域圖示
	@RequestMapping(value = "/updateIconByOption", method = RequestMethod.POST)
	public @ResponseBody Object updateIcon(@RequestParam(value = "placeId", defaultValue = "") String placeId,
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "iconOption", defaultValue = "") String iconOption, // 圖示
			ModelMap model) {
		try {

			// 更新圖
			String rootPath = System.getProperty("user.home");
			// img dir
			String imgPath = rootPath + File.separator + "iot_img";
			File imgDirFile = new File(imgPath);
			if (!imgDirFile.exists()) {
				imgDirFile.mkdir();
			}

			String filePath = rootPath + File.separator + "iot_img";			
			
			String fileName = null;
			if(iconOption.equals("img1")){
				fileName = "ic_default_cage_car.png";
			}else if(iconOption.equals("img2")){
				fileName = "ic_default_car.png";
			}else if(iconOption.equals("img3")){
				fileName = "ic_default_freezer.png";
			}else if(iconOption.equals("img4")){
				fileName = "ic_default_ice.png";
			}else if(iconOption.equals("img5")){
				fileName = "ic_default_warehouse.png";
			}			

			if (fileName != null) {
				File iFile = new File(filePath, fileName);
				try {
					FileCopyUtils.copy(iFile,
							new File(imgPath + File.separator + companyId + "-" + placeId + "_icon.png"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			return Pa.success;

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}

	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object create(@RequestParam(value = "disName", defaultValue = "") String disName,
			@RequestParam(value = "companyId", defaultValue = "") String companyId, ModelMap model) {
		try {
			placeDao.create(companyId, disName, getUpdateDate(),getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 下載圖片
	@RequestMapping(value = "/{type}/{companyId}/{placeId}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getFile(@PathVariable(value = "type") String type,
			@PathVariable(value = "companyId") String companyId, @PathVariable(value = "placeId") String placeId)
					throws FileNotFoundException, IOException {

		String rootPath = System.getProperty("user.home");
		String path = rootPath + File.separator + "iot_img";

		String fileName = "none.jpg";

		if (type.equals("img")) {
			fileName = companyId + "-" + placeId + ".jpg";
		} else if (type.equals("icon")) {
			fileName = companyId + "-" + placeId + "_icon.png";
		}

		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentType(MediaType.IMAGE_JPEG);
		File img = new File(path + File.separator + fileName);
		if (!img.exists()) {			
			if (type.equals("icon")) {
				img = new File(path + File.separator + "ic_default_cage_car.png");
			}else{
				img = new File(path + File.separator + "none.png");
			}
		}
		FileInputStream fi = new FileInputStream(img);
		byte[] bs = IOUtils.toByteArray(fi);
		fi.close();
		return new ResponseEntity<byte[]>(bs, headers, HttpStatus.CREATED);
	}

}
