package com.controller.dashboard;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.dashboard.RealTimeDashboardDao;
import com.dao.query.DebugQueryRealTimeDao;
import com.dao.upload.QueryDao;
import com.google.gson.Gson;
import com.security.SecurityController;
import com.utility.ExcelUtility;
/******************************************
 * 即時看板
 *
 * 套件:dashboard 
 * RealTimeDashboardController.java 
 * RealTimeDashboardDao.java 
 * realTimeDashboard.js 
 * realTimeDashboardView.jsp
 *
 * @author tim create by 2016/12/29
 *******************************************/
@Controller
@RequestMapping(value = "realTimeDashboard")
public class RealTimeDashboardController extends SecurityController {

	@Autowired
	Gson gson;

	@Autowired
	RealTimeDashboardDao realTimeDashboardDao;
	
	@RequestMapping()
	public String realTimeDashboard(ModelMap model) {
	    return "dashboard/realTimeDashboardView";
	}
	
	// 一次取得多個tag dataType資料    ex:macIdA_dataTypeA,macIdB_dataTypeB,......依此類推
	@RequestMapping(value = "/getNewestTagsData", method = RequestMethod.GET)
	public @ResponseBody Object getNewestTagsData(
			@RequestParam(value = "macIdDataTypes", defaultValue = "") String macIdDataTypes, 
			ModelMap model) {
		try {
			
			return new RsMsg("success", realTimeDashboardDao.getNewestTagsData(getCompanyId(), macIdDataTypes));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
	}
	
	// 取得看板設定
	@RequestMapping(value = "/findDashboardSetting", method = RequestMethod.GET)
	public @ResponseBody Object findDashboardSetting(
			ModelMap model) {
		try {
			return new RsMsg("success", realTimeDashboardDao.findDashboardSetting(getCompanyId()));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage().subSequence(0, e.getMessage().indexOf(";")));
		}
	}
	
}