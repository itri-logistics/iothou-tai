package com.controller.dashboard;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.dashboard.DashboardSettingDao;
import com.dao.dashboard.RealTimeDashboardDao;
import com.dao.query.DebugQueryRealTimeDao;
import com.dao.upload.QueryDao;
import com.google.gson.Gson;
import com.security.SecurityController;
import com.utility.ExcelUtility;
/******************************************
 * 即時看板
 *
 * 套件:dashboardSetting
 * DashboardSettingController.java 
 * DashboardSettingDao.java 
 * DashboardSetting.js 
 * DashboardSettingView.jsp
 *
 * @author ATone create by 2016/12/31
 *******************************************/
@Controller
@RequestMapping(value = "dashboardSetting")
public class DashboardSettingController extends SecurityController {

	@Autowired
	Gson gson;

	@Autowired
	DashboardSettingDao dashboardSettingDao;
	
	@RequestMapping()//
	public String realTimeDashboard(ModelMap model) {
	    return "dashboard/dashboardSettingView";
	}	
	
	// 取得看板設定
	@RequestMapping(value = "/findDashboardSetting", method = RequestMethod.GET)
	public @ResponseBody Object findDashboardSetting(
			ModelMap model) {
		try {
			return new RsMsg("success", dashboardSettingDao.findDashboardSetting(getCompanyId()));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage().subSequence(0, e.getMessage().indexOf(";")));
		}
	}
	
	// 更新看板設定
	@RequestMapping(value="/updateDashboardSetting",method = RequestMethod.POST)
	public @ResponseBody Object update(
			@RequestParam(value="dashboardId",defaultValue="") String dashboardId,
			@RequestParam(value="disName",defaultValue="") String disName,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="macId",defaultValue="") String macId,
			@RequestParam(value="dataType",defaultValue="") String dataType,
			@RequestParam(value="position",defaultValue="") String position,
			ModelMap model) {
		try {
			dashboardSettingDao.update(dashboardId, disName, companyId, macId, dataType, position);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}		
	
	@RequestMapping(value="/createDashboardSetting",method = RequestMethod.POST)
	public @ResponseBody Object create(
			@RequestParam(value="disName",defaultValue="") String disName,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="macId",defaultValue="") String macId,
			@RequestParam(value="dataType",defaultValue="") String dataType,
			@RequestParam(value="position",defaultValue="") String position,
			ModelMap model) {
		try {
			dashboardSettingDao.create(disName, companyId, macId, dataType, position);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}	
	
	@RequestMapping(value="/delDashboardSetting",method = RequestMethod.POST)
	public @ResponseBody Object del(
			@RequestParam(value="dashboardId",defaultValue="") String dashboardId,			
			ModelMap model) {
		try {
			dashboardSettingDao.del(dashboardId);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
	// 一次取得多個tag dataType資料    ex:macIdA_dataTypeA,macIdB_dataTypeB,......依此類推
		@RequestMapping(value = "/getNewestTagsData", method = RequestMethod.GET)
		public @ResponseBody Object getNewestTagsData(
				@RequestParam(value = "macIdDataTypes", defaultValue = "") String macIdDataTypes, 
				ModelMap model) {
			try {
				return new RsMsg("success", dashboardSettingDao.getNewestTagsData(getCompanyId(), macIdDataTypes));
			} catch (Exception e) {
				e.printStackTrace();
				return new RsMsg("error", e.getMessage());
			}
		}
	
}