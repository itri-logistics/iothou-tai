package com.controller.time;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.security.SecurityController;

/******************************************
 * 對時用
 *
 * 套件:time
 * TimeCorrectionController.java 
 *
 * @author ATone create by 2017/10/23
 *******************************************/
@Controller
@RequestMapping(value = "time")
public class TimeCorrectionController extends SecurityController{

	@RequestMapping()
	public @ResponseBody String shipDocAssign(ModelMap model) {
		return "T:"+getUpdateDate().substring(2);
	}
	
}
