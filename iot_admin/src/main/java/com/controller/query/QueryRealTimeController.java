package com.controller.query;

import itri.group.param.RsMsg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.query.DebugQueryRealTimeDao;
import com.dao.upload.QueryDao;
import com.google.gson.Gson;
import com.security.SecurityController;
import com.utility.DataSizeLimitUtility;
import com.utility.ExcelUtility;
/******************************************
 * (debug 專用)
 * (debug 專用)
 * (debug 專用)
 * (debug 專用)
 * 
 * 場域設定(debug 專用)
 *
 * 套件:place 
 * QueryRealTimeController.java 
 * QueryRealTimeDao.java 
 * queryRealTime.js 
 * queryRealTimeView.jsp
 *
 * @author tim create by 2016/12/02
 *******************************************/
@Controller
@RequestMapping(value = "queryRealTime")
public class QueryRealTimeController extends SecurityController {

	@Autowired
	Gson gson;

	@Autowired
	DebugQueryRealTimeDao queryRealTimeDao;
	
	// 取得Tag最新的即時資料
	@RequestMapping(value = "/getNewestTagsData", method = RequestMethod.GET)
	public @ResponseBody Object getNewestTagsData(
			@RequestParam(value = "macIdDataTypes", defaultValue = "") String macIdDataTypes, 
			ModelMap model) {
		try {
			return new RsMsg("success", queryRealTimeDao.getNewestTagsData(getCompanyId(), macIdDataTypes));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
	}

	@Autowired
	ExcelUtility excelUtility;

	// 取得查詢時間內有紀錄的Tag
	@RequestMapping(value = "/getTagsInQueryInterval", method = RequestMethod.GET)
	public @ResponseBody String getTagsInQueryInterval(
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "sd", defaultValue = "") String sd,
			@RequestParam(value = "ed", defaultValue = "") String ed, ModelMap model) {
		return gson.toJson(new RsMsg("success", queryRealTimeDao.getTagsInQueryInterval(companyId, sd, ed)));
	}

	// 一次取得一個Tag裡面所有類型的資料
	@RequestMapping(value = "/getQueryData", method = RequestMethod.GET)
	public @ResponseBody Object getQueryData(@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "sd", defaultValue = "") String sd,
			@RequestParam(value = "ed", defaultValue = "") String ed,
			@RequestParam(value = "macsStr", defaultValue = "") String macsStr, ModelMap model) {
		try {
			String[] macArray = macsStr.split(",");
			if(DataSizeLimitUtility.outOfLimit(macArray, sd, ed, 15)){	
				return new RsMsg("error", "data size out of limit");
			}
			return new RsMsg("success", queryRealTimeDao.getQueryData(companyId, sd, ed, macsStr));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
	}

	// 下載Excel
	@RequestMapping(value = "/getExcelFile", method = RequestMethod.GET)
	@ResponseBody
	public void getExcelFile(@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "sd", defaultValue = "") String sd,
			@RequestParam(value = "ed", defaultValue = "") String ed,
			@RequestParam(value = "macsStr", defaultValue = "") String macsStr, HttpServletResponse response) {
		try {
			String[] macArray = macsStr.split(",");
			if(DataSizeLimitUtility.outOfLimit(macArray, sd, ed)){
				response.setContentType("text/html; charset=utf-8");
				PrintWriter writer = response.getWriter();
				writer.println("檔案過大，無法下載，請減少選取的Tag數或縮短查詢的紀錄區間。");
				writer.println("Tag數量 * 查詢天數需小於70。");
				return;
			}
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=record_"+sd+"_"+ed+".xls");
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			final OutputStream outStream = response.getOutputStream();
			HashMap<String, String> tagDataMap = new HashMap<String, String>();
			
			for (String mac : macArray) {
				tagDataMap.put(mac, queryRealTimeDao.getQueryData(companyId, sd, ed, mac));
			}
			//System.out.println(tagDataMap.toString());
			outByteStream = excelUtility.exportOutRecordController(outByteStream, tagDataMap);
			outStream.write(outByteStream.toByteArray());
			outStream.close();
			outByteStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}