package com.controller.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.query.QueryByTagCodeDao;
import com.dao.tag.TagCodeDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.RsMsg;

/******************************************
 * 透過tag代號查詢紀錄
 *
 * 套件:query QueryByTagCodeController.java
 *
 * @author ATone create by 2017/09/21
 *******************************************/

@Controller
@RequestMapping(value = "queryByTagCode")
public class QueryByTagCodeController extends SecurityController {

	@Autowired
	QueryByTagCodeDao queryByTagCodeDao;
	
	@Autowired
	TagCodeDao tagCodeDao;

	@Autowired
	Gson gson;
	
	// 取得一個Tag裡面單日的port1資料
	@RequestMapping(value = "/getPort1Data", method = RequestMethod.GET)
	public @ResponseBody Object getQueryDataByCode(@RequestParam(value = "date", defaultValue = "") String date,
			@RequestParam(value = "tagCode", defaultValue = "") String tagCode, ModelMap model) {
		try {
			String macId = tagCodeDao.getTagMacByCode(tagCode);

			// 判斷mac
			if (macId.isEmpty()) {
				throw new Exception("tag not found.");
			} 
			
			return new RsMsg("success", queryByTagCodeDao.getPort1Data(date, macId));
						
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
	}

}
