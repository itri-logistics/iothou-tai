package com.controller.proccess;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.security.SecurityController;

/******************************************
 * 歷史圖控
 *
 * 套件:graphic
 * GraphicController.java
 * graphic.js
 * graphicView.jsp
 *
 * @author ATone create by 2016/12/22
 *******************************************/

@Controller            
@RequestMapping(value="proccessGraphic")
public class ProccessGraphicController extends SecurityController{
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "proccess/graphicView";
	}
	
}
