package com.controller.proccess;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller 
@RequestMapping("proccess")
public class ProccessController {
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "proccess/proccessView";
	}
	
}
