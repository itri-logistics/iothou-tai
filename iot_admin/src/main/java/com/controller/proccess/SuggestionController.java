package com.controller.proccess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.proccess.SuggestionDao;
import com.google.gson.Gson;
import com.utility.DataSizeLimitUtility;

import itri.group.param.RsMsg;

@Controller
@RequestMapping("suggestion")
public class SuggestionController {

	@Autowired
	Gson gson;

	@Autowired
	SuggestionDao suggestionDao;

	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "proccess/suggestionView";
	}

	// 取得標準建議
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public @ResponseBody Object getQueryData(ModelMap model) {
		try {			
			return new RsMsg("success", suggestionDao.queryAll());
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
	}

}
