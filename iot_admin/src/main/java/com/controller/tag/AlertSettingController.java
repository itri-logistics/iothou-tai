package com.controller.tag;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.tag.AlertSettingDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

/******************************************
 * 警示設定
 *
 * 套件:alert AlertSettingController.java AlertSettingControllerDao.java alert.js
 * alertView.jsp
 *
 * @author ATone create by 2016/01/10
 *******************************************/
@Controller
@RequestMapping(value = "alert")
public class AlertSettingController extends SecurityController {

	@Autowired
	Gson gson;

	@Autowired
	AlertSettingDao alertDao;

	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "alert/alertView";
	}

	// 根據公司帳號列出已建立的警示設定
	@RequestMapping(value = "/queryAlertSetting", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findAlertSettingByCompanyId(
			@RequestParam(value = "companyId", defaultValue = "") String companyId, ModelMap model) {

		if (companyId.isEmpty()) {
			companyId = getCompanyId();
		}

		return gson.toJson(new RsMsg("success", alertDao.findAlertSettingsByCompanyId(companyId)));
	}

	// 根據公司帳號列出已建立的警示關聯
	@RequestMapping(value = "/queryAlertRel", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findAlertRelByCompanyId(
			@RequestParam(value = "companyId", defaultValue = "") String companyId, ModelMap model) {

		if (companyId.isEmpty()) {
			companyId = getCompanyId();
		}

		return gson.toJson(new RsMsg("success", alertDao.findAlertRelByCompanyId(companyId)));
	}

	// 根據公司帳號列出已建立的詳細警示設定
	@RequestMapping(value = "/queryAlertSettingDetail", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findAlertSettingDetailByCompanyId(
			@RequestParam(value = "companyId", defaultValue = "") String companyId, ModelMap model) {

		if (companyId.isEmpty()) {
			companyId = getCompanyId();
		}

		return gson.toJson(new RsMsg("success", alertDao.findAlertSettingsDetailByCompanyId(companyId)));
	}

	// 更新警示設定
	@RequestMapping(value = "/updateAlertSetting", method = RequestMethod.POST)
	public @ResponseBody Object AlertSetting(@RequestParam(value = "mac", defaultValue = "") String mac,
			@RequestParam(value = "disName", defaultValue = "") String disName,
			@RequestParam(value = "alertSettingId", defaultValue = "") String alertSettingId,
			@RequestParam(value = "high", defaultValue = "") String high,
			@RequestParam(value = "low", defaultValue = "") String low,
			@RequestParam(value = "afterMilliSecondAlert", defaultValue = "") String afterMilliSecondAlert,
			@RequestParam(value = "phones", defaultValue = "") String phones,
			@RequestParam(value = "emails", defaultValue = "") String emails,
			@RequestParam(value = "criticalEmails", defaultValue = "") String criticalEmails, ModelMap model) {
		try {
			alertDao.update(alertSettingId, getCompanyId(), disName, high, low, afterMilliSecondAlert, emails,
					criticalEmails, phones, getUpdateDate(), getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 新增警示設定
	@RequestMapping(value = "/createAlertSetting", method = RequestMethod.POST)
	public @ResponseBody Object create(@RequestParam(value = "disName", defaultValue = "") String disName,
			@RequestParam(value = "alertSettingId", defaultValue = "") String alertSettingId,
			@RequestParam(value = "high", defaultValue = "") String high,
			@RequestParam(value = "low", defaultValue = "") String low,
			@RequestParam(value = "afterMilliSecondAlert", defaultValue = "") String afterMilliSecondAlert,
			@RequestParam(value = "phones", defaultValue = "") String phones,
			@RequestParam(value = "emails", defaultValue = "") String emails,
			@RequestParam(value = "criticalEmails", defaultValue = "") String criticalEmails, ModelMap model) {
		try {
			alertDao.createAlertSetting(getCompanyId(), disName, low, high, phones, emails, criticalEmails,
					afterMilliSecondAlert, getUpdateDate(), getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 新增警示關聯
	@RequestMapping(value = "/createTagDataTypeAndAlertRel", method = RequestMethod.POST)
	public @ResponseBody Object createTagDataTypeAndAlertRel(@RequestParam(value = "mac", defaultValue = "") String mac,
			@RequestParam(value = "alertSettingId", defaultValue = "") String alertSettingId,
			@RequestParam(value = "dataType", defaultValue = "") String dataType, ModelMap model) {
		try {
			alertDao.createTagDataTypeAndAlertRel(alertSettingId, mac, getCompanyId(), dataType, getUpdateDate(),
					getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 刪除關聯
	@RequestMapping(value = "/delRel", method = RequestMethod.POST)
	public @ResponseBody Object delRel(@RequestParam(value = "mac", defaultValue = "") String mac,
			@RequestParam(value = "alertSettingId", defaultValue = "") String alertSettingId,
			@RequestParam(value = "dataType", defaultValue = "") String dataType, ModelMap model) {
		try {
			alertDao.delTagDataTypeAndAlertRel(mac, alertSettingId, getCompanyId(), dataType);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 更新關聯
	@RequestMapping(value = "/updateRel", method = RequestMethod.POST)
	public @ResponseBody Object updateRel(@RequestParam(value = "mac", defaultValue = "") String mac,
			@RequestParam(value = "oldAlertSettingId", defaultValue = "") String oldAlertSettingId,
			@RequestParam(value = "newAlertSettingId", defaultValue = "") String newAlertSettingId,
			@RequestParam(value = "dataType", defaultValue = "") String dataType, ModelMap model) {
		try {
			// 先刪除舊的
			alertDao.delTagDataTypeAndAlertRel(mac, oldAlertSettingId, getCompanyId(), dataType);
			// 建立新的
			alertDao.createTagDataTypeAndAlertRel(newAlertSettingId, mac, getCompanyId(), dataType, getUpdateDate(),
					getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

	// 刪除警示設定
	@RequestMapping(value = "/delSetting", method = RequestMethod.POST)
	public @ResponseBody Object delSetting(
			@RequestParam(value = "alertSettingId", defaultValue = "") String alertSettingId, ModelMap model) {
		try {
			alertDao.delAlertSetting(alertSettingId);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
}
