package com.controller.tag;
import itri.group.param.Pa;
import itri.group.param.RsMsg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.tag.TagDao;
import com.google.gson.Gson;
import com.security.SecurityController;
/******************************************
 * Tag設定
 *
 * 套件:tag
 * TagController.java
 * TagDao.java
 * tag.js
 * tagView.jsp
 *
 * @author ATone create by 2016/12/07
 *******************************************/
@Controller            
@RequestMapping(value="tag")
public class TagController extends SecurityController{

	@Autowired
	Gson gson;	
	
	@Autowired
	TagDao tagDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "tag/tagView";
	}
	
	@RequestMapping("/test")
	public String getTagTestView(ModelMap model) {
	    return "tag/tagTestView";
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String find(			
			@RequestParam(value="mac",defaultValue="") String mac,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="placeId",defaultValue="") String placeId,
			@RequestParam(value="name",defaultValue="") String name,
			@RequestParam(value="type",defaultValue="") String type,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	tagDao.find(mac,name,type,placeId, companyId, page)));
	}
	
	@RequestMapping(value="/findPage",method = RequestMethod.GET)
	public @ResponseBody String findPage(
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="placeId",defaultValue="") String placeId,
			@RequestParam(value="name",defaultValue="") String name,
			@RequestParam(value="type",defaultValue="") String type,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	tagDao.findPage(name,type,placeId, companyId)));
	}
    
	@RequestMapping(value="/update",method = RequestMethod.POST)
	public @ResponseBody Object update(
			@RequestParam(value="mac",defaultValue="") String mac,
			@RequestParam(value="name",defaultValue="") String name,
			@RequestParam(value="type",defaultValue="") String type,
			@RequestParam(value="port1",defaultValue="") String port1,
			@RequestParam(value="port2",defaultValue="") String port2,
			@RequestParam(value="port3",defaultValue="") String port3,
			@RequestParam(value="port4",defaultValue="") String port4,
			@RequestParam(value="rate",defaultValue="") String rate,
			@RequestParam(value="placeId",defaultValue="") String placeId,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			ModelMap model) {
		try {
			tagDao.update(mac, name, type, port1, port2, port3, port4, rate, placeId, companyId, getUpdateDate());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}		
	
	@RequestMapping(value="/create",method = RequestMethod.POST)
	public @ResponseBody Object create(
			@RequestParam(value="mac",defaultValue="") String mac,
			@RequestParam(value="name",defaultValue="") String name,
			@RequestParam(value="type",defaultValue="") String type,
			@RequestParam(value="port1",defaultValue="") String port1,
			@RequestParam(value="port2",defaultValue="") String port2,
			@RequestParam(value="port3",defaultValue="") String port3,
			@RequestParam(value="port4",defaultValue="") String port4,
			@RequestParam(value="rate",defaultValue="") String rate,
			@RequestParam(value="placeId",defaultValue="") String placeId,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			ModelMap model) {
		try {
			tagDao.create(mac, name, type, port1, port2, port3, port4, rate, placeId, companyId, getUpdateDate());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}	
	
	@RequestMapping(value="/del",method = RequestMethod.POST)
	public @ResponseBody Object del(
			@RequestParam(value="mac",defaultValue="") String mac,			
			ModelMap model) {
		try {
			tagDao.del(mac);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
}
