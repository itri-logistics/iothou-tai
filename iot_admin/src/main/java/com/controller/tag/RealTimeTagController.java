package com.controller.tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.tag.RealTimeTagDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.RsMsg;

/******************************************
 * Tag即時資訊
 *
 * 套件:tag 
 * RealTimeTagController.java 
 * RealTimeTagDao.java 
 * realTimeTag.js
 * realTimeTagView.jsp
 *
 * @author ATone create by 2017/03/15
 *******************************************/

@Controller
@RequestMapping(value = "realTimeTag")
public class RealTimeTagController extends SecurityController{

	@Autowired
	Gson gson;

	@Autowired
	RealTimeTagDao realTimeTagDao;

	@RequestMapping()
	public String realTimeDashboard(ModelMap model) {
		return "tag/realTimeTagView";
	}

	// 一次取得多個tag dataType資料 ex:macIdA_dataTypeA,macIdB_dataTypeB,......依此類推
	@RequestMapping(value = "/getNewestTagsData", method = RequestMethod.GET)
	public @ResponseBody Object getNewestTagsData(
			@RequestParam(value = "macIdDataTypes", defaultValue = "") String macIdDataTypes, ModelMap model) {
		try {
			return new RsMsg("success", realTimeTagDao.getNewestTagsData(getCompanyId(), macIdDataTypes));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
	}
}
