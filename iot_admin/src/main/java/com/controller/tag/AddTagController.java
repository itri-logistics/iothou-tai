package com.controller.tag;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.place.PlaceDao;
import com.dao.tag.AddTagDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.RsMsg;

/******************************************
 * 透過Tag代碼或QR code新增Tag
 *
 * 套件:tag AddTagController.java AddTagDao.java addTagView.jsp
 * tagNotExistView.jsp
 *
 * @author ATone create by 2016/12/30
 *******************************************/
@Controller
@RequestMapping(value = "addTag")
public class AddTagController extends SecurityController {

	@Autowired
	Gson gson;

	@Autowired
	AddTagDao addTagDao;
	
	@Autowired
	PlaceDao placeDao;

	@RequestMapping()
	public String shipDocAssign(@RequestParam(value = "TAG_CODE", defaultValue = "") String tagCode,
			@RequestParam(value = "TAG_MAC", defaultValue = "") String tagMac,
			@RequestParam(value = "PLACE_ID", defaultValue = "") String placeId,
			@RequestParam(value = "PLACE_NAME", defaultValue = "") String placeName,
			@RequestParam(value = "COM_ID", defaultValue = "") String comId, ModelMap model) {

		// 檢查tag代碼格式
		if (tagCode.length() == 4) {
			try {
				tagMac = addTagDao.getTagMacByCode(tagCode);				
			} catch (Exception e) {
				e.printStackTrace();
				return "tag/tagNotExistView";
			}
		} else {
			return "tag/tagNotExistView";
		}

		// 檢查tag是否存在
		if (tagMac == null) {
			return "tag/tagNotExistView";
		} else {
			if (tagMac.length() == 0) {
				return "tag/tagNotExistView";
			}
		}

		// 檢查場域是否存在
		if (placeName.length() < 1 && placeId.length() > 0) {
			try {
				placeName = placeDao.getPlaceName(placeId);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (placeName.length() < 1 && placeId.length() < 1) {
			placeName = "";
			placeId = "";
		}

		model.addAttribute("tagName", tagCode);
		model.addAttribute("tagMac", tagMac.replace(":", ""));
		model.addAttribute("placeId", placeId);
		model.addAttribute("placeName", placeName);
		model.addAttribute("comId", comId);

		return "tag/addTagView";
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String find(			
			@RequestParam(value="tagCode",defaultValue="") String tagCode,
			ModelMap model) {
			String tagMac = addTagDao.getTagMacByCode(tagCode);
			// 檢查tag是否存在
			if (tagMac == null || "".equals(tagMac)) {
				return gson.toJson(new RsMsg("error","tag Not Exist"));
			} else {				
				return gson.toJson(new RsMsg("success",	tagMac));
			}
	}

}
