package com.controller.index;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.security.SecurityController;

@Controller 
@RequestMapping()
public class IndexController extends SecurityController{
	
	@RequestMapping()
	public String realTimeDashboard(ModelMap model) {
	    return "dashboard/realTimeDashboardView";
	}
	
	@RequestMapping(value="/index")
	public String index(ModelMap model) {
		return "dashboard/realTimeDashboardView";
//		if(getType().equals("su")){
//			return "dashboard/realTimeDashboardView";
//		}else{
//			return "record/recordChartView";
//		}
	}

}
