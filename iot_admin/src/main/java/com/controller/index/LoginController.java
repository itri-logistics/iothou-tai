package com.controller.index;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.security.ManageUser;
import com.security.SecurityManager;

import itri.group.param.RsMsg;;

@Controller
@RequestMapping(value = "login")
public class LoginController {

	@Autowired
	Gson gson;

	@Autowired
	SecurityManager securityManager;

	@RequestMapping()
	public String login(ModelMap model) {
		return "index/loginView";
	}

	@RequestMapping(value = "/fromApp", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public @ResponseBody Object loginFromApp(
			@RequestParam(value = "userId", defaultValue = "") String userId,
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "password", defaultValue = "") String password, ModelMap model) {

		try {
			ManageUser user = (ManageUser) securityManager.loadUserByUsernameAndPassword(companyId, userId, password, SecurityManager.OS_ANDROID);
			if (user != null) {
				return gson.toJson(new RsMsg("success", user));
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}

		return new RsMsg("error", "login failed.");
	}
	
	@RequestMapping(value = "/fromIos", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public @ResponseBody Object loginFromIos(
			@RequestParam(value = "userId", defaultValue = "") String userId,
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "password", defaultValue = "") String password, ModelMap model) {

		try {
			ManageUser user = (ManageUser) securityManager.loadUserByUsernameAndPassword(companyId, userId, password, SecurityManager.OS_IOS);
			if (user != null) {
				return gson.toJson(new RsMsg("success", user));
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}

		return new RsMsg("error", "login failed.");
	}

}
