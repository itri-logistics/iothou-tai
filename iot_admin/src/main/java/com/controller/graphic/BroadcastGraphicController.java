package com.controller.graphic;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.security.SecurityController;

/******************************************
 * 歷史圖控
 *
 * 套件:graphic
 * GraphicController.java
 * graphic.js
 * graphicView.jsp
 *
 * @author ATone create by 2016/12/22
 *******************************************/

@Controller            
@RequestMapping(value="graphic_b")
public class BroadcastGraphicController extends SecurityController{
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "graphic/graphicView_b";
	}
	
}
