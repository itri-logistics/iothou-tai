package com.controller.record;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.security.SecurityController;

import itri.group.param.Pa;
/******************************************
 * 紀錄列表
 *
 * 套件:record
 * RecordController.java
 * recordList.js
 * recordListView.jsp
 * recordChart.js
 * recordChartView.jsp
 *
 * @author ATone create by 2016/12/07
 *******************************************/
@Controller
public class RecordController extends SecurityController{	
	
	@RequestMapping(value="recordList",method = RequestMethod.GET)
	public String getRecordListView(ModelMap model) {
		return "record/recordListView";
	}
	
	@RequestMapping(value="broadcastRecordList",method = RequestMethod.GET)
	public String getBroadcastRecordListView(ModelMap model) {
	    return "record/broadcastRecordListView";
	}
	
	@RequestMapping(value="recordChart",method = RequestMethod.GET)
	public String getRecordChartView(ModelMap model) {
	    return "record/recordChartView";
	}
	
}
