package com.controller.upload;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.controller.upload.UploadRealTimeController.FileWriteRunnable;
import com.dao.job.UploadDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.RsMsg;

/******************************************
 * GPS資料上傳 
 * iot_proc目錄:每30秒會讀取解析，最後將解析出來的data轉至iot_data目錄裡面 
 * iot_raw目錄:上傳過來備份使用
 * iot_data目錄:解析完成後的Data，之後網頁呈現，要抓取的資料來源
 *
 * 套件:upload 
 * 
 * UploadLocationController.java 
 *
 * @author ATone create by 2017/02/03
 *******************************************/
@Controller
@RequestMapping(value = "uploadLocation")
public class UploadLocationController extends SecurityController {

	// GPS資料上傳至iot_proc目錄裡面的檔案結尾格式(_loc結尾)
	private final static String PROC_FILE_TYPE = ".wproc_loc";

	// GPS資料上傳至iot_raw目錄裡面的檔案結尾格式(_loc結尾)
	private final static String RAW_FILE_TYPE = ".rawTxt_loc";

	private final static String ROOT_PATH = System.getProperty("user.home");
	private final static String PROC_DIR = ROOT_PATH + File.separator + "iot_proc";
	private final static String RAW_DIR = ROOT_PATH + File.separator + "iot_raw";

	@Autowired
	UploadDao uploadDao;

	@Autowired
	Gson gson;

	@RequestMapping()
	public String upload(ModelMap model) {
		return "upload/uploadLocationView";
	}

	// startdatetime:批次讀取啟動時間12碼(年月日時分秒)ex:161130192030
	// filekey:161121121212-161122130001-6097AE92AE93
	// (上傳資料的第一筆時間-上傳資料的最後一筆時間-mac值)
	// sded:161121121212-161130120001
	// macId:6097AE92AE93
	// dataTypes: 用逗號分開 代表每組訊號ex:0001,0101,0201代表三組訊號
	// 四碼說明
	// AABB :AA代表port號 BB代表資料類型
	// 0001 :本體，溫度
	// 0002 :本體，濕度
	// 0102 :port1，濕度
	// 0508 :手機，經緯度

	// datas用#字號分開
	// datas=
	// fileKey#startdatetime#sded#macId#companyId#dataTypes#data1#data2#data3#data4#以此類推(後面的datax取決於dataTypes的個數)
	// 範例:

	// 會把Data拆開
	@RequestMapping(value = "/uploadLocationData", method = RequestMethod.POST)
	public @ResponseBody Object uploadLogData(@RequestParam(value = "datas", defaultValue = "") String datas,
			ModelMap model) {
		try {
			String[] ds = datas.split("#");
			String fileKey = ds[0];
			String startDatetime = ds[1];
			String[] sded = ds[2].split(",");
			String macId = ds[3];
			String companyId = ds[4];
			String[] dataTypes = ds[5].split(",");

			// 剩餘DATA(檢查使用)，有異常會丟出exception
			for (int i = 6; i < ds.length; i++) {
				String temp = ds[i];
			}

			FileWriteRunnable runnable = new FileWriteRunnable(ds, datas);
			Thread fileThread = new Thread(runnable);

			fileThread.start();

			//uploadDao.log(fileKey, "OK: upload_loc ok: fileKey " + fileKey + "", getUpdateDate());
		} catch (Exception e) {
			e.printStackTrace();
			//uploadDao.log("null", "NG: upload_loc fail:" + e.getMessage(), getUpdateDate());
			return new RsMsg("error", e.getMessage());
		}
		return new RsMsg("success", 200);
	}
	
	class FileWriteRunnable implements Runnable {

		String[] ds;
		String datas;

		public FileWriteRunnable(String[] ds, String datas) {
			super();
			this.ds = ds;
			this.datas = datas;
		}

		@Override
		public void run() {
			try {
				String fileKey = ds[0];
				String startDatetime = ds[1];
				String[] sded = ds[2].split("-");
				String macId = ds[3];
				String companyId = ds[4];
				String[] dataTypes = ds[5].split(",");

				File procFile = new File(PROC_DIR + File.separator + fileKey + PROC_FILE_TYPE);
				String rawFilePath = RAW_DIR + File.separator + companyId + File.separator + startDatetime.substring(0, 4)
						+ File.separator + startDatetime.substring(4, 6) + File.separator + fileKey + RAW_FILE_TYPE;
				File rawFile = new File(rawFilePath);
				
				// 判斷檔案是否已存在
				if (rawFile.exists() && procFile.exists()) {
					return;
				}
				
				FileUtils.write(procFile, datas); // 等待處裡Data
				FileUtils.write(rawFile, datas); // 整理完成Data

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

}
