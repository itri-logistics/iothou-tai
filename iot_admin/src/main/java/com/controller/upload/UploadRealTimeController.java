package com.controller.upload;

import itri.group.param.RsMsg;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.dao.job.UploadDao;
import com.google.gson.Gson;
import com.security.SecurityController;

/******************************************
 * 即時資料上傳 iot_proc目錄:每30秒會讀取解析，最後將解析出來的data轉至iot_data目錄裡面 iot_raw目錄:上傳過來備份使用
 * iot_data目錄:解析完成後的Data，之後網頁呈現，要抓取的資料來源
 *
 * 套件:upload UploadRealTimeController.java uploadRealTime.js
 * uploadRealTimeView.jsp
 *
 * @author Tim create by 2016/12/25
 *******************************************/
@Controller
@RequestMapping(value = "uploadRealTime")
public class UploadRealTimeController extends SecurityController {

	// 即時資料上傳至iot_proc目錄裡面的檔案結尾格式(_rt結尾)
	private final static String PROC_FILE_TYPE = ".wproc_rt";

	// 即時資料上傳至iot_raw目錄裡面的檔案結尾格式(_rt結尾)
	private final static String RAW_FILE_TYPE = ".rawTxt_rt";

	private final static String ROOT_PATH = System.getProperty("user.home");;
	private final static String PROC_DIR = ROOT_PATH + File.separator + "iot_proc";
	private final static String RAW_DIR = ROOT_PATH + File.separator + "iot_raw";

	// for 耐低溫探針
	final static double ratio[] = { 993.4780266, 991.4924763, 989.3961699, 987.1845375, 984.852528, 982.3954625,
			979.8083533, 977.0860578, 974.2236049, 971.2156786, 968.0574722, 964.7434841, 961.2687397, 957.628462,
			953.8173469, 949.8298868, 945.6622296, 941.3093229, 936.7664287, 932.0286333, 927.0927055, 921.9540993,
			916.6095666, 911.0542559, 905.2863734, 899.3029141, 893.1005778, 886.6793617, 880.0341919, 873.1677714,
			866.0776657, 858.764087, 851.2254842, 843.4672167, 835.4840111, 827.284987, 818.8676857, 810.239145,
			801.4009826, 792.3572366, 783.1155022, 773.6822137, 764.0553398, 754.2494666, 744.2720791, 734.1285172,
			723.8299818, 713.3769338, 702.7954831, 692.0928303, 681.2614386, 670.3233516, 659.3016597, 648.1926013,
			637.0007559, 625.7578657, 614.4655255, 603.1377255, 591.7865946, 580.421919, 569.0506487, 557.6976321,
			546.3435022, 535.0304651, 523.7801768, 512.5625812, 501.4177086, 490.3331249, 479.3481198, 468.4444444,
			457.6596427, 446.9685563, 436.4060366, 425.9739532, 415.6711222, 405.4931143, 395.4706604, 385.59601,
			375.8577125, 366.2827413, 356.8990228, 347.6459709, 338.5917001, 329.6685652, 320.9447305, 312.3947186,
			304.0393728, 295.8481121, 287.7860378, 279.976168, 272.2748495, 264.8066429, 257.4755595, 250.3523723,
			243.3336891, 236.5499846, 229.955335, 223.4371042, 217.1925622, 211.0435059, 205.0620601 };

	final static int ratioC[] = { -40, -39, -38, -37, -36, -35, -34, -33, -32, -31, -30, -29, -28, -27, -26, -25, -24,
			-23, -22, -21, -20, -19, -18, -17, -16, -15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
			30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
			57, 58, 59, 60 };

	@Autowired
	UploadDao uploadDao;

	@Autowired
	Gson gson;

	@RequestMapping()
	public String upload(ModelMap model) {
		return "upload/uploadRealTimeView";
	}

	// startdatetime:批次讀取啟動時間12碼(年月日時分秒)ex:161130192030
	// filekey:161121121212-161122130001-6097AE92AE93
	// (上傳資料的第一筆時間-上傳資料的最後一筆時間-mac值) 不是啟動Tag的時間!!!!
	// sded:161121121212-161130120001
	// macId:6097AE92AE93
	// dataTypes: 用逗號分開 代表每組訊號ex:0001,0101,0201代表三組訊號
	// 四碼說明
	// AABB :AA代表port號 BB代表資料類型
	// 0001 :本體，溫度
	// 0002 :本體，濕度
	// 0102 :port1，濕度
	// 0301 :port3，溫度

	// datas用#字號分開
	// datas=
	// fileKey#startdatetime#sded#macId#companyId#dataTypes#data1#data2#data3#data4#以此類推(後面的datax取決於dataTypes的個數)
	// 範例:

	// 會把Data拆開
	@RequestMapping(value = "/uploadBleData", method = RequestMethod.POST)
	public @ResponseBody Object uploadLogData(@RequestParam(value = "datas", defaultValue = "") String datas,
			ModelMap model) {

		dLog.debug("/uploadBleData  datas: " + datas);

		try {
			String[] ds = datas.split("#");
			String fileKey = ds[0];
			String[] sded = ds[2].split("-");
			String macId = ds[3];
			String companyId = ds[4];
			String[] dataTypes = ds[5].split(",");

			// 判斷日期
			if (sded[0].length() != 12 || sded[1].length() != 12) {
				throw new Exception("invalid date format.");
			}

			// 剩餘DATA(檢查使用)，有異常會丟出exception
			for (int i = 6; i < ds.length; i++) {
				String temp = ds[i];
			}

			FileWriteRunnable runnable = new FileWriteRunnable(ds, datas);
			Thread fileThread = new Thread(runnable);

			fileThread.start();
			nLog.info("OK, upload_rt ok, fileKey:" + fileKey);
		} catch (Exception e) {
			e.printStackTrace();
			eLog.error("NG: upload_rt fail:" + e.getMessage());
			return new RsMsg("error", e.getMessage());
		}

		return new RsMsg("success", 200);
	}

	@RequestMapping(value = "/uploadWifiData/{companyId}/{mac}/{dataType}/{data}/wifi", method = RequestMethod.GET)
	public @ResponseBody Object uploadWifiData(@PathVariable("companyId") String companyId,
			@PathVariable("mac") String mac, @PathVariable("dataType") String dataType,
			@PathVariable("data") String data, ModelMap model) {

		if (dataType.endsWith("01")) {// 溫度需進行轉換
			double raw = Double.parseDouble(data);
			if (raw <= ratio[ratio.length - 1]) {// 最小電阻值
				data = String.valueOf(ratioC[ratio.length - 1]);
			} else if (raw >= ratio[0]) {// 最大電壓值
				data = String.valueOf(ratioC[0]);
			} else {
				for (int i = 0; i < ratio.length - 1; i++) {
					if (raw >= ratio[i]) {
						Float rawf = new Float(raw);
						data = String.format("%.2f", ((rawf - ratio[i]) / (ratio[i + 1] - ratio[i]) + i) - 40);
						break;
					}
				}
			}
		}

		String time = getUpdateDate().substring(2);
		String datas = time + "-" + time + "-" + mac + "#" // filekey
				+ time + "#" // startdatetime
				+ time + "-" + time + "#" // sded
				+ mac + "#" + companyId + "#" + dataType + "#" + time + data;

		dLog.debug("/uploadWifiData  origin data: " + data + ",datas: " + datas);

		try {
			String[] ds = datas.split("#");

			// 剩餘DATA
			for (int i = 6; i < ds.length; i++) {
				String temp = ds[i];
			}

			FileWriteRunnable runnable = new FileWriteRunnable(ds, datas);
			Thread fileThread = new Thread(runnable);

			fileThread.start();

			nLog.info("OK, upload_rt ok, fileKey: " + time + "-" + time + "-" + mac);
		} catch (Exception e) {
			e.printStackTrace();
			eLog.error("NG: upload_rt fail:" + e.getMessage());
			return new RsMsg("error", e.getMessage());
		}

		return new RsMsg("success", 200);
	}

	class FileWriteRunnable implements Runnable {

		String[] ds;
		String datas;

		public FileWriteRunnable(String[] ds, String datas) {
			super();
			this.ds = ds;
			this.datas = datas;
		}

		@Override
		public void run() {
			try {
				String fileKey = ds[0];
				String startDatetime = ds[1];
				String[] sded = ds[2].split("-");
				String macId = ds[3];
				String companyId = ds[4];
				String[] dataTypes = ds[5].split(",");

//				String rawFilePath = RAW_DIR + File.separator + companyId + File.separator
//						+ startDatetime.substring(0, 4) + File.separator + startDatetime.substring(4, 6)
//						+ File.separator + fileKey + RAW_FILE_TYPE;
				File procFile = new File(PROC_DIR + File.separator + fileKey + PROC_FILE_TYPE);
				//File rawFile = new File(rawFilePath);

				// 判斷檔案是否已存在
				//if (rawFile.exists() && procFile.exists()) {
				if (procFile.exists()) {
					return;
				}
				FileUtils.write(procFile, datas); // 等待處裡Data
				//dLog.debug("write proc file: " + fileKey + PROC_FILE_TYPE);
				//FileUtils.write(rawFile, datas); // 整理完成Data
				//dLog.debug("write raw file: " + fileKey + RAW_FILE_TYPE);
			} catch (Exception e) {
				e.printStackTrace();
				eLog.error("write file fail. " + e.getMessage());
			}

		}

	}

}