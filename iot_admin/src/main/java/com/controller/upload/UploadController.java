package com.controller.upload;

import itri.group.param.RsMsg;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jodd.datetime.JDateTime;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.controller.upload.UploadRealTimeController.FileWriteRunnable;
import com.dao.job.UploadDao;
import com.google.gson.Gson;
import com.security.SecurityController;

@Controller
@RequestMapping(value = "upload")
public class UploadController extends SecurityController {

	@Autowired
	UploadDao uploadDao;

	@Autowired
	Gson gson;

	@RequestMapping()
	public String upload(ModelMap model) {
		return "upload/uploadView";
	}

	// startdatetime:批次讀取啟動時間12碼(年月日時分秒)ex:161130192030
	// filekey:161121121212-161122130001-6097AE92AE93
	// (上傳資料的第一筆時間-上傳資料的最後一筆時間-mac值) 不是啟動Tag的時間!!!!
	// sded:161121121212-161130120001
	// macId:6097AE92AE93
	// dataTypes: 用逗號分開 代表每組訊號ex:0001,0101,0201代表三組訊號
	// 四碼說明
	// AABB :AA代表port號 BB代表資料類型
	// 0001 :本體，溫度
	// 0002 :本體，濕度
	// 0102 :port1，濕度
	// 0301 :port3，溫度

	// datas用#字號分開
	// datas=
	// fileKey#startdatetime#sded#macId#companyId#dataTypes#data1#data2#data3#data4#以此類推(後面的datax取決於dataTypes的個數)
	// 範例:

	// 會把Data拆開
	@RequestMapping(value = "/uploadBleData", method = RequestMethod.POST)
	public @ResponseBody Object uploadLogData(@RequestParam(value = "datas", defaultValue = "") String datas,
			ModelMap model) {
		try {
			String[] ds = datas.split("#");
			String fileKey = ds[0];
			String startDatetime = ds[1];
			String[] sded = ds[2].split(",");
			String macId = ds[3];
			String companyId = ds[4];
			String[] dataTypes = ds[5].split(",");

			// 剩餘DATA
			for (int i = 6; i < ds.length; i++) {
				String temp = ds[i];
			}

			FileWriteRunnable runnable = new FileWriteRunnable(ds, datas);
			Thread fileThread = new Thread(runnable);

			fileThread.start();

			uploadDao.log(fileKey, "OK: upload ok: fileKey " + fileKey + "", getUpdateDate());
		} catch (Exception e) {
			e.printStackTrace();
			uploadDao.log("null", "NG: upload fail:" + e.getMessage(), getUpdateDate());
			return new RsMsg("error", e.getMessage());
		}
		return new RsMsg("success", 200);
	}

	// 解析資料是否正確
	@RequestMapping(value = "/parse", method = RequestMethod.GET)
	public @ResponseBody Object parse(@RequestParam(value = "datas", defaultValue = "") String datas, ModelMap model) {
		try {
			String[] ds = datas.split("#");
			String fileKey = ds[0];
			String startDatetime = ds[1];
			String[] sded = ds[2].split(",");
			String macId = ds[3];
			String companyId = ds[4];
			String[] dataTypes = ds[5].split(",");

			Map<String, String> rs = new HashMap<String, String>();
			rs.put("解析狀態", "正常");
			rs.put("啟動時間", startDatetime);
			rs.put("sded", ds[2]);
			rs.put("macId", macId);
			rs.put("companyId", companyId);
			rs.put("dataTypes", ds[5]);
			rs.put("dataTypesInfo", "共有" + dataTypes.length + "組資料");

			// 剩餘DATA
			for (int i = 6; i < ds.length; i++) {
				String temp = ds[i];
			}
			return new RsMsg("success", gson.toJson(rs));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("success", gson.toJson(e.toString()));
		}
	}

	// 取得測試資料
	@RequestMapping(value = "/getTestData", method = RequestMethod.GET)
	public @ResponseBody Object getTestData(@RequestParam(value = "companyId", defaultValue = "") String companyId,
			ModelMap model) {
		return new RsMsg("success", getTestData(companyId));
	}

	// 取得測試資料(static)
	private static String getTestData(String companyId) {
		String macId = "MACIDMACIDMA";
		JDateTime jd = new JDateTime();
		jd.subHour(48);
		Random r = new Random();
		int count = 10 + r.nextInt(30);// 資料筆數
		int dataType = 1 + r.nextInt(3);
		String[] str = { "0001", "0002", "0101", "0102" };
		StringBuilder[] sb = new StringBuilder[dataType];
		String firstDatetime = "", lastDateTime = "";
		for (int i = 0; i < dataType; i++) {
			sb[i] = new StringBuilder();
		}
		for (int j = 0; j < count; j++) {
			String ld = jd.toString("YYYYMMDDhhmmss").substring(2);
			if (j == 0)
				firstDatetime = ld;
			else if (j == count - 1)
				lastDateTime = ld;

			jd.addHour(5);

			for (int i = 0; i < dataType; i++) {
				int val = r.nextInt(20) - 10;
				sb[i].append("," + ld + val);
			}
		}
		for (int i = 0; i < dataType; i++) {
			sb[i].deleteCharAt(0);
		}

		StringBuilder dataTypes = new StringBuilder();
		for (int i = 0; i < dataType; i++) {
			dataTypes.append("," + str[i]);
		}
		dataTypes.deleteCharAt(0);
		String datas = String.format("%s-%s-%s#%s#%s-%s#%s#%s#%s", firstDatetime, lastDateTime, macId, firstDatetime,
				firstDatetime, lastDateTime, macId, companyId, dataTypes.toString());
		for (StringBuilder temp : sb) {
			datas += "#" + temp.toString();
		}
		return datas;
	}
	
	class FileWriteRunnable implements Runnable {

		String[] ds;
		String datas;

		public FileWriteRunnable(String[] ds, String datas) {
			super();
			this.ds = ds;
			this.datas = datas;
		}

		@Override
		public void run() {
			try {
				String fileKey = ds[0];
				String startDatetime = ds[1];
				String[] sded = ds[2].split("-");
				String macId = ds[3];
				String companyId = ds[4];
				String[] dataTypes = ds[5].split(",");

				String rootPath = System.getProperty("user.home");

				// raw dir
				String procDirPath = rootPath + File.separator + "iot_proc";
				File procDirFile = new File(procDirPath);
				if (!procDirFile.exists()) {
					procDirFile.mkdir();
				}
				File procFile = new File(procDirPath + File.separator + fileKey + ".wproc");

				// 檢查目錄
				String p = rootPath + File.separator + "iot_raw" + File.separator + companyId;
				File path = new File(p);
				if (!path.exists()) {
					path.mkdir();
				}

				// 年分目錄
				String startDate = startDatetime;
				p = p + File.separator + startDate.substring(0, 4);
				path = new File(p);
				if (!path.exists()) {
					path.mkdir();
				}
				// 月份目錄
				p = p + File.separator + startDate.substring(4, 6);
				path = new File(p);
				if (!path.exists()) {
					path.mkdir();
				}

				p = p + File.separator + fileKey + ".rawTxt";
				File rawFile = new File(p);

				FileUtils.write(procFile, datas); // 等待處裡Data
				FileUtils.write(rawFile, datas); // 整理完成Data

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

}