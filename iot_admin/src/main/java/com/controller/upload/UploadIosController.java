package com.controller.upload;

import itri.group.param.RsMsg;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.dao.tag.TagCodeDao;
import com.google.gson.Gson;
import com.security.SecurityController;

/******************************************
 * iOS資料上傳 iot_proc目錄:每30秒會讀取解析，最後將解析出來的data轉至iot_data目錄裡面 iot_raw目錄:上傳過來備份使用
 * iot_data目錄:解析完成後的Data，之後網頁呈現，要抓取的資料來源
 *
 * 套件:upload UploadIosController.java
 *
 * @author ATone create by 2017/07/28
 *******************************************/
@Controller
@RequestMapping(value = "uploadIos")
public class UploadIosController extends SecurityController {

	// 即時資料上傳至iot_proc目錄裡面的檔案結尾格式(_rt結尾)
	private final static String PROC_FILE_TYPE = ".wproc";

	// 即時資料上傳至iot_raw目錄裡面的檔案結尾格式(_rt結尾)
	private final static String RAW_FILE_TYPE = ".rawTxt";

	private final static String ROOT_PATH = System.getProperty("user.home");;
	private final static String PROC_DIR = ROOT_PATH + File.separator + "iot_proc";
	private final static String RAW_DIR = ROOT_PATH + File.separator + "iot_raw";

	@Autowired
	TagCodeDao tagCodeDao;

	@Autowired
	Gson gson;

	// startdatetime:批次讀取啟動時間12碼(年月日時分秒)ex:161130192030
	// filekey:161121121212-161122130001-A001
	// (上傳資料的第一筆時間-上傳資料的最後一筆時間-tag代碼) 不是啟動Tag的時間!!!!
	// sded:161121121212-161130120001
	// tagCode:A001
	// dataTypes: 用逗號分開 代表每組訊號ex:0001,0101,0201代表三組訊號
	// 四碼說明
	// AABB :AA代表port號 BB代表資料類型
	// 0001 :本體，溫度
	// 0002 :本體，濕度
	// 0102 :port1，濕度
	// 0301 :port3，溫度

	// datas用#字號分開
	// datas=
	// fileKey#startdatetime#sded#tagCode#companyId#dataTypes#data1#data2#data3#data4#以此類推(後面的datax取決於dataTypes的個數)
	// 範例:

	// 從iOS傳來之資料，改為Tag代碼
	@RequestMapping(value = "/uploadBleData", method = RequestMethod.POST)
	public @ResponseBody Object uploadLogDataFromIos(@RequestParam(value = "datas", defaultValue = "") String datas,
			ModelMap model) {
		// long st = System.nanoTime();
		try {
			String[] ds = datas.split("#");
			String[] sded = ds[2].split("-");
			String tagCode = ds[3];
			String macId = tagCodeDao.getTagMacByCode(tagCode);

			// 判斷mac
			if (macId.isEmpty()) {
				throw new Exception("tag not found.");
			} else {
				ds[3] = macId;
				ds[0] = sded + "-" + macId;
			}

			// 判斷日期
			if (sded[0].length() != 12 || sded[1].length() != 12) {
				throw new Exception("invalid date format.");
			}

			// 剩餘DATA(檢查使用)，有異常會丟出exception
			for (int i = 6; i < ds.length; i++) {
				String temp = ds[i];
			}

			FileWriteRunnable runnable = new FileWriteRunnable(ds);
			Thread fileThread = new Thread(runnable);

			fileThread.start();

		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
		return new RsMsg("success", 200);
	}

	class FileWriteRunnable implements Runnable {

		String[] ds;

		public FileWriteRunnable(String[] ds) {
			super();
			this.ds = ds;
		}

		@Override
		public void run() {
			try {
				String fileKey = ds[0];
				String startDatetime = ds[1];
				String[] sded = ds[2].split("-");
				String macId = ds[3];
				String companyId = ds[4];
				String[] dataTypes = ds[5].split(",");

				String rawFilePath = RAW_DIR + File.separator + companyId + File.separator
						+ startDatetime.substring(0, 4) + File.separator + startDatetime.substring(4, 6)
						+ File.separator + fileKey + RAW_FILE_TYPE;
				File procFile = new File(PROC_DIR + File.separator + fileKey + PROC_FILE_TYPE);
				File rawFile = new File(rawFilePath);

				StringBuilder stb = new StringBuilder();
				for (String s : ds) {
					stb.append("#" + s);
				}

				String datas = stb.deleteCharAt(0).toString();
				
				FileUtils.write(procFile, datas); // 等待處裡Data
				FileUtils.write(rawFile, datas); // 整理完成Data

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

}