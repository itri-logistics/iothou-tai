package com.controller.sensorPosition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.sensorPosition.SensorPositionDao;
import com.google.gson.Gson;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

import com.security.SecurityController;

/******************************************
 * 記錄Tag和其上之感測器在圖控的位置
 *
 * 套件:sensorPosition
 * SensorPositionController.java
 * SensorPositionDao.java
 * sensorPosition.js
 * sensorPositionView.jsp
 * graphic.js
 * graphicView.jsp
 *
 * @author ATone create by 2016/12/21
 *******************************************/
@Controller            
@RequestMapping(value="sensorPosition")
public class SensorPositionController extends SecurityController{

	@Autowired
	Gson gson;
	
	@Autowired
	SensorPositionDao sensorPositionDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "sensorPosition/sensorPositionView";
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String find(			
			@RequestParam(value="id",defaultValue="") String id,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="placeId",defaultValue="") String placeId,
			@RequestParam(value="mac",defaultValue="") String mac,
			@RequestParam(value="port",defaultValue="") String port,			
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	sensorPositionDao.find(id, companyId, placeId, mac, port)));
	}
	
	@RequestMapping(value="/update",method = RequestMethod.POST)
	public @ResponseBody Object update(
			@RequestParam(value="id",defaultValue="") String id,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="placeId",defaultValue="") String placeId,
			@RequestParam(value="mac",defaultValue="") String mac,
			@RequestParam(value="port",defaultValue="") String port,
			@RequestParam(value="x",defaultValue="") String x,
			@RequestParam(value="y",defaultValue="") String y,
			ModelMap model) {
		try {
			sensorPositionDao.update(id, companyId, placeId, mac, port, x, y, getUpdateDate(),getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}		
	
	@RequestMapping(value="/create",method = RequestMethod.POST)
	public @ResponseBody Object create(
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="placeId",defaultValue="") String placeId,
			@RequestParam(value="mac",defaultValue="") String mac,
			@RequestParam(value="port",defaultValue="") String port,
			@RequestParam(value="x",defaultValue="") String x,
			@RequestParam(value="y",defaultValue="") String y,
			ModelMap model) {
		try {
			sensorPositionDao.create(companyId, placeId, mac, port, x, y, getUpdateDate(),getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}	
	
	@RequestMapping(value="/createOrUpdate",method = RequestMethod.POST)
	public @ResponseBody Object createOrUpdate(
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="placeId",defaultValue="") String placeId,
			@RequestParam(value="mac",defaultValue="") String mac,
			@RequestParam(value="port",defaultValue="") String port,
			@RequestParam(value="x",defaultValue="") String x,
			@RequestParam(value="y",defaultValue="") String y,
			ModelMap model) {
		try {
			sensorPositionDao.createOrUpdate(companyId, placeId, mac, port, x, y, getUpdateDate(),getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
	@RequestMapping(value="/del",method = RequestMethod.POST)
	public @ResponseBody Object del(
			@RequestParam(value="id",defaultValue="") String id,			
			ModelMap model) {
		try {
			sensorPositionDao.del(id);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
	@RequestMapping(value="/delByMac",method = RequestMethod.POST)
	public @ResponseBody Object delByMac(
			@RequestParam(value="mac",defaultValue="") String mac,			
			ModelMap model) {
		try {
			sensorPositionDao.delByMac(mac);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
	@RequestMapping(value="/delByPlace",method = RequestMethod.POST)
	public @ResponseBody Object delByPlace(
			@RequestParam(value="placeId",defaultValue="") String placeId,			
			ModelMap model) {
		try {
			sensorPositionDao.delByPlace(placeId);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
}
