package com.controller.qualityManage;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/******************************************
 * 品質管理－車廂管理
 *
 * 套件: vehicleManage
 * VehicleManageController.java 
 * vehicleManage.js 
 * vehicleManageView.jsp
 *
 * @author ATone create by 2017/10/02
 *******************************************/
@Controller
@RequestMapping(value = "vehicleManage")
public class VehicleManageController {
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "qualityManage/vehicleManageView";
	}
}
