package com.controller.qualityManage;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/******************************************
 * 品質管理－車廂管理
 *
 * 套件: cartManage
 * CartManageController.java 
 * cartManage.js 
 * cartManageView.jsp
 *
 * @author ATone create by 2017/10/02
 *******************************************/
@Controller
@RequestMapping(value = "cartManage")
public class CartManageController {
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "qualityManage/cartManageView";
	}
}
