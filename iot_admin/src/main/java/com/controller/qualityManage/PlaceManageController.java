package com.controller.qualityManage;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/******************************************
 * 品質管理－場域管理
 *
 * 套件: qualityManage
 * PlaceManageController.java 
 * placeManage.js 
 * placeManageView.jsp
 *
 * @author ATone create by 2017/10/02
 *******************************************/
@Controller
@RequestMapping(value = "placeManage")
public class PlaceManageController {
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "qualityManage/placeManageView";
	}
}
