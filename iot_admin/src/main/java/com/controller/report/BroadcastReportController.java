package com.controller.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.report.BroadcastReportDao;
import com.dao.report.ReportDao;
import com.dao.upload.QueryDao;
import com.google.gson.Gson;
import com.security.SecurityController;
import com.utility.ChartUtility;
import com.utility.DataSizeLimitUtility;

import itri.group.param.RsMsg;

/******************************************
 * 即時廣播歷史報表
 *
 * 套件:realTimeReport 
 * BroadcastReportController.java 
 * BroadcastReportDao.java 
 * broadcast.js 
 * broadcastView.jsp
 *
 * @author ATone create by 2017/07/04
 *******************************************/

@Controller
@RequestMapping(value = "broadcastReport")
public class BroadcastReportController extends SecurityController {

	@Autowired
	BroadcastReportDao reportDao;
	
	@Autowired
	ChartUtility chartUtility;

	@Autowired
	Gson gson;

	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "record/broadcastReportView";
	}

	// 取得查詢時間區間內可用的Data（單位：小時）
	@RequestMapping(value = "/getAvailableDataInQueryInterval", method = RequestMethod.GET)
	public @ResponseBody String getAvailableDataInQueryInterval(
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "sd", defaultValue = "") String sd,
			@RequestParam(value = "ed", defaultValue = "") String ed, ModelMap model) {
		return gson.toJson(new RsMsg("success", reportDao.getAvailableDataInQueryInterval(companyId, sd, ed)));
	}

	// 一次取得一種類型的記錄資訊
	@RequestMapping(value = "/getDataInQueryIntervalByDataType", method = RequestMethod.GET)
	public @ResponseBody Object getDataInQueryIntervalByDataType(
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "sd", defaultValue = "") String sd,
			@RequestParam(value = "ed", defaultValue = "") String ed,
			@RequestParam(value = "datasStr", defaultValue = "") String datasStr,
			@RequestParam(value = "dataType", defaultValue = "") String dataType, ModelMap model) {

		if (companyId.isEmpty()) {
			companyId = getCompanyId();
		}

		try {			
			return new RsMsg("success",
					reportDao.getInfoInQueryIntervalByDataType(companyId, sd, ed, datasStr, dataType));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
	}

	// 下載圖片
	@RequestMapping(value = "/getChart", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getFile(@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "sd", defaultValue = "") String sd,
			@RequestParam(value = "ed", defaultValue = "") String ed,
			@RequestParam(value = "datasStr", defaultValue = "") String datasStr)
					throws FileNotFoundException, IOException {

		try {
			HashMap<String, String> dataInfoMap = reportDao.getDataInQueryIntervalByDataType(companyId, sd, ed,
					datasStr);
			HashMap<String, String> dataMap = new HashMap<>();
			float max = 100, min = 0;
			Iterator<String> k = dataInfoMap.keySet().iterator();
			while (k.hasNext()) {
				String key = k.next();
				if(key.equals("max")){
					max = Float.parseFloat(dataInfoMap.get(key));
				}else if(key.equals("min")){
					min = Float.parseFloat(dataInfoMap.get(key));
				}else{
					StringBuilder dataType = new StringBuilder();
					String[] keyArray = key.split("-");
					
					for(int i = 0;i<keyArray.length-1;i++)
						dataType.append(keyArray[i]+"-");
					
					if(keyArray[keyArray.length - 1].startsWith("00")){
						dataType.append("本體");
					}else{
						dataType.append("port"+keyArray[keyArray.length - 1].substring(1,2));
					}
					
					if(key.endsWith("01")){
						dataType.append("溫度");
					}else if(key.endsWith("02")){
						dataType.append("濕度");
					}
					
					dataMap.put(dataType.toString(), dataInfoMap.get(key));
				}
			}
			final HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentType(MediaType.IMAGE_JPEG);	
			
			File img = chartUtility.getChart(dataMap, companyId, min, max);
			FileInputStream fi = new FileInputStream(img);
			byte[] bs = IOUtils.toByteArray(fi);
			fi.close();
			return new ResponseEntity<byte[]>(bs, headers, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
