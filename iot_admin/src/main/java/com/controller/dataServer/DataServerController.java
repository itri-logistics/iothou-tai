package com.controller.dataServer;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.dao.dataServer.DataServerDao;
import com.utility.DataServerPool;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

/******************************************
 * Data Server 管理
 *
 * 套件:dataServer 
 * DataServerController.java 
 * dataServer.js 
 * dataServerView.jsp
 *
 * @author ATone create by 2017/08/07
 *******************************************/
@Controller
@RequestMapping(value = "dataServer")
public class DataServerController {

	@Autowired
	DataServerDao dataServerDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "dataServer/dataServerView";
	}
	
	@RequestMapping(value = "/getServerByCompanyId", method = RequestMethod.GET)
	public @ResponseBody Object getServerByCompanyId(
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			ModelMap model) {
		try {
			
			if(companyId.isEmpty()){
				return new RsMsg("error", "companyId should not be empty.");
			}
			
			String server = dataServerDao.getServer(companyId);
			
			if(server==null){
				return new RsMsg("error", "companyId isn't exist.");
			}
			
			return new RsMsg("success", server);
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
	}
	
	@RequestMapping(value = "/getNewestServerData", method = RequestMethod.GET)
	public @ResponseBody Object getNewestServerData(ModelMap model) {
		try {

			return new RsMsg("success", DataServerPool.getServerList());
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
	}

	@RequestMapping(value = "/updateServerData", method = RequestMethod.GET)
	public @ResponseBody Object getNewestServerData(@RequestParam(value = "server", defaultValue = "") String server,
			@RequestParam(value = "cpu", defaultValue = "") String cpu,
			@RequestParam(value = "mem", defaultValue = "") String mem,
			@RequestParam(value = "space", defaultValue = "") String space,
			@RequestParam(value = "status", defaultValue = "unknown") String status,
			@RequestParam(value = "name", defaultValue = "") String name,
			ModelMap model) {
		try {			
			if (!server.isEmpty())
				DataServerPool.updateServerTime(server, name, cpu, mem, space, status);
			return new RsMsg("success", 200);
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
	}

}
