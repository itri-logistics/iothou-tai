package com.controller.vehicle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.vehicle.VehicleDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

/******************************************
 * 車輛設定
 *
 * 套件:vehicle
 * VehicleController.java
 * VehicleDao.java
 * vehicle.js
 * vehicleView.jsp
 *
 * @author ATone create by 2017/02/03
 *******************************************/
@Controller            
@RequestMapping(value="vehicle")
public class VehicleController extends SecurityController{
	
	@Autowired
	Gson gson;	
	
	@Autowired
	VehicleDao vehicleDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "vehicle/vehicleView";
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String find(			
			@RequestParam(value="vehicleId",defaultValue="") String vehicleId,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="disName",defaultValue="") String disName,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	vehicleDao.find(vehicleId, companyId, disName,page)));
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody Object create(
			@RequestParam(value="vehicleId",defaultValue="") String vehicleId,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="disName",defaultValue="") String disName,
			ModelMap model) {
		try {
			vehicleDao.create(vehicleId, disName, companyId, getUpdateDate(), getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody Object update(@RequestParam(value="vehicleId",defaultValue="") String vehicleId,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="disName",defaultValue="") String disName,
			ModelMap model) {
		try {
			// 更新資訊
			vehicleDao.update(vehicleId, disName, companyId, getUpdateDate(), getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}

	}
	
	@RequestMapping(value="/del",method = RequestMethod.POST)
	public @ResponseBody Object del(
			@RequestParam(value="vehicleId",defaultValue="") String vehicleId,			
			ModelMap model) {
		try {
			vehicleDao.del(vehicleId);		
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}

}
