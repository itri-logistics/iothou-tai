package com.controller.vehicle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.dashboard.RealTimeDashboardDao;
import com.dao.vehicle.RealTimeVehicleDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.RsMsg;

/******************************************
 * 即時車輛位置
 *
 * 套件:realTimeVehicle 
 * RealTimeVehicleController.java 
 * RealTimeVehicleDao.java 
 * realTimeVehicle.js 
 * realTimeVehicleView.jsp
 *
 * @author ATone create by 2016/02/07
 *******************************************/
@Controller
@RequestMapping(value = "realTimeVehicle")
public class RealTimeVehicleController extends SecurityController {
	
	@Autowired
	Gson gson;

	@Autowired
	RealTimeVehicleDao realTimeVehicleDao;
	
	@RequestMapping()
	public String realTimeVehicle(ModelMap model) {
	    return "vehicle/realTimeVehicleView";
	}
	
	//一次取得一間公司所有車輛的位置
	@RequestMapping(value = "/getNewestLocationData", method = RequestMethod.GET)
	public @ResponseBody Object getNewestLocationData(ModelMap model) {
		try {
			return new RsMsg("success", realTimeVehicleDao.getNewestLocationData(getCompanyId()));
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
	}
}
