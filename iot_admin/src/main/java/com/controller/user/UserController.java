package com.controller.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.magiclen.magiccrypt.MagicCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.user.UserDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

/******************************************
 * 使用者管理（for 管理者新增、刪除使用者）
 *
 * 套件:user 
 * UserController.java 
 * UserDao.java 
 * userView.jsp
 * user.js
 *
 * @author ATone create by 2016/12/02
 *******************************************/
@Controller
@RequestMapping(value = "user")
public class UserController extends SecurityController{

	@Autowired
	Gson gson;	
	
	@Autowired
	UserDao userDao;
	
	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
	    return "user/userView";
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String find(			
			@RequestParam(value="userId",defaultValue="") String userId,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="companyDisName",defaultValue="") String companyDisName,
			@RequestParam(value="disName",defaultValue="") String disName,
			@RequestParam(value="type",defaultValue="") String type,
			@RequestParam(value="server",defaultValue="") String server,
			@RequestParam(value="page",defaultValue="") String page,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	userDao.find(userId, companyId, companyDisName, disName, type, server, page)));
	}
	
	@RequestMapping(value="/findPage",method = RequestMethod.GET)
	public @ResponseBody String findPage(
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="userId",defaultValue="") String userId,
			@RequestParam(value="companyDisName",defaultValue="") String companyDisName,
			@RequestParam(value="disName",defaultValue="") String disName,
			@RequestParam(value="type",defaultValue="") String type,
			@RequestParam(value="server",defaultValue="") String server,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	userDao.findPage(userId, companyId, companyDisName, disName, type, server)));
	}
    
	@RequestMapping(value="/update",method = RequestMethod.POST)
	public @ResponseBody Object update(
			@RequestParam(value="userId",defaultValue="") String userId,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="password",defaultValue="") String password,
			@RequestParam(value="companyDisName",defaultValue="") String companyDisName,
			@RequestParam(value="disName",defaultValue="") String disName,
			@RequestParam(value="type",defaultValue="") String type,
			@RequestParam(value="server",defaultValue="") String server,
			ModelMap model) {
		try {
			userDao.update(userId, companyId, password, companyDisName, disName, type, server, getUpdateDate(), getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}		
	
	@RequestMapping(value="/create",method = RequestMethod.POST)
	public @ResponseBody Object create(
			@RequestParam(value="userId",defaultValue="") String userId,
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="password",defaultValue="") String password,
			@RequestParam(value="companyDisName",defaultValue="") String companyDisName,
			@RequestParam(value="disName",defaultValue="") String disName,
			@RequestParam(value="type",defaultValue="") String type,
			@RequestParam(value="server",defaultValue="") String server,
			ModelMap model) {
		try {			
			userDao.create(userId, companyId, password, companyDisName, disName, type, server, getUpdateDate(), getUpdator());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}	
	
	@RequestMapping(value="/del",method = RequestMethod.POST)
	public @ResponseBody Object del(
			@RequestParam(value="userId",defaultValue="") String userId,
			@RequestParam(value="companyId",defaultValue="") String companyId,			
			ModelMap model) {
		try {
			userDao.del(userId, companyId);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
	@RequestMapping(value="/delCom",method = RequestMethod.POST)
	public @ResponseBody Object delCom(
			@RequestParam(value="companyId",defaultValue="") String companyId,			
			ModelMap model) {
		try {
			userDao.delCompany(companyId);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
}
