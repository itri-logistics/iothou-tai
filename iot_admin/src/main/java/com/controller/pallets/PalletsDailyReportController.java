package com.controller.pallets;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.pallets.palletsDailyReportDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

/******************************************
 * 警示設定
 *
 * 套件:alert 
 * PalletsDailyReportController.java 
 * PalletsDailyReportControllerDao.java 
 * palletsDailyReport.js
 * palletsDailyReportView.jsp
 *
 * @author WeiAn create by 2016/01/10
 *******************************************/
@Controller
@RequestMapping(value = "palletsDailyReport")
public class PalletsDailyReportController extends SecurityController {

	@Autowired
	Gson gson;

	@Autowired
	palletsDailyReportDao palletsDailyReportDao;

	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "pallets/palletsDailyReportView";
	}
	@RequestMapping(value = "/getTradingInQueryDateAndPlaceId", method = RequestMethod.GET)
	public @ResponseBody String getTradingInQueryDate(
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "ed", defaultValue = "") String ed, 
			@RequestParam(value = "placeId", defaultValue = "") int placeId,ModelMap model) {
		return gson.toJson(new RsMsg("success", palletsDailyReportDao.getTradingInQueryDateAndPlaceId(companyId,ed,placeId)));
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String find(
			@RequestParam(value="companyId",defaultValue="") String companyId,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	palletsDailyReportDao.find(companyId)));
	}
	
	@RequestMapping(value="/findType",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findType(
			@RequestParam(value="companyId",defaultValue="") String companyId,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	palletsDailyReportDao.getType(companyId)));
	}
	
	@RequestMapping(value="/addTrading",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody Object create(
			@RequestParam(value="companyId",defaultValue="") String companyId,
			@RequestParam(value="tagId",defaultValue="") String tagId,
			@RequestParam(value="inOut",defaultValue="") String inOut,
			@RequestParam(value="updateDate",defaultValue="") String updateDate,
			@RequestParam(value="updater",defaultValue="") String updater,
			@RequestParam(value="place",defaultValue="") String place,
			ModelMap model) {
			try {
				//palletsDailyReportDao.addTrading(companyId,tagId,inOut,updateDate,updater,place);
				return Pa.success;
			} catch (Exception e) {
				e.printStackTrace();
				return new RsMsg("error", e);
			}
	}
	
}