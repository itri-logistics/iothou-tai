package com.controller.pallets;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.pallets.PalletsTypeSettingControllerDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.Pa;
import itri.group.param.RsMsg;

/******************************************
 * 棧板類別設定
 *
 * 套件:pallets 
 * PalletsTypeSettingController.java 
 * PalletsTypeSettingControllerDao.java 
 * PalletsTypeSetting.js
 * PalletsTypeSettingControllerView.jsp
 *
 * @author WeiAn create by 2016/05/08
 *******************************************/
@Controller
@RequestMapping(value = "palletsTypeSetting")
public class PalletsTypeSettingController extends SecurityController {

	@Autowired
	Gson gson;

	@Autowired
	PalletsTypeSettingControllerDao palletsTypeDao;

	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "pallets/palletsTypeSettingView";
	}


//根據公司帳號列出已建立的TAG Type
	@RequestMapping(value = "/queryPalletsTypeSetting", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findPalletsTypeSettingCompanyId(
		ModelMap model) {
		return gson.toJson(new RsMsg("success", palletsTypeDao.findPalletsTypeSettingByCompanyId(getCompanyId())));
	}

	// 更新Pallets Type 設定
	@RequestMapping(value = "/updatePalletsTypeSetting", method = RequestMethod.POST)
	public @ResponseBody Object PalletsTypeSettong(
			@RequestParam(value = "id", defaultValue = "") String id,
			@RequestParam(value = "name", defaultValue = "") String name,
			@RequestParam(value = "color", defaultValue = "") String color,			
			@RequestParam(value = "customer", defaultValue = "") String customer,
			@RequestParam(value = "meterial", defaultValue = "") String meterial,
			@RequestParam(value = "property", defaultValue = "") String property,
			@RequestParam(value = "format", defaultValue = "") String format,
			@RequestParam(value = "type", defaultValue = "") String type,
			ModelMap model) {
		try {
			palletsTypeDao.update(id, name, color, customer, meterial, property, format, type ,getCompanyId());
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
	//新增Pallets Type 設定
	@RequestMapping(value = "/createPalletsTypeSetting", method = RequestMethod.POST)
	public @ResponseBody Object create(
			@RequestParam(value = "name", defaultValue = "") String name,
			@RequestParam(value = "color", defaultValue = "") String color,			
			@RequestParam(value = "customer", defaultValue = "") String customer,
			@RequestParam(value = "meterial", defaultValue = "") String meterial,
			@RequestParam(value = "property", defaultValue = "") String property,
			@RequestParam(value = "format", defaultValue = "") String format,
			@RequestParam(value = "type", defaultValue = "") String type,
			ModelMap model) {
		try {
			palletsTypeDao.createPalletsType(getCompanyId(), name, color, customer, meterial, property, format, type);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
	//刪除Pallets Type 設定
	@RequestMapping(value = "/delPalletsTypeSetting", method = RequestMethod.POST)
	public @ResponseBody Object delSetting(
			@RequestParam(value = "id", defaultValue = "") String id, ModelMap model) {
		try {			
			palletsTypeDao.delPalletsTypeSetting(id);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	@RequestMapping(value="/findParam",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findParam(
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	palletsTypeDao.findParam()));
	}
	@RequestMapping(value="/findType",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findType(
			@RequestParam(value="formName",defaultValue="") String formName,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	palletsTypeDao.findType(formName)));
	}
	
	@RequestMapping(value="/findProperty",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findProperty(
			@RequestParam(value="formName",defaultValue="") String formName,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	palletsTypeDao.findProperty(formName)));
	}
	
	@RequestMapping(value="/findFormat",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findFormat(
			@RequestParam(value="formName",defaultValue="") String formName,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	palletsTypeDao.findFormat(formName)));
	}

}
