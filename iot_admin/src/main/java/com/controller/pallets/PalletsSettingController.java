package com.controller.pallets;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.pallets.PalletsSettingDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.Pa;
import itri.group.param.RsMsg;


/******************************************
 * 警示設定
 *
 * 套件:pallets 
 * palletsSettingViewController.java 
 * palletsSettingControllerDao.java 
 * palletsSetting.js
 * palletsSettingView.jsp
 *
 * @author WeiAn create by 2016/05/10
 *******************************************/
@Controller
@RequestMapping(value = "palletsSetting")
public class PalletsSettingController extends SecurityController {

	@Autowired
	Gson gson;

	@Autowired
	PalletsSettingDao palletsSettingDao;

	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "pallets/palletsSettingView";
	}
	
	@RequestMapping(value = "/setPalletsTagTypeSetting", method = RequestMethod.POST)
	public @ResponseBody Object create(
			@RequestParam(value = "palletsType", defaultValue = "") int palletsType,
			@RequestParam(value = "palletsTags", defaultValue = "") String palletsTags,
			@RequestParam(value = "companyId", defaultValue = "") String companyId,	
			ModelMap model) {
		try {
			String[] tagIdArray = palletsTags.split(";");
			for(String tagId:tagIdArray){
				palletsSettingDao.setPalletsTagType(getCompanyId(), palletsType, tagId);	
			}
			//palletsSettingDao.setPalletsTagType(getCompanyId(), palletsType, palletsTags);
			return Pa.success;
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e);
		}
	}
	
	@RequestMapping(value="/findPalletsType",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String findPalletsType(
			@RequestParam(value = "companyId", defaultValue = "") String companyId,	
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	palletsSettingDao.findPalletsType(companyId)));
	}
	
}