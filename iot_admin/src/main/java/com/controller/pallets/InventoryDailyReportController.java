package com.controller.pallets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.pallets.InventoryDailyReportDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.RsMsg;

/******************************************
 * 資材管理系統
 *
 * 套件:pallets 
 * InventoryDailyReportController.java 
 * InventoryDailyReportControllerDao.java 
 * inventoryDailyReport.js
 * inventoryDailyReportView.jsp
 *
 * @author WeiAn create by 2016/05/10
 *******************************************/
@Controller
@RequestMapping(value = "inventoryDailyReport")
public class InventoryDailyReportController extends SecurityController {

	@Autowired
	Gson gson;

	@Autowired
	InventoryDailyReportDao dailyReportDao;

	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "pallets/inventoryDailyReportView";
	}
	
	// 取得查詢時間內有紀錄的Tag
	@RequestMapping(value = "/getTagsInQueryInterval", method = RequestMethod.GET)
	public @ResponseBody String getInventoryInQueryDate(
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "ed", defaultValue = "") String ed, ModelMap model) {
		return gson.toJson(new RsMsg("success", dailyReportDao.getInventoryInQueryDate(companyId,ed)));
	}
	
	// 取得類型
	@RequestMapping(value="/findType",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String getType(
			@RequestParam(value = "companyId", defaultValue = "") String companyId, ModelMap model) {
		return gson.toJson(new RsMsg("success", dailyReportDao.getType(companyId)));

	}
	
	// 取得地點
	@RequestMapping(value = "/findPlace", method = RequestMethod.GET)
	public @ResponseBody String getPlace(
			@RequestParam(value = "companyId", defaultValue = "") String companyId, ModelMap model) {
		return gson.toJson(new RsMsg("success", dailyReportDao.getPlace(companyId)));
	}
}