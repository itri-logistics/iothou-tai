package com.controller.pallets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dao.pallets.noIdentityPalletsDailyReportDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.RsMsg;

/******************************************
 * 無身份日結查詢
 *
 * 套件:pallets 
 * NoIdentityPalletsDailyReportController.java 
 * noIdentityPalletsDailyReportDao.java 
 * noIdentityPalletsDailyReport.js
 * noIdentityPalletsDailyReportView.jsp
 *
 * @author WeiAn create by 2016/05/10
 *******************************************/
@Controller
@RequestMapping(value = "noIdentityPalletsDailyReport")
public class NoIdentityPalletsDailyReportController extends SecurityController {

	@Autowired
	Gson gson;

	@Autowired
	noIdentityPalletsDailyReportDao noIdentityPalletsDao;

	@RequestMapping()
	public String shipDocAssign(ModelMap model) {
		return "pallets/noIdentityPalletsDailyReportView";
	}
	
	@RequestMapping(value = "/getTradingInQueryDateAndPlaceId", method = RequestMethod.GET)
	public @ResponseBody String getTradingInQueryDate(
			@RequestParam(value = "companyId", defaultValue = "") String companyId,
			@RequestParam(value = "ed", defaultValue = "") String ed, 
			@RequestParam(value = "placeId", defaultValue = "") int placeId,ModelMap model) {
		return gson.toJson(new RsMsg("success", noIdentityPalletsDao.getTradingInQueryDateAndPlaceId(companyId,ed,placeId)));
	}
	
	@RequestMapping(value="/find",method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody String find(
			@RequestParam(value="companyId",defaultValue="") String companyId,
			ModelMap model) {
		return gson.toJson(new RsMsg("success",	noIdentityPalletsDao.find(companyId)));
	}
}