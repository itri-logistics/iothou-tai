package com.bean;

import itri.group.param.LoggerPa;
import itri.group.param.Pa;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.security.SecurityController;
import com.utility.RealTimeDataPool;
import com.utility.SendEmailJob;

public class SysListener extends HttpServlet implements ServletContextListener {

	@Autowired
	JdbcTemplate jdbcTemplate;

	private static final long serialVersionUID = 1L;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		jdbcTemplate = new JdbcTemplate(new ConnoDataSource().helloWorld());
		List<Map<String, Object>> tags = jdbcTemplate.queryForList("select * from tag");
		List<Map<String, Object>> alerts = jdbcTemplate.queryForList("select * from alert_setting");
		List<Map<String, Object>> alert_rels = jdbcTemplate.queryForList("select * from tag_datatype_alert_rel");

		// check file dir
		// final String rootPath="";
		final String rootPath = System.getProperty("user.home");
		String[] ps = { "iot_errorData", "iot_data", "iot_proc", "iot_raw" };
		for (String s : ps) {
			File f = new File(rootPath + File.separator + s);
			if (!f.exists()) {
				f.mkdir();
			}
		}

		SecurityController.eLog = LogManager.getLogger(LoggerPa.LOGGER_ERROR);
		SecurityController.nLog = LogManager.getLogger(LoggerPa.LOGGER_NET_ERR);
		SecurityController.dLog = LogManager.getLogger(LoggerPa.LOGGER_DEBUG);

		sce.getServletContext().setAttribute("js_version", Pa.JS_VERSION);
		sce.getServletContext().setAttribute("lib_link", Pa.CDN_ADMIN_LIB_LINK);

		//RealTimeDataPool.initRealTimePool(tags, alerts, alert_rels);
		//SendEmailJob.init();
	}

}