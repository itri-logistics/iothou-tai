package com.bean;

import java.security.Security;
import java.util.Arrays;
import java.util.Base64;
import java.security.Key;
import java.security.MessageDigest;
import java.security.Provider;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * 採用PKCS7Padding的AES26 配合iOS登入使用
 * 
 * 使用前須先確保已下載JCE並將Server的jdk內相對應檔案替換。
 * 
 * by ATone 2017/11/03
 * 
 */
public class Aes256Encryption {

	public static final String KEY_ALGORITHM = "AES";
	public static final String CIPHER_ALGORITHM = "AES/ECB/PKCS7Padding";

	private Key key;
	final static Provider securityProvider = new org.bouncycastle.jce.provider.BouncyCastleProvider();

	public Aes256Encryption(String keyString) throws Exception {
		super();
		Security.addProvider(securityProvider);
		byte[] keyByte = initkey(keyString);
		this.key = toKey(keyByte);
	}
	
	public Aes256Encryption(byte[] keyByte){
		super();
		Security.addProvider(securityProvider);
		try {
			this.key = toKey(keyByte);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setKey(String keyString) throws Exception {
		byte[] keyByte = initkey(keyString);
		this.key = toKey(keyByte);
	}
	
	public void setKey(byte[] keyByte) throws Exception {

		this.key = toKey(keyByte);
	}

	// 產生密鑰
	public byte[] initkey(String keyStr) throws Exception {

		byte[] keyByte = (keyStr).getBytes("UTF-8");
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		sha.update(keyByte);
		
		return sha.digest();

	}

	// 將二進位編碼形式的密鑰轉換為轉換Key物件
	public Key toKey(byte[] key) throws Exception {

		SecretKey secretKey = new SecretKeySpec(key, KEY_ALGORITHM);

		return secretKey;
	}

	public String encrypt(String dataStr) throws Exception {
		
		byte[] data = dataStr.getBytes();
	
		Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, securityProvider);

		cipher.init(Cipher.ENCRYPT_MODE, key);

		return new String(Base64.getEncoder().encode(cipher.doFinal(data)), "UTF-8");
	}

	public String decrypt(String dataStr) throws Exception {

		byte[] data = Base64.getDecoder().decode(dataStr);
		
		Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, securityProvider);
		cipher.init(Cipher.DECRYPT_MODE, key);

		return new String(cipher.doFinal(data));
	}

}
