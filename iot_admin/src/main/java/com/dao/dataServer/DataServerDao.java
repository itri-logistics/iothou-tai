package com.dao.dataServer;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class DataServerDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public String getServer(String companyId) {
		String sql=String.format("select server from user where companyId='%s';", companyId);

		List<Map<String, Object>> qMap = null;
		try {			
			qMap = jdbcTemplate.queryForList(sql);
		} catch (Exception e) {			
			e.printStackTrace();
		}		

		return qMap.isEmpty() ? null : qMap.get(0).get("name").toString();
		
	}
	
}
