package com.dao.report;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.dao.tag.TagDao;

import jodd.datetime.JDateTime;

@Service
public class ReportDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	TagDao tagDao;

	// 取得查詢時間區間內可用的Data（單位：小時）
	public Object getAvailableDataInQueryInterval(String companyId, String sd, String ed) {

		TreeSet<String> qMonth = new TreeSet();// 要查詢的月份
		Set<String> qDay = new HashSet(); // 要查詢的日期
		if (Long.parseLong(sd) > Long.parseLong(ed))
			return "";
		JDateTime sdd = new JDateTime();
		sdd.parse(sd, "YYYYMMDDhh");
		String tempDate = sdd.toString("YYYYMMDD");

		if (sd.equals(ed)) {
			JDateTime edd = new JDateTime();
			edd.parse(ed, "YYYYMMDDhh");
			edd.addHour(1);
			ed = edd.toString("YYYYMMDDhh");
		}

		JDateTime edd = new JDateTime();
		edd.parse(ed, "YYYYMMDDhh");
		String endDate = edd.toString("YYYYMMDD");
		qMonth.add(tempDate.substring(2, 6));
		qDay.add(tempDate.substring(2));
		while (!tempDate.equals(endDate)) {
			sdd.addDay(1);
			tempDate = sdd.toString("YYYYMMDD");
			qMonth.add(tempDate.substring(2, 6));
			qDay.add(tempDate.substring(2));
		}

		final String rootPath = System.getProperty("user.home");
		final String dataDirPath = rootPath + File.separator + "iot_data" + File.separator + companyId;

		HashMap<String, HashMap<String, String>> datasMap = new HashMap<String, HashMap<String, String>>();

		long sdl = Long.parseLong(sd.substring(2));
		long edl = Long.parseLong(ed.substring(2));

		for (String temp : qMonth) {
			String y = temp.substring(0, 2);
			String m = temp.substring(2, 4);
			File f = new File(dataDirPath + File.separator + y + File.separator + m);
			File[] fs = f.listFiles();
			if (fs != null) {
				for (File tempf : fs) {
					String fileName = tempf.getName();
					String fileDay = fileName.substring(13, 15);
					String fileMac = fileName.substring(0, 12);
					String fileType = fileName.substring(16, 20);
					String fileKey = fileName.substring(0, 12) + "-" + fileName.substring(16, 20);
					if (qDay.contains(temp + fileDay) && fileName.endsWith(".txt")) {
						String raw = "";
						try {
							raw = FileUtils.readFileToString(tempf);
						} catch (IOException e) {
							e.printStackTrace();
						}

						if (raw.length() > 0) {
							String[] rawArray = raw.split(",");
							// 檢查檔案內的紀錄是否在查詢區間內
							for (String r : rawArray) {
								long rl = Long.parseLong(r.substring(0, 8));
								float val = Float.parseFloat(r.substring(12));
								if (rl >= sdl && rl < edl && val > -35) {// 若有記錄在查詢區間內，則加入列表
									String time = r.substring(0, 12);

									if (!datasMap.containsKey(fileKey)) {
										datasMap.put(fileKey, new HashMap<String, String>());
										datasMap.get(fileKey).put("mac", fileMac);
										datasMap.get(fileKey).put("dataType", fileType);
										datasMap.get(fileKey).put("sd", time);
										datasMap.get(fileKey).put("ed", time);
									}

									if (Long.parseLong(time) < Long.parseLong(datasMap.get(fileKey).get("sd"))) {
										datasMap.get(fileKey).put("sd", time);
									}

									if (Long.parseLong(time) > Long.parseLong(datasMap.get(fileKey).get("ed"))) {
										datasMap.get(fileKey).put("ed", time);
									}

								}
							}

						}
					}

				}
			}
		}

		List<HashMap<String, String>> datasList = new ArrayList<HashMap<String, String>>();
		Iterator<String> k = datasMap.keySet().iterator();

		while (k.hasNext()) {
			String key = k.next();
			String mac = datasMap.get(key).get("mac");
			StringBuilder dataTypeName = new StringBuilder(tagDao.getTagName(mac) + "-");
			HashMap<String, String> dataMap = new HashMap<String, String>();

			String rsd = datasMap.get(key).get("sd");
			String red = datasMap.get(key).get("ed");

			rsd = "20" + rsd.substring(0, 2) + "/" + rsd.substring(2, 4) + "/" + rsd.substring(4, 6);
			red = "20" + red.substring(0, 2) + "/" + red.substring(2, 4) + "/" + red.substring(4, 6);

			if (datasMap.get(key).get("dataType").substring(1, 2).equals("0")) {
				dataTypeName.append("本體");
			} else {
				dataTypeName.append("port" + datasMap.get(key).get("dataType").substring(1, 2));
			}

			if (datasMap.get(key).get("dataType").substring(2, 4).equals("01")) {
				dataTypeName.append("溫度");
			} else if (datasMap.get(key).get("dataType").substring(2, 4).equals("02")) {
				dataTypeName.append("濕度");
			}

			dataMap.put("key", key);
			dataMap.put("mac", mac);
			dataMap.put("dataType", datasMap.get(key).get("dataType"));
			dataMap.put("name", dataTypeName.toString());
			dataMap.put("sd", rsd);
			dataMap.put("ed", red);

			datasList.add(dataMap);
		}

		return datasList;
	}

	// 取得特定dataType和Tag在查詢時間區間內的Data資訊
	public Object getInfoInQueryIntervalByDataType(String companyId, String sd, String ed, String datasStr,
			String dataType) {
		HashSet<String> datas = new HashSet<String>(Arrays.asList((datasStr.split(","))));// 要查詢的Tag
		TreeSet<String> qMonth = new TreeSet();// 要查詢的月份
		Set<String> qDay = new HashSet(); // 要查詢的日期
		if (Long.parseLong(sd) > Long.parseLong(ed))
			return "";
		JDateTime sdd = new JDateTime();
		sdd.parse(sd, "YYYYMMDDhh");
		String tempDate = sdd.toString("YYYYMMDD");

		if (sd.equals(ed)) {
			JDateTime edd = new JDateTime();
			edd.parse(ed, "YYYYMMDDhh");
			edd.addHour(1);
			ed = edd.toString("YYYYMMDDhh");
		}

		JDateTime edd = new JDateTime();
		edd.parse(ed, "YYYYMMDDhh");
		String endDate = edd.toString("YYYYMMDD");
		qMonth.add(tempDate.substring(2, 6));
		qDay.add(tempDate.substring(2));
		while (!tempDate.equals(endDate)) {
			sdd.addDay(1);
			tempDate = sdd.toString("YYYYMMDD");
			qMonth.add(tempDate.substring(2, 6));
			qDay.add(tempDate.substring(2));
		}

		final String rootPath = System.getProperty("user.home");
		final String dataDirPath = rootPath + File.separator + "iot_data" + File.separator + companyId;

		HashMap<String, Double> dataValueMap = new HashMap<String, Double>();
		HashMap<String, String> dataInfoMap = new HashMap<String, String>();

		long sdl = Long.parseLong(sd.substring(2));
		long edl = Long.parseLong(ed.substring(2));

		for (String temp : qMonth) {
			String y = temp.substring(0, 2);
			String m = temp.substring(2, 4);
			File f = new File(dataDirPath + File.separator + y + File.separator + m);
			File[] fs = f.listFiles();
			if (fs != null) {
				for (File tempf : fs) {
					String fileName = tempf.getName();
					String fileDay = fileName.substring(13, 15);
					String fileMac = fileName.substring(0, 12);
					String fileType = fileName.substring(16, 20);					
					
					if (datas.contains(fileMac + "-" + fileType)) {
						if (qDay.contains(temp + fileDay) && fileName.endsWith(".txt")) {
							String date = y + m + fileDay;
							String raw = "";
							try {
								raw = FileUtils.readFileToString(tempf);
							} catch (IOException e) {
								e.printStackTrace();
							}

							if (raw.length() > 0) {
								String[] rawArray = raw.split(",");
								// 檢查檔案內的紀錄是否在查詢區間內
								for (String r : rawArray) {
									long rl = Long.parseLong(r.substring(0, 8));
									if (rl >= sdl && rl < edl) {// 若有記錄在查詢區間內，則加入計算
										double val = Double.parseDouble(r.substring(12));
										String time = r.substring(0, 12);
										if (val > -35) {
											if (!dataValueMap.containsKey("max")) {
												dataValueMap.put("max", val);
												dataInfoMap.put("maxTime", r.substring(0, 12));
												dataInfoMap.put("maxTag", fileMac);
												dataInfoMap.put("maxType", fileType);
												dataValueMap.put("min", val);
												dataInfoMap.put("minTime", r.substring(0, 12));
												dataInfoMap.put("minTag", fileMac);
												dataInfoMap.put("minType", fileType);
												dataValueMap.put("sum", 0.0);
												dataValueMap.put("count", 0.0);
												dataInfoMap.put("sd", time);
												dataInfoMap.put("ed", time);
											}

											double sum = dataValueMap.get("sum") + val;
											double count = dataValueMap.get("count") + 1;

											if (val > dataValueMap.get("max")) {
												dataValueMap.put("max", val);
												dataInfoMap.put("maxTime", r.substring(0, 12));
												dataInfoMap.put("maxTag", fileMac);
												dataInfoMap.put("maxType", fileType);
											}

											if (val < dataValueMap.get("min")) {
												dataValueMap.put("min", val);
												dataInfoMap.put("minTime", r.substring(0, 12));
												dataInfoMap.put("minTag", fileMac);
												dataInfoMap.put("minType", fileType);
											}

											if (Long.parseLong(time) < Long.parseLong(dataInfoMap.get("sd"))) {
												dataInfoMap.put("sd", time);
											}

											if (Long.parseLong(time) > Long.parseLong(dataInfoMap.get("ed"))) {
												dataInfoMap.put("ed", time);
											}

											dataValueMap.put("sum", sum);
											dataValueMap.put("count", count);
										}

									}
								}

							}
						}
					}
				}
			}
		}

		HashMap<String, String> dataMap = new HashMap<String, String>();
		StringBuilder dataTypeName = new StringBuilder();
		String unit = "";
		if (dataType.equals("01")) {
			dataTypeName.append("溫度");
			unit = "℃";
		} else if (dataType.equals("02")) {
			dataTypeName.append("濕度");
			unit = "%";
		}

		dataMap.put("dataType", dataType);
		dataMap.put("dataTypeName", dataTypeName.toString());
		dataMap.put("unit", unit);
		dataMap.put("max", String.format("%.2f", dataValueMap.get("max")));
		dataMap.put("min", String.format("%.2f", dataValueMap.get("min")));
		dataMap.put("avg", String.format("%.2f", dataValueMap.get("sum") / dataValueMap.get("count")));
		dataMap.put("count", String.format("%d", dataValueMap.get("count").intValue()));

		String t = dataInfoMap.get("maxTime");
		dataMap.put("maxTime", "20" + t.substring(0, 2) + "/" + t.substring(2, 4) + "/" + t.substring(4, 6) + " "
				+ t.substring(6, 8) + ":" + t.substring(8, 10) + ":" + t.substring(10));
		t = dataInfoMap.get("minTime");
		dataMap.put("minTime", "20" + t.substring(0, 2) + "/" + t.substring(2, 4) + "/" + t.substring(4, 6) + " "
				+ t.substring(6, 8) + ":" + t.substring(8, 10) + ":" + t.substring(10));

		t = dataInfoMap.get("sd");
		dataMap.put("sd", "20" + t.substring(0, 2) + "/" + t.substring(2, 4) + "/" + t.substring(4, 6) + " "
				+ t.substring(6, 8) + ":" + t.substring(8, 10) + ":" + t.substring(10));

		t = dataInfoMap.get("ed");
		dataMap.put("ed", "20" + t.substring(0, 2) + "/" + t.substring(2, 4) + "/" + t.substring(4, 6) + " "
				+ t.substring(6, 8) + ":" + t.substring(8, 10) + ":" + t.substring(10));

		String maxType = dataInfoMap.get("maxType");
		String maxTypeName;
		if (maxType.startsWith("01")) {
			maxTypeName = "port1";
		} else if (maxType.startsWith("02")) {
			maxTypeName = "port2";
		} else if (maxType.startsWith("03")) {
			maxTypeName = "port3";
		} else if (maxType.startsWith("04")) {
			maxTypeName = "port4";
		} else {
			maxTypeName = "本體";
		}

		if (maxType.endsWith("01")) {
			maxTypeName += "溫度";
		} else if (maxType.endsWith("02")) {
			maxTypeName += "濕度";
		} else if (maxType.endsWith("05")) {
			maxTypeName += "光度";
		} else if (maxType.endsWith("06")) {
			maxTypeName += "電流";
		} else if (maxType.endsWith("07")) {
			maxTypeName += "電量";
		}

		String minType = dataInfoMap.get("minType");
		String minTypeName;
		if (minType.startsWith("01")) {
			minTypeName = "port1";
		} else if (minType.startsWith("02")) {
			minTypeName = "port2";
		} else if (minType.startsWith("03")) {
			minTypeName = "port3";
		} else if (minType.startsWith("04")) {
			minTypeName = "port4";
		} else {
			minTypeName = "本體";
		}

		if (minType.endsWith("01")) {
			minTypeName += "溫度";
		} else if (minType.endsWith("02")) {
			minTypeName += "濕度";
		} else if (minType.endsWith("05")) {
			minTypeName += "光度";
		} else if (minType.endsWith("06")) {
			minTypeName += "電流";
		} else if (minType.endsWith("07")) {
			minTypeName += "電量";
		}

		dataMap.put("maxTag", dataInfoMap.get("maxTag"));
		dataMap.put("maxType", maxType);
		dataMap.put("maxTagName", tagDao.getTagName(dataInfoMap.get("maxTag")));
		dataMap.put("maxTypeName", maxTypeName);
		dataMap.put("minTag", dataInfoMap.get("minTag"));
		dataMap.put("minType", minType);
		dataMap.put("minTagName", tagDao.getTagName(dataInfoMap.get("minTag")));
		dataMap.put("minTypeName", minTypeName);

		return dataMap;
	}

	// 一次取得指定Tag-資料類型裡面的資料
	// input : dataStr - TagMac-0001,TagMac-0101,...
	public HashMap<String, String> getDataInQueryIntervalByDataType(String companyId, String sd, String ed,
			String datasStr) {

		TreeSet<String> qMonth = new TreeSet();// 要查詢的月份
		Set<String> qDay = new HashSet(); // 要查詢的日期
		if (Long.parseLong(sd) > Long.parseLong(ed))
			return null;
		JDateTime sdd = new JDateTime();
		sdd.parse(sd, "YYYYMMDDhh");
		String tempDate = sdd.toString("YYYYMMDD");

		if (sd.equals(ed)) {
			JDateTime edd = new JDateTime();
			edd.parse(ed, "YYYYMMDDhh");
			edd.addHour(1);
			ed = edd.toString("YYYYMMDDhh");
		}

		JDateTime edd = new JDateTime();
		edd.parse(ed, "YYYYMMDDhh");
		String endDate = edd.toString("YYYYMMDD");
		qMonth.add(tempDate.substring(2, 6));
		qDay.add(tempDate.substring(2));
		while (!tempDate.equals(endDate)) {
			sdd.addDay(1);
			tempDate = sdd.toString("YYYYMMDD");
			qMonth.add(tempDate.substring(2, 6));
			qDay.add(tempDate.substring(2));
		}

		long sdl = Long.parseLong(sd.substring(2));
		long edl = Long.parseLong(ed.substring(2));

		// 先將資料根據資料類型分類
		String[] dataArray = datasStr.split(",");

		HashMap<String, StringBuilder> dataMap = new HashMap<>();
		HashMap<String, String> resultMap = new HashMap<>();

		float max = -9999, min = 9999;

		// 讀檔
		final String rootPath = System.getProperty("user.home");
		final String dataDirPath = rootPath + File.separator + "iot_data" + File.separator + companyId;

		for (String keyStr : dataArray) {
			StringBuilder dataStringBuilder = new StringBuilder();

			for (String temp : qMonth) {
				String y = temp.substring(0, 2);
				String m = temp.substring(2, 4);
				File f = new File(dataDirPath + File.separator + y + File.separator + m);
				File[] fs = f.listFiles();
				if (fs != null) {
					for (File tempf : fs) {
						String fileName = tempf.getName();
						String fileDay = fileName.substring(13, 15);
						String fileMac = fileName.substring(0, 12);
						String fileType = fileName.substring(16, 20);
						if (keyStr.equals(fileMac + "-" + fileType)) {

							if (qDay.contains(temp + fileDay) && fileName.endsWith(".txt")) {
								String date = y + m + fileDay;
								String raw = "";
								try {
									raw = FileUtils.readFileToString(tempf);
								} catch (IOException e) {
									e.printStackTrace();
								}

								if (raw.length() > 0) {
									String[] rawArray = raw.split(",");
									// 檢查檔案內的紀錄是否在查詢區間內
									for (String r : rawArray) {
										long rl = Long.parseLong(r.substring(0, 8));
										float val = Float.parseFloat(r.substring(12));
										if (val > -35) {
											if (rl >= sdl && rl < edl) {// 若有記錄在查詢區間內，則加入
												dataStringBuilder.append("," + r);
												if (val < min) {
													min = val;
												}
												if (val > max) {
													max = val;
												}

											}
										}
									}
									if (dataMap.get(keyStr) == null) {
										dataMap.put(keyStr, new StringBuilder());
									}
									dataMap.get(keyStr).append(dataStringBuilder.toString());
								}
							}
						}
					}
				}
			}

			// dataMap.put(dataType,
			// dataStringBuilder.deleteCharAt(0).toString());
		}

		for (String key : dataMap.keySet()) {
			String[] keyArray = key.split("-");
			String tagName = tagDao.getTagName(keyArray[0]);
			resultMap.put(tagName + "-" + keyArray[1], dataMap.get(key).deleteCharAt(0).toString());
		}

		resultMap.put("max", String.format("%.2f", max));
		resultMap.put("min", String.format("%.2f", min));

		return resultMap;
	}

}
