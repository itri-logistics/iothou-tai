package com.dao.vehicle;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import itri.group.param.Pa;

@Service
public class VehicleDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	final private String TABLE_NAME = "vehicle";
	final private String FILED_VEHICLE_ID = "vehicleId";
	final private String FILED_DIS_NAME = "disName";
	final private String FILED_COMPANY_ID = "companyId";
	final private String FILED_UPDATE_DATE = "updateDate";
	final private String FILED_UPDATOR = "updator";
	
	public Object find(String vehicleId, String companyId, String disName, String page) {
		StringBuilder sql = new StringBuilder("select * from " + TABLE_NAME + " where ");
		List query = new ArrayList();

		if (StringUtils.isNotBlank(vehicleId)) {
			sql.append(FILED_VEHICLE_ID + " = ? and ");
			query.add(vehicleId);
		}

		if (StringUtils.isNotBlank(companyId)) {
			sql.append(FILED_COMPANY_ID + " like ? and ");
			query.add("%" + companyId + "%");
		}
		
		if (StringUtils.isNotBlank(disName)) {
			sql.append(FILED_DIS_NAME + " like ? and ");
			query.add("%" + disName + "%");
		}


		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		} else {
			sql = sql.delete(sql.length() - 4, sql.length());
		}

		if (StringUtils.isNotBlank(page)) {
			sql.append(String.format("limit  %s,%d ", (Integer.parseInt(page) - 1) * Pa.PAGE_NUM, Pa.PAGE_NUM));
		}
		
		//System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		return list;
	}
	
	@Transactional
	public void update(String vehicleId, String disName, String companyId, String updateDate, String updator) {

		String condi = "update " + TABLE_NAME + " set " + FILED_COMPANY_ID + "=?," + FILED_DIS_NAME + "=?," + FILED_UPDATE_DATE + "=?,"+ FILED_UPDATOR + "=?" + " where "
				+ FILED_VEHICLE_ID + "=?";
		jdbcTemplate.update(condi, new Object[] { companyId, disName, updateDate, updator, vehicleId });

	}

	@Transactional
	public void create(String vehicleId, String disName, String companyId, String updateDate, String updator) {
		
		String condi = "insert "+ TABLE_NAME +" ("
				+FILED_VEHICLE_ID+","
				+FILED_DIS_NAME+","
				+FILED_COMPANY_ID+","
				+FILED_UPDATE_DATE+","
				+FILED_UPDATOR+")"
				+ " values (?,?,?,?,?)";
		jdbcTemplate.update(condi,
				new Object[] { vehicleId, disName, companyId, updateDate, updator });
	}
	
	@Transactional
	public void del(String vehicleId) {
		//移除車輛
		String sql = String.format("delete from "+TABLE_NAME+" where "+FILED_VEHICLE_ID+"='%s' ", vehicleId);		
		//System.out.println(sql);
		jdbcTemplate.execute(sql);
	}
}
