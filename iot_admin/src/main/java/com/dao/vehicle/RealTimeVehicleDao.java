package com.dao.vehicle;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.utility.LocationDataPool;

@Service
public class RealTimeVehicleDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public Object getNewestLocationData(String companyId) {

		List<Map<String, Object>> vehicles = jdbcTemplate.queryForList("select * from vehicle where companyId=?",
				new Object[] { companyId });

		StringBuilder vehicleIds = new StringBuilder();

		for (Map vMap : vehicles) {			
			vehicleIds.append(","+vMap.get("vehicleId").toString());
		}
		
		if(vehicleIds.length()>0){
			vehicleIds.deleteCharAt(0);
		}

		return LocationDataPool.getNewLocationData(companyId, vehicleIds.toString());
	}

}
