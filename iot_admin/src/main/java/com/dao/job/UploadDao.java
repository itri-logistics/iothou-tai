package com.dao.job;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
 
@Service
public class UploadDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Transactional
	public void log(String fileKey,String status,String updateDate){
    	String condi ="insert upload_log (fileKey,updateDate,status) values (?,?,?)";
    	//System.out.println(condi);
    	jdbcTemplate.update(condi,new Object[]{fileKey,updateDate,status});

	}
	
	public boolean checkFileKeyExist(String fileKey) {
		String condi = String.format("select * from upload_log where fileKey = '%s'",fileKey);;
		return jdbcTemplate.queryForList(condi).size() > 0;
	}
	
}
