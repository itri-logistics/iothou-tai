package com.dao.job;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.security.SecurityController;
import com.utility.LocationDataPool;
import com.utility.RealTimeDataPool;


/******************************************
 * 資料處理執行緒 iot_proc目錄:每30秒會讀取解析，最後將解析出來的data轉至iot_data目錄裡面 iot_raw目錄:上傳過來備份使用
 * iot_data目錄:解析完成後的Data，之後網頁呈現所要抓取的資料來源
 *
 * 套件:upload UploadRealTimeController.java uploadRealTime.js
 * uploadRealTimeView.jsp
 *
 * @author Tim create by 2016/12/25
 *******************************************/
@Service
public class RoundJobDao {

	private final static String BATCH_FILE_TYPE = ".wproc";// 批次讀取的檔案格式
	private final static String RT_FILE_TYPE = ".wproc_rt";// 即時資料的檔案格式
	private final static String LOC_FILE_TYPE = ".wproc_loc";// GPS資料的檔案格式

	private final static String ROOT_PATH = System.getProperty("user.home");;
	private final static String PROC_DIR = ROOT_PATH + File.separator + "iot_proc";
	private final static String DATA_DIR = ROOT_PATH + File.separator + "iot_data";
	private final static String PROC_ERR_DIR = ROOT_PATH + File.separator + "iot_errorData";// 解析錯誤會把wproc或wproc_rt的檔案移動到此目錄並更改名稱_err和_err_rt結尾

	@Autowired
	JdbcTemplate jdbcTemplate;

	// datas=fileKey#startdatetime#20160910121314,20160914120001#macId#companyId#dataTypes#data1#data2#data3#data4#以此類推
	@Transactional
	public void proc() {
		SecurityController.dLog.debug("round job.");
		File[] procDirFile = new File(PROC_DIR).listFiles();
		ArrayList<File> deleteFileList = new ArrayList();
		for (File f : procDirFile) {
			//long st = System.nanoTime();
			String status = null;// 處裡完成後的log資訊
			String fileKey = "NULL";

			String txtEndFileType = null;
			String txtErrEndFileType = null;

			if (f.getName().endsWith(BATCH_FILE_TYPE)) {// 批次上傳資料
				txtEndFileType = ".txt";
				txtErrEndFileType = "_err";
			} else if (f.getName().endsWith(RT_FILE_TYPE)) {// 即時上傳資料
				txtEndFileType = ".txt_rt";
				txtErrEndFileType = "_err_rt";
			} else if (f.getName().endsWith(LOC_FILE_TYPE)) {// GPS上傳資料
				txtEndFileType = ".txt_loc";
				txtErrEndFileType = "_err_loc";
			} else {
				continue;
			}

			try {
				String datas = FileUtils.readFileToString(f);
				String[] ds = datas.split("#");
				fileKey = ds[0];// 之後會用到
				status = String.format("OK: proc ok: fileKey:'%s'", fileKey);
				String startDatetime = ds[1];// 之後會用到
				String[] sded = ds[2].split("-"); // 之後會用到
				String macId = ds[3];
				String companyId = ds[4];
				String[] dataTypes = ds[5].split(",");
				
				if(sded[0].length() != 12 || sded[1].length() !=12){
					throw new Exception("invalid date format.");
				}
				
				// 剩餘DATA
				String[][] details = new String[dataTypes.length][];
				int count = 0;
				for (int i = 6; i < ds.length; i++) {
					details[count++] = ds[i].split(",");
				}

				String tagName;

				try {
					if (ds[5].contains("0508")) {
						tagName = jdbcTemplate
								.queryForList(String.format("select * from vehicle where vehicleId='%s'", macId)).get(0)
								.get("disName").toString();
					} else {
						tagName = jdbcTemplate.queryForList(String.format("select * from tag where mac='%s'", macId))
								.get(0).get("name").toString();
					}
				} catch (Exception e) {
					tagName = macId;
				}

				// 更新即時資料pool
				if (f.getName().endsWith(RT_FILE_TYPE)) {
					RealTimeDataPool.updateTagData(companyId, macId, tagName, dataTypes, details);
				} else if (f.getName().endsWith(LOC_FILE_TYPE)) {
					LocationDataPool.updateVehicleData(companyId, macId, tagName, details[0]);
				}

				// 計算SD~ED共有幾個檔案要新增
				// file name = macId-port-sensorType
				TreeSet<String> dateMonthSet = new TreeSet<String>();
				for (int i = 0; i < details[0].length; i++) {
					String date = details[0][i].substring(0, 6);
					if (!dateMonthSet.contains(date)) {
						dateMonthSet.add(date);
					}
				}
				createCompanyMonthDir(companyId, dateMonthSet);

				// 每個一組不同類型資料和每天 切成一個檔案
				for (String date : dateMonthSet) {
					for (int i = 0; i < details.length; i++) {
						StringBuilder sb = new StringBuilder();
						for (String ddd : details[i]) {
							if (ddd.substring(0, 6).equals(date)) {
								sb.append("," + ddd);
							}
						}
						String fileName = DATA_DIR + File.separator + companyId + File.separator + date.substring(0, 2)
								+ File.separator + date.substring(2, 4) + File.separator + macId + "-"
								+ date.substring(4, 6) + "-" + dataTypes[i] + txtEndFileType;
						File tempf = new File(fileName);

						if (sb.length() > 0)
							sb.deleteCharAt(0);

						if (tempf.exists()) {// 有現有資料就用新增的
							insertData(tempf, sb.toString());
						} else {
							FileUtils.writeStringToFile(tempf, sb.toString());
						}
					}
				}
				// 完畢以後要將原本的資料刪除
				deleteFileList.add(f);
				SecurityController.nLog.info(status);
			} catch (Exception e) {
				status = "NG: proc fail:" + e.getMessage();
				e.printStackTrace();
				SecurityController.eLog.error(status);				
				// 如果出現讀寫意外就將原本的file放在error檔案區資料夾裡面
				f.renameTo(new File(PROC_ERR_DIR + File.separator + f.getName() + txtErrEndFileType));
			}
			//String updateDate = new JDateTime().toString("YYYYMMDDhhmmss");
			//String condi = "insert upload_log (fileKey,updateDate,status) values (?,?,?)";
			//System.out.println(condi);
			//jdbcTemplate.update(condi, new Object[] { fileKey, updateDate, status });
		}

		for (File f : deleteFileList) {
			f.delete();
		}
	}

	// 如果是資料庫裏面有檔案就要比對時間，如果時間一樣，以原始資料為主
	private void insertData(File f, String tarsStr) throws IOException {
		String[] tars = tarsStr.split(",");
		String[] orgs = FileUtils.readFileToString(f).split(",");
		long s2, e1;// 第一段結尾和第二段開頭
		s2 = NumberUtils.createLong(tars[0].substring(6, 12));
		e1 = NumberUtils.createLong(orgs[orgs.length - 1].substring(6, 12));

		// 直接append data就好
		if (e1 < s2) {
			FileUtils.writeStringToFile(f, "," + tarsStr, true);

			// 有資料重疊
		} else {
			long[] d1 = new long[orgs.length], d2 = new long[tars.length];
			for (int i = 0; i < orgs.length; i++) {
				d1[i] = NumberUtils.createLong(orgs[i].substring(6, 12));
			}
			for (int i = 0; i < tars.length; i++) {
				d2[i] = NumberUtils.createLong(tars[i].substring(6, 12));
			}

			List<String> combinedStrArray = StringParse(d1, d2, orgs, tars);// 排序過後的data
			StringBuilder writeStr = new StringBuilder();
			for (String s : combinedStrArray) {
				writeStr.append("," + s);
			}
			writeStr.deleteCharAt(0);
			FileUtils.writeStringToFile(f, writeStr.toString());
		}

	}

	// 比較大小
	private static List<String> StringParse(long p1[], long[] p2, String[] orgs, String[] tars) throws IOException {

		int compareIndex = 0;
		List<String> combinedStrArray = new ArrayList();

		for (int i = 0; i < p1.length; i++) {
			while (compareIndex < p2.length && p1[i] >= p2[compareIndex]) {
				if (p1[i] != p2[compareIndex])// 以原本資料為主
					combinedStrArray.add(tars[compareIndex]);
				compareIndex++;
			}
			combinedStrArray.add(orgs[i]);
		}

		// 剩下要補上
		while (compareIndex < p2.length) {
			combinedStrArray.add(tars[compareIndex]);
			compareIndex++;
		}
		return combinedStrArray;
	}

	// 先檢查公司裡面月份的目錄是否存在
	private void createCompanyMonthDir(String companyId, TreeSet<String> dateMonthSet) {
		crteateCompanyDir(companyId);
		String rootPath = System.getProperty("user.home");
		File tempFile = new File(rootPath + File.separator + "iot_data");
		if (!tempFile.exists())
			tempFile.mkdir();
		String dir = rootPath + File.separator + "iot_data" + File.separator + companyId;
		for (String str : dateMonthSet) {
			String y = str.substring(0, 2);
			String ydir = dir + File.separator + y;
			File f = new File(ydir);
			if (!f.exists()) {
				f.mkdir();
			}

			String month = str.substring(2, 4);
			String mdir = ydir + File.separator + month;
			f = new File(mdir);
			if (!f.exists()) {
				f.mkdir();
			}
		}
	}

	// 先檢查公司的目錄
	private void crteateCompanyDir(String companyId) {
		String rootPath = System.getProperty("user.home");
		String dir = rootPath + File.separator + "iot_data";
		File t = new File(dir);
		if (!t.exists()) {
			t.mkdir();
		}

		dir = dir + File.separator + companyId;
		t = new File(dir);
		if (!t.exists()) {
			t.mkdir();
		}
	}

}
