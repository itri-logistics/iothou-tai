package com.dao.dashboard;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.utility.RealTimeDataPool;

@Service
public class DashboardSettingDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public Object getNewestTagsData(String companyId, String macIdDataTypes) {
		return RealTimeDataPool.getNewTagsData(companyId, macIdDataTypes);
	}

	public Object findDashboardSetting(String companyId) {
		StringBuilder sql = new StringBuilder(
				"select * from dashboard where companyId=? and position >= 0 order by position");
		//System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), new Object[] { companyId });

		return list;
	}

	@Transactional
	public void create(String disName, String companyId, String macId, String dataType, String position) {

		Integer p = 0;

		if (StringUtils.isNotBlank(position)) {
			p = Integer.parseInt(position);
		}

		String condi = "insert dashboard (disName,companyId,macId,dataType,position)" + " values (?,?,?,?,?)";
		jdbcTemplate.update(condi, new Object[] { disName, companyId, macId, dataType, p });
	}

	@Transactional
	public void update(String id, String disName, String companyId, String macId, String dataType, String position) {

		Integer p = 0;

		if (StringUtils.isNotBlank(position)) {
			p = Integer.parseInt(position);
		}
		String condi = "update dashboard set disName=?,companyId=?,macId=?,dataType=?,position=? where dashboardId=?";
		jdbcTemplate.update(condi,
				new Object[] { disName, companyId, macId, dataType, p, id });

	}

	@Transactional
	public void del(String id) {
		String sql = String.format("delete from dashboard where dashboardId=%s ", id);
		//System.out.println(sql);
		jdbcTemplate.execute(sql);
	}

}
