package com.dao.dashboard;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.utility.RealTimeDataPool;

@Service
public class RealTimeDashboardDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public Object getNewestTagsData(String companyId, String macIdDataTypes) {
		return RealTimeDataPool.getNewTagsData(companyId, macIdDataTypes);
	}

	public Object findDashboardSetting(String companyId) {
		StringBuilder sql = new StringBuilder(
				"select * from dashboard where companyId=? and position >= 0 order by position");
		//System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), new Object[] { companyId });

		return list;
	}

}
