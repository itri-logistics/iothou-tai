package com.dao.sensorPosition;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.Field;

@Service
public class SensorPositionDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	final private String TABLE_NAME = "sensor_position";
	final private String FILED_ID = "id";
	final private String FILED_COMPANY_ID = "companyId";
	final private String FILED_PLACE_ID = "placeId";
	final private String FILED_MAC = "mac";
	final private String FILED_PORT = "port";
	final private String FILED_X = "x";
	final private String FILED_Y = "y";
	final private String FILED_UPDATE_DATE = "updateDate";
	final private String FILED_UPDATOR = "updator";

	public Object find(String id, String companyId, String placeId, String mac, String port) {
		StringBuilder sql = new StringBuilder("select * from " + TABLE_NAME + " where ");
		List query = new ArrayList();

		if (StringUtils.isNotBlank(id)) {
			sql.append(FILED_ID + " = ? and ");
			query.add(id);
		}

		if (StringUtils.isNotBlank(companyId)) {
			sql.append(FILED_COMPANY_ID + " like ? and ");
			query.add("%" + companyId + "%");
		}

		if (StringUtils.isNotBlank(placeId)) {
			sql.append(FILED_PLACE_ID + " = ? and ");
			query.add(placeId);
		}

		if (StringUtils.isNotBlank(mac)) {
			sql.append(FILED_MAC + " = ? and ");
			query.add(mac);
		}

		if (StringUtils.isNotBlank(port)) {
			sql.append(FILED_PORT + " = ? and ");
			query.add(port);
		}

		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		} else {
			sql = sql.delete(sql.length() - 4, sql.length());
		}

		//System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		return list;
	}

	@Transactional
	public void update(String id, String companyId, String placeId, String mac, String port, String x, String y,
			String updateDate, String updator) {

		Integer xInt = null, yInt = null;

		if (StringUtils.isNotBlank(x)) {
			xInt = Integer.parseInt(x);
		}

		if (StringUtils.isNotBlank(y)) {
			yInt = Integer.parseInt(y);
		}

		String condi = "update " + TABLE_NAME + " set " + FILED_COMPANY_ID + "=?," + FILED_PLACE_ID + "=?," + FILED_MAC
				+ "=?," + FILED_PORT + "=?," + FILED_X + "=?," + FILED_Y + "=?," + FILED_UPDATE_DATE + "=?,"+ FILED_UPDATOR + "=?" + " where "
				+ FILED_ID + "=?";
		jdbcTemplate.update(condi, new Object[] { companyId, placeId, mac, port, x, y, updateDate, updator, id });

	}

	@Transactional
	public void create(String companyId, String placeId, String mac, String port, String x, String y,
			String updateDate, String updator) {

		String id = companyId+"-"+placeId+"-"+mac+"-"+port;
		
		Integer xInt = null, yInt = null;

		if (StringUtils.isNotBlank(x)) {
			xInt = Integer.parseInt(x);
		}

		if (StringUtils.isNotBlank(y)) {
			yInt = Integer.parseInt(y);
		}
		
		String condi = "insert "+ TABLE_NAME +" ("
				+FILED_ID+","
				+FILED_COMPANY_ID+","
				+FILED_PLACE_ID+","
				+FILED_MAC+","
				+FILED_PORT+","
				+FILED_X+","
				+FILED_Y+","
				+FILED_UPDATE_DATE+","
				+FILED_UPDATOR+")"
				+ " values (?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(condi,
				new Object[] { id, companyId, placeId, mac, port, xInt, yInt, updateDate,updator });
	}

	@Transactional
	public void createOrUpdate(String companyId, String placeId, String mac, String port, String x, String y,
			String updateDate,String updator) {

		String id = companyId+"-"+placeId+"-"+mac+"-"+port;
		
		Integer xInt = null, yInt = null;

		if (StringUtils.isNotBlank(x)) {
			xInt = Integer.parseInt(x);
		}

		if (StringUtils.isNotBlank(y)) {
			yInt = Integer.parseInt(y);
		}
		
		String condi = "replace into "+ TABLE_NAME +" ("
				+FILED_ID+","
				+FILED_COMPANY_ID+","
				+FILED_PLACE_ID+","
				+FILED_MAC+","
				+FILED_PORT+","
				+FILED_X+","
				+FILED_Y+","
				+FILED_UPDATE_DATE+","
				+FILED_UPDATOR+")"
				+ " values (?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(condi,
				new Object[] { id, companyId, placeId, mac, port, xInt, yInt, updateDate,updator });
	}
	
	@Transactional
	public void del(String id) {
		String sql = String.format("delete from "+TABLE_NAME+" where "+FILED_ID+"='%s' ", id);
		System.out.println(sql);
		jdbcTemplate.execute(sql);
	}
	
	@Transactional
	public void delByMac(String mac) {
		String sql = String.format("delete from "+TABLE_NAME+" where "+FILED_MAC+"='%s' ", mac);
		System.out.println(sql);
		jdbcTemplate.execute(sql);
	}
	
	@Transactional
	public void delByPlace(String placeId) {
		String sql = String.format("delete from "+TABLE_NAME+" where "+FILED_PLACE_ID+"='%s' ", placeId);
		System.out.println(sql);
		jdbcTemplate.execute(sql);
	}

}
