package com.dao.proccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.utility.RealTimeDataPool;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

/******************************************
 * 標準建議Dao
 *
 * @author ATone create by 2017/01/09
 *******************************************/
@Service
public class SuggestionDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	// 取得標準建議
	public Object queryAll() {
		String sql = "select * from suggestion";

		// System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString());
		return list;
	}

}
