package com.dao.tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.utility.RealTimeDataPool;

@Service
public class RealTimeTagDao {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public Object getNewestTagsData(String companyId, String macIdDataTypes) {
		return RealTimeDataPool.getNewTagsData(companyId, macIdDataTypes);
	}
}
