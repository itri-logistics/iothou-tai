package com.dao.tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.utility.RealTimeDataPool;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

/******************************************
 * 警示功能Dao
 *
 * @author Tim create by 2017/01/09
 *******************************************/
@Service
public class AlertSettingDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	String alertServer = "http://192.168.20.198:8080/";

	// 根據公司取得警示設定
	public Object findAlertSettingsByCompanyId(String companyId) {
		String sql = String.format("select * from alert_setting where companyId = '%s'", companyId);

		// System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString());
		return list;
	}

	// 根據公司取得關聯設定
	public Object findAlertRelByCompanyId(String companyId) {
		String sql = String.format("select * from tag_datatype_alert_rel where companyId = '%s'", companyId);

		// System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString());
		return list;
	}

	// 根據公司取得關聯設定
	public Object findAlertSettingsDetailByCompanyId(String companyId) {
		String sql = String
				.format("select mac,dataType,tag_datatype_alert_rel.alertSettingId,low,high,alert_setting.updateDate"
						+ " from tag_datatype_alert_rel inner join alert_setting"
						+ " on tag_datatype_alert_rel.alertSettingId = alert_setting.alertSettingId"
						+ " where alert_setting.companyId = '%s';", companyId);

		// System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString());
		return list;
	}

	// 更新警示
	@Transactional
	public void update(String alertSettingId, String companyId, String disName, String high, String low,
			String afterMilliSecondAlert, String emails, String criticalEmails, String phones, String updateDate,
			String udpator) {
		String sql = "update alert_setting set companyId=?,disName=?,high=?,low=?,afterMilliSecondAlert=?,emails=?,criticalEmails=?,phones=?,updateDate=?,updator=? where alertSettingId=?";
		jdbcTemplate.update(sql, new Object[] { companyId, disName, high, low, afterMilliSecondAlert, emails,
				criticalEmails, phones, updateDate, udpator, Integer.parseInt(alertSettingId) });
		
		//通知刷新Pool
		HttpResponse response = HttpRequest
				.get(alertServer+"iot_alert/upload/updateAlertSettingPool")
				.send();
		
		// Map<String, Object> alertSetting = jdbcTemplate
		// .queryForList("select * from alert_setting where alertSettingId=?",
		// new Object[] { Integer.parseInt(alertSettingId) })
		// .get(0);
		// RealTimeDataPool.updateAlertSetting(alertSetting);
	}

	// 新增警示
	@Transactional
	public void createAlertSetting(final String companyId, final String disName, final String low, final String high,
			final String phones, final String emails, final String criticalEmails, final String afterMilliSecondAlert,
			final String updateDate, final String updator) {
		KeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert alert_setting (disName,low,high,phones,emails,criticalEmails,afterMilliSecondAlert,updateDate,updator,companyId)"
						+ " values (?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, disName);
				ps.setFloat(2, Float.parseFloat(low));
				ps.setFloat(3, Float.parseFloat(high));
				ps.setString(4, phones);
				ps.setString(5, emails);
				ps.setString(6, criticalEmails);
				ps.setDouble(7, Double.parseDouble(afterMilliSecondAlert));
				ps.setString(8, updateDate);
				ps.setString(9, updator);
				ps.setString(10, companyId);
				return ps;
			}
		}, holder);

		//通知刷新Pool
		HttpResponse response = HttpRequest
				.get(alertServer+"iot_alert/upload/updateAlertSettingPool")
				.send();

		// Integer newAlertSettingId = holder.getKey().intValue();
		// String sql = "select * from alert_setting where alertSettingId=?";
		// Map<String, Object> alertSetting = jdbcTemplate.queryForList(sql, new
		// Object[] { newAlertSettingId }).get(0);
		//
		// RealTimeDataPool.createAlertSetting(alertSetting);
	}

	// 新增關聯alert的基本資訊
	@Transactional
	public void createTagDataTypeAndAlertRel(String alertSettingId, String mac, String companyId, String dataType,
			String updateDate, String updator) {
		String condi = "insert tag_datatype_alert_rel (alertSettingId,mac,companyId,dataType,updateDate,updator)"
				+ " values (?,?,?,?,?,?)";
		jdbcTemplate.update(condi, new Object[] { alertSettingId, mac, companyId, dataType, updateDate, updator });
		
		//通知刷新Pool
		HttpResponse response = HttpRequest
				.get(alertServer+"iot_alert/upload/updateTagDataTypeAlertRel")
				.send();
		
		// Map<String, Object> alertSetting = jdbcTemplate
		// .queryForList("select * from alert_setting where alertSettingId=?",
		// new Object[] { alertSettingId })
		// .get(0);
		// RealTimeDataPool.createTagDataTypeAndAlertRel((int)
		// alertSetting.get("alertSettingId"), companyId, mac,
		// dataType);
	}

	// 刪除警示
	@Transactional
	public void delAlertSetting(String alertSettingId) {
		String sql = "delete from alert_setting where alertSettingId=?";
		// System.out.println(sql);
		jdbcTemplate.update(sql, new Object[] { Integer.parseInt(alertSettingId) });
		sql = "delete from tag_datatype_alert_rel where alertSettingId=?"; // 刪除相對應的關聯
		// System.out.println(sql);
		jdbcTemplate.update(sql, new Object[] { Integer.parseInt(alertSettingId) });
		
		//通知刷新Pool
		HttpResponse response = HttpRequest
				.get(alertServer+"iot_alert/upload/updateAlertSettingPool")
				.send();
				
		// RealTimeDataPool.delAlert(Integer.parseInt(alertSettingId));
	}

	// 刪除alert和Tag dataType的關聯
	@Transactional
	public void delTagDataTypeAndAlertRel(String mac, String alertSettingId, String companyId, String dataType) {
		String sql = "delete from tag_datatype_alert_rel where mac=? and alertSettingId=? and companyId=? and dataType=?";
		// System.out.println(sql);
		jdbcTemplate.update(sql, new Object[] { mac, Integer.parseInt(alertSettingId), companyId, dataType });
		//通知刷新Pool
		HttpResponse response = HttpRequest
				.get(alertServer+"iot_alert/upload/updateTagDataTypeAlertRel")
				.send();
		// RealTimeDataPool.delTagDataTypeAndAlertRel(Integer.parseInt(alertSettingId),
		// companyId, mac, dataType);
	}

}
