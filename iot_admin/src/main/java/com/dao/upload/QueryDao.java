package com.dao.upload;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jodd.datetime.JDateTime;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.tag.TagDao;

/**
 * 一個Dao最好配一個controller 這樣比較好維護 2016/12/25 tim
 *
 */
@Service
public class QueryDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	TagDao tagDao;

	// 取得查詢時間區間內的tag列表
	public Object getTagsInQueryInterval(String companyId, String sd, String ed) {
		TreeSet<String> qMonth = new TreeSet();// 要查詢的月份
		Set<String> qDay = new HashSet(); // 要查詢的日期
		if (Long.parseLong(sd) > Long.parseLong(ed))
			return "";
		JDateTime sdd = new JDateTime();
		sdd.parse(sd, "YYYYMMDD");
		String tempDate = sdd.toString("YYYYMMDD");
		qMonth.add(tempDate.substring(2, 6));
		qDay.add(tempDate.substring(2));

		if (tempDate.equals(ed) && qDay.size() == 0) {
			tempDate = sdd.toString("YYYYMMDD");
			qMonth.add(tempDate.substring(2, 6));
			qDay.add(tempDate.substring(2));
		} else {
			while (!tempDate.equals(ed)) {
				sdd.addDay(1);
				tempDate = sdd.toString("YYYYMMDD");
				qMonth.add(tempDate.substring(2, 6));
				qDay.add(tempDate.substring(2));
			}
		}

		final String rootPath = System.getProperty("user.home");
		final String dataDirPath = rootPath + File.separator + "iot_data" + File.separator + companyId;

		HashMap<String, HashMap<String, String>> tagsMap = new HashMap<String, HashMap<String, String>>();
		HashMap<String, StringBuilder> tagDataTypeMap = new HashMap<String, StringBuilder>();

		for (String temp : qMonth) {
			String y = temp.substring(0, 2);
			String m = temp.substring(2, 4);
			File f = new File(dataDirPath + File.separator + y + File.separator + m);
			File[] fs = f.listFiles();
			if (fs != null) {
				for (File tempf : fs) {
					if(tempf.isDirectory()){
						continue;
					}
					String fileName = tempf.getName();
					String fileDay = fileName.substring(13, 15);
					String fileMac = fileName.substring(0, 12);
					String fileType = fileName.substring(16, 20);
					if (qDay.contains(temp + fileDay) && fileName.endsWith(".txt")) {
						String date = y + m + fileDay;
						if (!tagsMap.containsKey(fileMac)) {
							tagsMap.put(fileMac, new HashMap<String, String>());
							tagDataTypeMap.put(fileMac, new StringBuilder());
							tagsMap.get(fileMac).put("sd", date);
							tagsMap.get(fileMac).put("ed", date);
						}

						if (Long.parseLong(date) < Long.parseLong(tagsMap.get(fileMac).get("sd"))) {
							tagsMap.get(fileMac).put("sd", date);
						}

						if (Long.parseLong(date) > Long.parseLong(tagsMap.get(fileMac).get("ed"))) {
							tagsMap.get(fileMac).put("ed", date);
						}

						if (tagDataTypeMap.get(fileMac).indexOf(fileType) < 0) {
							tagDataTypeMap.get(fileMac).append("," + fileType);
						}
					}
				}
			}
		}

		List<HashMap<String, String>> tagList = new ArrayList<HashMap<String, String>>();
		Iterator<String> k = tagsMap.keySet().iterator();

		while (k.hasNext()) {
			String key = k.next();
			HashMap<String, String> tagMap = new HashMap<String, String>();
			tagMap.put("mac", key);
			String rsd = tagsMap.get(key).get("sd");
			String red = tagsMap.get(key).get("ed");

			rsd = "20" + rsd.substring(0, 2) + "/" + rsd.substring(2, 4) + "/" + rsd.substring(4, 6);
			red = "20" + red.substring(0, 2) + "/" + red.substring(2, 4) + "/" + red.substring(4, 6);

			tagMap.put("name", tagDao.getTagName(key));
			tagMap.put("placeId", tagDao.getPlaceId(key));
			tagMap.put("sd", rsd);
			tagMap.put("ed", red);
			tagMap.put("signal", tagDataTypeMap.get(key).deleteCharAt(0).toString());
			tagList.add(tagMap);
		}

		return tagList;
	}

	// 取得單一Tag的Data
	public String getQueryData(String companyId, String sd, String ed, String macsStr) throws Exception {
		String[] macs = macsStr.split(",");
		TreeSet<String> qMonth = new TreeSet();// 要查詢的月份
		Set<String> qDay = new HashSet(); // 要查詢的日期
		if (Long.parseLong(sd) > Long.parseLong(ed))
			return "";
		JDateTime sdd = new JDateTime();
		sdd.parse(sd, "YYYYMMDD");
		String tempDate = sdd.toString("YYYYMMDD");
		qMonth.add(tempDate.substring(2, 6));
		qDay.add(tempDate.substring(2));

		if (tempDate.equals(ed) && qDay.size() == 0) {
			tempDate = sdd.toString("YYYYMMDD");
			qMonth.add(tempDate.substring(2, 6));
			qDay.add(tempDate.substring(2));
		} else {
			while (!tempDate.equals(ed)) {
				sdd.addDay(1);
				tempDate = sdd.toString("YYYYMMDD");
				qMonth.add(tempDate.substring(2, 6));
				qDay.add(tempDate.substring(2));
			}
		}

		final String rootPath = System.getProperty("user.home");
		final String dataDirPath = rootPath + File.separator + "iot_data" + File.separator + companyId;

		StringBuilder rs = new StringBuilder();
		LinkedHashMap<String, StringBuilder> datasMap = new LinkedHashMap();// <資料類型(溫度...),資料array>
		int readNum = 0;
		for (String temp : qMonth) {
			String y = temp.substring(0, 2);
			String m = temp.substring(2, 4);
			File f = new File(dataDirPath + File.separator + y + File.separator + m);
			File[] fs = f.listFiles();
			if (fs != null) {
				for (File tempf : fs) {
					if(tempf.isDirectory()){
						continue;
					}
					String fileName = tempf.getName();
					String fileDay = fileName.substring(13, 15);
					String fileType = fileName.substring(16, 20);
					if (qDay.contains(temp + fileDay) && fileName.indexOf(macs[0]) == 0
							&& fileName.endsWith(".txt")) {
						if (!datasMap.containsKey(fileType)) {
							datasMap.put(fileType, new StringBuilder());
						}
						StringBuilder sb = datasMap.get(fileType);
						sb.append("," + FileUtils.readFileToString(tempf));
						readNum++;
					}
				}
			}
		}

		// 前面一個#data 為key
		Iterator<String> k = datasMap.keySet().iterator();
		while (k.hasNext()) {
			String key = k.next();
			rs.append("#" + key + datasMap.get(key).toString());
		}
		if (rs.length() > 0)
			rs.deleteCharAt(0);
		//System.out.println("readNum:\t" + readNum);
		return rs.toString();
	}

	

}
