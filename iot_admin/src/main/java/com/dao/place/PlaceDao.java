package com.dao.place;

import itri.group.param.Pa;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlaceDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public String getPlaceName(String placeId) {
		String sql=String.format("select disName from place where placeId=%s;", placeId);

		List<Map<String, Object>> qMap = null;
		try {			
			qMap = jdbcTemplate.queryForList(sql);
		} catch (Exception e) {	
			e.printStackTrace();
		}
		
		if(qMap.isEmpty()){
			System.err.println("找不到場域名稱，請確認場域是否已登錄。placeId = "+placeId);
		}

		return qMap.isEmpty() ? "" : qMap.get(0).get("disName").toString();
		
	}
	
	public Object find(String disName, String companyId, String placeId, String page) {
		StringBuilder sql = new StringBuilder("select * from place where ");
		List query = new ArrayList();
		if (StringUtils.isNotBlank(placeId)) {
			sql.append("placeId = ? and ");
			query.add(placeId);
		}
		if (StringUtils.isNotBlank(disName)) {
			sql.append("disName like ? and ");
			query.add("%" +disName+ "%");
		}
		if (StringUtils.isNotBlank(companyId)) {
			sql.append("companyId = ? and ");
			query.add(companyId);
		}

		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		} else {
			sql = sql.delete(sql.length() - 4, sql.length());
		}

		if (StringUtils.isNotBlank(page)) {
			sql.append(String.format("order by updateDate DESC limit %d,%d;", (Integer.parseInt(page) - 1) * Pa.PAGE_NUM, Pa.PAGE_NUM));
		}else{
			sql.append("order by updateDate DESC");
		}

		//System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		
		return list;
	}

	public Object findPage(String disName, String companyId) {

		StringBuilder sql = new StringBuilder("select count(*) as total from place where ");
		List query = new ArrayList();
		if (StringUtils.isNotBlank(disName)) {
			sql.append("disName like ? and ");
			query.add("%" +disName+ "%");
		}
		if (StringUtils.isNotBlank(companyId)) {
			sql.append("companyId = ? and ");
			query.add(companyId);
		}

		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		} else {
			sql = sql.delete(sql.length() - 4, sql.length());
		}

		//System.out.println(sql);
		Map<String, Object> rs = jdbcTemplate.queryForList(sql.toString(), query.toArray()).get(0);
		rs.put("page_num", Pa.PAGE_NUM);
		return rs;
	}

	@Transactional
	public void update(String placeId, String companyId, String disName, String updateDate, String updator) {
		String condi = "update place set companyId=?,disName=?,updateDate=?,updator=? where placeId=?";
		jdbcTemplate.update(condi, new Object[] { companyId, disName, updateDate, updator, placeId });

	}

	@Transactional
	public void create(String companyId, String disName, String updateDate, String updator) {
		String condi = "insert place (disName,companyId,updateDate,updator) values (?,?,?,?)";
		jdbcTemplate.update(condi, new Object[] { disName, companyId, updateDate, updator});

	}
	
	@Transactional
	public void del(String placeId){
		//先移除場域關聯
		String sql=String.format("delete from sensor_position where placeId='%s'", placeId);
		//System.out.println(sql);
		jdbcTemplate.execute(sql);
		//將場域內的tag之場域消除
		sql=String.format("update tag set placeId=NULL where placeId='%s'", placeId);
		//System.out.println(sql);
		jdbcTemplate.execute(sql);
		//移除場域
		sql=String.format("delete from place where placeId='%s'", placeId);		
		//System.out.println(sql);
		jdbcTemplate.execute(sql);
	}

}
