package com.dao.pallets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jodd.datetime.JDateTime;

@Service
public class palletsDailyReportDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

//根據時間以及公司名稱列出結果
	public Object getTradingInQueryDateAndPlaceId(String companyId,String sd,int placeId) {
		System.out.println(sd);
		JDateTime sdd = new JDateTime();
		sdd.parse(sd, "YYYYMMDD");
		sdd.addDay(1);
		String ed = sdd.toString("YYYYMMDD");
		// -1 表示無身分
		String sql = String.format("select * from pallets_trading where companyId = '%s' and updateDate > '%s' and updateDate < '%s' and place = '%d' and palletsType <> -1", companyId,sd,ed,placeId);
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString());
		System.out.println(list);
		return list;
	}
	public Object find(String companyId) {
		String sql = String.format("select * from place where companyId = '%s'",companyId);
		List query = new ArrayList();
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		return list;
	}
	public Object getType(String companyId) {
		String sql = String.format("select * from pallets_type where companyId = '%s'", companyId);
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString());
		System.out.println(list);
		return list;
	}
	//@Transactional
	//public void addTrading(final String companyId,final String tagId,final String inOut,final String updateDate,final String updater,final String place){
		//final String palletsType = String.format("select * from pallets_tag where companyId = '%s' and tagId = '%s'",companyId,tagId);
		//KeyHolder holder = new GeneratedKeyHolder();
		//jdbcTemplate.update(new PreparedStatementCreator() {
			//@Override
			//public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				//String sql = "insert pallets_type (companyId,updateDate,place,in_out,palletsType)"
						//+ " values (?,?,?,?,?)";
				//PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				//ps.setString(1, companyId);
				//ps.setString(2, updateDate);
				//ps.setString(3, place);
			//ps.setString(4, inOut);
				//ps.setString(5, palletsType);
				//return ps;
			//}
		//}, holder);
	//}
	//public void addTrading(final String companyId, final String tagId, final String inOut, final String updateDate,  String updater,
	//		final String place) {
		// TODO Auto-generated method stub
		//final String palletsType = String.format("select * from pallets_tag where companyId = '%s' and tagId = '%s'",companyId,tagId);
		//KeyHolder holder = new GeneratedKeyHolder();
		//jdbcTemplate.update(new PreparedStatementCreator() {
			//@Override
			//public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				//String sql = "insert pallets_type (companyId,updateDate,place,in_out,palletsType)"
				//		+ " values (?,?,?,?,?)";
				//PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				//ps.setString(1, companyId);
				//ps.setString(2, updateDate);
				//ps.setString(3, place);
				//ps.setString(4, inOut);
				//ps.setString(5, palletsType);
				//return ps;
			//}
		//}, holder);
		
	//}
}