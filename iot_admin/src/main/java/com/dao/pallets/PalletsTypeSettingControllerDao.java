package com.dao.pallets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/******************************************
 * 棧板Tag Type Dao
 *
 * @author WeiAn create by 2017/05/09
 *******************************************/
@Service
public class PalletsTypeSettingControllerDao {

	@Autowired
	JdbcTemplate jdbcTemplate;



	// 根據公司取得警TAG TYPE設定
	public Object findPalletsTypeSettingByCompanyId(String companyId) {
		String sql = String.format("select * from pallets_type where companyId = '%s'", companyId);
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString());
		return list;
	}

	// 新增 Pallets Type Setting
	@Transactional
	public void createPalletsType(final String companyId, final String name, final String color, final String customer,
			final String meterial, final String property, final String format,final String type) {
		KeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert pallets_type (name,color,customer,meterial,property,format,type,companyId)"
						+ " values (?,?,?,?,?,?,?,?)";
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, name);
				ps.setString(2, color);
				ps.setString(3, customer);
				ps.setString(4, meterial);
				ps.setString(5, property);
				ps.setString(6, format);
				ps.setString(7, type);
				ps.setString(8, companyId);
				return ps;
			}
		}, holder);
	}
	// 更新 Pallets Type Setting
	@Transactional
	public void update(String id, String name, String color, String customer, String meterial, String property
		, String format, String type, String companyId) {
		String sql = "update pallets_type set name=?,color=?,customer=?,meterial=?,property=?,format=?,type=?,companyId=? where id=?";
		jdbcTemplate.update(sql, new Object[] {name,color,customer,meterial,property,format,type,companyId, Integer.parseInt(id) });
	}
	// 刪除Pallets Type Setting
	@Transactional
	public void delPalletsTypeSetting(String id) {
		String sql = "delete from pallets_type where id=?";
		System.out.println(id);
		jdbcTemplate.update(sql, new Object[] { Integer.parseInt(id) });
	}
	
	public Object findType(String formName) {
		String sql = String.format("select * from pallets_param where formName = '%s'",formName);
		List query = new ArrayList();
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		System.out.println(list);
		return list;
	}
	
	public Object findFormat(String formName) {
		String sql = String.format("select * from pallets_param where formName = '%s'",formName);
		List query = new ArrayList();
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		System.out.println(list);
		return list;
	}
	
	public Object findProperty(String formName) {
		String sql = String.format("select * from pallets_param where formName = '%s'",formName);
		List query = new ArrayList();
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		System.out.println(list);
		return list;
	}
	
	public Object findParam() {
		String sql = String.format("select * from pallets_param");
		List query = new ArrayList();
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		System.out.println(list);
		return list;
	}
}
