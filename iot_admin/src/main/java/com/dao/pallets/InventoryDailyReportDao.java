package com.dao.pallets;
import java.util.ArrayList;
/*
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
*/
import java.util.List;
/*
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jodd.datetime.JDateTime;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.NumberUtils;
*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;

import jodd.datetime.JDateTime;

//import com.dao.tag.TagDao;


@Service
public class InventoryDailyReportDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

//根據時間以及公司名稱列出結果
	public Object getInventoryInQueryDate(String companyId,String sd) {
		System.out.println(sd);
		JDateTime sdd = new JDateTime();
		sdd.parse(sd, "YYYYMMDD");
		sdd.addDay(1);
		String ed = sdd.toString("YYYYMMDD");
		String sql = String.format("select * from pallets_inventory where companyId = '%s' and updateDate > '%s' and updateDate < '%s'", companyId,sd,ed);
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString());
		System.out.println(list);
		return list;
	}
	
	public Object getPlace(String companyId) {
		String sql = String.format("select * from place where companyId = '%s'", companyId);
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString());
		System.out.println(list);
		return list;
	}
	
	public Object getType(String companyId) {
		String sql = String.format("select * from pallets_type where companyId = '%s'", companyId);
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString());
		System.out.println(list);
		return list;
	}
}