package com.dao.pallets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import itri.group.param.Pa;
import jodd.datetime.JDateTime;


@Service
public class noIdentityPalletsDailyReportDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

//根據時間以及公司名稱列出結果
	public Object getTradingInQueryDateAndPlaceId(String companyId,String sd,int placeId) {
		System.out.println(sd);
		JDateTime sdd = new JDateTime();
		sdd.parse(sd, "YYYYMMDD");
		sdd.addDay(1);
		String ed = sdd.toString("YYYYMMDD");
		// -1 表示無身分
		String sql = String.format("select * from pallets_trading where companyId = '%s' and updateDate > '%s' and updateDate < '%s'  and place = '%d' and palletsType = -1", companyId,sd,ed,placeId);
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString());
		System.out.println(list);
		return list;
	}
	
	public Object find(String companyId) {
		String sql = String.format("select * from place where companyId = '%s'",companyId);
		List query = new ArrayList();
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		return list;
	}
}