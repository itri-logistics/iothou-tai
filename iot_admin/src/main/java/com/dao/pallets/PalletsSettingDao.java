package com.dao.pallets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class PalletsSettingDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	// 新增 Pallets Type Setting
	@Transactional
	public void setPalletsTagType(final String companyId,final int typeId,final String tagId) {
		KeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert pallets_tag (tagId,typeId,companyId)"
						+ " values (?,?,?)";
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, tagId);
				ps.setInt(2, typeId);
				ps.setString(3, companyId);
				return ps;
			}
		}, holder);
	}
	
	public Object findPalletsType(String companyId) {
		String sql = String.format("select * from pallets_type where companyId = '%s'",companyId);
		List query = new ArrayList();
		System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		System.out.println(list);
		return list;
	}
}