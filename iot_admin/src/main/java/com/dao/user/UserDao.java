package com.dao.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.utility.RealTimeDataPool;

import itri.group.param.Pa;

@Service
public class UserDao {
	
	@Autowired
	StandardPasswordEncoder encoder;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public Object find(String userId, String companyId, String companyDisName, String disName, String type, String server,String page) {
		StringBuilder sql = new StringBuilder("select companyId,userId,companyDisName,disName,type,server from user where ");
		List query = new ArrayList();
		if (StringUtils.isNotBlank(companyId)) {
			sql.append("companyId like ? and ");
			query.add(companyId);
		}
		if (StringUtils.isNotBlank(userId)) {
			sql.append("userId = ? and ");
			query.add(userId);
		}		
		if (StringUtils.isNotBlank(disName)) {
			sql.append("disName like ? and ");
			query.add("%"+disName+"%");
		}
		
		if (StringUtils.isNotBlank(companyDisName)) {
			sql.append("companyDisName like ? and ");
			query.add("%"+companyDisName+"%");
		}
		
		if (StringUtils.isNotBlank(server)) {
			sql.append("server like ? and ");
			query.add("%"+server+"%");
		}
		
		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		}else{
			sql = sql.delete(sql.length() - 4, sql.length());
		}
		
		if (StringUtils.isNotBlank(page)) {
			sql.append(String.format("limit  %s,%d ", (Integer.parseInt(page) - 1) * Pa.PAGE_NUM, Pa.PAGE_NUM));
		} 

		//System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		return list;
	}

	public Object findPage(String userId, String companyId, String companyDisName, String disName, String type, String server) {

		StringBuilder sql = new StringBuilder("select count(*) as total from user where ");
		List query = new ArrayList();
		if (StringUtils.isNotBlank(companyId)) {
			sql.append("companyId like ? and ");
			query.add(companyId);
		}
		if (StringUtils.isNotBlank(userId)) {
			sql.append("userId = ? and ");
			query.add(userId);
		}		
		if (StringUtils.isNotBlank(disName)) {
			sql.append("disName like ? and ");
			query.add("%"+disName+"%");
		}		
		if (StringUtils.isNotBlank(companyDisName)) {
			sql.append("companyDisName like ? and ");
			query.add("%"+companyDisName+"%");
		}
		
		if (StringUtils.isNotBlank(server)) {
			sql.append("server like ? and ");
			query.add("%"+server+"%");
		}
		
		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		}else{
			sql = sql.delete(sql.length() - 4, sql.length());
		}
		
		System.out.println(sql);
		Map<String, Object> rs = jdbcTemplate.queryForList(sql.toString(), query.toArray()).get(0);
		rs.put("page_num", Pa.PAGE_NUM);
		return rs;
	}

	@Transactional
	public void update(String userId, String companyId, String password, String companyDisName, String disName
			, String type, String server, String updateDate, String updator) {
		
		if(StringUtils.isNotBlank(password)){
			String condi = "update user set password=?,companyDisName=?,disName=?,type=?,server=?,updateDate=?,updator=?"
					+ " where userId=? and companyId=?";
			jdbcTemplate.update(condi, new Object[] { encoder.encode(password), companyDisName, disName,
					type, server, updateDate, updator, userId, companyId});
		}else{
			String condi = "update user set companyDisName=?,disName=?,type=?,server=?,updateDate=?,updator=?"
					+ " where userId=? and companyId=?";
			jdbcTemplate.update(condi, new Object[] { companyDisName, disName,
					type, server, updateDate, updator, userId, companyId});
		}
	}

	@Transactional
	public void create(String userId, String companyId, String password, String companyDisName, String disName
			, String type, String server, String updateDate, String updator) {
		
		String condi = "insert user (userId,companyId,password,companyDisName,disName,type,server,updateDate,updator)"
				+ " values (?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(condi, new Object[] { userId, companyId, encoder.encode(password), companyDisName, 
				disName, type, server, updateDate, updator });
	}
	
	@Transactional
	public void del(String userId, String companyId){

		String sql=String.format("delete from user where companyId='%s' and userId='%s' ", companyId, userId);		
		System.out.println(sql);
		jdbcTemplate.execute(sql);		
	}
	
	@Transactional
	public void delCompany(String companyId){
		String sql;
		//移除公司下的所有使用者
		sql=String.format("delete from user where companyId='%s'", companyId);		
		System.out.println(sql);
		jdbcTemplate.execute(sql);
		
		//移除公司相關資料
		//從即時資料pool移除警示設定
		sql=String.format("select alertSettingId from alert_setting where companyId='%s'", companyId);
		List<Map<String, Object>> alertList = jdbcTemplate.queryForList(sql);
		for(Map<String, Object> aMap:alertList){
			String alertSettingId = aMap.get("alertSettingId").toString();
			System.out.println("delete alert "+alertSettingId+" from RealTimeDataPool");
			RealTimeDataPool.delAlert(Integer.parseInt(alertSettingId));
		}
		//移除警示關聯
		sql=String.format("delete from tag_datatype_alert_rel where companyId='%s'", companyId);
		System.out.println(sql);
		jdbcTemplate.execute(sql);
		//移除警示設定
		sql=String.format("delete from alert_setting where companyId='%s'", companyId);
		System.out.println(sql);
		jdbcTemplate.execute(sql);
		//移除即時看板設定
		sql=String.format("delete from dashboard where companyId='%s'", companyId);
		System.out.println(sql);
		jdbcTemplate.execute(sql);
		//移除圖形監控位置
		sql=String.format("delete from sensor_position where companyId='%s'", companyId);
		System.out.println(sql);
		jdbcTemplate.execute(sql);
		//移除tag
		sql=String.format("delete from tag where companyId='%s'", companyId);
		System.out.println(sql);
		jdbcTemplate.execute(sql);
		//移除場域
		sql=String.format("delete from place where companyId='%s'", companyId);
		System.out.println(sql);
		jdbcTemplate.execute(sql);
		//移除車輛
		sql=String.format("delete from vehicle where companyId='%s'", companyId);
		System.out.println(sql);
		jdbcTemplate.execute(sql);
	}
	
}
