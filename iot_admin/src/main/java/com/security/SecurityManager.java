package com.security;

import java.util.ArrayList;
import java.util.List;

import org.magiclen.magiccrypt.MagicCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Component;
import com.bean.Aes256Encryption;

/******************************************
 * 使用者帳號和DB間的連結
 *
 * 套件:security SecurityManager.java
 *
 * @author tim create by 2016/12/19
 *******************************************/
@Component
public class SecurityManager implements UserDetailsService {

	public final static String OS_ANDROID = "android";
	public final static String OS_IOS = "ios";

	// 用來產生密碼使用
	StandardPasswordEncoder encoder = new StandardPasswordEncoder("工研院");

	// APP解密用
	MagicCrypt mc = new MagicCrypt("itri", 128);

	@Autowired
	Aes256Encryption aes;


	@Autowired
	JdbcTemplate jdbcTemplate;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {

		String[] ss = username.split("_");
		username = ss[1];
		String companyId = ss[0];

		try {
			String sql = "select userId as username,disName,password,companyDisName,companyId,type,server from user where userId=? and companyId=?";

			List<ManageUser> list = jdbcTemplate.query(sql, new Object[] { username, companyId },
					new BeanPropertyRowMapper<ManageUser>(ManageUser.class));
			// System.out.println(sql);
			ArrayList<ManageUser> userList = new ArrayList<ManageUser>(list);
			if (userList.size() > 0) {
				return userList.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		throw new UsernameNotFoundException("User " + username + " has no GrantedAuthority");
	}

	public ManageUser loadUserByUsernameAndPassword(String companyId, String username, String password, String os)
			throws UsernameNotFoundException {
		try {
			if (os.equals(OS_ANDROID)) {
				password = mc.decrypt(password);
			} else if (os.equals(OS_IOS)) {
				password = aes.decrypt(password);
			} else {
				throw new UsernameNotFoundException("login from unknown platform.");
			}

			String sql = "select userId,disName,password,companyDisName,companyId,type,server from user where userId=? and companyId=?";

			List<ManageUser> list = jdbcTemplate.query(sql, new Object[] { username, companyId },
					new BeanPropertyRowMapper<ManageUser>(ManageUser.class));
			// System.out.println(sql);
			ArrayList<ManageUser> userList = new ArrayList<ManageUser>(list);
			if (userList.size() > 0) {
				ManageUser user = userList.get(0);
				if (encoder.matches(password, user.getPassword())) {
					user.setPassword("");
					return user;
				} else {
					return null;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		throw new UsernameNotFoundException("User " + username + " has no GrantedAuthority");
	}

}