package com.security;

import jodd.datetime.JDateTime;

import org.apache.logging.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityController {
	
	public static Logger eLog ;
	public static Logger nLog ;
	public static Logger dLog ;
	
	public String getUpdator(){
		if(SecurityContextHolder.getContext()==null||SecurityContextHolder.getContext().getAuthentication()==null){
			return "tim";
		}else{
			ManageUser user = (ManageUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return user.getUsername();
		}
	}
	
	public String getCompanyId(){
		try {
			ManageUser user = (ManageUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return user.getCompanyId();
		} catch (Exception e) {
			e.printStackTrace();
			return "null";
		}
	}
	
	public String getType() {
		try {
			ManageUser user = (ManageUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return user.getType();
		} catch (Exception e) {
			e.printStackTrace();
			return "null";
		}
	}	
	
	public String getUpdateDate(){
		return new JDateTime().toString("YYYYMMDDhhmmss");
	}
	
	public String getServer() {
		try {
			ManageUser user = (ManageUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return user.getServer();
		} catch (Exception e) {
			e.printStackTrace();
			return "a";
		}
	}
	
}
