package dao;

import java.util.Date;

import javax.mail.internet.MimeMessage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"file:src/test/java/spring-testmail.xml"}) 
public class EmailTest extends AbstractJUnit4SpringContextTests{
	
	@Autowired
	JavaMailSenderImpl mailSender;
	
	@Test
	public void email() throws Exception{
		String[] tos={"j033312003@gmail.com","TingChunKuo@itri.org.tw"};
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true,"UTF-8");// 處理中文編碼

		helper.setSubject("這是測試郵件"); // 主題
		
		helper.setFrom(mailSender.getUsername()); // 寄件者
		helper.setTo(tos); // 收件人
		helper.setText("上限:20.0，下限:6.0，異常數值:22.86，發生時間:"+new Date().toString(), true); // 內容(HTML)
		mailSender.send(message);
		System.out.println("send");
	}
}
