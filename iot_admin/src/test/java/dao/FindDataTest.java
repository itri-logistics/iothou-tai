package dao;

import java.io.IOException;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dao.job.RoundJobDao;
import com.dao.upload.QueryDao;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"file:src/test/java/spring-testbean.xml"}) 
public class FindDataTest extends AbstractJUnit4SpringContextTests{
	
	@Autowired
	QueryDao rawDataDao ;
	
	@Test
	public void genSql() throws Exception{
		Date a=new Date();
		String str=rawDataDao.getQueryData("FFCC", "20161126", "20161205", "MACIDMACIDMA");
		Date b=new Date();
		System.out.println("cost:"+(b.getTime()-a.getTime()));
		System.out.println(str);
//		roundJobDao.findData("itri", "20161126", "20161205", "MACIDMACIDMA");
	}
}
