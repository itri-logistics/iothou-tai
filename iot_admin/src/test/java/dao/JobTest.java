package dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dao.job.RoundJobDao;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"file:src/test/java/spring-testbean.xml"}) 
public class JobTest extends AbstractJUnit4SpringContextTests{
	
//	StandardPasswordEncoder encoder=new StandardPasswordEncoder("工研院");
	
	@Autowired
	RoundJobDao roundJobDao;
	
	@Test
	public void genSql(){
		roundJobDao.proc();
	}
}
