package rs232;

import java.io.IOException;
import java.io.InputStream;
  
public class ComReceiver extends Thread{  
	
    InputStream in;    
    Rs232Input protocol = new Rs232Input();    
     
    public ComReceiver(InputStream in) {    
        this.in = in;    
    }    
        
    public void run() {    
        try {    
            int b;    
            while(true) {    
                    
                // if stream is not bound in.read() method returns -1    
                while((b = in.read()) != -1) {    
                    protocol.onReceive((byte) b);    
                }    
                protocol.onStreamClosed();    
                sleep(1000);    
            }    
        } catch (IOException e) {    
            e.printStackTrace();    
        } catch (InterruptedException e) {    
            e.printStackTrace();    
        }     
    }  
}  