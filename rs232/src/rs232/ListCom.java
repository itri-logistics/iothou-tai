  
package rs232;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.util.Enumeration;

public class ListCom {
	final static byte[] PORT_0_ENABLE={(byte)0x5A,(byte)0x00,(byte)0x12,(byte)0x00,(byte)0x02,(byte)0x00,(byte)0x01,(byte)0x11,(byte)0x7E};
	final static byte[] PORT_0_POWER_170={(byte)0x5A,(byte)0x00,(byte)0x14,(byte)0x00,(byte)0x05,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0xAA,(byte)0xBB,(byte)0x7E};
	
	final static byte[] CONTINUE_READ_MODE={(byte)0x5A,(byte)0x00,(byte)0x0E,(byte)0x00,(byte)0x01,(byte)0x00,(byte)0x0F,(byte)0x7E};
	final static byte[] START_READ={(byte)0x5A,(byte)0x00,(byte)0x50,(byte)0x00,(byte)0x00,(byte)0x50,(byte)0x7E};
	final static byte[] STOP_READ={(byte)0x5A,(byte)0x00,(byte)0x5F,(byte)0x00,(byte)0x00,(byte)0x5F,(byte)0x7E};
	Thread reading;
	SerialPort serialPort;
	
    public void connect(String portName) throws Exception {
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);

        if (portIdentifier.isCurrentlyOwned()) {
            System.out.println("Port in use!");
        } else {
            serialPort = (SerialPort) portIdentifier.open("RFID connector", 115200);
            serialPort.setSerialPortParams(115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            CommPortSender.setWriterStream(serialPort.getOutputStream());
            Thread reading=new ComReceiver(serialPort.getInputStream());
            reading.start();
            
            Thread.sleep(100);
            CommPortSender.send(STOP_READ);
            
            Thread.sleep(100);
            CommPortSender.send(new byte[]{(byte)0x5A,(byte)0x00,(byte)0x73,(byte)0x00,(byte)0x00,(byte)0x73,(byte)0x7E });
            Thread.sleep(100);
            CommPortSender.send(PORT_0_ENABLE);
            Thread.sleep(100);
            CommPortSender.send(PORT_0_POWER_170);
            
            Thread.sleep(100);
            CommPortSender.send(CONTINUE_READ_MODE);

            
            //每隔幾秒就傳送
//            while(true){
                Thread.sleep(500);
                CommPortSender.send(START_READ);
                System.out.println("start");
                
//                Thread.sleep(3000);
//                CommPortSender.send(STOP_READ);
//                System.out.println("end");
//            }
        }
    }

    
    public static void main(String[] args) throws Exception {
    	new ListCom().list();
    }

    public void list() {
        Enumeration ports = CommPortIdentifier.getPortIdentifiers();
        while(ports.hasMoreElements())
        {
            CommPortIdentifier cpIdentifier = (CommPortIdentifier)ports.nextElement();
            System.out.println(cpIdentifier.getName());
        }
    }
    
    //檢查CHECK SUM
    public void checkSum(){
    	byte[] bs={0x01,0x5F,0x00,0x02,0x00,0x00};
    	byte temp=bs[0];
    	for( int i = 1; i < bs.length; i++ ){
    		temp=(byte)(temp ^ bs[i]);
    		System.out.println(temp);
    	}
    }
}  