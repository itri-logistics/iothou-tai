package wifi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

public class C3 extends Thread {
	BufferedReader br;
	private Socket socket;
	@Override
	public void run() {
	    try {
	        socket = new Socket();
//	        socket.connect(new InetSocketAddress("127.0.0.1", 8001));
	        socket.connect(new InetSocketAddress("192.168.11.254", 8001));
	        System.out.println("Client端開始輸入傳送訊息： \n");
	        PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
	        br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
	 
	        Thread msgReceiver = new Thread(new Runnable() {
	            public void run() {
	                String str;
	                try {
	                    while ((str = br.readLine()) != null) {
	                        if (str.length() > 0)
	                            System.out.println("\n接收訊息內容>>" + str);
	                    }
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
	            }
	        });
	        msgReceiver.start();
	 
	        while (true) {
	            System.out.print("訊息內容>>");
	            String str = buff.readLine();
	            output.write(str + "\n");
	            output.flush();
	        }
	    } catch (Exception e) {
	    }
	}

	public static void main(String[] args) {
		new C3().start();
	}

}
