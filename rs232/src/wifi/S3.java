package wifi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class S3 extends Thread {
	BufferedReader br;
	private ServerSocket sk;
	public S3() {
		
	}
	public void run() {
	    try {
	        sk = new ServerSocket(8001);
	        System.out.println("Server端接收到訊息內容顯示如下...\n");
	        Socket insk = sk.accept();
	        br = new BufferedReader(new InputStreamReader(insk.getInputStream()));
	        PrintWriter output = new PrintWriter(insk.getOutputStream(), true);
	        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
	 
	        Thread msgReceiver = new Thread(new Runnable() {
	            public void run() {
	                String str;
	                try {
	                    while ((str = br.readLine()) != null) {
	                        if (str.length() > 0)
	                            System.out.println("接收訊息內容>>" + str);
	                    }
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
	            }
	        });
	        msgReceiver.start();
	 
	        while (true) {
	            System.out.print("訊息內容>>");
	            String ss = buff.readLine();
	            output.write(ss + "\n");
	            output.flush();
	        }
	    } catch (Exception e) {
	    }
	}

	public static void main(String[] args) {
		new S3().start();
	}

}
