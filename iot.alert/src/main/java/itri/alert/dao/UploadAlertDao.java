package itri.alert.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import itri.alert.bean.ConnoDataSource;
import itri.alert.utility.AlertMailPool;

/******************************************
 * 手機上傳警示紀錄Dao
 *
 * @author WeiAn create by 2017/08/11
 *******************************************/
@Service
public class UploadAlertDao {

//	@Autowired
//	JdbcTemplate jdbcTemplate;
	
	JdbcTemplate jdbcTemplate = new JdbcTemplate(ConnoDataSource.helloWorld());
	
	// 新增警示
	@Transactional
	public void uploadAlert(final String tagName,final String companyId,final String updateDate,final String macAddress,final String dataTypes,final String isCritical,
			final String alertSettingId,final String alertValue,final String alertUpdateDate) {
		String status = "0";
		KeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "insert alert_records (updateDate,macAddress,dataTypes,companyId,isCritical,status,alertUpdateDate,alertValue,alertSettingId,tagName)"
						+ " values (?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, updateDate);
				ps.setString(2, macAddress);
				ps.setString(3, dataTypes);
				ps.setString(4, companyId);
				ps.setString(5, isCritical);
				ps.setString(6, status);
				ps.setString(7, alertUpdateDate);
				ps.setString(8, alertValue);
				ps.setString(9, alertSettingId);
				ps.setString(10,tagName);
				return ps;
			}
		}, holder);
		Integer newAlertId = holder.getKey().intValue();
		String sql = "select * from alert_records where alertId=?";
		Map<String, Object> newAlert = jdbcTemplate.queryForList(sql, new Object[] { newAlertId }).get(0);
		AlertMailPool.addAlertMail(newAlert);
	}
	//更新寄送狀態
	public void updateStatus(final String alertId) {
		String status = "1";
		String sql = "update alert_records set status=? where alertId=?";
		jdbcTemplate.update(sql, new Object[] {status,alertId});
	}
}
