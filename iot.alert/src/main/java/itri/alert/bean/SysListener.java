package itri.alert.bean;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import itri.alert.utility.AlertMailPool;
import itri.alert.utility.SendEmailJob;


/**
 * Servlet implementation class SysListener
 */

public class SysListener extends HttpServlet implements ServletContextListener {
	private static final long serialVersionUID = 1L;
	@Autowired
	JdbcTemplate jdbcTemplate;
	/**
	 * Default constructor.
	 */
	public SysListener() {		

	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("contextInitialized");
    	jdbcTemplate=new JdbcTemplate(new ConnoDataSource().helloWorld());
    	List<Map<String,Object>> alerts= jdbcTemplate.queryForList("select * from alert_records where status=0");
    	List<Map<String,Object>> alertSetting= jdbcTemplate.queryForList("select * from alert_setting");
    	List<Map<String,Object>> tagDataTypeAlertRel= jdbcTemplate.queryForList("select * from tag_datatype_alert_rel");
    	System.out.println(alerts);
    	AlertMailPool.initAlertMailPool(alerts,alertSetting,tagDataTypeAlertRel);
    	SendEmailJob.init();
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {

		System.out.println("contextDestroyed");
	}

}
