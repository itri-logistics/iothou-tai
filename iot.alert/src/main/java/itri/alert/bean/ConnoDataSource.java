package itri.alert.bean;

import itri.group.param.*;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class ConnoDataSource {
	
	@Bean(name = "dataSource")
	public static DriverManagerDataSource helloWorld() {
		DriverManagerDataSource data = new DriverManagerDataSource();
		data.setUrl("jdbc:mysql://"+Pa.DB+"/itri_iot?useUnicode=true&characterEncoding=UTF-8"); //自己電腦
		data.setDriverClassName("com.mysql.jdbc.Driver");
		data.setUsername(Pa.DB_USER);
		data.setPassword(Pa.DB_PWD);
		return data;
	}
}