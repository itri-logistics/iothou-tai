package itri.alert.controller;

import itri.alert.dao.UploadAlertDao;
import itri.alert.utility.AlertMailPool;
import itri.group.param.RsMsg;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

//import com.security.SecurityController;

@Controller
@RequestMapping(value = "upload")
public class UploadAlertController{

	//@Autowired
	//UploadDao uploadDao;

	//@Autowired
	//Gson gson;

	// datetime:事件發生時間12碼(年月日時分秒)ex:161130192030
	// macId:6097AE92AE93
	// dataTypes: 用逗號分開 代表每組訊號ex:0001,0101,0201代表三組訊號
	// 四碼說明
	// AABB :AA代表port號 BB代表資料類型
	// 0001 :本體，溫度
	// 0002 :本體，濕度
	// 0102 :port1，濕度
	// 0301 :port3，溫度
	// datas用#字號分開
	// datas=
	// companyId#macId##dateTime#dataTypes

	// 會把Data拆開
	UploadAlertDao alertDao = new UploadAlertDao();
	@RequestMapping(value = "/uploadAlertData", method = RequestMethod.POST)
	public @ResponseBody Object uploadLogData(@RequestParam(value = "datas", defaultValue = "") String datas,
			ModelMap model) {
		try {
			System.out.println(datas);
			String[] data = datas.split(";");
			boolean update = false;
			for(String d : data) {
				String[] ds = datas.split("#");
				String tagName = ds[0];
				String companyId = ds[1];
				String macId = ds[2];
				String updateDate = ds[3];
				String dataTypes = ds[4];
				String isCritical = ds[5];
				String alertSettingId = ds[6];
				String alertValue = ds[7];
				String alertUpdateDate = ds[8];
				if(AlertMailPool.checkSetting(alertSettingId,alertUpdateDate,macId,dataTypes)){
					alertDao.uploadAlert(tagName,companyId,updateDate,macId,dataTypes,isCritical,alertSettingId,alertValue,alertUpdateDate);
				}else{
					update = true;					
				}
			}
			
			if(update) {
				return new RsMsg("error","update your setting");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
		return new RsMsg("success", 200);
	}
	
	@RequestMapping(value = "/updateAlertSettingPool", method = RequestMethod.GET)
	public @ResponseBody Object updateAlertSettingPool(@RequestParam(value = "datas", defaultValue = "") String datas,
			ModelMap model) {
		try {
			AlertMailPool.updateAlertPool();	
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
		return new RsMsg("success", 200);
	}
	
	@RequestMapping(value = "/updateTagDataTypeAlertRel", method = RequestMethod.GET)
	public @ResponseBody Object updateTagDataTypeAlertRel(ModelMap model) {
		try {
			AlertMailPool.updateTagDataTypeAlertRel();
		} catch (Exception e) {
			e.printStackTrace();
			return new RsMsg("error", e.getMessage());
		}
		return new RsMsg("success", 200);
	}
}