package itri.alert.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import itri.alert.utility.SendEmailJob;

import itri.alert.bean.ConnoDataSource;
import jodd.datetime.JDateTime;

/******************************************
 * Alert資料的Pool
 *
 * @author WeiAn create by 2017/08/11
 *******************************************/
public class AlertMailPool {
	@Autowired
	static
	JdbcTemplate jdbcTemplate;
	
	private static Map<String, AlertMail> alertMailPool = new HashMap();// <alertId,Alert.class>
	private static Map<String, AlertSetting> alertSettingPool = new HashMap();// <alertId,Alert.class>
	private static Map<String, TagDataTypeAlertRel> tagDataTypeAlertRelPool = new HashMap();// <alertId,Alert.class>
	private static Map<String, String> alertMailLogPool = new HashMap();// <tagName.updateDate>
	
	public static void initAlertMailPool(List<Map<String, Object>> alerts,List<Map<String, Object>> alertSetting,List<Map<String, Object>> tagDataTypeAlertRel) {
	
		for (Map<String, Object> as : alertSetting) {
			alertSettingPool.put(as.get("alertSettingId").toString(),
			new AlertSetting(as.get("alertSettingId").toString(), as.get("disName").toString(),as.get("low").toString(),
			as.get("high").toString(),as.get("afterMilliSecondAlert").toString(), as.get("emails").toString(),
			as.get("criticalEmails").toString(), as.get("phones").toString(),as.get("updateDate").toString()));
		}
		
		for (Map<String, Object> a : alerts) {
			AlertSetting newAlertSetting = alertSettingPool.get(a.get("alertSettingId").toString());
			if(newAlertSetting!=null && a.get("status").toString().equals("0")){
				alertMailPool.put(a.get("alertId").toString(),new AlertMail(a.get("tagName").toString(),a.get("alertId").toString(),newAlertSetting,a.get("macAddress").toString(),
						a.get("dataTypes").toString(),a.get("isCritical").toString(),
						a.get("updateDate").toString(),a.get("alertValue").toString(),a.get("alertUpdateDate").toString(),a.get("dataTypes").toString()));
			}
		}
		
		for (Map<String, Object> td : tagDataTypeAlertRel) {
			tagDataTypeAlertRelPool.put(td.get("mac").toString()+td.get("dataType").toString(),new TagDataTypeAlertRel(td.get("dataType").toString(),td.get("mac").toString(),td.get("alertSettingId").toString()));
		}
		sendAlertMail();
	}
	
	//新增AlertMail
	public static void addAlertMail(Map<String, Object> newAlert) {
		AlertSetting newAlertSetting = alertSettingPool.get(newAlert.get("alertSettingId").toString());
		if(alertMailLogPool.get(newAlert.get("tagName").toString())!=null){
			String updateDateString = newAlert.get("updateDate").toString();
	        DateFormat dateFormat = new SimpleDateFormat("YYYYMMDDhhmmss");
	        String updateDateLogString = alertMailLogPool.get(newAlert.get("tagName").toString());
		     try {
		            Date updateDate = dateFormat.parse(updateDateString);
		            Date updateDateLog = dateFormat.parse(updateDateLogString);
		            if((updateDate.getTime() - updateDateLog.getTime()) > Integer.valueOf((alertSettingPool.get(newAlert.get("alertSettingId").toString()).afterMilliSecondAlert))){
		        		System.out.println(newAlertSetting);
		        		alertMailLogPool.remove(newAlert.get("tagName").toString());
		        		alertMailLogPool.put(newAlert.get("tagName").toString(), newAlert.get("updateDate").toString());
		        		alertMailPool.put(newAlert.get("alertId").toString(),new AlertMail(newAlert.get("tagName").toString(),newAlert.get("alertId").toString(),newAlertSetting,newAlert.get("macAddress").toString(),newAlert.get("dataTypes").toString(),
		        			newAlert.get("isCritical").toString(),newAlert.get("updateDate").toString(),newAlert.get("alertValue").toString(),
		        			newAlert.get("alertUpdateDate").toString(),newAlert.get("dataTypes").toString()));
		        		sendAlertMail();
		        		
		            }
		        } catch (ParseException e) {
		            e.printStackTrace();
		        }
		     
		     
		}else{
			alertMailLogPool.put(newAlert.get("tagName").toString(), newAlert.get("updateDate").toString());
    		alertMailPool.put(newAlert.get("alertId").toString(),new AlertMail(newAlert.get("tagName").toString(),newAlert.get("alertId").toString(),newAlertSetting,newAlert.get("macAddress").toString(),newAlert.get("dataTypes").toString(),
        			newAlert.get("isCritical").toString(),newAlert.get("updateDate").toString(),newAlert.get("alertValue").toString(),
        			newAlert.get("alertUpdateDate").toString(),newAlert.get("dataTypes").toString()));
        		sendAlertMail();
		}
	}
	
	//寄發Mail
	public static void sendAlertMail(){
		for (AlertMail alertMail : alertMailPool.values()) {
			System.out.println(alertMail.alertId);
			String dataTypeDisName = getDataTypeDisName(alertMail.dataTypes);
			if(alertMail.isCritical.equals("1")){
				SendEmailJob.sendMail(alertMail.criticalEmails,
						String.format("%s即時資料有%s狀況發生", dataTypeDisName + " " + alertMail.tagName,
								alertMail.disName),
						String.format("%s即時資料有%s狀況發生，上限:%s，下限:%s，異常數值:%s，發生時間點:%s",
								alertMail.tagName + " " + dataTypeDisName, alertMail.disName,
								String.valueOf(alertMail.high), String.valueOf(alertMail.low),
								String.valueOf(alertMail.alertValue),
								new JDateTime(alertMail.updateDate,"YYYYMMDDhhmmss").toString("YYYY/MM/DD hh:mm:ss")),alertMail.alertId);
			}else{
				SendEmailJob.sendMail(alertMail.emails,
						String.format("%s即時資料有%s狀況發生", dataTypeDisName + " " + alertMail.tagName,
								alertMail.disName),
						String.format("%s即時資料有%s狀況發生，上限:%s，下限:%s，異常數值:%s，發生時間點:%s",
								alertMail.tagName + " " + dataTypeDisName, alertMail.disName,
								String.valueOf(alertMail.high), String.valueOf(alertMail.low),
								String.valueOf(alertMail.alertValue),
								new JDateTime(alertMail.updateDate,"YYYYMMDDhhmmss").toString("YYYY/MM/DD hh:mm:ss")),alertMail.alertId);	
				
			}
		}
		synchronized (alertMailPool) {
			alertMailPool.clear();
		}
	}
	
	public static String getDataTypeDisName(String dataType){
		String port = "";
		String type = "";
		switch (dataType.substring(0,2)) {
        	case "00":
        		port = "本體";
        		break;
        	case "01":
        		port = "port1";
        		break;
        	case "02":
        		port = "port2";
        		break;
        	case "03":
        		port = "port3";
        		break;
        	case "04":
        		port = "port4";
        		break;
        	case "05":
        		port = "手機";
        		break;
        	default:
        		break;
        }
		switch (dataType.substring(2,4)) {
    	case "01":
    		type = "溫度（℃）";
    		break;
    	case "02":
    		type = "濕度（%rH）";
    		break;
    	case "05":
    		type = "光度（L）";
    		break;
    	case "06":
    		type = "電流（A）";
    		break;
    	case "07":
    		type = "電量（%）";
    		break;
    	case "08":
    		type = "經緯度";
    		break;
    	default:
    		break;
		}
		return port+type;
	} 
	//檢查關聯以及設定
	public static boolean checkSetting(String alertSettingId,String updateDate,String macAddress,String dataType) {
		if(updateDate.equals(alertSettingPool.get(String.valueOf(alertSettingId)).updateDate)){
			TagDataTypeAlertRel tagDataTypeAlertRel = tagDataTypeAlertRelPool.get(macAddress+dataType);
			System.out.println(tagDataTypeAlertRel.dataType);
			System.out.println(tagDataTypeAlertRel.alertSettingId);
			System.out.println(macAddress+dataType);
			if(dataType.equals(tagDataTypeAlertRel.dataType)&& (alertSettingId.equals(tagDataTypeAlertRel.alertSettingId))){
				return true;
			}else{
				if(!dataType.equals(tagDataTypeAlertRel.dataType)){
					System.out.println("dataType");
					System.out.println(dataType);
					System.out.println(tagDataTypeAlertRel.dataType);
				}
				if(!alertSettingId.equals(tagDataTypeAlertRel.alertSettingId)){
					System.out.println("AlertRel");
					System.out.println(alertSettingId);
					System.out.println(tagDataTypeAlertRel.alertSettingId);
				}
				return false;
			}
		}else{
			System.out.println("Alert Setting");
			System.out.println(alertSettingId);
			System.out.println(updateDate);
			System.out.println(macAddress);
			return false;
		}
	}
		
	
	//更新關聯pool
	public static void updateTagDataTypeAlertRel(){
		System.out.println("Update tag data type alert rel pool!");
    	jdbcTemplate=new JdbcTemplate(new ConnoDataSource().helloWorld());
    	List<Map<String,Object>> tagDataTypeAlertRel= jdbcTemplate.queryForList("select * from tag_datatype_alert_rel");
		for (Map<String, Object> td : tagDataTypeAlertRel) {
			tagDataTypeAlertRelPool.put(td.get("mac").toString()+td.get("dataType").toString(),new TagDataTypeAlertRel(td.get("dataType").toString(),td.get("mac").toString(),td.get("alertSettingId").toString()));
		}
		
	}
	//更新設定pool
	public static void updateAlertPool(){
		System.out.println("Update alert pool!");
    	jdbcTemplate=new JdbcTemplate(new ConnoDataSource().helloWorld());
    	List<Map<String,Object>> alertSetting= jdbcTemplate.queryForList("select * from alert_setting");
		for (Map<String, Object> as : alertSetting) {
			System.out.println(as.get("alertSettingId").toString());
			alertSettingPool.put(as.get("alertSettingId").toString(),
					new AlertSetting(as.get("alertSettingId").toString(), as.get("disName").toString(),as.get("low").toString(),
							as.get("high").toString(),as.get("afterMilliSecondAlert").toString(), as.get("emails").toString(),
							as.get("criticalEmails").toString(), as.get("phones").toString(),as.get("updateDate").toString()));
		}
		
	}
}


class AlertMail {
	public String tagName;
	public String alertId;
	public String alertSettingId;
	public String disName;// alert的名稱 之後會通報xxx異常
	public String low, high;
	public String afterMilliSecondAlert; // 持續發生多少秒後alert
	public String emails;
	public String criticalEmails;
	public String phones;
	public String companyId;
	public String updateDate;
	public String macAddress;
	public String dataTypes;
	public String isCritical;
	public String alertValue;
	public String alertUpdateDate;

	public AlertMail(String tagName,String alertId,AlertSetting alertSetting, String macAddress,String dataType,String isCritical,String updateDate,String alertValue,String alertUpdateDate,String dataTypes) {
		super();
		this.tagName = tagName;
		this.alertId = alertId;
		this.alertSettingId = alertSetting.alertSettingId;
		this.disName = alertSetting.disName;
		this.low = alertSetting.low;
		this.high = alertSetting.high;
		this.afterMilliSecondAlert = alertSetting.afterMilliSecondAlert;
		this.emails = alertSetting.emails;
		this.criticalEmails = alertSetting.criticalEmails;
		this.phones = alertSetting.phones;
		this.updateDate = alertSetting.updateDate;
		this.alertValue = alertValue;
		this.alertUpdateDate = alertUpdateDate;
		this.dataTypes = dataTypes;
		this.isCritical = isCritical;
	}
}

class TagDataTypeAlertRel {
	public String dataType;
	public String mac;
	public String relKey;
	public String alertSettingId;

	public TagDataTypeAlertRel(String dataType, String mac, String alertSettingId) {
		super();
		this.alertSettingId = alertSettingId;
		this.mac = mac;
		this.dataType = dataType;
		this.relKey = mac+dataType;
	}
}

class AlertSetting {
	public String alertSettingId;
	public String disName;// alert的名稱 之後會通報xxx異常
	public String low, high;
	public String afterMilliSecondAlert; // 持續發生多少秒後alert
	public String emails;
	public String criticalEmails;
	public String phones;
	public String updateDate;

	public AlertSetting(String alertSettingId, String disName, String low, String high, String afterMilliSecondAlert, String emails, String criticalEmails, String phones, String updateDate) {
		super();
		this.alertSettingId = alertSettingId;
		this.disName = disName;
		this.low = low;
		this.high = high;
		this.afterMilliSecondAlert = afterMilliSecondAlert;
		this.emails = emails;
		this.criticalEmails = criticalEmails;
		this.phones = phones;
		this.updateDate = updateDate;
	}
}
