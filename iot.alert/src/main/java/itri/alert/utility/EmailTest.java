package itri.alert.utility;

import java.util.Date;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

public class EmailTest {

	static JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	
	public static void main(String[] args) {
		String[] tos={"j033312003@gmail.com","TingChunKuo@itri.org.tw"};		

		try {
			
			mailSender.setHost("210.59.228.37");
			mailSender.setPort(25);
			mailSender.setUsername("iot@itrilogistics.org");
			mailSender.setPassword("itri#000itri");
			Properties properties = new Properties();
			properties.setProperty("mail.smtp.auth", "true");
			mailSender.setJavaMailProperties(properties);
			
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true,"UTF-8");// 處理中文編碼
			
			helper.setSubject("這是測試郵件"); // 主題
			
			helper.setFrom(mailSender.getUsername()); // 寄件者
			helper.setTo(tos); // 收件人
			helper.setText("上限:20.0，下限:6.0，異常數值:22.86，發生時間:"+new Date().toString(), true); // 內容(HTML)
			mailSender.send(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		System.out.println("test mail send");
	}

}
