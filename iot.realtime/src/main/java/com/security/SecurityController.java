package com.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import itri.group.param.LoggerPa;
import jodd.datetime.JDateTime;

public class SecurityController {
	public static Logger eLog = LogManager.getLogger(LoggerPa.LOGGER_ERROR);
	public static Logger nLog = LogManager.getLogger(LoggerPa.LOGGER_NET_ERR);;
	public static Logger dLog = LogManager.getLogger(LoggerPa.LOGGER_DEBUG);;
	
	public String getUpdateDate(){
		return new JDateTime().toString("YYYYMMDDhhmmss");
	}
}
