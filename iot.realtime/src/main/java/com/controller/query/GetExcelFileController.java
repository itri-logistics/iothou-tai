package com.controller.query;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.query.QueryRealTimeDao;
import com.security.SecurityController;
import com.utility.DataSizeLimitUtility;
import com.utility.ExcelUtility;

import itri.group.param.RsMsg;

/**
 * 下載即時資料的Excel
 * /getExcelFile 
 * 
 */
public class GetExcelFileController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	QueryRealTimeDao queryRealTimeDao = new QueryRealTimeDao();
	ExcelUtility excelUtility = new ExcelUtility();
	
    public GetExcelFileController() {
        super();       
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String companyId = request.getParameter("companyId");
		String sd = request.getParameter("sd");
		String ed = request.getParameter("ed");
		String macsStr = request.getParameter("macsStr");
		
		try {
			String[] macArray = macsStr.split(",");
			if(DataSizeLimitUtility.outOfLimit(macArray, sd, ed)){
				response.setContentType("text/html; charset=utf-8");
				PrintWriter writer = response.getWriter();
				writer.println("檔案過大，無法下載，請減少選取的Tag數或縮短查詢的紀錄區間。");
				writer.println("Tag數量 * 查詢天數需小於70。");
				writer.flush();
				writer.close();
				return;
			}
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=record_"+sd+"_"+ed+".xls");
			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			final OutputStream outStream = response.getOutputStream();
			HashMap<String, String> tagDataMap = new HashMap<String, String>();
			
			for (String mac : macArray) {
				tagDataMap.put(mac, queryRealTimeDao.getQueryData(companyId, sd, ed, mac));
			}
			
			//System.out.println(tagDataMap.toString());
			outByteStream = excelUtility.exportOutRecordController(outByteStream, tagDataMap);
			outStream.write(outByteStream.toByteArray());
			outStream.close();
			outByteStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			response.setContentType("text/html; charset=utf-8");
			SecurityController.eLog.error("get excel error: "+e.getMessage());
			//PrintWriter out = response.getWriter();
			//out.print("get excel error: "+e.getMessage());
			//out.flush();
			//out.close();
		}
		
	}

}
