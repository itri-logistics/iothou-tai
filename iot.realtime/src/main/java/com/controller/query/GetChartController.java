package com.controller.query;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import com.dao.query.QueryRealTimeDao;
import com.security.SecurityController;
import com.utility.ChartUtility;

/**
 * 下載圖片
 * /getChart
 */
public class GetChartController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	QueryRealTimeDao queryRealTimeDao = new QueryRealTimeDao();
	ChartUtility chartUtility = null;

    public GetChartController() {
        super();        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String companyId = request.getParameter("companyId");
		String sd = request.getParameter("sd");
		String ed = request.getParameter("ed");
		String datasStr = request.getParameter("datasStr");
		
		try {
			HashMap<String, String> dataInfoMap = queryRealTimeDao.getDataInQueryIntervalByDataType(companyId, sd, ed,
					datasStr);
			HashMap<String, String> dataMap = new HashMap<>();
			float max = 100, min = 0;
			Iterator<String> k = dataInfoMap.keySet().iterator();
			while (k.hasNext()) {
				String key = k.next();
				if(key.equals("max")){
					max = Float.parseFloat(dataInfoMap.get(key));
				}else if(key.equals("min")){
					min = Float.parseFloat(dataInfoMap.get(key));
				}else{
					StringBuilder dataType = new StringBuilder();
					String[] keyArray = key.split("-");
					
					for(int i = 0;i<keyArray.length-1;i++)
						dataType.append(keyArray[i]+"-");
					
					if(keyArray[keyArray.length - 1].startsWith("00")){
						dataType.append("本體");
					}else{
						dataType.append("port"+keyArray[keyArray.length - 1].substring(1,2));
					}
					
					if(key.endsWith("01")){
						dataType.append("溫度");
					}else if(key.endsWith("02")){
						dataType.append("濕度");
					}
					
					dataMap.put(dataType.toString(), dataInfoMap.get(key));
				}
			}		
			
			if(chartUtility == null){
				chartUtility = new ChartUtility(getServletContext());
			}
			
			File img = chartUtility.getChart(dataMap, companyId, min, max);
			String mimeType = getServletContext().getMimeType(img.getAbsolutePath());
	        if (mimeType == null) { 
	            mimeType = "application/octet-stream";
	        }
	        
	        response.setContentType(mimeType);
	        response.setContentLength((int) img.length());
	        
			FileInputStream inStream = new FileInputStream(img);
			byte[] bs = IOUtils.toByteArray(inStream);
			
			inStream.close();
			
			OutputStream outStream = response.getOutputStream();	        
	        outStream.write(bs);
	        outStream.close();     
			
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("get Chart error: "+e.getMessage());
			response.setContentType("text/html; charset=utf-8");
			PrintWriter out = response.getWriter();
			out.print("get Chart error: "+e.getMessage());
			out.flush();
			out.close();			
		}
		
	}

}
