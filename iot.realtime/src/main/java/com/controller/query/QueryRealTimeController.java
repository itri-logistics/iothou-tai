package com.controller.query;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.query.QueryRealTimeDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.RsMsg;

/**
 * Servlet implementation class QueryRealTimeController
 */
public class QueryRealTimeController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Gson gson = new Gson();
	
	QueryRealTimeDao queryRealTimeDao = new QueryRealTimeDao();
	
	public QueryRealTimeController() {
		super();
	}

	// /query/{func}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		String[] pathParts = pathInfo.split("/");
		String func = pathParts[1]; // {func}
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();

		Object result = 200;
		
		try {
			
			String companyId = request.getParameter("companyId");
			
			if (func.equals("getNewestTagsData")) {// 取得Tag最新的即時資料				
				
				String macIdDataTypes = request.getParameter("macIdDataTypes")==null ? "":request.getParameter("macIdDataTypes");
				result = queryRealTimeDao.getNewestTagsData(companyId, macIdDataTypes);

			} else if (func.equals("getTagsInQueryInterval")) {// 取得查詢時間內有紀錄的Tag
				
				String sd = request.getParameter("sd");
				String ed = request.getParameter("ed");
				
				result = queryRealTimeDao.getTagsInQueryInterval(companyId, sd, ed);
				
			} else if (func.equals("getQueryData")) {// 一次取得一個Tag裡面所有類型的資料

				String sd = request.getParameter("sd");
				String ed = request.getParameter("ed");
				String macsStr = request.getParameter("macsStr");
				
				result = queryRealTimeDao.getQueryData(companyId, sd, ed, macsStr);				
			} else if (func.equals("getAvailableDataInQueryInterval")) { // 取得查詢時間區間內可用的Data（單位：小時）
				String sd = request.getParameter("sd");
				String ed = request.getParameter("ed");
				result = queryRealTimeDao.getAvailableDataInQueryInterval(companyId, sd, ed);
			 
			} else if (func.equals("getDataInQueryIntervalByDataType")) { // 一次取得一種類型的記錄資訊
				String sd = request.getParameter("sd");
				String ed = request.getParameter("ed");
				String datasStr = request.getParameter("datasStr");
				String dataType = request.getParameter("dataType");
				result = queryRealTimeDao.getInfoInQueryIntervalByDataType(companyId, sd, ed, datasStr, dataType);
			} 
			
			
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("query data error: "+e.getMessage());
			out.print(gson.toJson(new RsMsg("error", e.getMessage())));
			out.flush();
			out.close();
		}
		
		
		out.print(gson.toJson(new RsMsg("success", result)));
		out.flush();
		out.close();

	}

}
