package com.utility;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.context.request.NativeWebRequest;

import com.dao.upload.RealTimeDataDao;
import com.security.SecurityController;
import jodd.datetime.JDateTime;

/******************************************
 * 即時資料Pool
 *
 * 每個整分會寫一次檔
 *
 * @author ATone create by 2017/08/04
 *******************************************/
public class RealTimeDataPool {

	private final static String ROOT_PATH = System.getProperty("user.home");
	private final static String RAW_DIR = ROOT_PATH + File.separator + "iot_raw";
	private final static String RAW_FILE_TYPE = ".rawTxt_rt";

	// <comapnyId, <macId_dataType, Value>>
	private static HashMap<String, ConcurrentHashMap<String, String>> tagDataPool = new HashMap<>();
	private static HashMap<String, ConcurrentHashMap<String, Long>> tagActive = new HashMap<>(); // 紀錄Tag的最後出現時間
	private static HashMap<String, HashMap<String, HashSet<String>>> todayInsert = new HashMap<>(); // 紀錄今日已insert的Tag
	
	private static String today = (new JDateTime().toString("YYYYMMDD")).substring(2);

	public static void init() { //將今日已有的檔案讀進來
		List<Map<String, Object>> list = RealTimeDataDao.getInsertedIndex(today);
		for (Map<String, Object> map : list) {
			String companyId = map.get("companyId").toString();
			String mac = map.get("mac").toString();
			String dataType = map.get("dataType").toString();
			if(!todayInsert.containsKey(companyId)){
				todayInsert.put(companyId, new HashMap<String, HashSet<String>>());
			}
			
			if(!todayInsert.get(companyId).containsKey(mac)){
				todayInsert.get(companyId).put(mac, new HashSet<String>());
			}
			
			todayInsert.get(companyId).get(mac).add(dataType);
		}
	}
	
	public static void updateTagData(String companyId, String tagMac, String dataType, String data) {

		if(!todayInsert.containsKey(companyId)){
			todayInsert.put(companyId, new HashMap<String, HashSet<String>>());
		}
		
		if (!tagDataPool.containsKey(companyId)) {
			tagDataPool.put(companyId, new ConcurrentHashMap<>());
			tagActive.put(companyId, new ConcurrentHashMap<>());
		}
		//檢查是否已寫入資料庫
		if(!todayInsert.get(companyId).containsKey(tagMac)){//若未寫入，則產生index
			if(RealTimeDataDao.insertToSQL(today, companyId, tagMac, dataType)){				
				todayInsert.get(companyId).put(tagMac, new HashSet<String>());
				todayInsert.get(companyId).get(tagMac).add(dataType);
			}
		}else{
			if(!todayInsert.get(companyId).get(tagMac).contains(dataType)){
				if(RealTimeDataDao.insertToSQL(today, companyId, tagMac, dataType)){
					todayInsert.get(companyId).get(tagMac).add(dataType);
				}
			}
		}
		tagDataPool.get(companyId).put(tagMac + "_" + dataType, data);
		// 更新tag的出現時間
		tagActive.get(companyId).put(tagMac + "_" + dataType, new JDateTime().getTimeInMillis());
	}
	
	public static void updateDate() {	//每日0點要做的工作
		today = (new JDateTime().toString("YYYYMMDD")).substring(2);
		//清空todayInsert
		synchronized (todayInsert) {
			todayInsert.clear();
		}
		
		//檢查昨天和前天是否有未寫入index表的檔案
		RealTimeDataDao.checkAndInsert();
	}

	public static void writeFile() {

		long now = new JDateTime().getTimeInMillis();
		// System.out.println(new JDateTime(now).toString("YYYYMMDDhhmmss"));
		SecurityController.dLog.debug("write file job start");
		int count = 0;
		long st = System.nanoTime();
		for (String com : tagActive.keySet()) {
			ConcurrentHashMap<String, String> comTagDataPool = tagDataPool.get(com);
			ConcurrentHashMap<String, Long> comTagActive = tagActive.get(com);
			String[] keys = new String[comTagActive.size()];
			comTagActive.keySet().toArray(keys);
			for (String key : keys) {
				// System.out.println(comTagActive.get(key));
				if (now - comTagActive.get(key) < 300000) {// 5分鐘有更新，寫檔
					RealTimeDataDao.insertData(now, com + "_" + key, comTagDataPool.get(key));
				}
				// else {
				// comTagDataPool.remove(key);
				// comTagActive.remove(key);
				// }
				count++;
			}
		}

		// System.out.println("write file job compelete. time = "
		// + String.valueOf(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() -
		// st)) + " ms, "
		// +"data length = " + count);

		SecurityController.nLog.info("write file job compelete. time = "
				+ String.valueOf(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - st)) + " ms, " + "data length = "
				+ count);
	}

	public static HashMap<String, HashMap<String, String>> getNewTagsData(String companyId, String macIdDataTypes) {
		HashMap<String, HashMap<String, String>> result = new HashMap<>();
		if (!tagDataPool.containsKey(companyId) || !tagActive.containsKey(companyId)) {
			return result;
		}
		String[] tagDatatypes;
		if (macIdDataTypes.isEmpty()) {
			tagDatatypes = new String[tagDataPool.get(companyId).size()];
			tagDataPool.get(companyId).keySet().toArray(tagDatatypes);
		} else {
			tagDatatypes = macIdDataTypes.split(",");
		}
		ConcurrentHashMap<String, String> tagData = tagDataPool.get(companyId);
		ConcurrentHashMap<String, Long> tagTime = tagActive.get(companyId);
		for (String key : tagDatatypes) {
			// System.out.println(key);
			String[] tagDatatype = key.split("_");
			HashMap<String, String> tag = new HashMap<>();
			tag.put("macId", tagDatatype[0]);
			tag.put("dataType", tagDatatype[1]);
			tag.put("lastData", tagData.containsKey(key) ? tagData.get(key) : "");
			tag.put("lastDataDatetime", tagTime.containsKey(key) ? tagTime.get(key).toString() : "");
			tag.put("lastDataDatetimeStr",
					tagTime.containsKey(key) ? new JDateTime(tagTime.get(key)).toString("YYYYMMDDhhmmss") : "");
			result.put(key, tag);
		}

		return result;
	}
}
