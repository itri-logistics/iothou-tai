package com.bean;

import itri.group.param.LoggerPa;
import itri.group.param.Pa;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.security.SecurityController;
import com.utility.RealTimeDataPool;


public class SysListener extends HttpServlet implements ServletContextListener{
	
	private static final long serialVersionUID = 1L;

	@Override
    public void contextDestroyed(ServletContextEvent arg0) {
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
 
    	File home=new File(Pa.HOME_DIR);
    	if(!home.exists())
    		home.mkdir();
    	
        sce.getServletContext().setAttribute("js_version", Pa.JS_VERSION);
        sce.getServletContext().setAttribute("lib_link", Pa.CDN_ADMIN_LIB_LINK);
        
        RealTimeDataPool.init();
        
        //System.out.println("contextInitialized");
        
    }

}