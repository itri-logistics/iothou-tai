package com.dao.tag;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.bean.ConnoDataSource;

import itri.group.param.Pa;

public class TagDao {
	
	JdbcTemplate jdbcTemplate = new JdbcTemplate(ConnoDataSource.getConnDataSource());
	
	public String getTagName(String mac) {
		String sql=String.format("select name from tag where mac='%s';", mac);

		List<Map<String, Object>> qMap = null;
		try {			
			qMap = jdbcTemplate.queryForList(sql);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		if(qMap.isEmpty()){
			System.err.println("找不到tag名稱，請確認tag是否已登錄。mac = "+mac);
		}

		return qMap.isEmpty() ? mac : qMap.get(0).get("name").toString();
		
	}
	
	public String getPlaceId(String mac) {
		String sql=String.format("select placeId from tag where mac='%s';", mac);

		List<Map<String, Object>> qMap = null;
		try {			
			qMap = jdbcTemplate.queryForList(sql);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		if(qMap.isEmpty()){
			System.err.println("找不到場域，請確認場域是否存在。mac = "+mac);
			return "";
		}else{
			if( qMap.get(0).get("placeId") == null){
				return "";
			}
		}

		return qMap.get(0).get("placeId").toString();
		
	}
	
	public Integer getTagNumByPlace(String placeId) {
		String sql=String.format("select count(*) as tagNum from tag where placeId = '%s';", placeId);

		List<Map<String, Object>> qMap = null;
		try {			
			qMap = jdbcTemplate.queryForList(sql);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		if(qMap.isEmpty()){
			System.err.println("找不到場域，請確認場域是否存在。placeId = "+placeId);
		}

		return qMap.isEmpty() ? 0 : Integer.parseInt(qMap.get(0).get("tagNum").toString());
		
	}

	public Object find(String mac, String disName, String type, String placeId, String companyId, String page) {
		StringBuilder sql = new StringBuilder("select * from tag where ");
		List query = new ArrayList();
		if (StringUtils.isNotBlank(mac)) {
			sql.append("mac = ? and ");
			query.add(mac);
		}	
		if (StringUtils.isNotBlank(disName)) {
			sql.append("name like ? and ");
			query.add("%"+disName+"%");
		}		
		if (StringUtils.isNotBlank(type)) {
			sql.append("type like ? and ");
			query.add("%"+type+"%");
		}
		if (StringUtils.isNotBlank(placeId)) {
			sql.append("placeId = ? and ");
			query.add(placeId);
		}
		if (StringUtils.isNotBlank(companyId)) {
			sql.append("companyId like ? and ");
			query.add(companyId);
		}
		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		}else{
			sql = sql.delete(sql.length() - 4, sql.length());
		}
		
		if (StringUtils.isNotBlank(page)) {
			sql.append(String.format("limit  %s,%d ", (Integer.parseInt(page) - 1) * Pa.PAGE_NUM, Pa.PAGE_NUM));
		} 

		//System.out.println(sql);
		List list = jdbcTemplate.queryForList(sql.toString(), query.toArray());
		return list;
	}

	public Object findPage(String name, String type, String placeId, String companyId) {

		StringBuilder sql = new StringBuilder("select count(*) as total from tag where ");
		List query = new ArrayList();
		if (StringUtils.isNotBlank(name)) {
			sql.append("name like ? and ");
			query.add("%"+name+"%");
		}
		if (StringUtils.isNotBlank(type)) {
			sql.append("type like ? and ");
			query.add("%"+type+"%");
		}
		if (StringUtils.isNotBlank(placeId)) {
			sql.append("placeId = ? and ");
			query.add(placeId);
		}
		if (StringUtils.isNotBlank(companyId)) {
			sql.append("companyId like ? and ");
			query.add(companyId);
		}
		
		if (query.isEmpty()) {
			sql = sql.delete(sql.length() - 6, sql.length());
		}else{
			sql = sql.delete(sql.length() - 4, sql.length());
		}
		
		//System.out.println(sql);
		Map<String, Object> rs = jdbcTemplate.queryForList(sql.toString(), query.toArray()).get(0);
		rs.put("page_num", Pa.PAGE_NUM);
		return rs;
	}

	@Transactional
	public void update(String mac, String name, String type,String p1, String p2, String p3, String p4, String r,
			String placeId, String companyId, String updateDate) {
		
		String port1 = "00",port2 = "00",port3 = "00",port4 = "00";
		Integer	rate = 15;		
		
		if (StringUtils.isNotBlank(p1)) {
			port1 = p1;
		}
		if (StringUtils.isNotBlank(p2)) {
			port2 = p2;
		}
		if (StringUtils.isNotBlank(p3)) {
			port3 = p3;
		}
		if (StringUtils.isNotBlank(p4)) {
			port4 = p4;
		}
		if (StringUtils.isNotBlank(r)) {
			rate = Integer.parseInt(r);
		}
		String condi = "update tag set name=?,type=?,port1=?,port2=?,port3=?,port4=?,rate=?,"
				+ "placeId=?,companyId=?,updateDate=? where mac=?";
		jdbcTemplate.update(condi, new Object[] { name, type,port1, port2, port3, port4,rate,
				StringUtils.isNotBlank(placeId)? (int)Double.parseDouble(placeId):null, companyId, updateDate, mac});

	}

	@Transactional
	public void create(String mac, String name, String type, String p1, String p2, String p3, String p4,String r,
			String placeId, String companyId, String updateDate) {
		
		String port1 = "00",port2 = "00",port3 = "00",port4 = "00";
		Integer	rate = 15;
		
		if (StringUtils.isNotBlank(p1)) {
			port1 = p1;
		}
		if (StringUtils.isNotBlank(p2)) {
			port2 = p2;
		}
		if (StringUtils.isNotBlank(p3)) {
			port3 = p3;
		}
		if (StringUtils.isNotBlank(p4)) {
			port4 = p4;
		}
		if (StringUtils.isNotBlank(r)) {
			rate = Integer.parseInt(r);
		}
		String condi = "insert tag (mac,name,type,port1,port2,port3,port4,rate,placeId,companyId,updateDate)"
				+ " values (?,?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(condi, new Object[] { mac, name, type, port1, port2, port3, port4,rate,
				StringUtils.isNotBlank(placeId)? (int)Double.parseDouble(placeId):null, companyId, updateDate });
	}
	
	@Transactional
	public void del(String mac){
		//先移除場域關聯
		String sql=String.format("delete from sensor_position where mac='%s' ", mac);
		//System.out.println(sql);
		jdbcTemplate.execute(sql);
		
		//移除警示相關
		sql=String.format("delete from tag_datatype_alert_rel where mac='%s' ", mac);
		//System.out.println(sql);
		jdbcTemplate.execute(sql);
		
		//移除看板相關
		sql=String.format("delete from dashboard where macId='%s' ", mac);
		//System.out.println(sql);
		jdbcTemplate.execute(sql);
		
		sql=String.format("delete from tag where mac='%s' ", mac);		
		//System.out.println(sql);
		jdbcTemplate.execute(sql);		
	}

}
