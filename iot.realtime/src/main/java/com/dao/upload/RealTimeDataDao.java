package com.dao.upload;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.bean.ConnoDataSource;
import com.itextpdf.awt.geom.misc.RenderingHints.Key;
import com.security.SecurityController;

import jodd.datetime.JDateTime;

public class RealTimeDataDao {

	static JdbcTemplate jdbcTemplate = new JdbcTemplate(ConnoDataSource.getConnDataSource());

	private final static String ROOT_PATH = System.getProperty("user.home");
	private final static String DATA_DIR = ROOT_PATH + File.separator + "iot_data";
	private final static String txtEndFileType = ".txt_rt";

	public static void insertData(long time, String key, String value) {
		String[] keys = key.split("_");
		String date = new JDateTime(time).toString("YYYYMMDDhhmmss");
		String fileName = DATA_DIR + File.separator + keys[0] + File.separator + date.substring(2, 4) + File.separator
				+ date.substring(4, 6) + File.separator + date.substring(6, 8) + File.separator + keys[1] + "-"
				+ keys[2] + txtEndFileType;
		try {
			File file = new File(fileName);
			FileUtils.writeStringToFile(file, date.substring(2) + value + ",", true);
			SecurityController.dLog.debug("insert data to file, key = " + key + ", value = " + value);
			// System.out.println("insert data to file, key = " + key + ", value
			// = " + value);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean insertToSQL(String date, String companyId, String mac, String dataType) {
		String condi = "insert realtime_data_index (mac,dataType,date,companyId) values (?,?,?,?)";
		return jdbcTemplate.update(condi, new Object[] { mac, dataType, date, companyId }) > 0;
	}

	public static List<Map<String, Object>> getInsertedIndex(String date) {
		String sql = "select * from realtime_data_index where date='" + date + "';";
		return jdbcTemplate.queryForList(sql);
	}

	public static void checkAndInsert() {// 檢查昨天和前天是否有未寫入index表的檔案
		// 檢查前兩天的檔案
		String[] queryDays = new String[2];
		JDateTime jDate = new JDateTime();
		jDate.subDay(1);
		queryDays[0] = (jDate.toString("YYYYMMDD")).substring(2);// 昨天
		jDate.subDay(1);
		queryDays[1] = (jDate.toString("YYYYMMDD")).substring(2);// 前天
		String sql = "select * from realtime_data_index where date='" + queryDays[0] + "' or date='" + queryDays[1]
				+ "';";
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);

		HashSet<String> inserted = new HashSet<>();
		for (Map<String, Object> map : list) {
			String com = map.get("companyId").toString();
			String date = map.get("date").toString();
			String mac = map.get("mac").toString();
			String dataType = map.get("dataType").toString();
			inserted.add(date + "-" + com + "-" + mac + "-" + dataType);
		}

		ArrayList<Object[]> insertList = new ArrayList<>();

		File dataDir = new File(DATA_DIR);
		for (File comDir : dataDir.listFiles()) {
			if (comDir.isDirectory()) {
				String companyId = comDir.getName();
				for (String date : queryDays) {
					String path = File.separator + companyId // 公司帳號
							+ File.separator + date.substring(0, 2) // 年
							+ File.separator + date.substring(2, 4) // 月
							+ File.separator + date.substring(4, 6); // 日
					File dayDir = new File(DATA_DIR + path);
					if (dayDir.exists()) {
						for (File file : dayDir.listFiles()) {
							String key = file.getName().split(".")[0];							
							if (!inserted.contains(date + "-" + companyId + "-" + key)) { // 需要寫入index表的檔案
								String[] macDataType = key.split("-");
								insertList.add(new Object[] { macDataType[0], macDataType[1], date, companyId });
							}
						}
					}
				}
			}
		}

		if (!insertList.isEmpty()) {
			sql = "insert realtime_data_index (mac,dataType,date,companyId) values (?,?,?,?)";
			jdbcTemplate.batchUpdate(sql, insertList);
		}

	}

}
