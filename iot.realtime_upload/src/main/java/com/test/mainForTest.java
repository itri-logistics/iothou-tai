package com.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

public class mainForTest {
    
	static String dataPath = "C:\\Users\\user\\Downloads\\BLE_EE20DA7632C5_200218_092812_51814.txt";
	
	public static void main(String[] args) {
		ArrayList<String> data = new ArrayList<String>();
		String apiInputData = "";
		
		File filename = new File(dataPath); // 要讀取以上路徑的input。txt檔案
		InputStreamReader reader;
		long readstart;
		readstart = System.currentTimeMillis();
		try {
			reader = new InputStreamReader(new FileInputStream(filename));
			BufferedReader br = new BufferedReader(reader);
			String line = "";
			try {
				line = br.readLine();
				while (line != null) {
					line = br.readLine(); // 一次讀入一行資料
					// System.out.println(line);
					if (line == null || line.isEmpty()) {
						break;
					} else {
						String[] rawData = line.split(",", 2);
						data.add(rawData[1]);
						// System.out.println(rawData[1]);
					}
				}
				
				br.close();
				apiInputData = data.get(0);
				
				for (int i = 1; i < data.size(); i++) {
					apiInputData = apiInputData + ";" + data.get(i);
				}
				
//				String geturl = "http://60.248.82.82/iot_realtime/b/multipleData/forTest/E4AA22372CE0/ble?datas=" + apiInputData;
//				HttpResponse response = HttpRequest.get(geturl).send();
				long start, end;
				start = System.currentTimeMillis();
				System.out.println("讀取資料花了：" + (start - readstart) / 1000 + "秒");
				//String posturl = "http://localhost:8080/iot_realtime/multipleData/forAnotherTest/EE20DA7632C5/ble";
				String posturl = "http://60.248.82.82/iot_realtime/b/multipleData/forAnotherTest/EE20DA7632C5/ble";
				//String posturl = "http://10.248.82.172:8080/iot_realtime/multipleData/forAnotherTest/EE20DA7632C5/ble";
				//String posturl = "http://210.242.68.166:8080/iot_realtime/multipleData/forAnotherTest/EE20DA7632C5/ble";
				HttpResponse response = HttpRequest.post(posturl).contentType("application/xml;charset=utf-8")
						.form("datas", apiInputData).send();
				//ObjectMapper updateMapper = new ObjectMapper();
				System.out.println(response.bodyText());
//				JsonNode root = updateMapper.readTree(response.bodyText());
//				if (root.path("rs").asText().equals("success")) {
//					System.out.println(root.path("content").asText());
//				} else {
//					System.out.println(root.path("content").asText());
//				}
				end = System.currentTimeMillis();
				System.out.println("總共花了：" + (end - start) / 1000 + "秒");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
}
