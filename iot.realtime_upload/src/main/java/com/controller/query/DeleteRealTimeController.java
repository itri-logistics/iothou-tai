package com.controller.query;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.query.QueryRealTimeDao;
import com.google.gson.Gson;

import itri.group.param.RsMsg;

/**
 * Servlet implementation class DeleteRealTimeController
 */
public class DeleteRealTimeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Gson gson = new Gson();
	
	QueryRealTimeDao queryRealTimeDao = new QueryRealTimeDao();
    
    public DeleteRealTimeController() {
        super();
    }

    // ./delete/company/mac/sd/ed
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		String[] pathParts = pathInfo.split("/");
		String com = pathParts[1];
		String mac = pathParts[2];
		String sd = pathParts[3];
		String ed = pathParts[4];
		
		queryRealTimeDao.deleteDataByTag(com, mac, sd, ed);
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		
		out.print(gson.toJson(new RsMsg("success", "")));
		out.flush();
		out.close();
	}
}
