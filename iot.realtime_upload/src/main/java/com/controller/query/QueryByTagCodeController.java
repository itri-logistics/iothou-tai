package com.controller.query;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.query.QueryByTagCodeDao;
import com.dao.tag.TagCodeDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.RsMsg;

/**
 * Servlet implementation class QueryByTagCodeController
 */
public class QueryByTagCodeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	final Gson gson = new Gson();
	TagCodeDao tagCodeDao = new TagCodeDao();
	QueryByTagCodeDao queryByTagCodeDao = new QueryByTagCodeDao();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QueryByTagCodeController() {
        super();
        // TODO Auto-generated constructor stub
    }

	// /queryByTagCode/getPort1Data
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		
		try {
			String pathInfo = request.getPathInfo();
			if(pathInfo.startsWith("/")) {
				pathInfo = pathInfo.substring(1);
			}
			String[] pathParts = pathInfo.split("/");
			if("getPort1Data".equals(pathParts[0])) {
				String date = request.getParameter("date");
				String tagCode = request.getParameter("tagCode");
				
				String macId = tagCodeDao.getTagMacByCode(tagCode);				

				// 判斷mac
				if (macId.isEmpty()) {
					throw new Exception("tag not found.");
				} 
				
				out.print(gson.toJson(new RsMsg("success", queryByTagCodeDao.getPort1Data(date, macId))));
				out.flush();
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("query data error: "+e.getMessage());
			out.print(gson.toJson(new RsMsg("error", e.getMessage())));
			out.flush();
			out.close();
		}
	}
}
