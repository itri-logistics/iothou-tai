package com.controller.upload;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.upload.UploadBatchWifiDataDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.RsMsg;

/******************************************
 * Wifi Tag 備援資料上傳
 *
 * 套件: upload
 * UploadRealTimeController.java
 * UploadWifiDataController
 * UploadBatchWifiDataController.java
 * RealTimeDataDao.java
 * UploadBatchWifiDataDao.java
 * 
 * url: ./multipleWifiData/{companyId}/{mac}/wifi?datas={data}
 *
 * @author ATone create by 2017/09/20
 *******************************************/
public class UploadBatchWifiDataController extends HttpServlet {
	private static final long serialVersionUID = 1L;     
    
	Gson gson = new Gson();
    public UploadBatchWifiDataController() {
        super();
        // TODO Auto-generated constructor stub
    }

    // /multipleWifiData/{companyId}/{mac}/wifi?datas={data}
	// /multipleWifiData/{companyId}/{mac}/wifi?datas={data}
    // data格式: YYMMDDhhmmss溫度,濕度,SSID;YYMMDDhhmmss溫度,濕度,SSID,探針溫度1,探針溫度2;YYMMDDhhmmss溫度,濕度,SSID,探針溫度1,探針溫度2;...
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		if(pathInfo.startsWith("/")) {
			pathInfo = pathInfo.substring(1);
		}
		String[] pathParts = pathInfo.split("/");
		String companyId = pathParts[0];
		String mac = pathParts[1];
		String datas = request.getParameter("datas");
		
		SecurityController.dLog.debug("./multipleWifiData company = " + companyId + ", mac = "+ mac + ", datas: " + datas);
		System.out.println("./multipleWifiData company = " + companyId + ", mac = "+ mac + ", datas: " + datas);
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		
		try {
			UploadBatchWifiDataDao.proccess(companyId, mac, datas);
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("parsing upload datas error: " + e.getMessage());
			out.print(gson.toJson(new RsMsg("error", e.getMessage())));
			out.flush();
			out.close();
		}
		out.print(gson.toJson(new RsMsg("success", 200)));
		out.flush();
		out.close();
	}
}
