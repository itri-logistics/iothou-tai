package com.controller.upload;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.upload.UploadBatchDataDao;
import com.google.gson.Gson;
import com.security.SecurityController;

import itri.group.param.RsMsg;

/******************************************
 * BLE Tag 備援資料上傳
 *
 * 套件: upload
 * UploadRealTimeController.java
 * UploadWifiDataController
 * UploadBatchWifiDataController.java
 * RealTimeDataDao.java
 * UploadBatchWifiDataDao.java
 * 
 * url: ./multipleData/{companyId}/{mac}/ble?datas={data}
 *
 * @author ATone create by 2017/09/20
 *******************************************/
public class UploadBatchDataController extends HttpServlet {
	private static final long serialVersionUID = 1L;     
    
	Gson gson = new Gson();
    public UploadBatchDataController() {
        super();
        // TODO Auto-generated constructor stub
    }

    // /multipleData/{companyId}/{mac}/ble?datas={data}
	// /multipleData/{companyId}/{mac}/ble?datas={data}
    // data格式: YYMMDDhhmmss溫度,濕度,port1,port2,電量;YYMMDDhhmmss溫度,濕度,port1,port2,電量;...
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		if(pathInfo.startsWith("/")) {
			pathInfo = pathInfo.substring(1);
		}
		String[] pathParts = pathInfo.split("/");
		String companyId = pathParts[0];
		String mac = pathParts[1];
		String datas = request.getParameter("datas");
		
		SecurityController.dLog.debug("/multipleData company = " + companyId + ", mac = " + mac + ", datas: " + datas);
		System.out.println("/multipleData company = " + companyId + ", mac = "+ mac + ", datas: " + datas);

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		
		try {
			UploadBatchDataDao.proccess(companyId, mac, datas);
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("parsing upload datas error: " + e.getMessage());
			out.print(gson.toJson(new RsMsg("error", e.getMessage())));
			out.flush();
			out.close();
		}
		String[] dataList = datas.split(";");
		out.print(gson.toJson(new RsMsg("success", "成功上傳" + dataList.length + "筆資料")));
		out.flush();
		out.close();
	}
	
	// /multipleData/{companyId}/{mac}/ble
	// data格式: YYMMDDhhmmss溫度,濕度,port1,port2,電量;YYMMDDhhmmss溫度,濕度,port1,port2,電量;...
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		long start, end;
		start = System.currentTimeMillis();
		String pathInfo = request.getPathInfo();
		if(pathInfo.startsWith("/")) {
			pathInfo = pathInfo.substring(1);
		}
		String[] pathParts = pathInfo.split("/");
		String companyId = pathParts[0];
		String mac = pathParts[1];
		String datas = request.getParameter("datas");
		SecurityController.dLog.debug("./multipleData company = " + companyId + ", mac = " + mac + ", datas: " + datas);
		System.out.println("./multipleData company = " + companyId + ", mac = " + mac + ", datas: ");

		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		String[] dataList = datas.split(";");
		
		try {
			if (dataList.length > 5000) {
				System.out.println("大筆資料");
				UploadBatchDataDao.proccessBig(companyId, mac, datas);
			} else {
				UploadBatchDataDao.proccess(companyId, mac, datas);
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("parsing upload datas error: " + e.getMessage());
			out.print(gson.toJson(new RsMsg("error", e.getMessage())));
			out.flush();
			out.close();
		}
		
		out.print(gson.toJson(new RsMsg("success", "成功上傳" + dataList.length + "筆資料")));
		out.flush();
		out.close();
		end = System.currentTimeMillis();
		System.out.println("公司" + companyId + "中的" + mac + "總共花了：" + (end - start) / 1000 + "秒");
	}
}
