package com.controller.upload;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.security.SecurityController;
import com.utility.RealTimeDataPool;

import itri.group.param.RsMsg;

/******************************************
 * Wifi Tag 資料上傳
 *
 * 套件: upload
 * UploadRealTimeController.java
 * UploadWifiDataController
 * UploadBatchWifiDataController.java
 * RealTimeDataDao.java
 * UploadBatchWifiDataDao.java
 * 
 * url: ./uploadWifiData/{companyId}/{mac}/{dataType}/{data}/wifi
 *
 * @author ATone create by 2017/09/20
 *******************************************/
public class UploadWifiDataController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Gson gson = new Gson();
	
	 // for 耐低溫探針
    final public static double ratio[] = { 993.4780266, 991.4924763, 989.3961699, 987.1845375, 984.852528, 982.3954625,
            979.8083533, 977.0860578, 974.2236049, 971.2156786, 968.0574722, 964.7434841, 961.2687397, 957.628462,
            953.8173469, 949.8298868, 945.6622296, 941.3093229, 936.7664287, 932.0286333, 927.0927055, 921.9540993,
            916.6095666, 911.0542559, 905.2863734, 899.3029141, 893.1005778, 886.6793617, 880.0341919, 873.1677714,
            866.0776657, 858.764087, 851.2254842, 843.4672167, 835.4840111, 827.284987, 818.8676857, 810.239145,
            801.4009826, 792.3572366, 783.1155022, 773.6822137, 764.0553398, 754.2494666, 744.2720791, 734.1285172,
            723.8299818, 713.3769338, 702.7954831, 692.0928303, 681.2614386, 670.3233516, 659.3016597, 648.1926013,
            637.0007559, 625.7578657, 614.4655255, 603.1377255, 591.7865946, 580.421919, 569.0506487, 557.6976321,
            546.3435022, 535.0304651, 523.7801768, 512.5625812, 501.4177086, 490.3331249, 479.3481198, 468.4444444,
            457.6596427, 446.9685563, 436.4060366, 425.9739532, 415.6711222, 405.4931143, 395.4706604, 385.59601,
            375.8577125, 366.2827413, 356.8990228, 347.6459709, 338.5917001, 329.6685652, 320.9447305, 312.3947186,
            304.0393728, 295.8481121, 287.7860378, 279.976168, 272.2748495, 264.8066429, 257.4755595, 250.3523723,
            243.3336891, 236.5499846, 229.955335, 223.4371042, 217.1925622, 211.0435059, 205.0620601 };

    final public static int ratioC[] = { -40, -39, -38, -37, -36, -35, -34, -33, -32, -31, -30, -29, -28, -27, -26, -25, -24,
            -23, -22, -21, -20, -19, -18, -17, -16, -15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0,
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
            30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
            57, 58, 59, 60 };
	
    public UploadWifiDataController() {
        super();       
    }

    // /uploadWifiData/{companyId}/{mac}/{dataType}/{data}/wifi
    // /uploadWifiData/{companyId}/{mac}/{dataType}/{data}/{ssid}/wifi   
    // /uploadWifiData/{companyId}/{mac}/{temp}/{humidi}/{ssid}/{adc}/wifi   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		if(pathInfo.startsWith("/")) {
			pathInfo = pathInfo.substring(1);
		}
		String[] pathParts = pathInfo.split("/");
		String companyId = pathParts[0];
		String mac = pathParts[1];
		String dataType = pathParts[2];		
		System.out.println("./uploadwifi datas = " + pathInfo);
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		
		try {
			if(!dataType.startsWith("0") && dataType.length()!=4) {				
				String temp = dataType;
				String hum = pathParts[3];
				String ssid = pathParts[4];
				String adc = pathParts[5];
				
				if (!adc.equals("null")) {// 溫度需進行轉換
		            double raw = Double.parseDouble(adc);
		            if (raw <= ratio[ratio.length - 1]) {// 最小電阻值
		            	adc = String.valueOf(ratioC[ratio.length - 1]);
		            } else if (raw >= ratio[0]) {// 最大電壓值
		            	adc = String.valueOf(ratioC[0]);
		            } else {
		                for (int i = 0; i < ratio.length - 1; i++) {
		                    if (raw >= ratio[i]) {
		                        Float rawf = new Float(raw);
		                        adc = String.format("%.2f", ((rawf - ratio[i]) / (ratio[i + 1] - ratio[i]) + i) - 40);
		                        break;
		                    }
		                }
		            }
		            // System.out.println("./uploadwifi origin = " + pathParts[5] + ", convert = " + adc);
		            RealTimeDataPool.updateTagData(companyId, mac, "0101", adc);
		        }
				
				if (!temp.equals("null")) {// 溫度需進行轉換		            
		            RealTimeDataPool.updateTagData(companyId, mac, "0001", temp);
		        }
				
				if(!hum.equals("null")) {
					RealTimeDataPool.updateTagData(companyId, mac, "0002", hum);
				}
				
				if(!ssid.equals("null")) {
					RealTimeDataPool.updateTagData(companyId, mac, "0010", ssid);
				}
				
			} else {
				String data = pathParts[3];
				String ssid = (pathParts[4].equals("wifi"))? "":pathParts[4];
				RealTimeDataPool.updateTagData(companyId, mac, dataType, data);
				if(!ssid.isEmpty()){
					RealTimeDataPool.updateTagData(companyId, mac, "0010", ssid);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("parsing upload datas error: " + e.getMessage());
			out.print(gson.toJson(new RsMsg("error", e.getMessage())));
			out.flush();
			out.close();
		}
		out.print(gson.toJson(new RsMsg("success", 200)));
		out.flush();
		out.close();
	}
}
