package com.controller.upload;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.security.SecurityController;
import com.utility.RealTimeDataPool;

import itri.group.param.RsMsg;

/******************************************
 * 即時資料上傳
 * 
 * 套件: upload
 * UploadRealTimeController.java
 * UploadWifiDataController
 * UploadBatchWifiDataController.java
 * RealTimeDataDao.java
 * UploadBatchWifiDataDao.java
 * 
 * url: ./upload
 *
 * @author ATone create by 2017/08/01
 *******************************************/
public class UploadRealTimeController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Gson gson = new Gson();
	
	public UploadRealTimeController() {
		super();
	}

	// datas = companyId#macId;dataTpye1data1,dataTpye2data2...#macId...
	// 範例:
	// itri#1035200168AA,0001-25.4,000278.01,010124.98,000721#3634200168AA,0001-25.4,000278.01,010124.98,000784...
	// /upload
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String datas = request.getParameter("datas");
		SecurityController.dLog.debug("./upload, datas = " + datas);
		System.out.println("./upload, datas = " + datas);
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		try {
			if (datas == null || "".equals(datas)) {
				out.print(gson.toJson(new RsMsg("success", 200)));
				out.flush();
				out.close();
				return;
			}
			String ds[] = datas.split("#");
			String companyId = ds[0];
			// 每個tag切開，放入pool中，若有異常會丟出exception
			for (int i = 1; i < ds.length; i++) {
				String[] tagData = ds[i].split(",");
				String tagMac = tagData[0];
				for (int j = 1; j < tagData.length; j++) {
					RealTimeDataPool.updateTagData(companyId, tagMac, tagData[j].substring(0, 4),
							tagData[j].substring(4));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("parsing upload datas error: " + e.getMessage());
			out.print(gson.toJson(new RsMsg("error", e.getMessage())));
			out.flush();
			out.close();
		}
		out.print(gson.toJson(new RsMsg("success", 200)));
		out.flush();
		out.close();
	}	
}
