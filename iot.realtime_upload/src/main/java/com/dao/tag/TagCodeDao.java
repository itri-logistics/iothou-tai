package com.dao.tag;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.bean.ConnoDataSource;


public class TagCodeDao {
	
	JdbcTemplate jdbcTemplate = new JdbcTemplate(ConnoDataSource.getConnDataSource());
	
	final private String TABLE_NAME = "tag_code";
	final private String FILED_CODE = "code";
	final private String FILED_MAC = "mac";
	final private String FILED_DESCRIPITION = "descrpition";
	
	public String getTagMacByCode(String code) {
		String sql=String.format("select "+ FILED_MAC + " from " + TABLE_NAME + " where " + FILED_CODE + "='%s';", code);

		List<Map<String, Object>> qMap = null;
		try {			
			qMap = jdbcTemplate.queryForList(sql);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		if(qMap.isEmpty()){
			System.err.println("找不到tag，請確認tag代碼是否已登錄。tag代碼 = " + code);
		}

		return qMap.isEmpty() ? "" : qMap.get(0).get("mac").toString().replace(":", "");
	}
}
