package com.dao.upload;

import java.util.Arrays;

import com.utility.RealTimeDataPool;

public class UploadBatchWifiDataDao {

	// for 耐低溫探針
//	final public static double ratio[] = { 122, 130, 138, 147, 157, 166, 177, 188, 199, 211, 224, 237, 251, 265, 281,
//			297, 313, 331, 349, 368, 388, 408, 429, 452, 475, 499, 523, 549, 576, 603, 632, 661, 691, 722, 754, 787,
//			820, 855, 890, 926, 963, 1001, 1040, 1079, 1119, 1159, 1200, 1242, 1285, 1327, 1371, 1414, 1458, 1503, 1548,
//			1593, 1638, 1683, 1728, 1774, 1819, 1865, 1910, 1955, 2000, 2045, 2090, 2134, 2178, 2222, 2265, 2308, 2350,
//			2392, 2433, 2473, 2514, 2553, 2592, 2630, 2668, 2705, 2741, 2777, 2812, 2846, 2879, 2912, 2944, 2975, 3006,
//			3036, 3065, 3094, 3122, 3149, 3175, 3201, 3226, 3251, 3275, 3298, 3321, 3343, 3364, 3385, 3405, 3425, 3444,
//			3463, 3481, 3498, 3515, 3532, 3548, 3563, 3579, 3593, 3608, 3621, 3635, 3648, 3660, 3673, 3685, 3696, 3707,
//			3718, 3729, 3739, 3749, 3758, 3768, 3777, 3785, 3794, 3802, 3810, 3818, 3826, 3833, 3840, 3847, 3853, 3860,
//			3866, 3872, 3878, 3884, 3890, 3895, 3900, 3905, 3910, 3915, 3920, 3924, 3928, 3933, 3937, 3941 };
//
//	final public static int ratioC[] = { -40, -39, -38, -37, -36, -35, -34, -33, -32, -31, -30, -29, -28, -27, -26, -25,
//			-24, -23, -22, -21, -20, -19, -18, -17, -16, -15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2,
//			-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
//			28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54,
//			55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81,
//			82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106,
//			107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120 };
	
	final public static double ratio[] = 
		{ 0, 14, 28, 42, 57, 71, 85, 99, 113, 127, 
			141, 155, 170, 184, 198, 212, 226, 240, 254, 269, 
			283, 297, 311, 325, 339, 353, 367, 382, 396, 410, 
			459, 493, 528, 562, 597, 631, 666, 700, 735, 770, 804, 
			839, 873, 908, 942, 977, 1011, 1087, 1128, 1169, 1210, 
			1251, 1292, 1333, 1375, 1416, 1457, 1498, 1539, 1580, 1621, 
			1701, 1741, 1780, 1820, 1859, 1899, 1938, 1978, 2017, 2056, 
			2096, 2135, 2175, 2214, 2254, 2324, 2356, 2387, 2418, 2449, 
			2481, 2512, 2543, 2574, 2606, 2637, 2668, 2699, 2731, 2762, 
			2793, 2824, 2856, 2988, 3088, 3189, 3289, 3390, 3490, 3591};

	final public static int ratioC[] = 
		{ -40, -39, -38, -37, -36, -35, -34, -33, -32, -31, 
			-30, -29, -28, -27, -26, -25, -24, -23, -22, -21, 
			-20, -19, -18, -17, -16, -15, -14, -13, -12, -11, 
			-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 
			11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 
			21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 
			31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
			41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 
			51, 52, 53, 54, 55, 56, 57, 58, 59, 60 };

	public static void proccess(String companyId, String mac, String datas) throws Exception {
		String[] dataList = datas.split(";");
		String insertDate = "";
		for (String data : dataList) {
			String date = data.substring(0, 6);
			String dateTime = data.substring(0, 12);
			try {
				Double.parseDouble(dateTime);
			} catch (Exception e) {
				throw new Exception("format error in " + data + ". data format must be YYMMDDhhmmss溫度,濕度,SSID,探針溫度1,探針溫度2");
			}
			String[] values;
			if (data.charAt(12) == ',') {
				values = (data.substring(13)).split(",");
			} else {
				values = (data.substring(12)).split(",");
			}

			// System.out.println(Arrays.toString(values));
			if (!date.equals(insertDate)) {
				if (!values[0].equals("null")) {
					RealTimeDataDao.insertToSQL(date, companyId, mac, "0001"); // 溫度
				}
					
				if (values.length >= 2) {
					if (!values[1].equals("null")) {
						RealTimeDataDao.insertToSQL(date, companyId, mac, "0002"); // 濕度
					}
				}
				if (values.length >= 3) {
					if (!values[2].equals("null")) {
						RealTimeDataDao.insertToSQL(date, companyId, mac, "0010"); // ssid
					}
				}
				if (values.length >= 4) {
					if (!values[3].equals("null")) {
						RealTimeDataDao.insertToSQL(date, companyId, mac, "0101"); // 探針溫度
						// System.out.println("UploadBatchWifiDataDao.insertToSQL: " + date + ", " + companyId + ", " + mac + ", 0101");
					}
				}
				if (values.length >= 5) {
					if (!values[4].equals("null")) {
						RealTimeDataDao.insertToSQL(date, companyId, mac, "0201"); // 探針溫度
					}
				}
				insertDate = date;
			}

			if (!values[0].equals("null")) {
				RealTimeDataDao.insertData(dateTime, companyId, mac, "0001", values[0]); // 溫度
				RealTimeDataPool.updateTagData(dateTime, companyId, mac, "0001", values[0]);
			}
				
			if (values.length >= 2) {
				if (!values[1].equals("null")) {
					RealTimeDataDao.insertData(dateTime, companyId, mac, "0002", values[1]); // 濕度
					RealTimeDataPool.updateTagData(dateTime, companyId, mac, "0002", values[1]);
				}
			}

			if (values.length >= 3) {
				if (!values[2].equals("null")) {
					RealTimeDataDao.insertData(dateTime, companyId, mac, "0010", values[2]); // ssid
					RealTimeDataPool.updateTagData(dateTime, companyId, mac, "0010", values[2]);
				}
			}

			if (values.length >= 4) {
				if (!values[3].equals("null")) {
					double raw = Double.parseDouble(values[3]);
					if (raw >= ratio[ratio.length - 1]) {// 最大電阻值
						values[3] = String.valueOf(ratioC[ratio.length - 1]);
					} else if (raw <= ratio[0]) {// 最小電壓值
						values[3] = String.valueOf(ratioC[0]);
					} else {
						for (int i = 0; i < ratio.length - 1; i++) {
							if (raw >= ratio[i] && raw <= ratio[i + 1]) {
								Float rawf = new Float(raw);
								values[3] = String.format("%.2f",
										(rawf - ratio[i]) * ((ratioC[i + 1] - ratioC[i]) / (ratio[i + 1] - ratio[i]))
												+ ratioC[i]);
								break;
							}
						}
					}
					// System.out.println("./uploadBatchWifi origin = " + raw + ", convert = " + values[3]);
					RealTimeDataDao.insertData(dateTime, companyId, mac, "0101", values[3]); // 溫度探針
					RealTimeDataPool.updateTagData(dateTime, companyId, mac, "0101", values[3]);
				}
			}

			if (values.length >= 5) {
				if (!values[4].equals("null")) {
					double raw = Double.parseDouble(values[4]);
					if (raw >= ratio[ratio.length - 1]) {// 最大電阻值
						values[4] = String.valueOf(ratioC[ratio.length - 1]);
					} else if (raw <= ratio[0]) {// 最小電壓值
						values[4] = String.valueOf(ratioC[0]);
					} else {
						for (int i = 0; i < ratio.length - 1; i++) {
							if (raw >= ratio[i] && raw <= ratio[i + 1]) {
								Float rawf = new Float(raw);
								values[4] = String.format("%.2f",
										(rawf - ratio[i]) * ((ratioC[i + 1] - ratioC[i]) / (ratio[i + 1] - ratio[i]))
												+ ratioC[i]);
								break;
							}
						}
					}
					// System.out.println("./uploadBatchWifi origin = " + raw + ", convert = " + values[4]);
					RealTimeDataDao.insertData(dateTime, companyId, mac, "0201", values[4]); // 溫度探針
					RealTimeDataPool.updateTagData(dateTime, companyId, mac, "0201", values[4]);
				}
			}
			
			RealTimeDataPool.updateTagData(dateTime, companyId, mac, "0007", "");
		}
	}
}
