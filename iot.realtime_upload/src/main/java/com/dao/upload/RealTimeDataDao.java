package com.dao.upload;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.bean.ConnoDataSource;
import com.security.SecurityController;

import jodd.datetime.JDateTime;

public class RealTimeDataDao {

	private static JdbcTemplate jdbcTemplate = new JdbcTemplate(ConnoDataSource.getConnDataSource());

	private final static String ROOT_PATH = System.getProperty("user.home");
	private final static String DATA_DIR = ROOT_PATH + File.separator + "iot_data";
	private final static String txtEndFileType = ".txt_rt";
	private static ArrayList<Object[]> indexInsertList = new ArrayList<>();

	public static void writeLogFile(String content) {

		String fileName = ROOT_PATH + File.separator + "iot_logs" + File.separator + "log.txt";
		String date = new JDateTime().toString("YYYY/MM/DD hh:mm:ss");
		try {
			File file = new File(fileName);
			FileUtils.writeStringToFile(file, date + ": " + content, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void insertData(String date, String com, String mac, String type, String value) {
		if(value.equals("null")) {
			return;
		}
		String fileName = DATA_DIR + File.separator + com + File.separator + date.substring(0, 2) + File.separator
				+ date.substring(2, 4) + File.separator + date.substring(4, 6) + File.separator + mac + "-" + type
				+ txtEndFileType;
		
		try {
			Thread insertThread = new Thread() {
				@Override
				public void run() {
					File file = new File(fileName);
					synchronized (file) {						
						try {
							if(!file.exists()) {
								FileUtils.writeStringToFile(file, date + value + ",", true);
							} else {
								String content;
								content = FileUtils.readFileToString(file);
								int pos = content.indexOf(date);
								if (pos < 0) {
									FileUtils.writeStringToFile(file, date + value + ",", true);
								} else {
									int end = content.indexOf(",", pos);
									content = content.substring(0, pos) + date + value + content.substring(end);
									FileUtils.writeStringToFile(file, content);
								}
							}
							SecurityController.dLog.debug("insert batch data to file, key = " + com + "_" + mac + "-" + type + ", value = " + value + ", time = " + date);
						} catch (IOException e) {
							e.printStackTrace();
							SecurityController.eLog.error(e.toString());
						}						
					}
					
					// System.out.println("insert batch data to file, key = " + com + "_" + mac + "-" + type + ", value = " + value + ", time = " + date);
					super.run();
				}
			};
			
			insertThread.start();
			
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error(e.toString());
		}
	}
	
	public static void insertDataBu(String date, String com, String mac, String type, String value) {
		if(value.equals("null")) {
			return;
		}
		
		String filePath = DATA_DIR + File.separator + com + File.separator + date.substring(0, 2) + File.separator
				+ date.substring(2, 4) + File.separator + date.substring(4, 6) + File.separator;
		String fileName = mac + "-" + type + txtEndFileType;
		try {
			Thread insertThread = new Thread() {
				@Override
				public void run() {
					File fileP = new File(filePath);
					File fileN = new File(fileP, fileName);
					synchronized (fileN) {						
						try {
							if(!fileP.exists()) {
								fileP.mkdirs();
							}
							if (!fileN.exists()) {
								fileN.createNewFile();
							}
							
							FileReader fr = new FileReader(fileN); 
							BufferedReader bufr = new BufferedReader(fr);
							
					        String content;
					        if ((content = bufr.readLine()) == null) {
					        	content = "";
					        }
					        
							int pos = content.indexOf(date);
							
							if (pos < 0) {
								FileWriter fw = new FileWriter(fileN, true);  
						        BufferedWriter bufw = new BufferedWriter(fw);
								bufw.write(date + value + ",");
								bufw.close();
							} else {
								int end = content.indexOf(",", pos);
								content = content.substring(0, pos) + date + value + content.substring(end);
								FileWriter fw = new FileWriter(fileN);  
						        BufferedWriter bufw = new BufferedWriter(fw);
								bufw.write(content);
								bufw.close();
							}
							bufr.close();
							SecurityController.dLog.debug("insert batch data to file, key = " + com + "_" + mac + "-" + type + ", value = " + value + ", time = " + date);
						} catch (IOException e) {
							e.printStackTrace();
							SecurityController.eLog.error(e.toString());
						}						
					}
					
					// System.out.println("insert batch data to file, key = " + com + "_" + mac + "-" + type + ", value = " + value + ", time = " + date);
					super.run();
				}
			};
			
			insertThread.start();
			
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error(e.toString());
		}
	}

	public static void insertData(long time, String key, String value) {
		if(value.equals("null")) {
			return;
		}
		String[] keys = key.split("_");
		String date = new JDateTime(time).toString("YYYYMMDDhhmmss");
		String fileName = DATA_DIR + File.separator + keys[0] + File.separator + date.substring(2, 4) + File.separator
				+ date.substring(4, 6) + File.separator + date.substring(6, 8) + File.separator + keys[1] + "-"
				+ keys[2] + txtEndFileType;
		try {
			File file = new File(fileName);
			FileUtils.writeStringToFile(file, date.substring(2) + value + ",", true);
			SecurityController.dLog.debug("insert realtime data to file, key = " + key + ", value = " + value + ", time = " + date);
			// System.out.println("insert realtime data to file, key = " + key + ", value = " + value);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void insertToSQL(String date, String companyId, String mac, String dataType) {
		try {
			indexInsertList.add(new Object[] { mac, dataType, date, companyId });
			SecurityController.dLog
					.debug("insert data to index table, key = " + mac + "-" + dataType + "-" + date + "-" + companyId);
			insertToSQLBatch();
		} catch (DuplicateKeyException e) {
			return;
		} catch (Exception e) {
			SecurityController.eLog.error(
					"insert data to index table fail, key = " + mac + "-" + dataType + "-" + date + "-" + companyId);
			SecurityController.eLog.error(e.toString());
			e.printStackTrace();
		}
	}

	public static List<Map<String, Object>> getInsertedIndex(String date) {
		String sql = "select * from realtime_data_index where date='" + date + "';";
		return jdbcTemplate.queryForList(sql);
	}

	public static void insertToSQLBatch() {
		SecurityController.dLog.debug("insertToSQLBatch()");
		String condi = "insert ignore realtime_data_index (mac,dataType,date,companyId) values (?,?,?,?)";
		try {
			if (!indexInsertList.isEmpty()) {
				ArrayList<Object[]> insertList = new ArrayList<>();
				for (int i = 0; i < indexInsertList.size(); i++) {
					synchronized (indexInsertList) {
						insertList.add(indexInsertList.get(i));
					}
					if (insertList.size() == 500 || i == indexInsertList.size() - 1) {
//						StringBuilder stb = new StringBuilder();
//						for(Object[] obs : insertList) {
//							stb.append(", "+Arrays.toString(obs));
//						}
//						System.out.println(condi+stb.toString());
						//System.out.println(insertList.toString());
						jdbcTemplate.batchUpdate(condi, insertList);
						indexInsertList.clear();
						Thread.sleep(1000);
					}
				}
				synchronized (indexInsertList) {
					indexInsertList.clear();
				}
			}
		} catch (DuplicateKeyException e) {
			SecurityController.eLog.error("insert data list to index table Duplicate Key");
			SecurityController.eLog.error(e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			SecurityController.eLog.error("insert data list to index table fail");
			SecurityController.eLog.error(e.toString());
			e.printStackTrace();
		}
	}

	public static void checkAndInsert() {// 檢查昨天和前天是否有未寫入index表的檔案
		// 檢查前兩天的檔案
		String[] queryDays = new String[2];
		JDateTime jDate = new JDateTime();
		jDate.subDay(1);
		queryDays[0] = (jDate.toString("YYYYMMDD")).substring(2); // 昨天
		jDate.subDay(1);
		queryDays[1] = (jDate.toString("YYYYMMDD")).substring(2); // 前天
		String sql = "select * from realtime_data_index where date='" + queryDays[0] + "' or date='" + queryDays[1]
				+ "';";
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);

		HashSet<String> inserted = new HashSet<>();
		for (Map<String, Object> map : list) {
			String com = map.get("companyId").toString();
			String date = map.get("date").toString();
			String mac = map.get("mac").toString();
			String dataType = map.get("dataType").toString();
			inserted.add(date + "-" + com + "-" + mac + "-" + dataType);
		}

		ArrayList<Object[]> insertList = new ArrayList<>();

		File dataDir = new File(DATA_DIR);
		for (File comDir : dataDir.listFiles()) {
			if (comDir.isDirectory()) {
				String companyId = comDir.getName();
				for (String date : queryDays) {
					String path = File.separator + companyId // 公司帳號
							+ File.separator + date.substring(0, 2) // 年
							+ File.separator + date.substring(2, 4) // 月
							+ File.separator + date.substring(4, 6); // 日
					File dayDir = new File(DATA_DIR + path);
					if (dayDir.exists()) {
						for (File file : dayDir.listFiles()) {
							String key = file.getName().split(".")[0];
							if (!inserted.contains(date + "-" + companyId + "-" + key)) { // 需要寫入index表的檔案
								String[] macDataType = key.split("-");
								insertList.add(new Object[] { macDataType[0], macDataType[1], date, companyId });
							}
						}
					}
				}
			}
		}

		if (!insertList.isEmpty()) {
			sql = "insert realtime_data_index (mac,dataType,date,companyId) values (?,?,?,?)";
			jdbcTemplate.batchUpdate(sql, insertList);
		}
	}
}
