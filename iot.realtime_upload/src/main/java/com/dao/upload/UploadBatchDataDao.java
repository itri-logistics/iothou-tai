package com.dao.upload;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;

import jodd.util.StringUtil;

import com.utility.RealTimeDataPool;

public class UploadBatchDataDao {

	public static void proccess(String companyId, String mac, String datas) throws Exception {
		String[] dataList = datas.split(";");
		String insertDate = "";
		for (String data : dataList) {
			String date = data.substring(0, 6);
			String dateTime = data.substring(0, 12);			
			try {
				Double.parseDouble(dateTime);
			} catch (Exception e) {
				throw new Exception("format error in " + data + ". data format must be YYMMDDhhmmss溫度,濕度,SSID,探針溫度1,探針溫度2");
			}
			
			String[] values;
			if(data.charAt(12)==',') {
				values = (data.substring(13)).split(",");
			}else {
				values = (data.substring(12)).split(",");
			}
			
			if (!date.equals(insertDate)) {
				if (!values[0].equals("null") && StringUtil.isNotBlank(values[0])) {
					RealTimeDataDao.insertToSQL(date, companyId, mac, "0001"); // 溫度
				}
				if (values.length >= 2) {
					if (!values[1].equals("null") && StringUtil.isNotBlank(values[1])) {
						RealTimeDataDao.insertToSQL(date, companyId, mac, "0002"); // 濕度
					}
				}
				if (values.length >= 3) {
					if (!values[2].equals("null") && StringUtil.isNotBlank(values[2])) {
						RealTimeDataDao.insertToSQL(date, companyId, mac, "0101"); // port1
						// System.out.println("UploadBatchDataDao.insertToSQL: " + date + ", " + companyId + ", " + mac + ", 0101");
					}
				}
				if (values.length >= 4) {
					if (!values[3].equals("null") && StringUtil.isNotBlank(values[3])) {
						RealTimeDataDao.insertToSQL(date, companyId, mac, "0201"); // port2
					}
				}
				if (values.length >= 5) {
					if (!values[4].equals("null") && StringUtil.isNotBlank(values[4])) {
						RealTimeDataDao.insertToSQL(date, companyId, mac, "0007"); // 電量
					}
				}
				insertDate = date;
			}

			// System.out.println(values.length + ": " + Arrays.toString(values));
			
			if (!values[0].equals("null") && StringUtil.isNotBlank(values[0])) {
				RealTimeDataDao.insertData(dateTime, companyId, mac, "0001", values[0]); // 溫度
				RealTimeDataPool.updateTagData(dateTime, companyId, mac, "0001", values[0]);
			}
				
			if (values.length >= 2) {
				if (!values[1].equals("null") && StringUtil.isNotBlank(values[1])) {
					RealTimeDataDao.insertData(dateTime, companyId, mac, "0002", values[1]); // 濕度
					RealTimeDataPool.updateTagData(dateTime, companyId, mac, "0002", values[1]);
				}
			}

			if (values.length >= 3) {
				if (!values[2].equals("null") && StringUtil.isNotBlank(values[2])) {
					RealTimeDataDao.insertData(dateTime, companyId, mac, "0101", values[2]); // port1
					RealTimeDataPool.updateTagData(dateTime, companyId, mac, "0101", values[2]);
				}
			}

			if (values.length >= 4) {
				if (!values[3].equals("null") && StringUtil.isNotBlank(values[3])) {
					RealTimeDataDao.insertData(dateTime, companyId, mac, "0201", values[3]); // port2
					RealTimeDataPool.updateTagData(dateTime, companyId, mac, "0201", values[3]);
				}

			}
			
			if (values.length >= 5) {
				if (!values[4].equals("null") && StringUtil.isNotBlank(values[4])) {
					RealTimeDataDao.insertData(dateTime, companyId, mac, "0007", values[4]); // 電量
					RealTimeDataPool.updateTagData(dateTime, companyId, mac, "0007", values[4]);
				}
			}
		}
	}
	
	private final static String ROOT_PATH = System.getProperty("user.home");
	private final static String DATA_DIR = ROOT_PATH + File.separator + "iot_data";
	private final static String txtEndFileType = ".txt_rt";
	
	public static void proccessBig(String companyId, String mac, String datas) throws Exception {
		String[] dataList = datas.split(";");
		String insertDate = "";
		
		String[] contentType = {"0001", "0002", "0101", "0201", "0007"};
		HashMap<String, HashMap<String, String>> content = new HashMap<String, HashMap<String, String>>();
		
		File fp;
		File fn;
		FileReader fr;
		FileWriter fw;
		BufferedReader bufr;
		BufferedWriter bufw;
		String filePath = "";
		String fileName = "";
		String readContent = "";
		int count = 0;
		
		for (String data : dataList) {
			String date = data.substring(0, 6);
			String dateTime = data.substring(0, 12);		
			try {
				Double.parseDouble(dateTime);
			} catch (Exception e) {
				throw new Exception("format error in " + data + ". data format must be YYMMDDhhmmss溫度,濕度,SSID,探針溫度1,探針溫度2");
			}
			
			String[] values;
			
			if(data.charAt(12)==',') {
				values = (data.substring(13)).split(",");
			}else {
				values = (data.substring(12)).split(",");
			}
			
			if (!date.equals(insertDate)) {
				if (count > 0) { // 換天後將檔案寫入主機。 (匯入第一筆時不啟動)
					for (String type : content.keySet()) {
						fileName = mac + "-" + type + txtEndFileType;
						fn = new File(filePath, fileName);
						//System.out.println(fn.getAbsoluteFile());
						if (fn.exists()) {
							fn.delete();
							fn.createNewFile();
						} else {
							fn.createNewFile();
						}
						fw = new FileWriter(fn, true);
						bufw = new BufferedWriter(fw);
						for (String subDateTime : content.get(type).keySet()) {
							bufw.write(subDateTime + content.get(type).get(subDateTime) + ",");
						}
						bufw.close();
						fw.close();
					}
				}
				
				// 初始化
				content.clear();
				filePath = DATA_DIR + File.separator + companyId + File.separator + date.substring(0, 2) + File.separator
						+ date.substring(2, 4) + File.separator + date.substring(4, 6) + File.separator;
				fp = new File(filePath);
				
				// 讀取檔案
				// System.out.println(fp.getAbsolutePath());
				if (!fp.exists()) {
					// System.out.println("新增路徑資料夾");
					fp.mkdirs();
				}
				
				for (String type : contentType) {
					RealTimeDataDao.insertToSQL(date, companyId, mac, type);
					fileName = mac + "-" + type + txtEndFileType;
					fn = new File(fp, fileName);
					if (!fn.exists()) {
						fn.createNewFile();
					}
					content.put(type, new HashMap<String, String>());
					fr = new FileReader(fn);
					bufr = new BufferedReader(fr);
					if ((readContent = bufr.readLine()) != null) {
						String[] splitString = readContent.split(",");
						for(String split : splitString) {
							try  {
								String subdateTime = split.substring(0, 12);
								String subData = split.substring(12);
								content.get(type).put(subdateTime, subData);
							} catch(IndexOutOfBoundsException iobe) {
								System.out.println(iobe + "錯誤字串：" + split);
							}
						}
			        }
					
					bufr.close();
					fr.close();
				}
				
				insertDate = date;
				
//				System.out.println("日期: " + date);
//				for (String type : content.keySet()) {
//					System.out.println(type + "類型位於主機內已含有" + content.get(type).size() + "筆資料");
//				}
			}

			// System.out.println(values.length + ": " + Arrays.toString(values));
			
			// 寫入並比對資料
			for (int i = 0; i < values.length; i++) { // 溫度, 濕度, port1, port2, 電量
				if (i > 4) {
					System.out.println(i + "超出範圍");
					break;
				}
				
				if (!values[i].equals("null") && StringUtil.isNotBlank(values[i])) { 
					content.get(contentType[i]).put(dateTime, values[i]);
					RealTimeDataPool.updateTagData(dateTime, companyId, mac, contentType[i], values[i]);
				}
			}
			
			count++;
		}
		
		if (count > 0) { // 換天後將檔案寫入主機
			for (String type : content.keySet()) {
				fileName = mac + "-" + type + txtEndFileType;
				fn = new File(filePath, fileName);
				//System.out.println(fn.getAbsoluteFile());
				if (fn.exists()) {
					fn.delete();
					fn.createNewFile();
				} else {
					fn.createNewFile();
				}
				fw = new FileWriter(fn, true);
				bufw = new BufferedWriter(fw);
				for (String subDateTime : content.get(type).keySet()) {
					bufw.write(subDateTime + content.get(type).get(subDateTime) + ",");
				}
				bufw.close();
				fw.close();
			}
		}
	}
}
