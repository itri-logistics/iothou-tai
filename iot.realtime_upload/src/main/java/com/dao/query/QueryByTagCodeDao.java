package com.dao.query;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import com.bean.ConnoDataSource;
import com.dao.tag.TagDao;

public class QueryByTagCodeDao {

	JdbcTemplate jdbcTemplate = new JdbcTemplate(ConnoDataSource.getConnDataSource());

	TagDao tagDao = new TagDao();

	// 取得單一Tag的Data
	public String getPort1Data(String date, String mac) throws Exception {

		String companyId = tagDao.getCompanyId(mac);
		
		if(companyId.isEmpty()) {
			return "";
		}
		
		String y = date.substring(0, 2);
		String m = date.substring(2, 4);
		String d = date.substring(4, 6);

		final String fileName = mac + "-0101.txt_rt"; 
		
		final String rootPath = System.getProperty("user.home");
		final String dataDirPath = rootPath + File.separator + "iot_data" + File.separator + companyId 
				+ File.separator + y 
				+ File.separator + m
				+ File.separator + d;

		File f = new File(dataDirPath + File.separator + fileName);
				
		if (f.exists()) {			
			return FileUtils.readFileToString(f);						
		}
		
		return "";
	}
}
