package com.utility;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.dao.upload.RealTimeDataDao;
import com.security.SecurityController;
//import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

import jodd.datetime.JDateTime;

/******************************************
 * 即時資料Pool
 *
 * 每個整分會寫一次檔
 *
 * @author ATone create by 2017/08/04
 *******************************************/
public class RealTimeDataPool {

	private final static String ROOT_PATH = System.getProperty("user.home");
	private final static String RAW_DIR = ROOT_PATH + File.separator + "iot_raw";
	private final static String RAW_FILE_TYPE = ".rawTxt_rt";

	// <comapnyId, <macId_dataType, Value>>
	private static HashMap<String, HashMap<String, String>> tagDataPool = new HashMap<>();
	private static HashMap<String, HashMap<String, Long>> tagActive = new HashMap<>(); // 紀錄Tag的最後出現時間
	private static HashMap<String, HashMap<String, HashSet<String>>> todayInsert = new HashMap<>(); // 紀錄今日已insert的Tag

	private static String todayDate = (new JDateTime().toString("YYYYMMDD")).substring(2);

	public static void init() { // 將今日已有的檔案讀進來
		List<Map<String, Object>> list = RealTimeDataDao.getInsertedIndex(todayDate);
		for (Map<String, Object> map : list) {
			String companyId = map.get("companyId").toString();
			String mac = map.get("mac").toString();
			String dataType = map.get("dataType").toString();
			if (!todayInsert.containsKey(companyId)) {
				todayInsert.put(companyId, new HashMap<String, HashSet<String>>());
			}

			if (!todayInsert.get(companyId).containsKey(mac)) {
				todayInsert.get(companyId).put(mac, new HashSet<String>());
			}

			todayInsert.get(companyId).get(mac).add(dataType);
		}
	}

	public static void updateTagData(String companyId, String tagMac, String dataType, String data) {
		if (!todayInsert.containsKey(companyId)) {
			todayInsert.put(companyId, new HashMap<String, HashSet<String>>());
		}

		if (!tagDataPool.containsKey(companyId) || !tagActive.containsKey(companyId)) {
			tagDataPool.put(companyId, new HashMap<>());
			tagActive.put(companyId, new HashMap<>());
		}
		
		// 檢查是否已寫入資料庫
		if (!todayInsert.get(companyId).containsKey(tagMac)) { // 若未寫入，則產生index
			RealTimeDataDao.insertToSQL(todayDate, companyId, tagMac, dataType);
			todayInsert.get(companyId).put(tagMac, new HashSet<String>());
			todayInsert.get(companyId).get(tagMac).add(dataType);

		} else {
			if (!todayInsert.get(companyId).get(tagMac).contains(dataType)) {
				RealTimeDataDao.insertToSQL(todayDate, companyId, tagMac, dataType);
				todayInsert.get(companyId).get(tagMac).add(dataType);
			}
		}
		tagDataPool.get(companyId).put(tagMac + "_" + dataType, data);
		
		// 更新tag的出現時間
		tagActive.get(companyId).put(tagMac + "_" + dataType, new JDateTime().getTimeInMillis());
	}
	
	public static void updateTagData(String dateTime, String companyId, String tagMac, String dataType, String data) {
		String insertDate = dateTime.substring(0, 6);
		if (todayDate.equals(insertDate)) {
			if (!todayInsert.containsKey(companyId)) {
				todayInsert.put(companyId, new HashMap<String, HashSet<String>>());
			}
			
			if (!todayInsert.get(companyId).containsKey(tagMac)) {
				todayInsert.get(companyId).put(tagMac, new HashSet<String>());
				todayInsert.get(companyId).get(tagMac).add(dataType);
			} else {
				if (!todayInsert.get(companyId).get(tagMac).contains(dataType)) {
					todayInsert.get(companyId).get(tagMac).add(dataType);
				}
			}
		}
		
		String tagDataType = tagMac + "_" + dataType;
		String time = 20 + dateTime;
		// System.out.println(time);
		JDateTime dateParser = new JDateTime();
		dateParser.parse(time, "YYYYMMDDhhmmss");
		long lastDataDatetime = dateParser.getTimeInMillis();
		
		if (!tagActive.containsKey(companyId)) {
			tagActive.put(companyId, new HashMap<>());
		} 
		
		if (!tagActive.get(companyId).containsKey(tagDataType)) {
			tagActive.get(companyId).put(tagDataType, lastDataDatetime);
		}

		if (!tagDataPool.containsKey(companyId)) {
			tagDataPool.put(companyId, new HashMap<>());
		}
		
		if (!tagDataPool.get(companyId).containsKey(tagDataType)) {
			tagDataPool.get(companyId).put(tagDataType, data);
		}
		
		if (tagActive.get(companyId).get(tagDataType) < lastDataDatetime) {
			tagActive.get(companyId).put(tagDataType, lastDataDatetime);
			tagDataPool.get(companyId).put(tagDataType, data);
		}
	}

	public static void updateDate() { // 每日0點要做的工作
		todayDate = (new JDateTime().toString("YYYYMMDD")).substring(2);
		// 清空todayInsert
		synchronized (todayInsert) {
			todayInsert.clear();
		}

		// 檢查昨天和前天是否有未寫入index表的檔案
		RealTimeDataDao.checkAndInsert();
	}

	public static void writeFile() {

		//ArrayList<String> keyList = new ArrayList<String>(Arrays.asList(keyInDb));

		long now = new JDateTime().getTimeInMillis();
		SecurityController.dLog.debug("write file job start");

		int count = 0;
		long st = System.nanoTime();
		for (String com : tagActive.keySet()) {
			
			HashMap<String, String> comTagDataPool = tagDataPool.get(com);
			HashMap<String, Long> comTagActive = tagActive.get(com);
			String[] keys = new String[comTagActive.size()];
			comTagActive.keySet().toArray(keys);
					
			// keyList.removeAll(comTagActive.keySet());
			
			for (String key : keys) {
				long lastTime = comTagActive.get(key);
				if (now - lastTime < 60000) {	// 1分鐘有更新，寫檔
					String value = comTagDataPool.get(key);
					RealTimeDataDao.insertData(now, com + "_" + key, value);					
				} 
				//else {
					// System.out.println("offline");
					// comTagDataPool.remove(key);
					// comTagActive.remove(key);
				//}
				count++;
			}
		}

		// SecurityController.nLog.info("tags did not upload: "
		// + keyList.toString());
		
		//RealTimeDataDao.writeLogFile("tags did not upload: " + keyList.toString());

		Thread insertSqlThread = new Thread("insert to SQL") {
			@Override
			public void run() {
				super.run();
				RealTimeDataDao.insertToSQLBatch();
			}
		};

		insertSqlThread.start();

		SecurityController.nLog.info("write file job compelete. time = "
				+ String.valueOf(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - st)) + " ms, " + "data length = "
				+ count);
	}

	public static HashMap<String, HashMap<String, String>> getNewTagsData(String companyId, String macIdDataTypes) {
		HashMap<String, HashMap<String, String>> result = new HashMap<>();
		if (!tagDataPool.containsKey(companyId) || !tagActive.containsKey(companyId)) {
			System.out.println("讀取" + companyId + "失敗");
			return result;
		}
		
		String[] tagDatatypes;
		if (macIdDataTypes.isEmpty()) {
			tagDatatypes = new String[tagDataPool.get(companyId).size()];
			tagDataPool.get(companyId).keySet().toArray(tagDatatypes);
		} else {
			tagDatatypes = macIdDataTypes.split(",");
		}
		
		HashMap<String, String> tagData = tagDataPool.get(companyId);
		HashMap<String, Long> tagTime = tagActive.get(companyId);
		System.out.println("確認讀取到" + companyId + "底下的即時資訊");
		for (String key : tagDatatypes) {
			System.out.println(key);
			String[] tagDatatype = key.split("_");
			HashMap<String, String> tag = new HashMap<>();
			tag.put("macId", tagDatatype[0]);
			tag.put("dataType", tagDatatype[1]);
			tag.put("lastData", tagData.containsKey(key) ? tagData.get(key) : "");
			tag.put("lastDataDatetime", tagTime.containsKey(key) ? tagTime.get(key).toString() : "");
			tag.put("lastDataDatetimeStr",
					tagTime.containsKey(key) ? new JDateTime(tagTime.get(key)).toString("YYYYMMDDhhmmss") : "");

			result.put(key, tag);
		}

		return result;
	}
}
