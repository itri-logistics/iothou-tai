package com.utility;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import com.dao.tag.TagDao;

public class ExcelUtility {	
	
	TagDao tagDao = new TagDao();	
	
	// Tag批次讀取歷史資料輸出Excel
	public ByteArrayOutputStream exportOutRecordController(ByteArrayOutputStream os,
			Map<String, String> tagDataMap) throws Exception {
		HSSFWorkbook workbook = new HSSFWorkbook();

		String[] head = { "紀錄時間", "溫度（℃）", "濕度（%rH）", "port1溫度（℃）", "port2溫度（℃）", "電量(%)"};
		String[] idx = { "time", "temperature", "humidity", "temperature1", "temperature2", "battery"};

		// 產生工作表，一個Tag一個
		Iterator<String> k = tagDataMap.keySet().iterator();
		while (k.hasNext()) {
			
			String tagMac = k.next();
			
			String tagName = tagDao.getTagName(tagMac).replace(":", "_");

			workbook.createSheet(tagName);
			HSSFSheet sheet = workbook.getSheet(tagName);
			sheet.createRow(0);
			
			//parse Data			
			String[] datas = tagDataMap.get(tagMac).split("#");
			TreeMap<String, String[]> typeDataMap = new TreeMap<String, String[]>();
			for(String s:datas){
				String[] dataDetails = s.substring(5).split(",");
				typeDataMap.put(s.substring(0, 4), dataDetails);				
			}
			
			TreeMap<String, List<String>> colDataMap = new TreeMap<String, List<String>>();
			
			String[] _dataDetails = typeDataMap.firstEntry().getValue();
			Arrays.sort(_dataDetails, new Comparator<String>() {
				@Override
				public int compare(String o1, String o2) {
					try {
						return (int)(Long.parseLong(o1.substring(0,12)) - Long.parseLong(o2.substring(0,12)));
					} catch (Exception e) {								
						return 0;
					}							
				}
			});
			ArrayList<String> time =  new ArrayList<String>();
			for(int j = 0;j<_dataDetails.length;j++){						
				if(_dataDetails[j].length() > 12){
					try {
						time.add(_dataDetails[j].substring(0,12));						
					} catch (Exception e) {
						// TODO: handle exception
					}							
				}						
			}
			colDataMap.put(idx[0], time);
			
			for (String dataType : typeDataMap.keySet()){
				String[] dataDetails = typeDataMap.get(dataType);
				if(dataType.equals("0001")){//本體溫度
					String temperature[] =  new String[colDataMap.get(idx[0]).size()];
					Arrays.fill(temperature, "");
					
					for(int j = 0;j<dataDetails.length;j++){						
						if(dataDetails[j].length() > 12){
							try {
								time.add(dataDetails[j].substring(0,12));
								String t = dataDetails[j].substring(0,12);
								int pos = colDataMap.get(idx[0]).indexOf(t);
								if(pos>=0){
									temperature[pos] = dataDetails[j].substring(12);
								}
							} catch (Exception e) {
								// TODO: handle exception
							}							
						}						
					}
					colDataMap.put(idx[1], Arrays.asList(temperature));
				}else if(dataType.equals("0002")){//本體濕度
					String humidity[] =  new String[colDataMap.get(idx[0]).size()];
					Arrays.fill(humidity, "");
					for(int j = 0;j<dataDetails.length;j++){
						if(dataDetails[j].length() > 12){
							try {
								String t = dataDetails[j].substring(0,12);
								int pos = colDataMap.get(idx[0]).indexOf(t);
								if(pos>=0){
									humidity[pos] = dataDetails[j].substring(12);
								}
							} catch (Exception e) {
								// TODO: handle exception
							}							
						}						
					}
					colDataMap.put(idx[2], Arrays.asList(humidity));
				}else if(dataType.equals("0101")){//port1溫度
					String temperature1[] =  new String[colDataMap.get(idx[0]).size()];
					Arrays.fill(temperature1, "");
					for(int j = 0;j<dataDetails.length;j++){
						if(dataDetails[j].length() > 12){
							try {
								if(Double.parseDouble(dataDetails[j].substring(12))>=-35){
									String t = dataDetails[j].substring(0,12);
									int pos = colDataMap.get(idx[0]).indexOf(t);
									if(pos>=0){
										temperature1[pos] = dataDetails[j].substring(12);
									}
								}
							} catch (Exception e) {
								// TODO: handle exception
							}							
						}												
					}
					colDataMap.put(idx[3], Arrays.asList(temperature1));
				}else if(dataType.equals("0201")){//port2溫度
					String temperature2[] =  new String[colDataMap.get(idx[0]).size()];
					Arrays.fill(temperature2, "");					
					for(int j = 0;j<dataDetails.length;j++){
						if(dataDetails[j].length() > 12){
							try {
								if(Double.parseDouble(dataDetails[j].substring(12))>=-35){
									String t = dataDetails[j].substring(0,12);
									int pos = colDataMap.get(idx[0]).indexOf(t);
									if(pos>=0){
										temperature2[pos] = dataDetails[j].substring(12);
									}
								}
							} catch (Exception e) {
								// TODO: handle exception
							}							
						}												
					}
					colDataMap.put(idx[4], Arrays.asList(temperature2));
				}else if(dataType.equals("0007")){//電量
					String battery[] =  new String[colDataMap.get(idx[0]).size()];
					Arrays.fill(battery, "");					
					for(int j = 0;j<dataDetails.length;j++){
						if(dataDetails[j].length() > 12){
							try {
								if(Double.parseDouble(dataDetails[j].substring(12))>=0){
									String t = dataDetails[j].substring(0,12);
									int pos = colDataMap.get(idx[0]).indexOf(t);
									if(pos>=0){
										battery[pos] = dataDetails[j].substring(12);
									}
								}
							} catch (Exception e) {
								// TODO: handle exception
							}							
						}												
					}
					colDataMap.put(idx[5], Arrays.asList(battery));
				}
			}
			
			int colCount = 0;

			for (String col : head) {
				sheet.getRow(0).createCell(colCount++).setCellValue(col);
			}
			int rowIdx = 1;
			int length = colDataMap.get(idx[0]).size();
			for (int i = 0;i<length;i++) {
				if (sheet.getRow(rowIdx) == null) {
					sheet.createRow(rowIdx);
				}				
				
				String date = colDataMap.get(idx[0]).get(i);
				date = "20"+date.substring(0, 2) + "/" + date.substring(2, 4) + "/" + date.substring(4, 6) + " "
						+ date.substring(6, 8) + ":" + date.substring(8, 10) + ":" + date.substring(10, 12);
				sheet.getRow(rowIdx).createCell(0).setCellValue(date);
				if(colDataMap.get(idx[1]) != null && colDataMap.get(idx[1]).size() > i) {
					try {
						sheet.getRow(rowIdx).createCell(1).setCellValue(Double.parseDouble(colDataMap.get(idx[1]).get(i))!=-9999  ? colDataMap.get(idx[1]).get(i) : "");
					} catch (Exception e) {
						sheet.getRow(rowIdx).createCell(1).setCellValue("");
					}
				}else {
					sheet.getRow(rowIdx).createCell(1).setCellValue("");
				}
				if(colDataMap.get(idx[2]) != null && colDataMap.get(idx[2]).size() > i) {
					try {
						sheet.getRow(rowIdx).createCell(2).setCellValue(Double.parseDouble(colDataMap.get(idx[2]).get(i))!=-9999  ? colDataMap.get(idx[2]).get(i) : "");
					} catch (Exception e) {
						sheet.getRow(rowIdx).createCell(2).setCellValue("");
					}
				}else {
					sheet.getRow(rowIdx).createCell(2).setCellValue("");
				}
				if(colDataMap.get(idx[3]) != null && colDataMap.get(idx[3]).size() > i) {
						
					try {
						sheet.getRow(rowIdx).createCell(3).setCellValue(Double.parseDouble(colDataMap.get(idx[3]).get(i))!=-9999  ? colDataMap.get(idx[3]).get(i) : "");
					} catch (Exception e) {
						sheet.getRow(rowIdx).createCell(3).setCellValue("");
					}
				}else {
					sheet.getRow(rowIdx).createCell(3).setCellValue("");
				}
				if(colDataMap.get(idx[4]) != null && colDataMap.get(idx[4]).size() > i) {
					try {
						sheet.getRow(rowIdx).createCell(4).setCellValue(Double.parseDouble(colDataMap.get(idx[4]).get(i))!=-9999  ? colDataMap.get(idx[4]).get(i) : "");
					} catch (Exception e) {
						sheet.getRow(rowIdx).createCell(4).setCellValue("");
					}
					
				}else {
					sheet.getRow(rowIdx).createCell(4).setCellValue("");
				}
				if(colDataMap.get(idx[5]) != null && colDataMap.get(idx[5]).size() > i) {
					try {
						sheet.getRow(rowIdx).createCell(5).setCellValue(Double.parseDouble(colDataMap.get(idx[5]).get(i))!=-9999  ? colDataMap.get(idx[5]).get(i) : "");
					} catch (Exception e) {
						sheet.getRow(rowIdx).createCell(5).setCellValue("");
					}
					
				}else {
					sheet.getRow(rowIdx).createCell(5).setCellValue("");
				}
				
				rowIdx++;
			}
		}

		workbook.write(os);
		return os;
	}
}
