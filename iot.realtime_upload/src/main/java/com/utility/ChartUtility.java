package com.utility;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chart.example.ChartUtils;
import com.chart.example.chart.model.ChartData;
import jodd.datetime.JDateTime;
import jodd.util.ComparableComparator;

public class ChartUtility {
	
	private ServletContext servletContext;	
	
	public static final String FILE_NAME = "reportChart.jpg";
	public static final int CHART_WIDTH = 800;
	public static final int CHART_HEIGHT = 600;

	public static final String PATH = System.getProperty("user.home") + File.separator + "iot_img";

	public ChartUtility(ServletContext context) {
		this.servletContext = context;
	}
	
	public File getChart(Map<String, String> dataMap, String fileName,float limitLower, float limitUpper) {
		
		TreeMap<String, ChartData[]> chartDataList = new TreeMap<String, ChartData[]>();
		
		// parse data
		Iterator<String> k = dataMap.keySet().iterator();
		while (k.hasNext()) {
			String dataName = k.next();
			String[] datas = dataMap.get(dataName).split(",");
			
			JDateTime jd = new JDateTime();
			ArrayList<ChartData> chartDatas = new ArrayList<>(); 
			
			for(String s:datas){
				String timeString = "20"+s.substring(0,12);	
				String valString = s.substring(12);			
				
				jd.parse(timeString, "YYYYMMDDhhmmss");
				try {
					ChartData chartData = new ChartData(jd.convertToDate(), Float.parseFloat(valString));
					chartDatas.add(chartData);
				} catch (Exception e) {
					System.err.println("exception : " + s);
					e.printStackTrace();
				}				
			
			}
			
			Collections.sort(chartDatas, new Comparator<ChartData>() {

				@Override
				public int compare(ChartData o1, ChartData o2) {					
					return (int)(o1.date.getTime() - o2.date.getTime());
				}
			});
			
			chartDataList.put(dataName, chartDatas.toArray(new ChartData[chartDatas.size()]));
			
		}
		
		File file = new File(PATH + File.separator + fileName +FILE_NAME);
		
		Font font = null;
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, servletContext.getResourceAsStream("/WEB-INF/fonts/msjh.ttf"));
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		List<Integer> colors = ChartUtils.drawChartGraphic(file, CHART_WIDTH, CHART_HEIGHT, limitLower, limitUpper, chartDataList, font);
		
		return file;
	}
}
