package com.utility;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.util.HashMap;

import com.security.SecurityController;
import com.sun.management.OperatingSystemMXBean;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

/******************************************
 * 每10分鐘向Web Server刷一次存在感
 *
 * @author ATone create by 2017/08/07
 *******************************************/
public class ServerNotification {

	private static OperatingSystemMXBean osmb = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
	private static File root = new File(System.getProperty("user.home"));
	private static String ip = "";

	public static void notifyWebServer() {
		try {
			if(ip.isEmpty()){
				ip = InetAddress.getLocalHost().getHostAddress();
			}
			
			HashMap<String, String> param = new HashMap<>();
			param.put("server", ip);
			param.put("cpu", String.valueOf(osmb.getSystemCpuLoad() * 100));
			param.put("mem", String.valueOf(osmb.getFreePhysicalMemorySize()));
			param.put("space", String.valueOf(root.getUsableSpace()));
			
			System.out.println(param.toString());
			
			HttpResponse response = HttpRequest.get("http://60.248.82.82/iot_admin/dataServer/updateServerData")
					.query(param).send();
			SecurityController.dLog.debug("notifyWebServer: " + response.bodyText());
			
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("notifyWebServer: " + e.getMessage());
		}
	}
}
