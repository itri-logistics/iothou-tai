package com.utility;

import jodd.datetime.JDateTime;

public class DataSizeLimitUtility {
	
	public static final int DATA_SIZE_LIMIT = 70;
	
	public static boolean outOfLimit(String[] tagArray, String sd, String ed) {
		
		if(sd.length() > 8){
			sd = sd.substring(0, 8);
			ed = ed.substring(0, 8);
		}
		
		JDateTime sdd = new JDateTime();
		sdd.parse(sd, "YYYYMMDD");
		JDateTime edd = new JDateTime();
		edd.parse(ed, "YYYYMMDD");
		int day = sdd.daysBetween(edd);
		
		//System.out.println("data size = " + tagArray.length * day);
		//System.out.println("data limit = " + DATA_SIZE_LIMIT);
		
		if(day > DATA_SIZE_LIMIT){
			return true;
		}
		
		return tagArray.length * day > DATA_SIZE_LIMIT;
	}
	
	public static boolean outOfLimit(String[] tagArray, String sd, String ed, int limit) {
		
		if(sd.length() > 8){
			sd = sd.substring(0, 8);
			ed = ed.substring(0, 8);
		}
		
		JDateTime sdd = new JDateTime();
		sdd.parse(sd, "YYYYMMDD");
		JDateTime edd = new JDateTime();
		edd.parse(ed, "YYYYMMDD");
		int day = sdd.daysBetween(edd);
		
		//System.out.println("data size = " + tagArray.length * day);
		//System.out.println("data limit = " + DATA_SIZE_LIMIT);
		
		if(day > DATA_SIZE_LIMIT){
			return true;
		}
		
		return tagArray.length * day > limit;
	}
}
