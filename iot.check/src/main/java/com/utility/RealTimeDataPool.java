package com.utility;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.springframework.jdbc.core.JdbcTemplate;

import com.bean.ConnoDataSource;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import com.google.gson.reflect.TypeToken;

import itri.group.param.Pa;
import jodd.datetime.JDateTime;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

/******************************************
 * 即時資料Pool
 *
 * 每1分鐘檢查一次是否有未上傳資料
 * 
 * 每1小時發出一次漏資料通知
 *
 * @author ATone create by 2017/09/13
 *******************************************/
public class RealTimeDataPool {

	static Gson gson;

	static JdbcTemplate jdbcTemplate = new JdbcTemplate(ConnoDataSource.getConnDataSource());

	private final static String TAG_NOT_CONNECT = ", 無連線紀錄, 上次連線時間: ";
	private final static String TAG_NO_DATA = ", 有連線紀錄但無資料, 上次紀錄時間: ";

	public static Stack<String> notConnect = new Stack<>();

	private static HashMap<String, String> tagNames = new HashMap<>();
	private static HashMap<String, HashSet<String>> tagList = new HashMap<>(); // 各公司即時看板Tag列表
																				// <companyId,
																				// <mac_type>>
	private static HashMap<String, HashMap<String, Float>> tagValue = new HashMap<>(); // 紀錄Tag的最後連線時間
																						// <companyId,
																						// <mac_type,
																						// value>>
	private static HashMap<String, HashMap<String, Long>> tagActive = new HashMap<>(); // 紀錄Tag的最後有資料時間
																						// <companyId,
																						// <mac_type,
																						// timeInMs>>

	private static HashMap<String, HashMap<String, String>> alertSetting = new HashMap<>();
	private static HashMap<String, HashMap<String, Long>> tagAlertTime = new HashMap<>(); // 紀錄Tag的最後警示時間
	// <companyId,
	// <mac_type,
	// timeInMs>>

	// 將警示資料和WifiTag資料撈出
	public static void init() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setLongSerializationPolicy(LongSerializationPolicy.STRING);

		gson = gsonBuilder.create();

		updateSetting();

		System.out.println("pool initialized");
		// System.out.println(tagList.toString());
		// System.out.println(tagConnect.toString());
		// System.out.println(tagActive.toString());

	}

	public static void updateSetting() {
		updateTagSetting();
		updateAlertSetting();
	}

	public static void updateTagSetting() {// 撈取Wifi Tag

		String sql = "select * from tag where type='WIFI'";
		List<Map<String, Object>> tags = jdbcTemplate.queryForList(sql);
		HashMap<String, HashSet<String>> settings = new HashMap<>();
		HashMap<String, String> names = new HashMap<>();
		String[] dataTypes = new String[] { "0001", "0002", "0101" };
		for (Map<String, Object> entry : tags) {
			String com = entry.get("companyId").toString();
			for (String dataType : dataTypes) {
				String key = entry.get("mac").toString() + "_" + dataType;
				if (!settings.containsKey(com)) {
					settings.put(com, new HashSet<String>());
				}
				if (!tagValue.containsKey(com)) {
					tagValue.put(com, new HashMap<String, Float>());

				}
				if (!tagActive.containsKey(com)) {
					tagActive.put(com, new HashMap<String, Long>());
				}

				settings.get(com).add(key);
			}
			names.put(entry.get("mac").toString(), entry.get("name").toString());
		}

		synchronized (tagList) {
			tagList = settings;
		}
		
		synchronized (tagNames) {
			tagNames = names;
		}
	}

	public static void updateAlertSetting() {
		String sql = "select mac,dataType,tag_datatype_alert_rel.alertSettingId,low,high,alert_setting.updateDate"
				+ " from tag_datatype_alert_rel inner join alert_setting"
				+ " on tag_datatype_alert_rel.alertSettingId = alert_setting.alertSettingId";

		// System.out.println(sql);
		HashMap<String, HashMap<String, String>> settings = new HashMap<>();
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql.toString());
		for (Map<String, Object> map : list) {
			HashMap<String, String> setting = new HashMap<>();
			setting.put("alertSettingId", map.get("alertSettingId").toString());
			setting.put("high", map.get("high").toString());
			setting.put("low", map.get("low").toString());
			setting.put("updateDate", map.get("updateDate").toString());
			setting.put("mac", map.get("mac").toString());
			setting.put("dataType", map.get("dataType").toString());
			settings.put(map.get("mac") + "_" + map.get("dataType"), setting);
		}
		
		synchronized (alertSetting) {
			alertSetting = settings;
		}
	}

	public static void check() {
		JDateTime nowTime = new JDateTime();
		long now = nowTime.getTimeInMillis();
		String nowStr = nowTime.toString("YYMMDDhhmmss");
		System.out.println("check");
		System.out.println(tagList.keySet().toString());
		
		StringBuilder alertInfo = new StringBuilder();

		for (String com : tagList.keySet()) {
			updateTagActive(com);
			for (String key : tagList.get(com)) {				
				Long activeTime = tagActive.get(com).get(key);

				if (activeTime != null && now - activeTime < 60000) {					
					float high = Float.parseFloat(alertSetting.get(key).get("high"));
					float low = Float.parseFloat(alertSetting.get(key).get("low"));
					float value = tagValue.get(com).get(key);
					float diff = Math.max(value - high, low - value);
					if(diff > 3) {
						String mac = alertSetting.get(key).get("mac");
						String type = alertSetting.get(key).get("dataType");
						String name = tagNames.get(mac);
						String aid = alertSetting.get(key).get("alertSettingId");
						String update = alertSetting.get(key).get("updateDate");
						alertInfo.append(name+"#"+com+"#"+mac+"#"+nowStr+"#"+type+"#1#"+aid+"#"+value+"#"+update+";");
					}else if(diff > 0) {
						String mac = alertSetting.get(key).get("mac");
						String type = alertSetting.get(key).get("dataType");
						String name = tagNames.get(mac);
						String aid = alertSetting.get(key).get("alertSettingId");
						String update = alertSetting.get(key).get("updateDate");
						alertInfo.append(name+"#"+com+"#"+mac+"#"+nowStr+"#"+type+"#0#"+aid+"#"+value+"#"+update+";");
					}
				} 

			}
		}
		
		if(alertInfo.length() > 0) {
			try {
				String destination = Pa.WEB_IP + "iot_alert/upload/uploadAlertData";
				HttpResponse response = null;
				response = HttpRequest.post(destination).form("datas", alertInfo.toString()).send();

				Map<String, Object> jsonMap = gson.fromJson(response.bodyText(), new TypeToken<Map>() {
				}.getType());
				if (jsonMap.get("rs").toString().equals("error") && jsonMap.get("content").toString().equals("update your setting")) {
					updateAlertSetting();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static void updateTagActive(String companyId) {
		// 取得pool內的資料
		for (String server : Pa.SERVER_LIST) {
			HttpResponse response = null;
			try {
				String destination = Pa.WEB_IP + "iot_realtime/" + server + "/query/getNewestTagsData?companyId="
						+ companyId;
				
				response = HttpRequest.get(destination).send();
				//System.out.println(response.bodyText());
				Map<String, Object> jsonMap = gson.fromJson(response.bodyText(), new TypeToken<Map>() {
				}.getType());
				if (jsonMap.get("rs").toString().equals("success") && !jsonMap.get("content").toString().equals("{}")) {
					Map<String, Object> jsonList = (Map<String, Object>) jsonMap.get("content");
					if (jsonList != null) {
						for (String key : jsonList.keySet()) {
							// String key = m.get("macId").toString() + "_" + m.get("dataType").toString();
							Map<String, Object> m = (Map<String, Object>) jsonList.get(key);
							if(m.get("lastData").toString().equals("null")) {
									continue;
							}
							long time = Long.parseLong(m.get("lastDataDatetime").toString());
							float value = Float.parseFloat(m.get("lastData").toString());

							if (tagActive.get(companyId).containsKey(key)) {
								long last = tagActive.get(companyId).get(key);
								// System.out.println(companyId+", "+key+", now = "+time+", last = "+last);
								if (time - last > 0) {
									tagActive.get(companyId).put(key, time);
									tagValue.get(companyId).put(key, value);
								}
							} else {
								tagActive.get(companyId).put(key, time);
								tagValue.get(companyId).put(key, value);
							}

						}
					}
				}
			} catch (Exception e) {
				System.out.println(response.bodyText());
				e.printStackTrace();
			}

		}
	}

}
