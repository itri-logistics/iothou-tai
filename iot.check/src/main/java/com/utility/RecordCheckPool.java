package com.utility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Stack;

import org.apache.commons.io.FileUtils;

import jodd.datetime.JDateTime;

public class RecordCheckPool {

	public static ArrayDeque<Record> fileKeyQueue = new ArrayDeque<>();
	public static Stack<String> noData = new Stack<>();

	final static String rawDirPath = System.getProperty("user.home") + File.separator + "iot_raw" + File.separator;

	public static boolean offerRecord(String companyId, String fileKey, String dataTypes, int length, int interval) {
		Record record = new Record();
		String[] key = fileKey.split("-");
		record.companyId = companyId;
		record.fileKey = fileKey;
		record.sd = key[0];
		record.ed = key[1];
		record.interval = interval;
		record.length = length;
		record.dataTypes = dataTypes;
		fileKeyQueue.offer(record);
		
		System.out.println("companyId = "+companyId+", fileKey= "+fileKey);
		return true;
	}

	public static void check() {
		if(fileKeyQueue.isEmpty()){
			System.out.println("目前沒有批次紀錄連線紀錄");
			return;
		}
		JDateTime nowTime = new JDateTime();
		ArrayDeque<Record> checkQueue = fileKeyQueue.clone();
		fileKeyQueue.clear();
		for (Record record : checkQueue) {
			String YYMM = record.sd.substring(0, 4);
			String DD = record.sd.substring(4, 6);
			File rawFile = new File(rawDirPath + record.companyId + File.separator + YYMM + File.separator + DD
					+ File.separator + record.fileKey + ".rawTxt");
			if (!rawFile.exists()) {
//				noData.push(nowTime.toString("YYYY/MM/DD hh:mm:ss") + " - " + record.companyId + ", " + record.fileKey
//						+ ", 找不到上傳檔案");
			} else {
				try {
					String fileContext = FileUtils.readFileToString(rawFile);
					String[] datas = fileContext.split("#");

					if (!datas[5].equals(record.dataTypes)) {
						noData.push(nowTime.toString("YYYY/MM/DD hh:mm:ss") + " - " + record.companyId + ", "
								+ record.fileKey + ", 資料類型不正確");
					} else {
						String[] types = record.dataTypes.split(",");
						for (int i = 6; i < datas.length; i++) {
							String[] rawData = datas[i].split(",");
							String rsd = rawData[0].substring(0, 12);
							String red = rawData[rawData.length - 1].substring(0, 12);

							if (!rsd.equals(record.sd) || !red.equals(record.ed)) {
								noData.push(nowTime.toString("YYYY/MM/DD hh:mm:ss") + " - " + record.companyId + ", "
										+ record.fileKey + ", 資料時間錯誤");
								break;
							} else if (rawData.length != record.length) {
								noData.push(nowTime.toString("YYYY/MM/DD hh:mm:ss") + " - " + record.companyId + ", "
										+ record.fileKey + ", 資料長度錯誤或資料遺失");
								break;
							} else {
								for (int j = 1; j < rawData.length; j++) {
									JDateTime last = new JDateTime("20" + rawData[j - 1].substring(0, 12),
											"YYYYMMDDhhmmss");
									JDateTime cur = new JDateTime("20" + rawData[j].substring(0, 12), "YYYYMMDDhhmmss");
									last.addSecond(record.interval);
									if (!last.equals(cur)) {
										noData.push(nowTime.toString("YYYY/MM/DD hh:mm:ss") + " - " + record.companyId
												+ ", " + record.fileKey + ", 資料時間錯誤或資料遺失");
										break;
									}
								}
							}
						}
					}

				} catch (IOException e) {
					e.printStackTrace();
					noData.push(nowTime.toString("YYYY/MM/DD hh:mm:ss") + " - " + record.companyId + ", "
							+ record.fileKey + ", 檔案讀取失敗, " + e.toString());
				}
			}
		}
	}

}

class Record {
	String companyId;
	String fileKey;
	String sd;
	String ed;
	String dataTypes;
	int length;
	int interval;
}