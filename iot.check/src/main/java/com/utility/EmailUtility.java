package com.utility;

import java.util.Date;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

public class EmailUtility {

	JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	
	public EmailUtility() {
		super();
		mailSender.setHost("210.59.228.37");
		mailSender.setPort(25);
		mailSender.setUsername("iot@itrilogistics.org");
		mailSender.setPassword("itri#000");
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.auth", "true");
		mailSender.setJavaMailProperties(properties);
	}



	public void send(String title, String content) {
		String[] tos={"TingChunKuo@itri.org.tw", "WeiAnChen@itri.org.tw", "itriA20062@itri.org.tw"};		

		try {		
			
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true,"UTF-8");// 處理中文編碼
			
			helper.setSubject(title); // 主題
			
			helper.setFrom(mailSender.getUsername()); // 寄件者
			helper.setTo(tos); // 收件人
			helper.setText(content, true); // 內容(HTML)
			mailSender.send(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
