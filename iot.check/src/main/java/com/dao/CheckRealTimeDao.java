package com.dao;

import com.utility.RealTimeDataPool;

import jodd.datetime.JDateTime;

public class CheckRealTimeDao {

	// datas: mac,type1,type2,...#mac,type1,type2,...
	public int updatePool(String companyId, String datas) {

		long now = new JDateTime().getTimeInMillis();

		int count = 0;

		String[] tags = datas.split("#");

		for (String tag : tags) {
			String[] types = tag.split(",");
			for (int i = 1; i < types.length; i++) {				
				count++;				
			}
		}
		
		System.out.println("companyId = "+companyId+", datas length= "+count);
		
		return count;
	}

}
