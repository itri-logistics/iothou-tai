package com.bean;

import java.util.Stack;
import java.util.TimerTask;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.utility.EmailUtility;
import com.utility.RealTimeDataPool;
import com.utility.RecordCheckPool;

public class SendRecordMailTask{
	
	static EmailUtility emailUtility = new EmailUtility();
	
	public static void sendMails() {
		if(RecordCheckPool.noData.isEmpty()){
			return;
		}
		
		Stack<String> list = new Stack<>();
		
		synchronized (RecordCheckPool.noData) {
			list.addAll(RecordCheckPool.noData);
			RecordCheckPool.noData.clear();
		}		
		
		StringBuilder content = new StringBuilder();
		while(!list.isEmpty()){
			content.append(list.pop()+"<br><br>");
		}
		
		emailUtility.send("批次紀錄資料錯誤", content.toString());
		
	}

}
