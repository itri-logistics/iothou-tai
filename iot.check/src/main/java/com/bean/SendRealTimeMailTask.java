package com.bean;

import java.util.Stack;
import java.util.TimerTask;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.utility.EmailUtility;
import com.utility.RealTimeDataPool;

public class SendRealTimeMailTask{
	
	static EmailUtility emailUtility = new EmailUtility();
	
	public static void sendMails() {
		//更新設定
		RealTimeDataPool.updateSetting();
		
		if(RealTimeDataPool.notConnect.isEmpty()){
			return;
		}
		
		Stack<String> list = new Stack<>();
		
		synchronized (RealTimeDataPool.notConnect) {
			list.addAll(RealTimeDataPool.notConnect);
			RealTimeDataPool.notConnect.clear();
		}		
		
		StringBuilder content = new StringBuilder();
		while(!list.isEmpty()){
			content.append(list.pop()+"<br><br>");
		}
		
		emailUtility.send("即時感測資料遺失", content.toString());
		
	}

}
