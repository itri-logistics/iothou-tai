package com.bean;

import itri.group.param.*;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class ConnoDataSource {	
	
	public static DriverManagerDataSource getConnDataSource() {
		DriverManagerDataSource data = new DriverManagerDataSource();
		data.setUrl("jdbc:mysql://192.168.20.103:3306/itri_iot?useUnicode=true&characterEncoding=UTF-8"); //自己電腦
		data.setDriverClassName("com.mysql.jdbc.Driver");
		data.setUsername(Pa.DB_USER);
		data.setPassword(Pa.DB_PWD);
		return data;
	}
}