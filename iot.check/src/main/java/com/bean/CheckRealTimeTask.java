package com.bean;

import com.utility.RealTimeDataPool;

public class CheckRealTimeTask {
	public static void check(){
		Thread thread = new Thread(){
			public void run(){
				RealTimeDataPool.check();
			}
		};
		
		thread.start();
	}
}
