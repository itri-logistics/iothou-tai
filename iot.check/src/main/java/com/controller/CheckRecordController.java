package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.security.SecurityController;
import com.utility.RecordCheckPool;

import itri.group.param.RsMsg;

/**
 * Servlet implementation class CheckRecordController
 */
public class CheckRecordController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Gson gson = new Gson();   
	
    public CheckRecordController() {
        super();
    }

    //
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		try {
			String companyId = request.getParameter("companyId");		
			String fileKey = request.getParameter("fileKey");
			String dataTypes = request.getParameter("dataTypes");
			int length = Integer.parseInt(request.getParameter("length"));
			int interval = Integer.parseInt(request.getParameter("interval"));
			
			RecordCheckPool.offerRecord(companyId, fileKey, dataTypes, length, interval);
			
			out.print(gson.toJson(new RsMsg("success", "update "+fileKey)));
			out.flush();
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("parsing upload datas error: " + e.getMessage());
			out.print(gson.toJson(new RsMsg("error", e.getMessage())));
			out.flush();
			out.close();
		}
		
		
		
	}

}
