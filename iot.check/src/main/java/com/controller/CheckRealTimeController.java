package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ObjectUtils.Null;

import com.dao.CheckRealTimeDao;
import com.google.gson.Gson;
import com.security.SecurityController;
import com.utility.RealTimeDataPool;

import itri.group.param.RsMsg;

/**
 * Servlet implementation class CheckRealTimeController
 */
public class CheckRealTimeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	CheckRealTimeDao checkRealTimeDao = new CheckRealTimeDao();
	Gson gson = new Gson();
    
    public CheckRealTimeController() {
        super();
        // TODO Auto-generated constructor stub
    }

	// /realtime
    // datas: mac,type1,type2,...#mac,type1,type2,...
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String companyId = request.getParameter("companyId");
		String datas = request.getParameter("datas");
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		
		try {
			int num = checkRealTimeDao.updatePool(companyId, datas);
			
			out.print(gson.toJson(new RsMsg("success", "update data num = "+num)));
			out.flush();
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("parsing upload datas error: " + e.getMessage());
			out.print(gson.toJson(new RsMsg("error", e.getMessage())));
			out.flush();
			out.close();
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		
		try {
			String method = req.getParameter("method");
			if("tag".equals(method)) {
				RealTimeDataPool.updateTagSetting();
			}else if("alert".equals(method)) {
				RealTimeDataPool.updateAlertSetting();
			}else {
				RealTimeDataPool.updateSetting();
			}
			out.print(gson.toJson(new RsMsg("success", "200")));
			out.flush();
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			SecurityController.eLog.error("update data info error: " + e.getMessage());
			out.print(gson.toJson(new RsMsg("error", e.getMessage())));
			out.flush();
			out.close();
		}
		
	}

	

}
