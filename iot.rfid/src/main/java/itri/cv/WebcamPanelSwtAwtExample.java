package itri.cv;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

public class WebcamPanelSwtAwtExample extends JPanel {
	private static final long serialVersionUID = 1L;
	private BufferedImage image;

	// Create a constructor method
	public WebcamPanelSwtAwtExample() {
		super();

	}

	private BufferedImage getimage() {
		return image;
	}

	private void setimage(BufferedImage newimage) {
		image = newimage;
		return;
	}

	public static BufferedImage matToBufferedImage(Mat matrix) {
		int cols = matrix.cols();
		int rows = matrix.rows();
		int elemSize = (int) matrix.elemSize();
		byte[] data = new byte[cols * rows * elemSize];
		int type;
		matrix.get(0, 0, data);
		switch (matrix.channels()) {
		case 1:
			type = BufferedImage.TYPE_BYTE_GRAY;
			break;
		case 3:
			type = BufferedImage.TYPE_3BYTE_BGR;
			// bgr to rgb
			byte b;
			for (int i = 0; i < data.length; i = i + 3) {
				b = data[i];
				data[i] = data[i + 2];
				data[i + 2] = b;
			}
			break;
		default:
			return null;
		}
		BufferedImage image2 = new BufferedImage(cols, rows, type);
		image2.getRaster().setDataElements(0, 0, cols, rows, data);
		return image2;
	}

	public void paintComponent(Graphics g) {
		BufferedImage temp = getimage();
		g.drawImage(temp, 10, 10, temp.getWidth(), temp.getHeight(), this);
	}

	public static void main(String arg[]) {
		// Load the native library.
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		JFrame frame = new JFrame("BasicPanel");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 400);
		WebcamPanelSwtAwtExample panel = new WebcamPanelSwtAwtExample();
		frame.setContentPane(panel);
		frame.setVisible(true);
		Mat webcam_mat = new Mat();
		BufferedImage temp;
		VideoCapture capture = new VideoCapture(0);
		if (capture.isOpened()) {
			while (true) {
				capture.read(webcam_mat);

				if (!webcam_mat.empty()) {
//					frame.setSize(webcam_image.width() + 40,webcam_image.height() + 60);

//					MatOfRect faceDetections = new MatOfRect();
//					faceDetector.detectMultiScale(webcam_image, faceDetections);

					// Draw a bounding box around each face.
//					for (Rect rect : faceDetections.toArray()) {
//						Core.rectangle(webcam_mat, new Point(9,20),
//								new Point(80,150), new Scalar(0, 255, 0));
//					}
					
					Mat destination = new Mat(webcam_mat.rows(),webcam_mat.cols(),webcam_mat.type());	
					webcam_mat.convertTo(destination, -1, 0.1, 0.6);
					
					Mat mat_hsv = new Mat(webcam_mat.rows(),webcam_mat.cols(), Imgproc.COLOR_RGB2HSV);
					Imgproc.cvtColor(destination, mat_hsv, Imgproc.COLOR_RGB2HSV);
					
					temp = matToBufferedImage(mat_hsv);
					panel.setimage(temp);
					panel.repaint();
				} else {
					System.out.println(" --(!) No captured frame -- Break!");
					break;
				}
			}
		}
		return;
	}
}