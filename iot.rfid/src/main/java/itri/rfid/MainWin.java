package itri.rfid;

import jodd.datetime.JDateTime;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.BrowserFunction;
import org.eclipse.swt.browser.ProgressEvent;
import org.eclipse.swt.browser.ProgressListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import rs232.CommPortSender;
import rs232.ListCom;
import rs232.Rs232Input;


public class MainWin {
	static private ListCom listCom=new ListCom();
	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		
		
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns=1;
		gridLayout.makeColumnsEqualWidth=true;
//		GridData gd=new GridData();
//		gd.horizontalAlignment=GridData.FILL;
//		gd.grabExcessHorizontalSpace = true;
//		gd.grabExcessVerticalSpace=false;
//		gd.heightHint=120;
		shell.setLayout(gridLayout);
		shell.setText("RFID棧板管理系統 ver 1.0");
//		
//		Button btn=new Button(shell, SWT.PUSH);
//		btn.setText("test");
//		btn.setLayoutData(gd);
//		btn.setSize(50, 400);
		
		
//		Button btn2=new Button(shell, SWT.PUSH);
//		btn2.setText("test");
//		btn2.setLayoutData(gd);
		
//		Label la=new Label (shell,  SWT.HORIZONTAL);
//		la.setText("this is test");
//		Color col2 = new Color(display, 0, 171, 235);
//		
//        shell.setBackground(col2);
//        la.setBackground(col2);
//        
//        col2.dispose(); 
//		
//		
//		Button btn3=new Button(shell, SWT.PUSH);
//		btn3.setText("test");
//		btn3.setLayoutData(gd);
//		
//		Button btn4=new Button(shell, SWT.PUSH);
//		btn4.setText("test");
//		btn4.setLayoutData(gd);
		
		
		final Browser browser;
		try {
			GridData gd2=new GridData();
			gd2.horizontalAlignment=GridData.FILL;
			gd2.grabExcessHorizontalSpace = true;
			gd2.verticalAlignment=GridData.FILL;
			gd2.grabExcessVerticalSpace=true;
			gd2.horizontalSpan=4;
			browser = new Browser(shell, SWT.NONE);
			browser.setLayoutData(gd2);
			browser.setSize(800, 400);
		} catch (SWTError e) {
			System.out.println("Could not instantiate Browser: "+ e.getMessage());
			display.dispose();
			return;
		}
		
		new BrowserFunction(browser, "startRFID"){
			@Override
			public Object function(Object[] arg0) {
				try {
					System.out.println("開始"+arg0[0]);
					listCom.connect(String.valueOf(arg0[0]),browser);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return super.function(arg0);
			}
		};
		
		new BrowserFunction(browser, "NOTE_delTag"){
			@Override
			public Object function(Object[] arg0) {
				try {
					System.out.println("刪除"+arg0[0]);
					Rs232Input.tagReads.remove(arg0[0]);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return super.function(arg0);
			}
		};
		
		new BrowserFunction(browser, "stopRFID"){
			@Override
			public Object function(Object[] arg0) {
				System.out.println("停止");
				
				listCom.stopRead();
				return super.function(arg0);
			}
		};
		
		
		shell.setSize(800, 600);
		shell.open();
		browser.setUrl("http://127.0.0.1/RFID_test/index.html");
		browser.addProgressListener(new ProgressListener() {
            public void changed(ProgressEvent event){

            }
            public void completed(ProgressEvent event){

            }
          });
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
	}
}
class Br extends BrowserFunction{

	public Br(Browser arg0, String arg1) {
		super(arg0, arg1);
	}
	 
	
}
