package rs232;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedHashMap;

import jodd.datetime.JDateTime;

import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Display;

public class Rs232Input {
	private Browser browser;
	
	public Rs232Input(Browser browser) {
		this.browser=browser;
	}
	
    byte[] buffer = new byte[1024];    
    int tail = 0;    
//    String weight="";
    DecimalFormat df=new DecimalFormat("0.##");
    public static LinkedHashMap<String, Long> tagReads=new LinkedHashMap<>();
    
    public void onReceive(byte b) {  
//    	System.out.println(b);
        if (b==0x7e&&tail>4&&buffer[4]+6==tail) {//
            buffer[tail] = b;    
            tail++;
            onMessage();
        } else {    
            buffer[tail] = b;    
            tail++;    
        }    
    }    
     
    public void onStreamClosed() {    
        onMessage();    
    }    
        
    private void onMessage() {    
        if (tail!=0) {
        	System.out.println(bytesToHex(buffer,tail));
        	if(buffer[0]==0x5A&&buffer[tail-1]==0x7E){//開頭結尾是對的
        		//tag的回饋
        		if(buffer[1]==0x02&&buffer[2]==0x50){
        			byte[] tagIdArray=parseTagId(buffer, tail);
        			byte port=buffer[5];
        			String tagId=bytesToHex(tagIdArray, tagIdArray.length);
        			
        			System.out.println("port:"+port+"\t"+"tagId:"+tagId);
        			if(!tagReads.containsKey(tagId)){
        				JDateTime jd=new JDateTime();
        				
        				tagReads.put(tagId,jd.getTimeInMillis());
        				String funcName=String.format("return addTag('%d','%s','%s');",tagReads.size(), tagId,jd.toString("YYYY/MM/DD hh:mm:ss"));
        				Display.getDefault().asyncExec(new Runnable() {
        			        public void run() {
        			        	String htm=(String)browser.evaluate(funcName); 
        			        }
        			    });
        			}
        		}
        	}
            tail = 0;    
        }    
    }    
    
    public static byte[] parseTagId(byte[] raw,int tail){
    	byte[] rs=new byte[tail-16];
    	for(int i=14;i<tail-2;i++){
    		rs[i-14]=raw[i];
    	}
    	return rs;
    }
    
    // helper methods     
    public static byte[] getMessage(String message) {    
        return (message+"\n").getBytes();    
    }    
        
    
    
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    
    //顯示\hex String 
    public static String bytesToHex(byte[] bytes,int len) {
        char[] hexChars = new char[len * 2];
        for ( int j = 0; j < len; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    
    
//    public String getMessage(byte[] buffer, int len) {    
//        return new String(buffer, 0, tail);    
//    }  
}  