package rs232;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.swt.browser.Browser;
  
public class ComReceiver extends Thread{  
	
    InputStream in;    
    Rs232Input protocol;
    private Browser browser;
    public ComReceiver(InputStream in,Browser _browser) {    
        this.in = in;    
        this.browser=_browser;
        protocol = new Rs232Input(browser);
    }    
        
    public void run() {    
        try {    
            int b;    
            while(true) {    
                    
                // if stream is not bound in.read() method returns -1    
                while((b = in.read()) != -1) {    
                    protocol.onReceive((byte) b);    
                }    
                protocol.onStreamClosed();    
                sleep(1000);    
            }    
        } catch (IOException e) {    
            e.printStackTrace();    
        } catch (InterruptedException e) {    
            e.printStackTrace();    
        }     
    }  
}  