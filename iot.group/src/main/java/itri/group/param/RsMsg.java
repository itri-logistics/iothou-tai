package itri.group.param;

public class RsMsg {
	private String rs;
	private Object content;
	public RsMsg(String rs, Object content) {
		super();
		this.setRs(rs);
		this.setContent(content);
	}
	public String getRs() {
		return rs;
	}
	public void setRs(String rs) {
		this.rs = rs;
	}
	public Object getContent() {
		return content;
	}
	public void setContent(Object content) {
		this.content = content;
	}
}
