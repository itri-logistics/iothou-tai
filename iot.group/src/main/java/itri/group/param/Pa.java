package itri.group.param;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class Pa {
	
	final public static String HOME_DIR = System.getProperty("user.home");
	
	static {
		
		final Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Map<String,String> mapConfig=new HashMap();;
		Random r=new Random();
		
		// log4j2 config setting
		System.setProperty("log4j.configurationFile",
				HOME_DIR + File.separator + "log4j-config" + File.separator + "iot-log4j2.xml");
		
		File confFile = new File(HOME_DIR + File.separator + "iot_conf" + File.separator + "iot.config");
		if(!confFile.exists()) {
			System.err.println("設定檔不存在: " + confFile.getAbsolutePath());
			//System.exit(0);
		}else {
			try {
				String gsonStr="";
				try {
					gsonStr = FileUtils.readFileToString(confFile);
					if(!gsonStr.isEmpty()){
						mapConfig=gson.fromJson(gsonStr, Map.class);
					}else{
						throw new Exception("config error");
					}
				} catch (IOException e) {
					e.printStackTrace();
					throw new Exception("config error");
				}				
				
				//check all config key
				String[] configKs=new String[]{"CDN_IP","CDN_LIB","DB","DB_NAME","DB_USER","DB_PWD","WEB_IP","SERVER_LIST"};
				for(String k:configKs){
					if(mapConfig.get(k)==null){
						throw new Exception("config error");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("設定檔讀取錯誤");
			}
		}
	}
	final public static String DB_USER = "itri_iot";
	final public static String DB_PWD = "ej/ u06m04j4xu06j;3";
	
	// final public static String DB="60.248.82.89:1521";
	// final public static String CDN_IP="http://127.0.0.1/";
	final public static String CDN_IP = "http://imeasures.org/";
	// final public static String DB="45.32.45.49:3306";
	//final public static String DB = "localhost";
	//final public static String DB = "192.168.20.198:3306";
	final public static String DB = "192.168.20.103:3306";

	final public static String WEB_IP = "http://60.248.82.82/";
	//final public static String[] SERVER_LIST = {"a","b"};
	final public static String[] SERVER_LIST = {"a"};

	// IOT連線程式
	// final public static String CDN_IP="http://192.168.1.126/";
	// final public static String DB="localhost";

	public static RsMsg error = new RsMsg("error", "error");
	public static RsMsg success = new RsMsg("success", "success");
	public static int PAGE_NUM = 16;
	public static int BUY_PAGE_NUM = 9;

	final public static String JS_VERSION = String.valueOf(new Date().getTime());
	final public static String CDN_ADMIN_LIB_LINK = CDN_IP + "itri_iot_q";
}
