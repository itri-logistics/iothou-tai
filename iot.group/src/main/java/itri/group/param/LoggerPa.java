package itri.group.param;

public class LoggerPa {
	public final static boolean LOGGER_AOP=false;
	
	public final static String LOGGER_DEBUG="debug";
	public final static String LOGGER_ERROR="exception";
	public final static String LOGGER_NET_ERR="network";
}
