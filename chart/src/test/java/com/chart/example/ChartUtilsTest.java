package com.chart.example;

import static org.junit.Assert.*;

import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import org.junit.BeforeClass;
import org.junit.Test;

import com.chart.example.chart.model.ChartData;

public class ChartUtilsTest {
	public static final String DEST = "resultTest.jpg";
	private static TreeMap<String, ChartData[]> datas = new TreeMap<String, ChartData[]>();
	
	@BeforeClass
	public static void setUp() {
		Calendar calendar = Calendar.getInstance();
		for (int i = 0; i < 10; i++) {
			calendar = Calendar.getInstance();
			ChartData[] chartDatas = getRandomChartData(10, 50, calendar);
			datas.put("資料"+i,chartDatas);
		}
	}

	@Test
	public void test() {
		int width = 800;
		int height = 600;
		Font font = null;
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, new File("fonts/msjh.ttc"));
		} catch (Exception e) {			
			e.printStackTrace();
		}
		List<Integer> colors = ChartUtils.drawChartGraphic(new File(DEST), width, height, 15, 45, datas,font);
		assertEquals(datas.size(), colors.size());
	}

	private static ChartData[] getRandomChartData(int minValue, int maxValue, Calendar calendar) {
		ChartData[] chartDatas = new ChartData[10];
		for (int i = 0; i < 10; i++) {
			calendar.add(Calendar.HOUR, i);
			chartDatas[i] = new ChartData(calendar.getTime(),
					(float) ((Math.random() * (maxValue - minValue + 1)) + minValue));
		}
		return chartDatas;
	}

}
