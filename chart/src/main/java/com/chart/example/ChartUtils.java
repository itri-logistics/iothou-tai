package com.chart.example;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import com.chart.example.chart.GraphicChart;
import com.chart.example.chart.model.ChartData;

/**
 * Utility functions for Chart graph implementation.
 * 
 * @author gigichien
 *
 */
public class ChartUtils {

	/**
	 * Returns a list of colors for each chart line
	 * 
	 * @param file
	 *            the file path of image
	 * @param width
	 *            the width of image
	 * @param height
	 *            the height of image
	 * @param limitLower
	 *            the lower limit of chartDatas
	 * @param limitUpper
	 *            the upper limit of chartDatas
	 * @param chartDatas
	 *            the data of chart
	 * @return
	 */
	public static List<Integer> drawChartGraphic(File file, int width, int height, float limitLower, float limitUpper,
			TreeMap<String, ChartData[]> chartDatas) {
		try {
			BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = bufferedImage.createGraphics();
			GraphicChart graphicChart = new GraphicChart(g2d);
			graphicChart.drawData(width, height, limitLower, limitUpper, chartDatas);
			ImageIO.write(bufferedImage, "JPEG", file);
			return graphicChart.getDataColors();
		} catch (IOException ie) {
			ie.printStackTrace();
		}

		return null;
	}

	public static List<Integer> drawChartGraphic(File file, int width, int height, float limitLower, float limitUpper,
			TreeMap<String, ChartData[]> chartDatas, Font font) {
		try {
			BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = bufferedImage.createGraphics();
			GraphicChart graphicChart = new GraphicChart(g2d);
			graphicChart.setCustomFont(font);
			graphicChart.drawData(width, height, limitLower, limitUpper, chartDatas);
			ImageIO.write(bufferedImage, "JPEG", file);
			return graphicChart.getDataColors();
		} catch (IOException ie) {
			ie.printStackTrace();
		}

		return null;
	}

}
