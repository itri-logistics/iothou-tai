package com.chart.example.chart.model;

/**
 * Base on Bezier curve, calculate root/value of formula
 * @author gigichien
 *
 */
public class BeizeCurveFormula {
	private float coefficientA;
	private float coefficientB;
	private float coefficientC;
	private float coefficientD;
	private float root;

	public BeizeCurveFormula(float startPoint, float controllPoint1, float controllPoint2, float endPoint) {
		coefficientC = (float) (3.0 * (controllPoint1 - startPoint));
		coefficientB = (float) (3.0 * (controllPoint2 - controllPoint1) - coefficientC);
		coefficientA = endPoint - startPoint - coefficientC - coefficientB;
		coefficientD = startPoint;
	}

	public void setRoot(float root) {
		this.root = root;
	}

	public float getBeizeValue() {
		float rootSquared = root * root;
		float rootCubed = rootSquared * root;
		return (coefficientA * rootCubed) + (coefficientB * rootSquared) + (coefficientC * root) + coefficientD;
	}
}